// Copyright (C) 2016, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sourceforge.dibdib.android_qm;

import com.gitlab.dibdib.dib2qm.*;

// import net.vreeken.quickmsg.*;

import com.gitlab.dibdib.picked.net.*;

public class preferences extends QmDb_0 {
  // =====

  public preferences(Object context) {
    super(context);
    //  if (null == Dib2Root.qContextQm) {
    //  Dib2Root.init( 'A', "qm", qDbFileName, this );
    Pgp.privProvs =
        (null == Pgp.privProvs) // ,
            ? new PrivProvIf[] {}//new EcDhQm() /*net.vreeken.quickmsg.pgp*/} // ,
            : Pgp.privProvs;
  }

  // =====
}
