// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sourceforge.dibdib.android_qm;

public class netwatcher extends com.gitlab.dibdib.dib2qm.Netwatcher {
  // DIFF (_0):
  // NetworkInfo info = (null == cm) ? null : cm.getActiveNetworkInfo();
}
