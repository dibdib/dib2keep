// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sourceforge.dibdib.android_qm;

public final class MainActivity extends com.gitlab.dibdib.dib2qm.QmAct_0 {}
