// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sourceforge.dibdib.android_qm;

import com.gitlab.dibdib.dib2qm.QmDb_0;

public final class SettingsActivity extends com.gitlab.dibdib.dib2qm.SettingsActivity_0 {
  // =====

  @Override
  public void onPause() {
    QmDb_0 db = new QmDb_0(this);
    db.save();
    super.onPause();
  }

  // =====
}
