// Copyright (C) 2021, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_io;

import net.sf.dibdib.generic.QIfs;
import net.sf.dibdib.thread_any.*;

public enum QOpIo implements QIfs.QEnumIf {
  zzACCESS,
  zzLOAD_INITIAL,
  zzSAV2,
  zzSAV2_zzEXIT,

  /////

  BAK2EXT,
  ;

  public static QIfs.QEnumIf[] create() {
    return values();
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }
}
