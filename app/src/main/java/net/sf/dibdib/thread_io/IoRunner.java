// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_io;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.io.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.FeederRf;
import net.sf.dibdib.thread_feed.QOpFeed;
import net.sf.dibdib.thread_wk.*;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

// =====

/** For file handling thread as triggered by MainThreads. */
public enum IoRunner implements QIfs.QRunnableIf {

  // =====

  INSTANCE;

  /** 0: initial value, 1: failed load attempt, >1000: done */
  static long zLastSaveMillis = 0;

  QToken mTask;
  QToken mNext = null;

  // =====

  public static String[] listPaths(String extension, boolean rootPath, String... dirTypes) {
    extension = ('.' == extension.charAt(0)) ? extension : ("." + extension);
    final String extbak = extension + '.';
    String[] out = new String[20];
    int count = 0;
    for (String dt : dirTypes) {
      final File dir = Dib2Root.platform.getFilesDir(dt);
      final String[] names = dir.list();
      if (null != names) {
        for (String x0 : names) {
          if (x0.endsWith(extension) || x0.contains(extbak)) {
            try {
              final File f0 = new File(dir, x0);
              final String entry =
                  (rootPath ? f0.getCanonicalPath() : f0.getAbsolutePath())
                      + '\t'
                      + f0.length()
                      + '\t'
                      + DateFunc.dateShort4Millis(f0.lastModified());
              if (count >= out.length) {
                out = Arrays.copyOf(out, count << 1);
              }
              out[count++] = entry;
            } catch (IOException e0) {
              ExceptionAdapter.throwAdapted(e0, IoRunner.class, x0);
            }
          }
        }
      }
    }
    return Arrays.copyOf(out, count);
  }

  private static long copyFileExc(File xFrom, File xTo) throws IOException {
    long out = 0;
    InputStream is = null;
    OutputStream os = null;
    try {
      is = new FileInputStream(xFrom);
      os = new FileOutputStream(xTo);
      byte[] buf = new byte[2048];
      int len;
      while ((len = is.read(buf)) > 0) {
        os.write(buf, 0, len);
        out += len;
      }
    } finally {
      is.close();
      os.close();
    }
    return out;
  }

  public static long copyFile(File xFrom, File xTo) {
    try {
      return copyFileExc(xFrom, xTo);
    } catch (Exception e0) {
      ExceptionAdapter.throwAdapted(e0, IoRunner.class, xFrom.getName());
    }
    return 0;
  }

  public static int backupFiles(String dirTypeFrom, String dirTypeTo, String... except) {
    final File dirFrom = Dib2Root.platform.getFilesDir(dirTypeFrom);
    final File dirTo = Dib2Root.platform.getFilesDir(dirTypeTo);
    if ((null == dirFrom)
        || (null == dirTo)
        || (dirFrom.equals(dirTo) || !dirFrom.exists() || !dirTo.exists() || !dirFrom.isDirectory())
        || !dirTo.isDirectory()) {
      return 0;
    }
    final String[] names = dirFrom.list();
    int out = 0;
    if (null != names) {
      for (String x0 : names) {
        if (x0.endsWith("." + Dib2Constants.MAGIC_BYTES_STR)
            || x0.contains("." + Dib2Constants.MAGIC_BYTES_STR + ".")) {
          for (String not : except) {
            not = not.toUpperCase(Locale.ROOT);
            if (x0.toUpperCase(Locale.ROOT).contains(not)
                || not.contains(x0.toUpperCase(Locale.ROOT))) {
              x0 = null;
              break;
            }
          }
          if (null == x0) {
            continue;
          }
          final File f0 = new File(dirFrom, x0);
          File f1 = new File(dirTo, x0);
          if (f1.exists()) {
            f1 = new File(dirTo, x0 + ".x.bak");
            if (f1.exists()) {
              continue;
            }
          }
          copyFile(f0, f1);
        }
      }
    }
    return out;
  }

  public static String findLatest(File dir, boolean tryHarder) {
    File file = null;
    String[] names = dir.list();
    if (null != names) {
      String name = null;
      long time = -1;
      for (String x0 : names) {
        if (x0.matches(Dib2Root.app.dbFileName.replace(".", "[\\.0-9]*\\."))
            || (x0.startsWith(Dib2Root.app.dbFileName)
                && (x0.endsWith(".bak") || x0.endsWith(".old")))) {
          long x1 = new File(dir, x0).lastModified();
          if (time < x1) {
            time = x1;
            name = x0;
          }
        }
      }
      if ((null == name) && tryHarder) {
        final String matchingType =
            ("" + (char) (Dib2Root.app.appShort.charAt(0) - ('a' - 'A')))
                + Dib2Root.app.appShort.substring(1)
                + "."
                + Dib2Constants.MAGIC_BYTES_STR;
        for (String x0 : names) {
          if (x0.contains(matchingType)) {
            long x1 = new File(dir, x0).lastModified();
            if (time < x1) {
              time = x1;
              name = x0;
            }
          }
        }
      }
      if ((null == name) && tryHarder) {
        for (String x0 : names) {
          if (x0.contains("." + Dib2Constants.MAGIC_BYTES_STR)) {
            long x1 = new File(dir, x0).lastModified();
            if (time < x1) {
              time = x1;
              name = x0;
            }
          }
        }
      }
      if (0 < time) {
        file = new File(dir, name);
      }
    }
    return (null == file) ? null : file.getAbsolutePath();
  }

  public static String check4Load() {

    ///// Fallback solution
    File dir = Dib2Root.platform.getFilesDir("external");
    if ((null != dir) && !dir.equals(Dib2Root.platform.getFilesDir("main"))) {
      dir = new File(dir, "keep");
      if ((null != dir) && dir.exists()) {
        File file = new File(dir, Dib2Root.app.dbFileName);
        if ((null != file) && file.exists() && file.isFile()) {
          Dib2Root.app.dbFileOptionalPath = file.getAbsolutePath();
          return Dib2Root.app.dbFileOptionalPath;
        }
      }
    }

    dir = Dib2Root.platform.getFilesDir("main");
    String path = null;
    if (null != dir) {
      File file =
          (null == Dib2Root.app.dbFileOptionalPath)
              ? new File(dir, Dib2Root.app.dbFileName)
              : new File(Dib2Root.app.dbFileOptionalPath);
      path = file.isFile() ? file.getAbsolutePath() : null;
      if (null != path) {
        return path;
      }
      path = findLatest(dir, false);
      if (null != path) {
        return path;
      }
    }
    File oldDir = dir;
    dir = Dib2Root.platform.getFilesDir("external");
    if ((null != dir) && dir.exists() && !dir.equals(oldDir)) {
      path = findLatest(dir, false);
    }
    return path;
  }

  /*synchronized*/
  void saveAll(byte[] dat, long b_toDownloads_asBak) {
    if (0 == b_toDownloads_asBak) {
      TcvCodec.instance.writePhrase();
    }
    File file =
        (null == Dib2Root.app.dbFileOptionalPath) || (0 != b_toDownloads_asBak)
            ? new File(Dib2Root.platform.getFilesDir("main"), Dib2Root.app.dbFileName)
            : new File(Dib2Root.app.dbFileOptionalPath);
    String path = file.getAbsolutePath();
    if (file.exists()) {
      String dx = DateFunc.dateShort4Millis().substring(5, 6);
      String px =
          path.replace(".dm", "." + ((0 != (2 & b_toDownloads_asBak)) ? "bak" : dx) + ".dm");
      File old = new File(px);
      if (old.exists()) {
        old.delete();
      }
      file.renameTo(old);
    }
    writeEncoded(path, dat, true, true, false);

    zLastSaveMillis = DateFunc.currentTimeMillisLinearized();
    if (0 != (1 & b_toDownloads_asBak)) {
      File downloadFolder = Dib2Root.platform.getFilesDir("external");
      if (null != downloadFolder) {
        String name = DateFunc.dateShort4Millis().substring(4, 6);
        name =
            Dib2Root.app.dbFileName.replace(
                ".dm", "." + ((0 != (2 & b_toDownloads_asBak)) ? "bak" : name) + ".dm");
        file = new File(downloadFolder, name);
        if ((3 != b_toDownloads_asBak)
            || ((file.lastModified() + 2 * 3600 * 1000) < DateFunc.currentTimeMillisLinearized())) {
          path = file.getAbsolutePath();
          writeEncoded(path, dat, true, true, false);
          //        zLastBackup = zLastSaveMillis;
        }
      }
    }
  }

  QSTuple[] loadData(String xPath) {
    QSTuple[] out = null;
    String path = xPath;
    // Check if proper phrase is available:
    final byte[] pass = TcvCodec.instance.getPassFull();
    if (null == pass) {
      return null;
    }
    File dir = Dib2Root.platform.getFilesDir("main");
    if (null == path) {
      File file = new File(dir, Dib2Root.app.dbFileName);
      path = file.isFile() ? file.getAbsolutePath() : null;
    }
    if (null == path) {
      path = findLatest(dir, true);
    } else {
      File check = new File(path);
      if (!check.exists() && (0 > path.indexOf('/') && (0 > path.indexOf('\\')))) {
        path = new File(dir, path).getAbsolutePath();
      }
    }
    if (null != path) {
      out = findPhraseLoad(path);
    }
    //			if (null != out) { // || (dummy && new File(path).exists())) {
    // ? Do not pull old data if password not set.

    if ((null == out) && (new File(path + ".bak").isFile())) {
      out = findPhraseLoad(path + ".bak");
    }
    if ((null == out) && (new File(path + ".old").isFile())) {
      out = findPhraseLoad(path + ".old");
    }
    if ((null == out) && (null == xPath)) {
      dir = Dib2Root.platform.getFilesDir("external");
      if ((null != dir) && (dir.exists())) {
        path = findLatest(dir, false);
        if (null != path) {
          out = findPhraseLoad(new File(path).getAbsolutePath());
        }
      }
    }
    if (1000 >= zLastSaveMillis) {
      zLastSaveMillis = (null != out) ? 1001 : 1;
      // if ((null != out) && (Dib2Root.app.feederCurrent != Dib2Root.app.mainFeeder) && !dummy) {
      //	File downloadFolder = Dib2Root.platform.getFilesDir("external");
      //	if (null != downloadFolder) {
      //		String name = getShortDay().substring(4, 6);
      //		name = Dib2Root.app.dbFileName.replace(".dm", ".bak.dm");
      //	}
      // }
    }
    return out;
  }

  @Override
  public boolean guard(QToken xTask, long... xParameters) {
    return xTask.op instanceof QOpIo;
  }

  @Override
  public int start(long xTick, QToken xmTask) {
    mNext = null;
    mTask = (xmTask.op instanceof QOpIo) ? xmTask : null;
    return (null == mTask) ? 0 : 1;
  }

  @Override
  public int stepAsync() {
    QSeqIf arg = mTask.argX;
    switch ((QOpIo) mTask.op) {
      case BAK2EXT:
        IoRunner.backupFiles("main", "external");
        break;
      case zzACCESS:
        TcvCodec.instance.settleHexPhrase("");
        break;
      case zzLOAD_INITIAL:
        if (null == TcvCodec.instance.getPassFull()) {
          mTask = null;
          Dib2Root.app.feederNext = FeederRf.LOGIN;
          ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).reset();
          return 0;
        }
        QSTuple[] dat = loadData((null == arg) ? null : arg.toString());
        if (null == dat) {
          if (Dib2Root.app.appState.ordinal() < Dib2Lang.AppState.ACTIVE.ordinal()) {
            mTask = null;
            Dib2Root.app.feederNext = FeederRf.LOGIN;
            ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).reset();
            return 0;
          } else {
            ExceptionAdapter.LOAD.reThrow();
          }
          dat = new QSTuple[0];
        }
        // Dib2Root.app.appState = Dib2Lang.AppState.CONFIG;
        mNext = mTask;
        mNext.op = QOpWk.zzDATA;
        mNext.wip = dat;
        mTask = null;
        break;
      case zzSAV2_zzEXIT:
        if (0 >= zLastSaveMillis) {
          break;
        }
        // Fall through.
      case zzSAV2:
        String path = (arg instanceof QSeq) ? ((QSeq) arg).toString() : "";
        if (0 >= path.length()) {
          saveAll((byte[]) mTask.wip, 0L);
        } else {
          File dir = Dib2Root.platform.getFilesDir("external");
          if ((null != dir) && dir.exists()) {
            path = new File(dir, path).getAbsolutePath();
          }
          int c0 = writeEncoded(path, (byte[]) mTask.wip, true, false, false);
          mNext = mTask;
          mNext.wip = (0 <= c0) ? QWord.TRUE : QWord.FALSE;
          mNext.op = QOpFeed.zzGET;
        }
        if ((QOpIo.zzSAV2_zzEXIT == mTask.op)
            && (Dib2Lang.AppState.EXIT_REQUEST.ordinal() <= Dib2Root.app.appState.ordinal())) {
          Dib2Root.app.appState = Dib2Lang.AppState.EXIT_DONE;
        }
        mTask = null;
        break;
      default:
        ;
    }
    // TODO
    return 0;
  }

  @Override
  public int step(long dummy) {
    return 1;
  }

  @Override
  public QToken call() {
    return mNext;
  }

  public static byte[] exportTsv(QSTuple[] xzMap, String fieldNames) {
    String header = new String(Dib2Constants.MAGIC_BYTES, StringFunc.CHAR8);
    // ld: header += "\t" + DateFunc.toDate4Millis() + ... + '\n';
    header +=
        "("
            + DateFunc.dateShort4Millis()
            + ')'
            + Dib2Constants.FILE_STRUC_VERSION_STR
            + Dib2Root.app.appShort
            + fieldNames
            + '\n';
    StringBuilder out = new StringBuilder(100 * xzMap.length);
    out.append(header);
    for (QSTuple entry : xzMap) {
      if (null == entry) {
        continue;
      }
      out.append("0#" + BigSxg.sxgChecked64(entry.stamp));
      out.append('\t');

      // if ((entry instanceof CcmSto.CcmEntry) && (((CcmSto.CcmEntry) entry).at(0) ==
      // QWord.V_WILD)) {
      if (QWord.V_WILD == entry.at(0)) {
        out.append(CcmSto.toString4VariableList(entry, '\t'));
      } else {
        final String dat = entry.toStringFull('\t', CcmTag.DAT.ordinal());
        out.append(dat);
      }
      out.append('\n');
    }

    // File debug = new File("/tmp", "tmp.1.csv");
    // byte[] csvData = out.toString().getBytes(StringFunc.CHAR16UTF8);
    // MiscFunc.writeFile(debug.getAbsolutePath(), csvData, 0, csvData.length, new byte[0]);

    return out.toString().getBytes(StringFunc.CHAR16UTF8);
  }

  public static QSTuple[] ccm4Tsv(byte[] tsvData, int flagsMarkAdjusttimeKeyHex) {
    if (tsvData.length <= 2) {
      return new QSTuple[0];
    }
    int i1 = 1;
    int i0 = 0;
    int iHeader = 3;
    CcmSto.CcmTag[] map = null;
    boolean bId = false;
    boolean hasTimeStamp = true;
    if ((tsvData[0] == Dib2Constants.MAGIC_BYTES[0])
        && (tsvData[1] == Dib2Constants.MAGIC_BYTES[1])) {
      // Mark header:
      i0 = 1 + MiscFunc.indexOf(tsvData, new byte[] {'\n'});
      iHeader = (tsvData[2] == '\t') ? 3 : 2;
    } else {
      i0 = 1 + MiscFunc.indexOf(tsvData, new byte[] {'\n'});
      final String firstLine = new String(Arrays.copyOf(tsvData, i0), StringFunc.CHAR8);
      hasTimeStamp =
          firstLine.contains("\tTIME\t")
              && firstLine.contains("\tDAT")
              && firstLine.contains("\tCAT");
      if ((' ' >= tsvData[0]) || hasTimeStamp) {
        // Mark header without magic bytes:
        iHeader = 0;
      } else {
        ///// No header.
        i0 = 0;
        int cFields = firstLine.split("\t").length;
        map = CcmSto.getTagMapStd((7 <= cFields) ? 7 : 3); // createTagMap(null);
      }
    }
    if (null == map) {
      String[] tags = StringFunc.string4Utf8(Arrays.copyOfRange(tsvData, iHeader, i0)).split("\t");
      map = CcmSto.createTagMap(tags);
      bId = CcmTag.DAT_X == map[0];
    }
    QSTuple[] out = new QSTuple[24];
    int count = 0;
    for (; i0 < tsvData.length; i0 = i1 + 1) {
      i1 = MiscFunc.indexOf(tsvData, new byte[] {'\n'}, i0);
      if (i1 < 0) {
        i1 = tsvData.length;
      }
      String line;
      try {
        line = new String(tsvData, i0, i1 - i0, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        line = new String(tsvData, i0, i1 - i0);
      }
      if (count >= out.length) {
        out = Arrays.copyOf(out, count * 2);
      }

      long stamp =
          (!bId)
              ? 0
              : (line.startsWith("0#")
                  ? BigSxg.bits4SxgChecked64(
                      line.substring(2).replaceFirst("[^0-9A-z].*", ""), false)
                  : BigSxg.long4String(line.replaceFirst("[^0-9A-z].*", ""), 1));
      QSTuple tup = CcmSto.createTuple4Tsv(line, map);
      if (null == tup) {
        continue;
      }
      tup.stamp = stamp;
      out[count++] = tup;
    }
    return Arrays.copyOf(out, count);
  }

  static int writeEncoded(
      String path, byte[] dat, boolean immediately, boolean backupOld, boolean includeTrash) {
    byte[] phrase = TcvCodec.instance.getPassFull();
    if (null == phrase) {
      return -1;
    }
    if (!immediately && ((zLastSaveMillis + 60 * 1000) >= DateFunc.currentTimeMillisLinearized())) {
      return -1;
    }
    // Dib2Root.log("save", " .. " + CcmSto.zHidden.size());
    File pathFTemp = new File(path + ".tmp");
    if (pathFTemp.isFile() && pathFTemp.exists()) {
      pathFTemp.delete();
    }
    //    zLastSaveMillis = DateFunc.currentTimeMillisLinearized();
    //    byte[] dat = null; // INSTANCE.toCsv(null, 0, ~0, includeTrash ? (~0) : (~2));
    Dib2Root.log("exportLines", "ok " + dat.length);

    int len =
        TcvCodec.instance.writePacked(
            dat, 0, dat.length, pathFTemp.getAbsolutePath()); // , optVersion);
    Dib2Root.log("save", "ok? " + len);
    if (0 <= len) {
      File pathFNew = new File(path);
      if (pathFNew.isFile()) {
        if (backupOld) {
          path = pathFNew.getAbsolutePath();
          File old = new File(path + ".old");
          if (old.exists()) {
            if (!immediately || (1000 > zLastSaveMillis)) { // !CcmSto.zLoadSuccess) {
              old = new File(path + ".bak");
            }
            if (old.exists()) {
              old.delete();
            }
          }
          pathFNew.renameTo(old);
        } else {
          pathFNew.delete();
        }
      }
      pathFTemp.renameTo(pathFNew);
    }
    //    zLastSaveMillis = DateFunc.currentTimeMillisLinearized();
    return len;
  }

  /**
   * Import encoded file.
   *
   * @param filePath Path with name of file
   * @param phrase Pass phrase
   * @param replace true for overriding everything
   * @return number of imported records or -1
   */
  static QSTuple[] importEncoded(String filePath, boolean replace) {
    byte[] header = new byte[8];
    byte[] dat = TcvCodec.instance.readPacked(filePath, header, null, null);
    if (null == dat) {
      Dib2Root.log("import", "read/decode failed.");
      return null;
    }
    int version = header[2] & 0xff;
    version = (6 >= version) ? (10 * version) : header[4];
    //    int count = 0;
    int flagsMarkAdjusttimeKeyhex = (replace ? 0 : 2) | ((30 >= version) ? 1 : 0);
    flagsMarkAdjusttimeKeyhex |= (50 > version) ? 4 : 0;
    try {
      QSTuple[] out = ccm4Tsv(dat, flagsMarkAdjusttimeKeyhex);
      Dib2Root.log("import", "" + out.length);
      return out;
    } catch (Exception e) {
      Dib2Root.log("import", e.getMessage());
    }
    return null;
  }

  private static QSTuple[] findPhraseLoad(String path) {
    //    int out = -1;
    byte[] phrase = TcvCodec.instance.getPassFull();
    if ((null == path) || (null == phrase)) {
      // zLastSaveMillis = DateFunc.currentTimeMillisLinearized();
      return (null == phrase) ? null : new QSTuple[0];
    }
    // Dib2Root.log("load", " .. " + CcmSto.zHidden.size());
    QSTuple[] out = importEncoded(path, true);
    return out;
  }

  public static boolean check4AutoSave() {
    long cmp = DateFunc.currentTimeMillisLinearized();
    return (zLastSaveMillis + Dib2Root.app.autosaveInterval_msec) < cmp;
  }

  @Override
  public void removeWipData4Interrupts() {}

  // =====

}
