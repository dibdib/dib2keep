// Copyright (C) 2021, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_wk.QOpWk;

// =====

public enum QOpFeed implements QIfs.QEnumIf {

  // =====

  // zzAUTOLOGIN,
  zzACCESSCODE,
  zzAPPLY, // apply operator on stack
  // zzCHECK4UI,
  zzEXEC, // execute 'as is'
  zzFILL,
  zzGET,
  zzINIT_NEW,
  zzINIT_LOAD,
  zzPHRASE,
  zzPUSH,
  zzREFRESH,
  zzSAV0,
  zzSAV0_zzEXIT,

  /////

  CLRALL, // (0, 0, "clear all 'volatile' data (stack + memory)"),
  FSAVE,
  ;

  static QSTuple zStack = new QSTuple(null, 100);

  public static QIfs.QEnumIf[] create() {
    QOpMain.opsInternal.put("EXEC", zzEXEC);
    QOpMain.opsInternal.put("APPLY", zzAPPLY);
    QOpMain.opsInternal.put("GO", zzAPPLY);
    return values();
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }

  static boolean process4Stack(QToken task) {
    switch ((QOpMain) task.op) {
      case DUP:
        // zStack.push(zStack.peek(0));
        task.op = zzGET;
        if (task.argX instanceof QSeq) {
          task.wip = new QSeq[] {(QSeq) task.argX, (QSeq) task.argX};
          MainThreads.push(task);
          return true;
        }
        break;
      case SWAP:
        task.op = zzGET;
        if ((task.argX instanceof QSeq) && (task.argY instanceof QSeq)) {
          task.wip = new QSeq[] {(QSeq) task.argY, (QSeq) task.argX};
          MainThreads.push(task);
          return true;
        }
        break;
      case CLR3:
      case CLR2:
      case CLR1:
      case CLEAR:
        // Just drop the arguments:
        return true;
      case CLRN:
        if ((task.argX instanceof QWord) && ((QWord) task.argX).isNumeric()) {
          for (int i0 = (int) ((QWord) task.argX).i64(); i0 > 0; --i0) {
            zStack.pop(true);
          }
          return true;
        }
        break;
      case NOP:
        return true;
      default:
        ;
    }
    return false;
  }

  static void process(QToken task) {
    switch ((QOpFeed) task.op) {
      case zzACCESSCODE:
        {
          String path = IoRunner.check4Load();
          if (null != path) {
            ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).setPath(path);
            Dib2Root.app.feederNext = FeederRf.LOGIN;
          }
        }
        break;
      case zzPHRASE:
        {
          ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).requestPhrase();
          Dib2Root.app.feederNext = FeederRf.LOGIN;
        }
        break;
      case zzINIT_LOAD:
        {
          task.op = QOpIo.zzLOAD_INITIAL;
          zStack.insert(0, task.pushWip4Seq(null));
          MainThreads.push(task);
        }
        break;
      case zzSAV0:
      case zzSAV0_zzEXIT:
        {
          task.op = (QOpFeed.zzSAV0_zzEXIT == task.op) ? QOpWk.zzSAV1_zzEXIT : QOpWk.zzSAV1;
          task.argX = QSeq.NIL;
          task.pushWip4Seq(((QSTuple) zStack.clone()).items);
          MainThreads.push(task);
        }
        break;
      case FSAVE:
        {
          task.op = QOpWk.zzSAV1;
          if (null == task.argX) {
            task.argX = zStack.pop(true);
          }
          if (null != task.argX) {
            zStack.push(task.pushWip4Seq(((QSTuple) zStack.clone()).items));
            MainThreads.push(task);
          }
        }
        break;
      case zzFILL:
        {
          QToken.zWip.remove(task.stamp);
          if (QOpMain.zzWIPCALC == (QIfs.QWordIf) ((QSeq) zStack.peek(0)).at(0)) {
            zStack.remove(0);
          } else if (0 < zStack.size()) {
            break;
          }
          if (null != task.wip) {
            for (QSeqIf val : (QSeqIf[]) task.wip) {
              if (null == val) {
                continue;
              }
              zStack.insert(0, val);
            }
          }
          if (Dib2Lang.AppState.ACTIVE.ordinal() > Dib2Root.app.appState.ordinal()) {
            Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          }
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        }
        break;
      case zzPUSH:
        {
          final QSeqIf sq = task.argX;
          if (sq instanceof QSeqIf) {
            zStack.push(task.argX);
          }
        }
        break;
      case zzAPPLY:
        {
          if (null == task.argX) {
            task.argX = zStack.pop(true);
            task.argX = (null == task.argX) ? QWord.V_BLANK : task.argX;
          }
          task.op =
              (task.argX instanceof QEnumIf)
                  ? (QEnumIf) task.argX
                  : Dib2Root.valueOfOr(task.argX.toString(), false, null);
          if (null == task.op) {
            break;
          }
          task.shiftArgs();
          if (task.op instanceof QOpMain) {
            if ((null == task.argZ) && (3 <= ((QOpMain) task.op).cArgs)) {
              if (null == (task.argZ = zStack.pop(true))) {
                return;
              }
            }
            if ((null == task.argY) && (2 <= ((QOpMain) task.op).cArgs)) {
              if (null == (task.argY = zStack.pop(true))) {
                return;
              }
            }
            if ((null == task.argX) && (1 <= ((QOpMain) task.op).cArgs)) {
              if (null == (task.argX = zStack.pop(true))) {
                return;
              }
            }
          }
          final int c0 = (task.op instanceof QOpMain) ? ((QOpMain) task.op).cReturnValues : 1;
          final QSeq sq = (0 >= c0) ? null : task.pushWip4Seq(null);
          for (int i0 = 0; i0 < c0; ++i0) {
            zStack.push(sq);
          }
          MainThreads.push(task);
        }
        break;
      case zzGET:
        {
          QToken.zWip.remove(task.stamp);
          int cTop = 0;
          QSeqIf el = null;
          for (; true; ++cTop) {
            el = zStack.peek(cTop);
            if (null == el) {
              break;
            } else if (task.parN0 == el.getShash()) {
              break;
            }
          }
          if (task.wip instanceof QSeq[][]) {
            final QSeq[][] seqs = (QSeq[][]) task.wip;
            for (QSeq[] dat : seqs) {
              if (el != zStack.peek(cTop)) {
                break;
              }
              final QSeqIf sq = (1 == dat.length) ? dat[0] : QSeq.createFlat(dat);
              zStack.replace(-1 - cTop, sq);
              ++cTop;
            }
          } else if (task.wip instanceof QSeq[]) {
            final QSeq[] seqs = (QSeq[]) task.wip;
            for (int i0 = seqs.length - 1; i0 >= 0; --i0, ++cTop) {
              final QSeq dat = seqs[i0];
              if (el != zStack.peek(cTop)) {
                continue;
              }
              zStack.replace(-1 - cTop, dat);
            }
          } else if (task.wip instanceof QSeq) {
            zStack.replace(-1 - cTop, (QSeq) task.wip);
          }
        }
        break;
      case CLRALL:
        {
          zStack = new QSTuple(null, 100);
        }
        break;
      default:
        ;
    }
  }

  // =====
}
