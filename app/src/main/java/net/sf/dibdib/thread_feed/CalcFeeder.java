// Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.*;

// =====

final class CalcFeeder extends FeederRf.GenericTextFeeder {

  // =====

  static class IntroCalc extends FeederRf.GenericTextFeeder {

    // =====

    private int step = 0;
    private int lastStep = -1;
    private String[][] feeds =
        new String[][] {
          Dib2Lang.kUiIntro100,
          Dib2Lang.kUiIntro110,
          Dib2Lang.kUiIntroCalc200,
          Dib2Lang.kUiIntroCalc210,
          Dib2Lang.kUiIntroCalc220,
          Dib2Lang.kUiIntroCalc230,
          Dib2Lang.kUiIntroCalc240,
          Dib2Lang.kUiIntroCalc250,
        };

    public IntroCalc(FeederRf owner) {
      super(owner);
    }

    private void initCanvasGap() {
      final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X);
      token.parX = UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X.getInitial();
      UiPres.INSTANCE.wxGateIn4Feed.push(token);
    }

    @Override
    public FeederRf start() {
      super.start();
      initCanvasGap();
      return me;
    }

    @Override
    protected int prepareTextLines() {
      if (lastStep != step) {
        lastStep = step;
        if (0 >= step) {
          zFeedTxt = feeds[step];
        } else {
          zFeedTxt = Dib2Lang.pickTransl(feeds[step]);
        }
      }
      return zFeedTxt.length;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      if (7 <= step) {
        step = feeds.length;
      } else if (cmd.op instanceof QOpFeed) { // || (.zzAPPLY==cmd.op) {
        switch (step) {
          case 2:
            if ('+' == cmd.parX) {
              ++step;
            }
            break;
          case 3:
          case 4:
            if (QOpFeed.zzPUSH == cmd.op) {
              ++step;
            } else if (' ' > cmd.parX) {
              return cmd;
            }
            break;
          case 5:
          case 6:
            if ('*' == cmd.parX) {
              ++step;
            }
            break;
          default:
            return cmd;
        }
      } else if (cmd.op instanceof QOpMain) {
        switch ((QOpMain) cmd.op) {
          case NOP:
          case DUP:
            if (0 == step) {
              initCanvasGap();
            }
            ++step;
            break;
          case ESCAPE:
            step = feeds.length;
            break;
          default:
            ;
        }
      } else if ("ESCAPE".equals(cmd.op.name())) {
        step = feeds.length;
      }
      if (step >= feeds.length) {
        Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
      }
      return (cmd.op instanceof QOpUi) ? cmd : null;
    }
  }

  // =====

  public CalcFeeder(FeederRf owner) {
    super(owner);
    cSlides = 4;
  }

  @Override
  public FeederRf start() {
    super.start();
    final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X);
    token.parX = UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X.getInitial();
    UiPres.INSTANCE.wxGateIn4Feed.push(token);
    return me;
  }

  public static int stackRead(
      final QSTuple stk,
      int xOffsetData,
      long xbHexMemory,
      String[] yLines,
      int xOffsetOut,
      long xbPartialLess) {
    final int cStack = stk.size();
    if ((0 == (1 & xbPartialLess)) && (cStack > (yLines.length - xOffsetOut))) {
      return -cStack - xOffsetOut;
    }
    int to = xOffsetOut;
    int ceil = cStack + xOffsetOut;
    final int max = (0 != (2 & xbPartialLess)) ? (xOffsetOut + 4) : yLines.length;
    for (int next = cStack - 1 - xOffsetData; (next >= 0) && (to < max); --next, ++to) {
      if (((max - 1) == to) && ((ceil - 1) > to)) {
        yLines[to] = "...";
        continue;
      }
      final int iKey = to - xOffsetOut - 2 + xOffsetData;
      String key = (-2 == iKey) ? "X" : ((-1 == iKey ? "Y" : ((0 == iKey ? "Z" : ("Z" + iKey)))));
      final QSeqIf val = (QSeqIf) stk.at(next);
      if ((null == val) || (QWord.NaN == val) || !(val instanceof QSeq)) {
        yLines[to] = key + "\t^";
      } else {
        yLines[to] = key + '\t' + ((QSeq) val).format((0 != (1 & xbHexMemory)));
      }
    }
    if (0 != (2 & xbHexMemory)) {
      to = Dib2Root.ccmSto.peekVariables(yLines, to);
    }
    if (to < yLines.length) {
      yLines[to] = null;
    }
    return to;
  }

  @Override
  protected int prepareTextLines() {
    if (1 < (nSlide30Supp & 0x3fffffff)) {
      // TODO: No supp pages yet.
      nSlide30Supp = ((nSlide30Supp >>> 30) << 30) | 1;
    }
    final int page = (int) (nSlide30Supp >>> 30);
    final long bHexMemory = ((1 == page) || (3 == page)) ? 3 : 1;
    final int lines = linesPerSlide();
    final int offset = 1;
    zFeedTxt = (null == zFeedTxt) || (zFeedTxt.length <= lines) ? new String[lines + 1] : zFeedTxt;
    zFeedTxt[offset - 1] = "";
    int end = offset;
    if (3 >= page) {
      final long bPartialLess = (2 <= page) ? 3 : 1;
      end = stackRead(QOpFeed.zStack, 0, bHexMemory, zFeedTxt, offset, bPartialLess);
    }
    if ((2 == page) || (4 <= page)) {
      end = (end <= (zFeedTxt.length - 5)) ? end : (zFeedTxt.length - 5);
      int offs = (4 >= page) ? 0 : ((page - 4) * (lines - end));
      QMMap map = CcmSto.peekMappings();
      QIfs.QItemIf[] items = new QIfs.QItemIf[lines - end];
      int c0 = map.dump(items, 0, offs);
      if (0 > c0) {
        int pg = -c0 / lines + 4;
        cSlides = (pg > cSlides) ? pg : cSlides;
      }
      final QSeq cats = Dib2Root.ccmSto.variable_get("C");
      final QSeq lbx = (2 < page) ? null : Dib2Root.ccmSto.variable_get("Q");
      String lbl = ((null == lbx) ? "." : lbx.toStringFull());
      if (2 >= page) {
        zFeedTxt[end++] = "C\t" + ((null == cats) ? "." : cats.toStringFull());
        zFeedTxt[end++] = "Q\t" + lbl;
      }
      long flags = (null == cats) ? 0 : Cats.toFlags(cats.atoms());
      lbl = (1 >= lbl.length()) ? null : (lbl + ".*");
      long last = -1L;
      String hint = "\t...";
      for (int i0 = 0; (i0 <= items.length) && (end < zFeedTxt.length); ++i0) {
        if ((0 <= c0) && (c0 <= i0)) {
          break;
        }
        String line = null;
        if ((i0 >= items.length) || (null == items[i0])) {
          if ((4 <= page) || (-1L == last)) {
            break;
          }
          i0 = 1;
          items[0] = map.searchNext(last, items, i0);
          if (null == items[0]) {
            break;
          } else if (!(items[0] instanceof QSTuple)) {
            zFeedTxt[end++] = "...";
            break;
          }
        }
        if (!(items[i0] instanceof QSTuple)) {
          if (3 <= page) {
            line = "\t" + items[i0].toString();
          } else if (null != hint) {
            line = hint;
            hint = null;
          }
        } else {
          last = ((QSTuple) items[i0]).getValue(CcmSto.CcmTag.LABEL).getShash();
          boolean take = (3 <= page);
          if (!take) {
            QSeqIf cx = ((QSTuple) items[i0]).getValue(CcmSto.CcmTag.CATS);
            if ((cx instanceof QSeq) && (flags == (flags & ((QSeq) cx).atom().i64()))) {
              if ((null == lbl)
                  || (((QSTuple) items[i0]).getAsString(CcmSto.CcmTag.LABEL).matches(lbl))) {
                take = true;
              }
            }
          }
          if (take) {
            line = QWord.createQWordInt(items[i0].getShash()).toString();
            line =
                ".."
                    + line.substring(line.length() - 2)
                    + "\t"
                    + ((QSTuple) items[i0]).toStringFull('\t');
          }
        }
        if (null != line) {
          zFeedTxt[end++] = line;
        }
      }
      if ((end >= lines) && (cSlides <= page)) {
        ++cSlides;
      } else if ((offset >= end) && (4 < page)) {
        --cSlides;
      }
    }
    for (int i0 = zFeedTxt.length - 1; i0 >= end; --i0) {
      zFeedTxt[i0] = null;
    }
    zFeedTxt[0] = "" + '\t' + '@';
    for (int i0 = end - 1; i0 >= 0; --i0) {
      zFeedTxt[i0] = StringFunc.makePrintable(zFeedTxt[i0]);
    }
    return end;
  }

  @Override
  public QToken tryOrFilter4Ui(QToken cmd) {
    return cmd;
  }

  // =====
}
