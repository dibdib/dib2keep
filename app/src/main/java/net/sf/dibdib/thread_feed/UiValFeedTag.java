// Copyright (C) 2020, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;

// =====

/** Additional variables and helpers for presenting data, cmp. UiValTag. */
public enum UiValFeedTag implements QComponentTagged {

  // =====

  UI_FONT_SIZE_PT10(12 << Dib2Constants.UI_PT10_SHIFT),
  UI_LINE_SPACING_PT10(Dib2Constants.UI_DSPL_NMZ_LF),

  // TODO
  UI_PAGE_HEIGHT(0),
  UI_PAGE_WIDTH(0),
  // One or several sheets per slide ...
  UI_SHEET_HEIGHT(0), // Partial page or several pages ...
  UI_SHEET_WIDTH(0),

  // UI_FILTER_CATS(0),

  X_UI_SIZE(0),
  ;

  static QComponent zComponent = new QComponent(new QSeqInt(X_UI_SIZE.ordinal()));

  final int nInit;

  private UiValFeedTag(int i32) {
    nInit = i32;
  }

  /** Secondary component of UiVal, singleton. */
  public static QComponent create() {
    for (UiValFeedTag el : values()) {
      if (X_UI_SIZE == el) {
        break;
      }
      el.setInitial(el.nInit);
    }
    return zComponent;
  }

  public int getInitial() {
    return ((QSeqInt) zComponent.initial).items[ordinal()];
  }

  public void setInitial(int i32) {
    ((QSeqInt) zComponent.initial).items[ordinal()] = i32;
  }

  public long i64(long tick, Object... dummy) {
    return ((QSeqInt) Dib2Root.uiVals.get(1, tick)).items[ordinal()];
  }

  public int i32(long tick, Object... dummy) {
    return ((QSeqInt) Dib2Root.uiVals.get(1, tick)).items[ordinal()];
  }

  public double d4(long tick, Object... dummy) {
    return Dib2Constants.INT_D4_FACT * ((QSeqInt) Dib2Root.uiVals.get(1, tick)).items[ordinal()];
  }

  public String strFull(long tick, Object... dummy) {
    return "" + ((QSeqInt) Dib2Root.uiVals.get(1, tick)).items[ordinal()];
  }

  int i32Fut() {
    return ((QSeqInt) Dib2Root.uiVals.components[1].future).items[ordinal()];
  }

  @Override
  public long getShash() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void setShashOrIgnore(long arg0) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public static QSeq peekStack() {
    final QSeqIf out = QOpFeed.zStack.peek(0);
    return (out instanceof QSeq) ? (QSeq) out : QWord.NaN;
  }
}
