// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import static net.sf.dibdib.thread_any.StringFunc.*;

import com.gitlab.dibdib.picked.common.Codata;
import java.io.File;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QToken.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_io.IoRunner;
import net.sf.dibdib.thread_io.QOpIo;
import net.sf.dibdib.thread_ui.*;

// =====

/** 'View model' provider, enumeration as owner referencing actual implementation. */
public enum FeederRf {

  // =====

  DISCLAIMER("NN"),
  LICENSE("LC", "LICENSE"),
  HELP("HP"),
  ABOUT("AB"),
  LOGIN("LG"),
  INTROCALC("IN"),
  // CALEND("CN"),
  CALC("CC", "AWT", "TTY", "J", "CO"),
  ;

  // =====

  private FeederIf mFeeder;
  private final String[] mOptionalNames;

  private FeederRf(String... optionalNames) {
    mFeeder = null;
    mOptionalNames = optionalNames;
  }

  // =====

  public static interface FeederIf {

    // =====

    public FeederRf start();

    /**
     * Get initial/ next/ previous feeder.
     *
     * @param handle 0=initial, 1=next
     * @return feeder
     */
    // public FeederRf prepareFeeder(int handle, String... optionalName);

    //    public FeederIf setFeeder(int handle, FeederRf next);

    public int getCountSlides();

    public long getNumSlide30Supp();

    public long findSlideSupplement(int vSlide, long bAbsRelSupp);

    public QScript prepareFeed(String... param);

    public QScript getLastFeed();

    /** Filter commands, exclusive usage: platform's UI thread, or mouse handler. */
    public QToken tryOrFilter4Ui(QToken out);
  }

  // =====

  /**
   * Get the actual implementation.
   *
   * @return implementer.
   */
  public FeederIf get() {
    if (null != mFeeder) {
      return mFeeder;
    }
    switch (this) {
      case DISCLAIMER:
        mFeeder = new GenericTextFeeder(this);
        break;
      case LICENSE:
        mFeeder = new LicenseFeeder(this);
        break;
      case HELP:
      case ABOUT:
        mFeeder = new HelpFeeder(this);
        break;
      case LOGIN:
        mFeeder = new LoginFeeder(this);
        break;
      case INTROCALC:
        mFeeder = new CalcFeeder.IntroCalc(this);
        break;
      case CALC:
        mFeeder = new CalcFeeder(this);
        break;
      default:
        return null;
    }
    return mFeeder;
  }

  public String getShortId2() {
    return mOptionalNames[0];
  }

  public static FeederRf find(FeederIf impl) {
    for (FeederRf fd : FeederRf.values()) {
      if (impl.getClass().getName().toUpperCase(Locale.ROOT).contains(fd.name())) {
        return fd;
      }
    }
    return DISCLAIMER;
  }

  // =====

  public abstract static class GenericFeeder implements FeederIf {

    // =====

    protected final FeederRf me;
    protected int cSlides = 1;
    protected volatile QScript mFeed = null;
    protected volatile long nSlide30Supp = 1L << 30;

    public GenericFeeder(FeederRf owner) {
      me = owner;
    }

    protected int linesPerSlide() {
      return -1 + UiValTag.UI_BOARD_HEIGHT.i32(0) / UiValFeedTag.UI_LINE_SPACING_PT10.i32Fut();
    }

    @Override
    public FeederRf start() {
      // UiValTag.UI_DISPLAY_SPLIT_CANVAS_X.setInitial(Dib2Constants.UI_DSPL_INIT_SPLIT_X);
      Dib2Root.app.feederNext = find(this);
      mFeed = null;
      return me;
    }

    public FeederRf getOwner() {
      return me;
    }

    // To be implemented:
    // public QScript prepareFeed(String... param) {

    @Override
    public QScript getLastFeed() {
      return mFeed;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      return cmd;
    }

    @Override
    public int getCountSlides() {
      return cSlides;
    }

    @Override
    public long getNumSlide30Supp() {
      return nSlide30Supp;
    }

    @Override
    public long findSlideSupplement(int vNum, long bAbsRelSupp) {
      long nSlide = vNum;
      int supp = 0;
      if (4 == bAbsRelSupp) {
        nSlide = (nSlide30Supp >>> 30);
        supp = (int) (vNum + (nSlide30Supp & 0x3fffffff));
        supp = (0 <= supp) ? supp : 0;
        supp = ((1 << 20) > supp) ? supp : 0;
      } else if (2 == bAbsRelSupp) {
        nSlide = (nSlide30Supp >>> 30) + vNum;
      }
      nSlide = (cSlides <= nSlide) ? cSlides : nSlide;
      nSlide = (1 >= nSlide) ? 1 : nSlide;
      nSlide30Supp = supp | (nSlide << 30);
      return nSlide30Supp;
    }
  }

  // =====

  public static class GenericTextFeeder extends GenericFeeder {

    // =====

    protected int GenericTextFeeder_iLang = 0;

    public GenericTextFeeder(FeederRf owner) {
      super(owner);
    }

    protected String[] zFeedTxt = Dib2Lang.kUiAgree.clone();
    protected int zFeedLinesSkip = 0;

    public void setText(String[] xmTxt) {
      zFeedTxt = xmTxt;
    }

    @Override
    public FeederRf start() {
      super.start();
      // Start with English:
      GenericTextFeeder_iLang = 0;
      return me;
    }

    protected int splitTextLines(String[] all) {
      final int lines = linesPerSlide();
      if (100 > zFeedTxt.length) {
        zFeedTxt = Arrays.copyOf(zFeedTxt, (100 > lines) ? 100 : lines);
      }
      if (all.length > lines) {
        cSlides = 1 + (all.length / lines);
      }
      int start = ((int) (nSlide30Supp >>> 30) - 1) * lines;
      start = (all.length <= start) ? (all.length - 1) : start;
      int end = start + lines;
      end = (all.length < end) ? all.length : end;
      final int len = end - start;
      System.arraycopy(all, start, zFeedTxt, 0, len);
      if (len < zFeedTxt.length) {
        zFeedTxt[len] = null;
      }
      return len;
    }

    protected int prepareTextLines() {
      // Disclaimer and license as default
      String[] out =
          (Dib2Root.app.appState == Dib2Lang.AppState.LOGIN)
              ? Dib2Lang.kFeedLoadSave
              : Dib2Lang.kUiAgree;
      return splitTextLines(out);
    }

    @Override
    public QScript prepareFeed(String... param) {
      int c0 = prepareTextLines();
      String[] lines = zFeedTxt;
      int no = 0;
      int count = 0;
      QScript[] script = new QScript[2 + 6 * c0];
      final int linesp = UiValFeedTag.UI_LINE_SPACING_PT10.i32Fut();
      final int height = UiValFeedTag.UI_FONT_SIZE_PT10.i32Fut();
      final int base =
          linesp
              - ((height * Dib2Constants.UI_FONT_NMZ_DESCENT >> Dib2Constants.UI_FONT_NMZ_SHIFT));
      script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base);
      for (int i0 = 0; i0 < c0; i0 += 1 + zFeedLinesSkip) {
        String[] tabbed = (null == lines[i0]) ? new String[0] : lines[i0].split("\t");
        if ((count + tabbed.length * 2 + 2) >= script.length) {
          script = Arrays.copyOf(script, 2 * (script.length + tabbed.length));
        }
        // Margin to be calculated extra for display or paper:
        int posx = 0;
        for (String s0 : tabbed) {
          script[count++] = QScript.makeScriptEl(++no, QOpGraph.POSX, posx);
          if ((0 >= i0) && lines[i0].startsWith("\t@") && s0.startsWith("@")) {
            script[count++] = QScript.makeScriptEl(++no, QOpGraph.ENTRY);
          } else {
            script[count++] = QScript.makeScriptEl(++no, QOpGraph.TEXT, s0);
          }
          posx += UiFunc.boundWidthNmz(s0 + 'm', height);
          posx = (((posx - 2) / Dib2Constants.UI_DSPL_NMZ_TAB) + 1) * Dib2Constants.UI_DSPL_NMZ_TAB;
        }
        script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXLF);
      }
      QScript out = QScript.makeScript(0);
      out.script = script;
      out.cScript = count;
      // Atomic:
      mFeed = out;
      return out;
    }

    @Override
    public QScript getLastFeed() {
      if (null == mFeed) {
        mFeed = prepareFeed();
      }
      return mFeed;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      ///// For disclaimer and license.
      if ((QOpFeed.zzAPPLY == cmd.op) || (QOpFeed.zzPUSH == cmd.op)) {
        Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
      } else {
        boolean disclaimer =
            (Dib2Root.app.appState.ordinal() <= Dib2Lang.AppState.DISCLAIMER.ordinal());
        // Get name in case of delegated operation:
        final String nam = cmd.op.name();
        if ("ESCAPE".equals(nam)) {
          if (disclaimer) {
            Dib2Root.app.appState = AppState.EXIT_DONE;
            return null;
          }
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        } else if ((QOpMain.NOP == cmd.op) || (QOpMain.DUP == cmd.op)) {
          final FeederRf next = (!disclaimer) ? (FeederRf) Dib2Root.app.mainFeeder : INTROCALC;
          Dib2Root.app.feederNext = next;
        } else {
          return (cmd.op instanceof QOpMain) ? null : cmd;
        }
      }
      if (Dib2Root.app.appState.ordinal() < Dib2Lang.AppState.ACTIVE.ordinal()) {
        Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
        MainThreads.push(QToken.createTask(QOpFeed.zzSAV0));
      }
      GenericTextFeeder_iLang = -1;
      return null;
    }
  }

  // =====

  static class LicenseFeeder extends GenericTextFeeder {

    private String[] GenericTextFeeder_license = null;

    public LicenseFeeder(FeederRf owner) {
      super(owner);
    }

    @Override
    public FeederRf start() {
      super.start();
      // Language might have changed ...
      GenericTextFeeder_iLang = -1;
      // Window not split:
      final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X);
      token.parX = 0;
      UiPres.INSTANCE.wxGateIn4Feed.push(token);
      return me;
    }

    protected int prepareTextLines() {
      if ((Dib2Root.ui.iLang != GenericTextFeeder_iLang) || (null == GenericTextFeeder_license)) {
        GenericTextFeeder_iLang = Dib2Root.ui.iLang;
        GenericTextFeeder_license =
            Dib2Root.platform.getLicense(Dib2Lang.pickTransl(Dib2Lang.kLicensePre));
      }
      return splitTextLines(GenericTextFeeder_license);
    }

    // =====

  }

  // =====

  static class HelpFeeder extends GenericTextFeeder {

    // =====

    public HelpFeeder(FeederRf owner) {
      super(owner);
    }

    private static String[] getHelp_lines = null;

    static String[] getHelp() {
      if (null == getHelp_lines) {
        int count = 0;
        String[] lines =
            new String
                [3
                    + QOpMain.values().length
                    + 1
                    + Codata.values().length
                    + 2
                    + DateFunc.DateFormat.values().length
                    + 2];
        lines[count++] =
            "Version " + Dib2Constants.VERSION_STRING + ". " + Dib2Constants.NO_WARRANTY[0];
        lines[count++] =
            "List of available FUNCTIONS (see below, e.g.:"
                + (Dib2Root.ui.bTerminalMode
                    ? " type '\\' + file name, press ENTER, ';EXPORT', ENTER):"
                    : " ^FileName, ^ENTER, 'EXPORT', GO):");
        lines[count++] = "(Not fully implemented yet !)";
        if (!Dib2Root.ui.bTerminalMode) {
          lines[count++] = "(E.g. type '3', press > or ENTER, '4', > or ENTER,";
          lines[count++] = "type 'ADD', (press > or ENTER,) press GO)";
        }
        for (QOpMain funct : QOpMain.values()) {
          String descr = funct.getDescription();
          if ('.' != descr.charAt(0)) {
            lines[count++] = descr;
          }
        }
        if (Dib2Root.ui.bTerminalMode) {
          lines[count++] = "(Use with preceding ';' for commands, '\\' for data.";
          lines[count++] =
              "E.g.: press '\\', type file name, press ENTER, type ';EXPORT', press ENTER)";
        }
        if (!Dib2Root.ui.bTerminalMode) {
          lines[count++] = "";
          lines[count++] = "Constants:";
          for (Codata val : Codata.values()) {
            lines[count++] = val.name() + '\t' + val.quantity;
          }
          lines[count++] = "";
          lines[count++] = "Date format:";
          for (DateFunc.DateFormat val : DateFunc.DateFormat.values()) {
            lines[count++] = val.name() + '\t' + val.descr;
          }
        }
        getHelp_lines = Arrays.copyOf(lines, count);
      }
      return getHelp_lines;
    }

    @Override
    protected int prepareTextLines() {
      return splitTextLines(getHelp());
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      return (cmd.op instanceof QOpMain) ? null : cmd;
    }
  }

  // =====

  public static class LoginFeeder extends GenericTextFeeder {

    // =====

    private String mPathDataFile = null;
    private boolean mbAccessCodeFirst = true;
    private boolean mbLoading = false;
    private String[] zTxt = null;
    long lastUpdateNanos = 0;

    public LoginFeeder(FeederRf owner) {
      super(owner);
      zTxt = null;
    }

    @Override
    public FeederRf start() {
      super.start();
      final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X);
      token.parX = 0;
      UiPres.INSTANCE.wxGateIn4Feed.push(token);
      return me;
    }

    public void reset() {
      mbAccessCodeFirst = true;
      lastUpdateNanos = 0;
    }

    public void setPath(String pathDataFile) {
      mPathDataFile = pathDataFile;
      reset();
    }

    public void requestPhrase() {
      mbAccessCodeFirst = false;
      lastUpdateNanos = 0;
    }

    @Override
    protected int prepareTextLines() {
      final long nanos = DateFunc.currentTimeNanobisLinearized(false);
      final boolean delay = (nanos < (lastUpdateNanos + (2L << 30)));
      lastUpdateNanos = nanos;
      if (mbLoading) {
        mbLoading = delay;
        zFeedTxt = Dib2Lang.kUiStepAcLoad_x;
        return zFeedTxt.length;
      }
      if (null == mPathDataFile) {
        mPathDataFile = IoRunner.check4Load();
      }
      if ((null == zTxt) || !delay) {
        final String[] info = // TcvCodec.instance.setAccessCode(null) ? Dib2Lang.kUiStepPw :
            // Dib2Lang.kUiStepAc;
            //            Dib2Lang.pickTransl(
            (Dib2Root.app.bAllowDummyPass && mbAccessCodeFirst)
                ? Dib2Lang.kUiStepAcOpt
                : (mbAccessCodeFirst ? Dib2Lang.kUiStepAc : Dib2Lang.kUiStepPw);
        final String[] optionalPaths =
            IoRunner.listPaths(Dib2Constants.MAGIC_BYTES_STR, false, "main", "external");
        zTxt = Arrays.copyOf(info, 3 + info.length + optionalPaths.length);
        int i1 = info.length;
        zTxt[i1++] =
            "==> "
                + ((null == mPathDataFile)
                    ? Dib2Root.app.dbFileName
                    : (mPathDataFile.substring(1 + mPathDataFile.lastIndexOf('/')))
                        + '\t'
                        + mPathDataFile);
        zTxt[i1++] = "";
        for (int i0 = 0; i0 < optionalPaths.length; ++i0) {
          zTxt[i1++] =
              optionalPaths[i0].substring(1 + optionalPaths[i0].lastIndexOf('/'))
                  + '\t'
                  + optionalPaths[i0];
        }
      }
      zFeedTxt = zTxt;
      return zFeedTxt.length;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      lastUpdateNanos = 0;
      if ((QOpFeed.zzAPPLY == cmd.op) || (QOpFeed.zzPUSH == cmd.op)) {
        cmd.op = QOpMain.NOP;
      }
      if (!(cmd.op instanceof QOpMain)) {
        if (QOpUi.zzKEY == cmd.op) {
          final char key = (char) cmd.parX;
          switch (key) {
            case CR:
            case LF:
            case PUSH:
              cmd.op = QOpMain.NOP;
              return cmd;
            case StringFunc.XCOPY:
              IoRunner.backupFiles("main", "external");
              return null;
            case StringFunc.XPASTE:
              File dirFrom = Dib2Root.platform.getFilesDir("external");
              File dirTo = Dib2Root.platform.getFilesDir("main");
              if ((null == dirFrom)
                  || (null == dirTo)
                  || (dirFrom.equals(dirTo)
                      || !dirFrom.exists()
                      || !dirTo.exists()
                      || !dirFrom.isDirectory())
                  || !dirTo.isDirectory()) {
                return null;
              }
              File from = new File(dirFrom, Dib2Root.app.dbFileName);
              if (!from.exists()) {
                return null;
              }
              File to = new File(dirTo, Dib2Root.app.dbFileName);
              if (to.exists()) {
                File to2 = new File(dirTo, Dib2Root.app.dbFileName + ".x.bak");
                if (to2.exists()) {
                  to2.delete();
                }
                to.renameTo(to2);
              }
              IoRunner.copyFile(from, to);
              return null;
            default:
              return cmd;
          }
        }
        return (cmd.op instanceof QOpUi) ? cmd : null;
      }
      String entry = cmd.parS0;
      cmd.parS0 = null;
      cmd.argX = null;
      mbLoading = false;
      switch ((QOpMain) cmd.op) {
        case ESCAPE:
          File path;
          if ((null != mPathDataFile) && (path = new File(mPathDataFile)).exists()) {
            String name = DateFunc.dateShort4Millis().substring(4, 6);
            name = mPathDataFile.replace(".dm", "." + name + ".bak");
            File path2 = new File(name);
            if (path2.exists()) {
              path2.delete();
            }
            path.renameTo(path2);
            mPathDataFile = null;
          }
          if (Dib2Root.app.bAllowDummyPass) {
            TcvCodec.instance.setDummyPhrase(true);
            Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
            Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          } else if (TcvCodec.instance.setAccessCode(null)
              && (4 < TcvCodec.instance.getPassFull().length)) {
            mbLoading = true;
            Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
            Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
          } else {
            // Clear it:
            TcvCodec.instance.setAccessCode(new byte[0]);
            reset();
          }
          if (!mbLoading) {
            return null;
          }
          break;
          // case zzEXEC:
        case NOP:
          ///// Considering the special case of AC == PW ...
          if ((null != entry) && (0 < entry.length())) {
            if (mbAccessCodeFirst) {
              boolean pwFromFile = !TcvCodec.instance.settleHexPhrase(null);
              if (TcvCodec.instance.setAccessCode(StringFunc.bytesUtf8(entry))) {
                if ((2 <= entry.length())
                    && ((null == TcvCodec.instance.getPassPhraseHex())
                        || (4 >= TcvCodec.instance.getPassPhraseHex().length()))) {
                  if (TcvCodec.instance.settleHexPhrase(null)) {
                    if (pwFromFile) {
                      mbLoading = true;
                    }
                  }
                }
              }
            } else {
              if (TcvCodec.instance.settleHexPhrase(StringFunc.hexUtf8(entry, false))) {
                mbLoading = true;
              }
            }
            mbAccessCodeFirst = !mbAccessCodeFirst;
            lastUpdateNanos = 0;
          } else if (Dib2Root.app.bAllowDummyPass) {
            TcvCodec.instance.setDummyPhrase(true);
            reset();
            Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
            return null;
          }
          // final byte[] temp = TcvCodec.instance.getPassFull();
          if (!mbLoading) {
            return null;
          }
          break;
        default:
          ;
      }
      if (mbLoading) {
        // Thus allowing network operations ...
        Dib2Root.app.bAllowDummyPass = false;
        if (null == mPathDataFile) {
          File dir = Dib2Root.platform.getFilesDir("main");
          if ((null == dir) || !(new File(dir, Dib2Root.app.dbFileName).exists())) {
            mPathDataFile = IoRunner.findLatest(Dib2Root.platform.getFilesDir("main"), false);
          } else {
            mPathDataFile = Dib2Root.app.dbFileName;
          }
        }
        if (null == mPathDataFile) {
          Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        } else {
          cmd.op = QOpIo.zzLOAD_INITIAL;
          cmd.argX = QWord.createQWord(mPathDataFile, true);
          QOpFeed.zStack.insert(0, cmd.pushWip4Seq(null));
          Dib2Root.schedulerTrigger.trigger(cmd);
        }
      }
      return null;
    }
  }

  // =====

  public static FeederRf findFeeder(String name) {
    name = name.toUpperCase(Locale.ROOT);
    FeederRf feeder;
    try {
      feeder = FeederRf.valueOf(name);
    } catch (Exception e) {
      feeder = null;
    }
    if (null == feeder) {
      for (FeederRf fx : FeederRf.values()) {
        for (String opt : fx.mOptionalNames) {
          if (opt.equals(name)) {
            feeder = fx;
            break;
          }
        }
      }
      if (null == feeder) {
        return null;
      }
    }
    return feeder;
  }

  // =====
}
