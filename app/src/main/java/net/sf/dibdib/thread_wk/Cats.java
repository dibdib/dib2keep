// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_wk;

import java.util.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.ShashFunc;

/** Pre-defined categories */
public enum Cats implements QIfs.QEnumIf {
  // =====

  ///// Singular
  PREF(1),
  VAR(2),
  REF(1 << 2),
  DEF(1 << 3),
  RULE(1 << 4),

  ///// Attribute
  HIDDEN(1L << 8),
  TRASH(1L << 9),
  DONE(1L << 10),

  /////
  OTHERS(1L << 15),
  NOTE(1L << 16),
  EVENT(1L << 17),
  CONTACT(1L << 18),
  CHAT(1L << 19),
  MSG(1L << 20),
  //
  _MAX(1L << 21),
//
;

  public final long flag;
  //    public final QWord uName;

  public static final Cats DEFAULT = NOTE;

  private static HashMap<Long, Cats> map = null;

  private Cats(long xFlag) {
    flag = xFlag;
    // uName = qval4AsciiShort(name());
  }

  @Override
  public long getShash() {
    return ordinal();
  }

  public static Cats valueOf4Q(QWordIf name) {
    if (null == map) {
      HashMap<Long, Cats> tmp = new HashMap<Long, Cats>();
      for (Cats cat : Cats.values()) {
        tmp.put(ShashFunc.shashBits4Ansi(cat.name()), cat);
      }
      map = tmp;
    }
    Cats out = map.get(name.getShash());
    if (null == out) {
      out = map.get(ShashFunc.shashBits4Ansi(name.toString().toUpperCase(Locale.ROOT)));
    }
    return out;
  }

  public static String cats4Flags(long flags) {
    StringBuilder out = new StringBuilder(30);
    int count = 0;
    for (Cats cat : Cats.values()) {
      if (((cat.flag & flags) != 0) && (OTHERS != cat)) {
        if (0 != count) {
          out.append(" ");
        }
        out.append(cat.name());
        ++count;
      }
    }
    if (0 == count) {
      out.append(OTHERS.name());
    } else if (count >= (Cats.values().length - 1)) {
      return "*";
    }
    return out.toString();
  }

  // public static QWordIf[] list4Flags(long flags) {
  //	QWordIf[] out = new QWordIf[32];
  //	int count = 0;
  //	for (Cats cat : Cats.values()) {
  //		if ((cat.flag & flags) != 0) {
  //			out[count++] = qval4AsciiShort(cat.name());
  //		}
  //	}
  //	if (0 == count) {
  //		out[count++] = qval4AsciiShort(OTHERS.name());
  //	}
  //	return Arrays.copyOf(out, count);
  // }

  public static long toFlags(QWordIf... cats) {
    long out = 0;
    if (null == cats) {
      return 0;
    }
    if (null == map) {
      // Populate map:
      valueOf4Q(QWord.V_NULL);
    }
    for (QWordIf nam : cats) {
      Cats cat = Cats.valueOf4Q(nam);
      if (null == cat) {
        out |= OTHERS.flag;
      } else {
        out |= cat.flag;
      }
    }
    return out;
  }

  public static long toFlags(String cats) {
    final String[] a0 = cats.split(" ");
    long out = 0;
    for (String cat : a0) {
      try {
        Cats v0 = Cats.valueOf(cat.toUpperCase(Locale.ROOT));
        out |= v0.flag;
      } catch (Exception e0) {
        if ("SRC".equals(cat)) {
          out |= CONTACT.flag;
        } else if ("GROUP".equals(cat)) {
          out |= CHAT.flag;
        }
      }
    }
    return (0 == out) && (0 < cats.trim().length()) ? OTHERS.flag : out;
  }

  //// =====
}
