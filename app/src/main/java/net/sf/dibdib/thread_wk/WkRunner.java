// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_wk;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.QEnumIf;
import net.sf.dibdib.thread_any.QOpMain;
import net.sf.dibdib.thread_feed.QOpFeed;

// =====

/** For worker thread as triggered by MainThreads. */
public enum WkRunner implements QIfs.QRunnableIf {

  // =====

  INSTANCE,
  ;

  // =====

  long mStartTimeNanobis = 0;
  private QToken mTask = null;
  private QToken mResult;
  private QIfs.QEnumIf mOperator;
  private QSeq[] maArgs;
  private QSeq[] zArgs;
  private int cArgItemsDone = 0;

  // =====

  @Override
  public boolean guard(QToken xmTask, long... xParameters) {
    final QIfs.QEnumIf op = xmTask.op;
    return (op instanceof QOpMain) || (op instanceof QOpWk);
  }

  private int maxArgPieces() {
    if (((QOpMain) mOperator).zipped) {
      int max = 0;
      for (int i0 = ((QOpMain) mOperator).cArgs - 1; i0 >= 0; --i0) {
        if (null == maArgs[i0]) {
          return 0;
        }
        final int size = maArgs[i0].size();
        max = (max >= size) ? max : size;
      }
      return max;
    }
    return 1;
  }

  @Override
  public int start(long xTick, QToken xmToken) {
    mStartTimeNanobis = xTick;
    mResult = null;
    if ((null == xmToken) || (null == xmToken.op)) {
      return 0;
    }
    Dib2Root.ccmSto.variableForceOrIndex("E", null);
    mTask = xmToken;
    mOperator = xmToken.op;
    maArgs = new QSeq[3];
    maArgs[0] = (QSeq) xmToken.argX;
    maArgs[1] = (QSeq) xmToken.argY;
    maArgs[2] = (QSeq) xmToken.argZ;

    if (!(mOperator instanceof QOpMain)) {
      return 1;
    }
    // TODO fast processing for simple values ...

    if ((xmToken.argX instanceof QSeq)
        && (QOpMain.INIT != mOperator)
        && (QOpMain.PW != mOperator)
        && (QOpMain.PWAC != mOperator)) {
      Dib2Root.ccmSto.variableForceOrIndex("L", (QSeq) xmToken.argX);
      Dib2Root.ccmSto.variableForceOrIndex("T", QSeq.createQSeq(xmToken.toString()));
    }
    zArgs = new QSeq[((QOpMain) mOperator).cArgs];
    cArgItemsDone = xmToken.parY;
    if (0 >= cArgItemsDone) {
      cArgItemsDone = 0;
      xmToken.wip = new QSeq[((QOpMain) mOperator).cReturnValues][];
      final int max = maxArgPieces();
      for (int i0 = 0; i0 < ((QSeq[][]) xmToken.wip).length; ++i0) {
        if (null == (((QSeq[][]) xmToken.wip)[i0] = new QSeq[max])) {
          xmToken.wip = null;
          ExceptionAdapter.MEMORY.reThrow();
        }
      }
    }
    return 1;
  }

  private QSeq[] extractArgs(int inx) {
    if (!((QOpMain) mOperator).zipped) {
      return maArgs;
    }
    for (int i0 = 0; i0 < zArgs.length; ++i0) {
      final int size = maArgs[i0].size();
      if (0 < size) {
        zArgs[i0] = (0 < size) ? maArgs[i0].atomAt(inx % size) : QWord.NaN;
      }
    }
    return zArgs;
  }

  @Override
  public int step(long dummy) {
    if (mOperator instanceof QOpWk) {
      mResult = null;
      final QEnumIf op = QOpWk.execOpWk(mTask);
      if (null != op) {
        mResult = mTask;
        mResult.op = op;
      }
    }
    mTask = null;
    return 0;
  }

  @Override
  public int stepAsync() {
    if ((null == mOperator) || (QOpMain.NOP == mOperator)) {
      mResult = null;
      maArgs = null;
      return 0;
    }
    if (!(mOperator instanceof QOpMain)) {
      return 1;
    }
    if ((null == maArgs) || (0 > cArgItemsDone)) {
      return 0;
    }
    // Results still needed?
    if ((null == QToken.zWip.get(mTask.stamp) && (0 < ((QOpMain) mOperator).cReturnValues))) {
      return 0;
    }
    Object done = null;
    QEnumIf nextOp = QOpFeed.zzGET;
    if (!((QOpMain) mOperator).zipped
        || (0 >= ((QOpMain) mOperator).cReturnValues)
        || (0 >= ((QOpMain) mOperator).cArgs)) {
      done = ((QOpMain) mOperator).calc(extractArgs(0));
      if (null == done) {
        done = QOpWk.execOpMain((QOpMain) mOperator, maArgs);
        if (null == done) {
          return 0;
        } else if (((QSeq) done).at(0) instanceof QIfs.QEnumIf) {
          nextOp = (QIfs.QEnumIf) ((QSeq) done).at(0);
        }
      }
    } else {
      final QSeq[][] im = (QSeq[][]) mTask.wip;
      final int cTodo =
          (mOperator.ordinal() >= QOpMain.zzSLOW.ordinal()) ? 2 : Dib2Root.app.minLargeSeq;
      int i1 = cArgItemsDone + cTodo;
      for (i1 = (i1 <= im[0].length) ? i1 : im[0].length; cArgItemsDone < i1; ++cArgItemsDone) {
        final Object result = ((QOpMain) mOperator).calc(extractArgs(cArgItemsDone));
        for (int ix = 0; ix < im.length; ++ix) {
          im[ix][cArgItemsDone] =
              (result instanceof QSeq[])
                  ? ((QSeq[]) result)[ix]
                  : ((null == result) ? QWord.NaN : (QSeq) result);
        }
      }
      done = im;
      if (cArgItemsDone < im[0].length) {
        mTask.parY = cArgItemsDone;
        nextOp = mTask.op;
      }
    }
    if (null != done) {
      mTask.wip = done;
      cArgItemsDone = -1;
      mResult = mTask;
      mResult.op = nextOp;
      mTask = null;
      maArgs = null;
      return 0;
    }
    return 1;
  }

  @Override
  public QToken call() {
    maArgs = null;
    final QToken out = mResult;
    mTask = null;
    mResult = null;
    return out;
  }

  @Override
  public void removeWipData4Interrupts() {}

  // =====
}
