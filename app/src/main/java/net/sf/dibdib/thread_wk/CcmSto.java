// Copyright (C) 2016, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_wk;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;

/**
 * Concept mapping, saved as CSV with tabs (TSV, commas if tabs missing) as single line (trailing
 * '\n' as '\t').
 */
public final class CcmSto {
  // =====

  /** 'My' ID for exporting, internally using 0 as id to refer to myself! */
  public long mUserPid;
  /** Main ID: e-mail address etc. */
  public String mUserAddr;

  QMMap zMappingsPid;
  QMMap zMappingsLabel;

  ///// TODO Use QIfs ..., make variables local for each stack

  /** To be saved as hex literals! (with or without leading "X'") */
  public static ConcurrentHashMap<String, byte[]> qHidden = null;

  /** X/Y/Z ==> stack, T=Term, Q=Memory, L=Last, G=Graphics, E=Error */
  private ConcurrentHashMap<String, String> zVariables = null;

  /////

  public static enum CcmTag implements QIfs.QTagIf {
    // ID (stored as 'stamp')
    LABEL, // NAME
    CATS,
    DAT, // DAT is last for CSV line, = DAT_* or DAT_0
    TIME, // TIME_STAMP,
    TAGSREFS,
    CNTRB, // 0 for myself, 1 for N.N.
    SRCS,
    RECV,
    TRASHED,
    DAT_X, // if available: DAT_1, DAT_2 etc.
    ;

    private long shash = 0;

    @Override
    public long getShash() {
      if (0 == shash) {
        shash = ShashFunc.shashBits4Ansi(name());
      }
      return shash;
    }

    public long getAsKey(QSTuple xTuple) {
      return xTuple.getAsKey(this);
    }

    public QItemIf getValue(QSTuple xTuple) {
      return xTuple.getValue(ordinal());
    }

    public QWord getWord(QSTuple xTuple) {
      QItemIf out = xTuple.getValue(ordinal());
      return (out instanceof QWord) ? (QWord) out : QWord.V_0;
    }

    @Override
    public String formatValue(QItemIf xValue) {
      switch (this) {
        case CATS:
          {
            if (xValue instanceof QWord) {
              final long flags = ((QWord) xValue).i64();
              return Cats.cats4Flags(flags);
            }
          }
        case CNTRB:
          {
            if (xValue instanceof QWord) {
              final long stamp = ((QWord) xValue).i64();
              return "0#" + BigSxg.sxgChecked64(stamp);
            }
          }
        case TAGSREFS:
          {
            if ((xValue instanceof QSeq) && (0 < ((QSeq) xValue).size())) {
              StringBuilder out = new StringBuilder(10 * ((QSeq) xValue).size());
              boolean filler = false;
              for (QWordIf wd : ((QSeq) xValue).atoms()) {
                if (filler) {
                  out.append(' ');
                } else {
                  filler = true;
                }
                if ((wd instanceof QWord) && ((QWord) wd).isDate()) {
                  final long stamp = ((QWord) wd).i64();
                  out.append("0#" + BigSxg.sxgChecked64(stamp));
                } else {
                  out.append(wd.toString());
                }
              }
              return out.toString();
            }
          }
        default:
          ;
      }
      return xValue.toString();
    }

    private static String tsvFieldNames_z = null;

    public static String tsvFieldNames() {
      if (null == tsvFieldNames_z) {
        StringBuilder fieldNames = new StringBuilder(100);
        for (int i0 = 0; i0 < DAT_X.ordinal(); ++i0) {
          CcmTag field = CcmTag.values()[i0];
          if (field == DAT) {
            continue;
          }
          fieldNames.append('\t');
          fieldNames.append(field);
        }
        fieldNames.append("\tDAT\tDAT1\tDAT2\tDAT3");
        tsvFieldNames_z = fieldNames.toString();
      }
      return tsvFieldNames_z;
    }
  }

  /////

  public CcmSto() {
    mUserPid = DateFunc.createId();
    mUserAddr = ".";
    qHidden = new ConcurrentHashMap<String, byte[]>();
    zVariables = new ConcurrentHashMap<String, String>();
    // Identity: PID connected to e-mail address or phone number etc.:
    // variableForceOrIndex(".", QWord.createQWord("" + BigSxg.sxgChecked64(mUserPid) + "@?",
    // true));
    qHidden.put("pub", "".getBytes(StringFunc.CHAR8));
    qHidden.put("sec", "".getBytes(StringFunc.CHAR8));
    zMappingsLabel = QMMap.create(CcmTag.LABEL);
    zMappingsPid = QMMap.create(null);
  }

  public static CcmTag mapTag(String xTag) {
    try {
      return CcmTag.valueOf(xTag);
    } catch (Exception e0) {
      // NOP
    }
    if (xTag.startsWith("NAM")) {
      return CcmTag.LABEL;
    } else if (xTag.startsWith("TIM")) {
      return CcmTag.TIME;
    } else if (xTag.startsWith("DAT")) {
      return CcmTag.DAT;
    } else if (xTag.startsWith("CON")) {
      return CcmTag.CNTRB;
    } else if (xTag.startsWith("SOU")) {
      return CcmTag.SRCS;
    } else if (xTag.startsWith("TAGS")) {
      return CcmTag.TAGSREFS;
    }
    return CcmTag.DAT_X;
  }

  public static QSeqIf mapItem(String xItemText, CcmTag xTag) {
    // ID:
    if ((null == xTag) || (CcmTag.CNTRB == xTag)) {
      return QWord.createQWordInt(
          xItemText.startsWith("0#")
              ? BigSxg.bits4SxgChecked64(xItemText.substring(2), false)
              : ((0 >= xItemText.length()) ? 0 : BigSxg.long4String(xItemText, 1)));
    }
    // Others:
    switch (xTag) {
      case LABEL:
        return QWord.createQWord(xItemText, false);
      case CATS:
        return QWord.createQWordInt(Cats.toFlags(xItemText));
      case DAT:
      case TRASHED:
        return QSeq.createQSeq(xItemText);
      case TIME:
        return (2 < xItemText.length()) ? QWord.createQWordDate(xItemText) : QWord.V_0;
      case TAGSREFS:
      case SRCS:
      case RECV:
        return QSeq.createQSeq(xItemText);
      default:
        ;
    }
    ExceptionAdapter.throwAdapted(new IndexOutOfBoundsException(), CcmSto.class, "" + xTag);
    return null;
  }

  static final CcmTag[] createTagMap_MAP_0 = new CcmTag[] {CcmTag.LABEL, CcmTag.CATS, CcmTag.DAT};
  static final CcmTag[] createTagMap_MAP_MAIL =
      new CcmTag[] {
        CcmTag.DAT_X,
        CcmTag.LABEL,
        CcmTag.CATS,
        CcmTag.TIME,
        CcmTag.CNTRB,
        CcmTag.SRCS,
        CcmTag.RECV,
        CcmTag.DAT
      };
  static CcmTag[] createTagMap_FULL = null;

  /** CcmTag.DAT_X == map[0] if first field has PID. */
  public static CcmTag[] createTagMap(String[] tags) {
    if ((null == tags) || (3 > tags.length)) {
      return (null == tags) ? createTagMap_MAP_MAIL : createTagMap_MAP_0;
    }
    CcmTag[] map = new CcmSto.CcmTag[tags.length];
    long bSet = 0;
    for (int ix = 0; ix < tags.length; ++ix) {
      map[ix] = CcmSto.mapTag(tags[ix]);
      if (0 != (bSet & (1L << map[ix].ordinal()))) {
        map[ix] = CcmTag.DAT_X;
      }
      bSet |= 1L << map[ix].ordinal();
    }
    return map;
  }

  public static CcmTag[] getTagMapStd(int len) {
    switch (len) {
      case 3:
        return createTagMap_MAP_0;
      case 7:
        return createTagMap_MAP_MAIL;
      default:
        ;
    }
    if (null == createTagMap_FULL) {
      CcmTag[] out = new CcmTag[CcmTag.values().length];
      System.arraycopy(CcmTag.values(), 0, out, 1, out.length - 1);
      // Atomic:
      createTagMap_FULL = out;
    }
    return ((0 >= len) || (createTagMap_FULL.length == len)) ? createTagMap_FULL : null;
  }

  public static QSTuple createTuple4Tsv(String line, CcmTag[] map) {

    // First element as PID?
    boolean bId = CcmTag.DAT_X == map[0];
    if ((line.indexOf("\t") < 0) && (line.indexOf(",") > 0)) {
      line = line.replaceAll("\"? *, *\"?", "\t");
    }
    String[] a0 = line.split("\t"); // , 6 + iOid );
    QSTuple tup = new QSTuple(CcmTag.values(), CcmTag.DAT_X.ordinal());
    tup.jEnd = tup.items.length;
    // tup.items = new QItemIf[CcmTag.DAT_X.ordinal()];
    if (2 >= a0.length) {
      if ((0 >= line.trim().length()) || (0 >= a0.length)) {
        return null;
      }
      tup.items[createTagMap_MAP_0[0].ordinal()] =
          CcmSto.mapItem(
              ((1 < a0.length) || (30 > a0[0].length())) ? a0[0] : a0[0].substring(0, 15),
              createTagMap_MAP_0[0]);
      tup.items[createTagMap_MAP_0[2].ordinal()] =
          CcmSto.mapItem((1 < a0.length) ? a0[1] : a0[0], createTagMap_MAP_0[2]);
      tup.stamp = DateFunc.currentTimeNanobisLinearized(true);
      return tup;
    }
    final long stampMax = DateFunc.createId();
    StringBuilder dat = new StringBuilder(line.length());
    // int iTime = -2;
    tup.stamp = 0;
    if (bId && (5 <= a0[0].trim().length())) {
      try {
        String id = a0[0].trim();
        String idx = null;
        char ch0 = id.charAt(0);
        if (('0' == ch0) && id.matches("0#[0-9A-NP-Za-y]+")) {
          id = id.substring(2);
          tup.stamp = BigSxg.bits4SxgChecked64(id, true);
          idx = BigSxg.sxgChecked64(tup.stamp);
        } else if (('0' == ch0) && id.matches("0z[0-9A-NP-Za-y]+")) {
          id = id.substring(2);
          tup.stamp = BigSxg.bits4sxg(id);
          idx = BigSxg.sxg4Long(tup.stamp).substring(2);
        } else if (('Z' == ch0) && id.matches("Z'[0-9A-NP-Za-y]+'")) {
          id = id.substring(2, id.length() - 1);
          tup.stamp = BigSxg.bits4SxgChecked64(id, true);
          idx = BigSxg.sxgChecked64(tup.stamp);
        } else if (id.matches("[0-9A-NP-Za-y]+")) {
          tup.stamp = BigSxg.bits4sxg(id);
          idx = BigSxg.sxg4Long(tup.stamp).substring(2);
        }
        if (!id.equals(idx)
            || (0 != (tup.stamp & 1L))
            || (tup.stamp > stampMax)
            || (99888 > tup.stamp)) {
          tup.stamp = 0;
        }
      } catch (Exception e0) {
        tup.stamp = 0;
      }
      if (0 == tup.stamp) {
        tup.items[CcmTag.TRASHED.ordinal()] = QSeq.createQSeq(":ID:" + a0[0]);
      }
    }
    if (0 == tup.stamp) {
      tup.stamp = DateFunc.currentTimeNanobisLinearized(true);
    }
    boolean found = false;
    for (int ix = bId ? 1 : 0; ix < a0.length; ++ix) {
      if ((ix >= map.length) || (CcmTag.DAT_X == map[ix])) {
        dat.append('\n');
        dat.append(a0[ix]);
      } else if (CcmTag.DAT == map[ix]) {
        found = true;
        if (dat.length() > 0) {
          dat = new StringBuilder(a0[ix] + dat.toString());
        } else {
          dat.append(a0[ix]);
        }
      } else {
        tup.items[map[ix].ordinal()] = CcmSto.mapItem(a0[ix], map[ix]);
      }
    }
    if (0 >= dat.length()) {
      if (!found) {
        dat.append(a0[a0.length - 1]);
      } else {
        dat.append("(" + a0[0] + ")");
      }
    }
    if (null == tup.items[CcmTag.LABEL.ordinal()]) {
      tup.items[CcmTag.LABEL.ordinal()] =
          QWord.createQWord(DateFunc.currentTimeNanobisLinearized(true));
    }
    if (0 < dat.length()) {
      tup.items[CcmTag.DAT.ordinal()] = QSeq.createQSeq(dat.toString());
      return tup;
    }
    return null;
  }

  private static QWord getQWord(QSTuple xTup, CcmTag xTag, QWord... optFallBack) {
    final QSeqIf val = xTup.getValue(xTag);
    return (val instanceof QWord) ? (QWord) val : optFallBack[0];
  }

  private static QSeq getQSeq(QSTuple xTup, CcmTag xTag, QSeq... optFallBack) {
    final QSeqIf val = xTup.getValue(xTag);
    return (val instanceof QSeq) ? (QSeq) val : optFallBack[0];
  }

  public static QSTuple createEntry(QWordIf xmLabel, long xCats, QSeqIf... xmDat) {
    final int size = CcmTag.DAT_X.ordinal() - 1 + ((0 >= xmDat.length) ? 1 : xmDat.length);
    final QSTuple out = new QSTuple(CcmTag.values(), size);
    out.stamp = DateFunc.createId();
    out.jEnd = CcmTag.DAT_X.ordinal();
    out.set(CcmTag.LABEL, xmLabel);
    out.set(CcmTag.CATS, QWord.createQWordInt(xCats));
    out.set(CcmTag.DAT, (0 < xmDat.length) ? xmDat[0] : QSeq.NIL);
    if (1 < xmDat.length) {
      System.arraycopy(xmDat, 1, out.items, CcmTag.DAT_X.ordinal(), xmDat.length - 1);
      out.jEnd += xmDat.length - 1;
    }
    return out;
  }

  ///// TODO ==> Dib2Root values

  public byte[] hidden_get(String key) {
    key = key.replace('\t', ' ').replace('\n', ' ');
    if (".".equals(key)) {
      return null;
    }
    byte[] out = qHidden.get(key);
    return (out == null) ? null : out.clone();
  }

  public String hidden_getHex(String key, boolean marked) {
    byte[] out = hidden_get(key);
    return (null == out) ? "" : StringFunc.hex4Bytes(out, marked);
  }

  public synchronized void hidden_set(String key, byte[] value) { // , QWord oid4ForcingInitial) {
    key = key.replace('\t', ' ').replace('\n', ' ').trim();
    if (null == value) {
      qHidden.remove(key);
      return;
    } else if (0 >= key.length()) {
      return;
    }
    // if (null != oid4ForcingInitial) {
    if (".".equals(key)) {
      // Should not be 'hidden' ...
      if (1 >= value.length) {
        return;
      }
      byte[] trash = qHidden.get("trash");
      trash =
          (null == trash)
              ? value
              : StringFunc.bytesUtf8(
                  StringFunc.string4Utf8(trash) + '\t' + StringFunc.string4Utf8(value));
      qHidden.put("trash", trash);
      return;
    } else if ("email_address".equals(key)) {
      // For new root ID or keep ID for new address!
      if (1 >= mUserAddr.length()) {
        final String s0 = StringFunc.string4Utf8(value);
        if (0 < s0.indexOf('@')) {
          mUserAddr = s0;
        }
      }
    } else if ("lastId".equals(key)) {
      final long id = BigSxg.long4String(new String(value, StringFunc.CHAR8), 0);
      if (0 < id) {
        DateFunc.alignId(id);
      }
      // return;
    }
    qHidden.put(key, value.clone()); // MiscFunc.toHexLiteral( value, true ) );
  }

  public synchronized void hidden_setHex(
      String key, String hexLiteral) { // , QWord oid4ForcingInitial) {
    byte[] val = null;
    if (null == hexLiteral) {
    } else if (0 >= hexLiteral.length()) {
      val = new byte[0];
    } else {
      char ch = hexLiteral.charAt(0);
      if (('X' == ch) && hexLiteral.startsWith("X'")) {
        val = StringFunc.bytes4Hex(hexLiteral);
      } else if (((('0' <= ch) && (ch <= '9')) || (('A' <= ch) && (ch <= 'F')))
          && hexLiteral.matches("[0-9A-F ]+")) {
        // Bad luck if it is meant to be a decimal number.
        val = StringFunc.bytes4Hex(hexLiteral);
      } else {
        // Not hex after all.
        val = StringFunc.bytesUtf8(hexLiteral);
      }
    }
    hidden_set(key, val);
  }

  public synchronized void hidden_remove(String key) {
    Dib2Root.log("preference_remove", key);
    qHidden.remove(key);
  }

  /////

  public QSeq variable_get(String name) {
    final QSeq out = QValPool.qval4String(zVariables.get(name));
    return (null == out) ? QSeq.NIL : out;
  }

  public int peekVariables(String[] yLines, int offs) {
    if (null == yLines) {
      return zVariables.size() + offs;
    }
    if ((yLines.length - offs) >= zVariables.size()) {
      TreeSet<String> sorted = new TreeSet<String>();
      for (String var : zVariables.keySet()) {
        // Atomic:
        final String val = zVariables.get(var);
        if (null != val) {
          sorted.add(var + '\t' + val);
        }
      }
      for (String line : sorted) {
        if (offs >= yLines.length) {
          // Race condition, ignore.
          break;
        }
        yLines[offs++] = line;
      }
    } else {
      char iMemory = '0';
      for (; (iMemory <= 'z') && (offs < yLines.length); ++iMemory) {
        // Atomic:
        final String val = zVariables.get("M" + iMemory);
        if (null != val) {
          yLines[offs++] = "M" + iMemory + '\t' + val;
        }
      }
    }
    return offs;
  }

  /**
   * Set variable. Name is expected to have more than 1 char for user variables.
   *
   * @param name Name of variable.
   * @param value null iff variable is to be removed.
   */
  public synchronized void variable_set(String name, QSeq value) {
    if (0 >= name.length()) {
      return;
    }
    name = StringFunc.nameNormalize(name, 0xff);
    ///// Reserved?
    if (1 >= name.length() && (('A' > name.charAt(0)) || ('X' <= name.charAt(0)))) {
      // e.g. '.' for ROOT
      return;
    } else if (('X' <= name.charAt(0))
        && ('Z' >= name.charAt(0))
        && (name.substring(1).matches("[0-9]+"))) {
      return;
    }
    if ((null == value) || (QWord.V_NULL == value)) {
      zVariables.remove(name);
    } else {
      zVariables.put(
          name, // QValMapSto.string4QVal(
          value.toStringFull());
    }
  }

  public synchronized void variable_remove(String name) {
    name = StringFunc.nameNormalize(name, 0xff);
    zVariables.remove(name);
  }

  public synchronized int variableForceOrIndex(String name, QSeq value) {
    //    final JResult pooled = JResult.get8Pool();
    name = StringFunc.nameNormalize(name, 0xff);
    int iTo = -1;
    if (1 >= name.length()) {
      if (0 >= name.length()) {
        return -1;
      } else if (('X' <= name.charAt(0)) && ('Z' >= name.charAt(0))) {
        iTo = name.charAt(0) - 'X';
      } else if (name.equals(".")) {
        mUserPid = value.atom().qValLong;
        return -1;
      }
    } else if (name.substring(1).matches("[0-9]+")) {
      if (('X' <= name.charAt(0)) && ('Y' >= name.charAt(0))) {
        return -1;
      } else if (('Z' == name.charAt(0))) {
        iTo = Integer.parseInt(name.substring(1)) + 2;
      }
    }
    if (0 > iTo) {
      if ((null == value) || (QWord.V_NULL == value)) {
        zVariables.remove(name);
      } else {
        zVariables.put(name, value.toStringFull());
      }
    }
    // UiPres.INSTANCE.setStackValueForced(iTo, value);
    return iTo;
  }

  public synchronized void clearXVar(boolean all) {
    variableForceOrIndex("C", QWord.V_NULL);
    variableForceOrIndex("E", QWord.V_NULL);
    variableForceOrIndex("L", QWord.V_NULL);
    variableForceOrIndex("T", QWord.V_NULL);
    if (all) {
      zVariables = new ConcurrentHashMap<String, String>();
    }
  }

  public static String toString4VariableList(QSTuple tup, char separator) {
    if (tup.jStart >= tup.jEnd) {
      return "";
    }
    final StringBuilder out = new StringBuilder((tup.jEnd - tup.jStart) * 10 + 10);
    final long flags = ((QWord) tup.items[CcmTag.CATS.ordinal()]).i64();
    if (Cats.REF.flag != flags) {
      return tup.toStringFull(separator, CcmTag.DAT.ordinal());
    }
    int iVarName = -2;
    for (int i0 = tup.jEnd - 1;
        i0 >= CcmTag.DAT.ordinal(); // ,
        i0 = (CcmTag.DAT_X.ordinal() == i0) ? CcmTag.DAT.ordinal() : (i0 - 1)) {
      if (null == tup.items[i0]) {
        continue;
      }
      if (-2 < iVarName) {
        out.append("\n0#");
        out.append(BigSxg.sxgChecked64(DateFunc.currentTimeNanobisLinearized(true)));
        out.append(separator);
      }
      out.append(
          (-2 == iVarName)
              ? "X"
              : ((-1 == iVarName) ? "Y" : ((0 == iVarName) ? "Z" : ("Z" + iVarName))));
      out.append(separator);
      out.append(Cats.VAR.name());
      for (int ix = 1 + CcmTag.CATS.ordinal(); ix < CcmTag.DAT_X.ordinal(); ++ix) {
        out.append(separator);
      }
      String dat = tup.items[i0].toString();
      if (0 != separator) {
        dat = StringFunc.makePrintable(dat);
      }
      out.append(dat);
      ++iVarName;
    }
    return out.toString();
  }

  public static QMMap peekMappings() {
    return Dib2Root.ccmSto.zMappingsLabel;
  }

  public static QItemIf peek(long xPid, boolean forRef) {
    if (0 == xPid) {
      return null;
    }
    QItemIf out = Dib2Root.ccmSto.zMappingsPid.search(xPid);
    if (null == out) {
      out = Dib2Root.ccmSto.zMappingsPid.search(xPid ^ 1);
      if ((null == out) && forRef) {
        out = Dib2Root.ccmSto.zMappingsPid.search(xPid | (1L << 63));
        if (null == out) {
          out = Dib2Root.ccmSto.zMappingsPid.search((xPid ^ 1) | (1L << 63));
        }
      }
    }
    return out;
  }

  private static final QIfs.QItemIf[] searchBunch_empty = new QIfs.QItemIf[0];

  public static QIfs.QItemIf[] searchBunch(QSeq xPidOrLabel, long xCats) {
    QIfs.QItemIf[] items = null;
    if ((xPidOrLabel instanceof QWord) && (0 != ((QWord) xPidOrLabel).i64())) {
      final long pid = ((QWord) xPidOrLabel).i64();
      QIfs.QItemIf item = peek(pid, false);
      if (null != item) {
        items = new QIfs.QItemIf[] {item};
      }
    }
    if (null == items) {
      items = Dib2Root.ccmSto.zMappingsLabel.searchBunch(xPidOrLabel.shash);
      if ((null == items) || (0 == items.length)) {
        return searchBunch_empty;
      }
      final String label = xPidOrLabel.toStringFull();
      for (int i0 = 0; i0 < items.length; ++i0) {
        if (!(items[i0] instanceof QSTuple)) {
          items[i0] = null;
        } else if (xCats != ((CcmTag.CATS.getWord((QSTuple) items[i0]).i64() & xCats))) {
          items[i0] = null;
        } else if (!label.equals(CcmTag.LABEL.getWord((QSTuple) items[i0]).toStringFull())) {
          items[i0] = null;
        }
      }
    }
    int i1 = items.length - 1;
    for (int i0 = 0; i0 < i1; ++i0) {
      for (; (i0 < i1) && (null != items[i0]); ++i0)
        ;
      for (; (i0 < i1) && (null == items[i1]); --i1)
        ;
      items[i0] = items[i1];
      items[i1] = null;
    }
    return Arrays.copyOf(items, i1 + ((null == items[i1]) ? 0 : 1));
  }

  /** Trying to find matching unique data, preferring CTRB 0 over others. */
  // @return 0 if nothing found or not unique.
  public static long peek(QSeq xPidOrLabel, long xCats, boolean xUnique) {
    QIfs.QItemIf[] items = searchBunch(xPidOrLabel, xCats);
    QSTuple found = null;
    boolean unique = true;
    for (QIfs.QItemIf item : items) {
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = (QSTuple) item;
      if (null == found) {
        found = mpg;
        continue;
      }
      if (CcmTag.CNTRB.getWord(found).i64() == 0) {
        if (CcmTag.CNTRB.getWord(mpg).i64() == 0) {
          return 0;
        }
      } else if (CcmTag.CNTRB.getWord(mpg).i64() == 0) {
        found = mpg;
        unique = true;
      } else {
        unique = false;
      }
    }
    return ((null == found) || (!unique && xUnique)) ? 0 : found.getShash();
  }

  public static String getTaggedValueOr(String tag, String xDat, String xDelim, String xyOr) {
    tag = tag.startsWith(":") ? tag : (":" + tag + ':');
    if ((null == xDat) || !xDat.contains(tag)) {
      return xyOr;
    }
    int i1 = xDat.indexOf(tag) + tag.length();
    for (; (i1 < xDat.length()) && (' ' == xDat.charAt(i1)); ++i1) {}
    int i0 = i1;
    for (; (i1 < xDat.length()) && (0 > xDelim.indexOf(xDat.charAt(i1))); ++i1) {}
    return xDat.substring(i0, i1).trim();
  }

  public static String getFirstEmail(String xDat) {
    int ix = xDat.indexOf('@');
    if (0 >= ix) {
      return null;
    }
    int iTag = xDat.indexOf(":EMAIL:");
    if (ix < iTag) {
      ix = xDat.indexOf('@', iTag);
    }
    int ia = ix - 1;
    for (; ia >= 0; --ia) {
      final char ch = xDat.charAt(ia);
      if (('+' > ch) || (('9' < ch) && (ch < 'A')) || (('z' < ch) && (ch < 0x80))) {
        break;
      }
    }
    for (++ix; ix < xDat.length(); ++ix) {
      final char ch = xDat.charAt(ix);
      if (('+' > ch) || (('9' < ch) && (ch < 'A')) || (('z' < ch) && (ch < 0x80))) {
        break;
      }
    }
    final String addr = xDat.substring(ia + 1, ix);
    return (5 > addr.length()) ? null : addr;
  }

  public static ConcurrentHashMap<String, Long> findEMail4Cats(long xCatFlags) {
    ConcurrentHashMap<String, Long> out = new ConcurrentHashMap<String, Long>(500);
    long pid = -1L;
    do {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.searchNext(pid, null, 0);
      if (null == item) {
        break;
      }
      pid = item.getShash();
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = ((QSTuple) item);
      long cats = CcmTag.CATS.getWord(mpg).i64();
      if (xCatFlags != (xCatFlags & cats)) {
        continue;
      }
      String dat = ((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull();
      if (0 <= dat.indexOf(":GROUP:")) {
        continue;
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      String email = getFirstEmail(dat);
      String opt = null;
      if (0 < label.indexOf('@')) {
        opt = email;
        email = getFirstEmail(label);
      } else if (null != mpg.getValue(CcmTag.SRCS)) {
        String srcs = ((QSeq) mpg.getValue(CcmTag.SRCS)).toStringFull();
        opt = getFirstEmail(srcs);
      }
      if ((null == email) || ((null != opt) && !email.equals(opt))) {
        if ((null == email) || (0 == (Cats.CONTACT.flag & cats))) {
          email = opt;
        }
      }
      if (null != email) {
        if (null != out.get(email)) {
          QSTuple cmp = (QSTuple) Dib2Root.ccmSto.zMappingsPid.search(out.get(email));
          if (0 < CcmTag.LABEL.getWord(cmp).toStringFull().indexOf('@')) {
            continue;
          }
        }
        out.put(email, mpg.stamp);
      }
    } while (pid != -1L);
    return out;
  }

  public static HashSet<Long> findMsgs4Chat(long xhChat) {
    QItemIf chat = Dib2Root.ccmSto.zMappingsPid.search(xhChat);
    if (null == chat) {
      return null;
    }
    String chatName = CcmTag.LABEL.getWord((QSTuple) chat).toStringFull();
    HashSet<Long> out = new HashSet<Long>(500);
    long pid = -1L;
    do {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.searchNext(pid, null, 0);
      if (null == item) {
        break;
      }
      pid = item.getShash();
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = ((QSTuple) item);
      long cats = CcmTag.CATS.getWord(mpg).i64();
      if (0 == (Cats.MSG.flag & cats)) {
        continue;
      }
      QItemIf ref = CcmTag.TAGSREFS.getValue(mpg);
      if ((null != ref)
          && (null != ((QSeq) ref).atomAt(0))
          && ((((QSeq) ref).atomAt(0).i64() & ((1L << 63) - 1)) == (xhChat & ((1L << 63) - 1)))) {
        out.add(mpg.stamp);
        continue;
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      if (label.startsWith(chatName)) {
        final int ic = label.indexOf(':');
        String s0 = label;
        if (0 < ic) {
          s0 = label.substring(0, ic);
        }
        if (chatName.equals(s0)) {
          out.add(mpg.stamp);
        }
      }
    } while (pid != -1L);
    return out;
  }

  public static long ensureContact4Chat(long xPidChat) {
    QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.search(xPidChat);
    if (!(item instanceof QSTuple)) {
      return 0;
    }
    final QSTuple chat = ((QSTuple) item);
    long cats = CcmTag.CATS.getWord(chat).i64();
    if (0 == (Cats.CHAT.flag & cats)) {
      return 0;
    }
    String label = CcmTag.LABEL.getValue(chat).toString();
    String dat = ((QSeq) CcmTag.DAT.getValue(chat)).toStringFull();
    if (0 > label.indexOf('@')) {
      return dat.contains(":GROUP:") ? xPidChat : 0;
    }
    ConcurrentHashMap<String, Long> emails = findEMail4Cats(Cats.CONTACT.flag);
    QSTuple contact = null;
    long pid = 0;
    if (null == emails.get(label)) {
      item = CcmTag.TAGSREFS.getValue(chat);
      if (item instanceof QSeq) {
        pid = ((QSeq) item).atomAt(0).i64();
      } else {
        item = CcmTag.SRCS.getValue(chat);
        if (item instanceof QSeq) {
          String email = ((QSeq) item).toStringFull();
          email = getFirstEmail(email);
          if ((null != email) && !label.equals(email) && (0 < email.indexOf('@'))) {
            return 0;
          }
        }
      }
    } else {
      pid = emails.get(label);
    }
    if (0 != pid) {
      item = Dib2Root.ccmSto.zMappingsPid.search(pid);
      if (item instanceof QSTuple) {
        contact = (QSTuple) item;
        String dx = ((QSeq) CcmTag.DAT.getValue(contact)).toStringFull();
        if (dx.contains(":EMAIL:") && !label.equals(getFirstEmail(dx))) {
          return 0;
        } else if (!dx.contains(":EMAIL:")) {
          dx = ":EMAIL:" + label + '\n' + dx;
          contact.set(CcmTag.DAT, QSeq.createQSeq(dx));
        }
      }
    }
    long out = xPidChat;
    if (null == contact) {
      contact =
          CcmSto.createEntry(
              QWord.createQWord(label.substring(0, label.indexOf('@')), true),
              Cats.CONTACT.flag,
              QSeq.createQSeq(":EMAIL: " + label + "\n" + dat));
      Dib2Root.ccmSto.zMappingsPid.add4Handle(contact);
      Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(contact, CcmTag.LABEL).getShash(), contact);
      out = contact.stamp;
    }
    if (dat.contains(":FP:")) {
      String fp = dat.substring(dat.indexOf(":FP:") + 4);
      if (0 < fp.indexOf('\n')) {
        fp = fp.substring(0, fp.indexOf('\n'));
      }
      fp = fp.trim();
      item = CcmTag.DAT.getValue(contact);
      if (item instanceof QSeq) {
        String dx = ((QSeq) item).toStringFull();
        if (!dx.contains(fp) && (8 <= fp.length())) {
          out = contact.stamp;
          dx = dx.replaceAll("\n\\:FP\\:[^\n]*", "");
          dx += (dx.endsWith("\n") ? "" : "\n") + ":FP: " + fp;
          contact.set(CcmTag.DAT, QSeq.createQSeq(dx));
        }
      }
    }
    return out;
  }

  private static boolean importPrefsNVars_pidFound = false;

  private static QSeq[] importPrefsNVars(
      boolean replace,
      long flagsMarkAdjusttimeKeyHex,
      QSTuple mpg,
      long cats,
      long[] optIds,
      QSeq[] stack,
      StringBuilder trash) {
    final String key = getQSeq(mpg, CcmTag.LABEL).toStringFull();
    final QSeq value = getQSeq(mpg, CcmTag.DAT);
    String valueS = value.toStringFull();
    if (0 >= key.length() || (0 >= valueS.length())) {
      if (0 < key.length()) {
        trash.append('\t');
        trash.append(key);
      }
      if (0 < valueS.length()) {
        trash.append('\t');
        trash.append(valueS);
      }
      return stack;
    }
    final boolean oldFormatPref =
        (0 == (Cats.HIDDEN.flag & cats)) && (0 != (Cats.PREF.flag & cats));
    ///// Do not override current entries when importing older data.
    if (((0 != (Cats.HIDDEN.flag & cats)) || oldFormatPref) && (1 < key.length())) {
      ///// 'value' as hexstring!
      if (replace
          || !qHidden.containsKey(key)
          || (null == qHidden.get(key))
          || (0 >= qHidden.get(key).length)) {
        if (0 != (flagsMarkAdjusttimeKeyHex & 4)
            && key.startsWith("KEY")
            && (valueS.length() > 3 * "CAAU".length())
            && valueS.matches("3.3.3.3.*")) {
          Dib2Root.ccmSto.hidden_setHex(key, StringFunc.string4HexUtf8(valueS)); // , null);
        } else {
          Dib2Root.ccmSto.hidden_setHex(
              key, valueS); // , replace ? QWord.createQWordInt(mpg.stamp) : null);
        }
      }
    } else {
      if (replace
          || !Dib2Root.ccmSto.zVariables.containsKey(key)
          || (null == Dib2Root.ccmSto.zVariables.get(key))
          || (0 >= Dib2Root.ccmSto.zVariables.get(key).toString().length())) {
        if (key.equals(".")) {
          if (replace && !importPrefsNVars_pidFound) {
            importPrefsNVars_pidFound = true;
            DateFunc.alignTime(-1, mpg.stamp);
            Dib2Root.ccmSto.mUserPid = DateFunc.alignTime(mpg.stamp, 1);
            if ((1L << 32) > Dib2Root.ccmSto.mUserPid) {
              Dib2Root.ccmSto.mUserPid = optIds[0];
            } else {
              Dib2Root.ccmSto.mUserAddr = "";
            }
            if (null != mpg.items[CcmTag.TIME.ordinal()]) {
              final QSeqIf tim = mpg.items[CcmTag.TIME.ordinal()];
              if ((tim instanceof QWord) && ((QWord) tim).isDate()) {
                long h62 = ((QWord) tim).getShash();
                double tx = DateFunc.eraTicks4Hash62(h62);
                DateFunc.alignId(DateFunc.nanobis4EraTicks(tx));
              }
            }
            if (null != mpg.items[CcmTag.DAT.ordinal()]) {
              final String udat = mpg.items[CcmTag.DAT.ordinal()].toString();
              if ((1 >= Dib2Root.ccmSto.mUserAddr.length()) && (1 < udat.length())) {
                Dib2Root.ccmSto.mUserAddr = udat;
              }
            }
            final long id = getQWord(mpg, CcmTag.CNTRB, QWord.V_0).i64();
            if (((1L << 32) < id) && (id == DateFunc.alignTime(id, 1))) {
              optIds[1] = id;
            }
          }
          return stack;
        }
        final int ix = Dib2Root.ccmSto.variableForceOrIndex(key, value);
        if (0 <= ix) {
          if (stack.length <= ix) {
            stack = Arrays.copyOf(stack, ((ix >>> 3) + 2) << 3);
          }
          stack[ix] = value;
        }
      }
    }
    return stack;
  }

  private static QSTuple update(QSTuple cur, QSTuple mpg) {
    if (Dib2Root.ccmSto.mUserPid == CcmTag.CNTRB.getWord(cur).i64()) {
      cur.set(CcmTag.CNTRB, QWord.V_0);
    }
    String[] newData = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull().split("\n");
    String trash = "";
    if (0 < newData.length) {
      String[] curData = ((QSeq) CcmTag.DAT.getValue(cur)).toStringFull().split("\n");
      HashSet<String> cSet = new HashSet<String>();
      cSet.addAll(Arrays.asList(newData));
      for (String line : curData) {
        line = line.trim();
        if ((0 < line.length()) && !cSet.contains(line)) {
          trash += '\n' + line;
        }
      }
    }
    if (0 != (CcmTag.CATS.getWord(cur).i64() & (Cats.CHAT.flag | Cats.MSG.flag))) {
      if (0
          != (CcmTag.CATS.getWord(mpg).i64()
              & (Cats.CONTACT.flag | Cats.CHAT.flag | Cats.MSG.flag))) {
        boolean isChat = 0 != (CcmTag.CATS.getWord(cur).i64() & (Cats.CHAT.flag));
        if (trash.length() < 2000) {
          trash +=
              "\n"
                  + ((null == CcmTag.TRASHED.getValue(cur))
                      ? ""
                      : ((QSeq) CcmTag.TRASHED.getValue(cur)).toStringFull());
          if (trash.length() >= 2048) {
            trash = trash.substring(0, 2005) + "\n...";
          }
        }
        for (QIfs.QEnumIf tag : mpg.tags) {
          cur.set(tag, mpg.getValue(tag));
        }
        cur.set(CcmTag.CATS, QWord.createQWordInt(isChat ? Cats.CHAT.flag : Cats.MSG.flag));
        trash = trash.replace("\n\n", "\n");
        if (1 < trash.length()) {
          cur.set(CcmTag.TRASHED, QSeq.createQSeq(trash.substring(1)));
        }
        return cur;
      }
    }
    if (0 == (CcmTag.CATS.getWord(cur).i64() & (CcmTag.CATS.getWord(mpg).i64()))) {
      return null;
    }
    if (1 >= CcmTag.CNTRB.getWord(cur).i64()) {
      if ((1 >= trash.length())
          && ((CcmTag.CATS.getWord(mpg).i64() == (CcmTag.CATS.getWord(cur).i64())))) {
        cur.set(CcmTag.DAT, (QSeq) CcmTag.DAT.getValue(mpg));
        cur.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(-1)));
        return cur;
      }
      mpg.setShashOrIgnore((mpg.stamp + 2) | 1);
      mpg.set(CcmTag.CNTRB, QWord.V_1);
      return (QSTuple) Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
    }
    if (CcmTag.CNTRB.getWord(mpg).i64() != CcmTag.CNTRB.getWord(cur).i64()) {
      cur.set(CcmTag.TRASHED, (QSeqIf) CcmTag.DAT.getValue(mpg));
      return cur;
    }
    return null;
  }

  static QSeq[] importData(
      boolean asInternal, long updateCats, long flagsMarkAdjusttimeKeyHex, QSTuple... xDat) {
    QSeq[] stack = new QSeq[8];
    ConcurrentHashMap<String, Long> emails = CcmSto.findEMail4Cats(Cats.CONTACT.flag);
    ConcurrentHashMap<String, Long> chats = CcmSto.findEMail4Cats(Cats.CHAT.flag);
    StringBuilder trash = new StringBuilder();
    long[] optIds = new long[] {Dib2Root.ccmSto.mUserPid, Dib2Root.ccmSto.mUserPid};
    Dib2Root.ccmSto.variableForceOrIndex("L", QSeq.createQSeq("" + xDat.length + " records"));
    try {
      for (QSTuple mpg : xDat) {
        if (null == mpg) {
          continue;
        }
        final long cats = getQWord(mpg, CcmTag.CATS).i64();
        final String key = getQSeq(mpg, CcmTag.LABEL).toStringFull();
        if (0 != ((Cats.PREF.flag | Cats.VAR.flag) & cats)) {
          ///// Pairs that are stored separately (PREF, VAR, ...).
          if (!asInternal) { // && !VAR) || (2<=key.length()) ...
            continue;
          }
          stack =
              importPrefsNVars(
                  (0 != ((Cats.PREF.flag | Cats.VAR.flag) & updateCats)),
                  flagsMarkAdjusttimeKeyHex,
                  mpg,
                  cats,
                  optIds,
                  stack,
                  trash);
        } else if ((0 == cats)
            || (0 >= key.length() || ((1 == key.length()) && ('0' > key.charAt(0))))) {
          final QSeq value = getQSeq(mpg, CcmTag.DAT);
          String valueS = value.toStringFull();
          if (0 < key.length() || (0 < valueS.length())) {
            trash.append("\t" + key + ' ' + valueS);
          }
          continue;
        } else {
          ///// Note: Not duplicating values if they are used as PREF or VAR.
          if (mpg.items[CcmTag.CNTRB.ordinal()] instanceof QWord) {
            final long ctrb = ((QWord) mpg.items[CcmTag.CNTRB.ordinal()]).qValLong;
            if (asInternal) {
              if ((optIds[0] == ctrb)
                  || (optIds[1] == ctrb)
                  || (Dib2Root.ccmSto.mUserPid == ctrb)) {
                mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
              } else if (9999 < ctrb) {
                if ((optIds[0] == optIds[1]) || (optIds[0] == Dib2Root.ccmSto.mUserPid)) {
                  optIds[0] = ctrb;
                  mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
                }
              } else if (1 < ctrb) {
                mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
              }
            } else if (9999 > ctrb) {
              mpg.set(CcmSto.CcmTag.CNTRB, QWord.V_1);
              final QIfs.QSeqIf src = mpg.getValue(CcmSto.CcmTag.SRCS);
              if (src instanceof QSeq) {
                String sx = ((QSeq) src).toStringFull();
                if (0 >= sx.indexOf('@')) {
                  final String label = mpg.getValue(CcmSto.CcmTag.LABEL).toString();
                  sx = getFirstEmail(label);
                  sx = (null == sx) ? "" : sx;
                }
                if (0 < sx.indexOf('@')) {
                  if (sx.contains(Dib2Root.ccmSto.mUserAddr)) {
                    mpg.set(CcmSto.CcmTag.CNTRB, asInternal ? QWord.V_0 : QWord.V_1);
                  } else {
                    sx = CcmSto.getFirstEmail(sx);
                    Long id = emails.get(sx);
                    if (null != id) {
                      mpg.set(CcmSto.CcmTag.CNTRB, QWord.createQWordInt((long) id));
                    }
                  }
                }
              }
            }
          }

          if (!asInternal) {
            QIfs.QItemIf cx = Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
            if (null == cx) {
              long cur =
                  peek((QSeq) mpg.getValue(CcmTag.LABEL), CcmTag.CATS.getWord(mpg).i64(), false);
              if (0 != cur) {
                cx = Dib2Root.ccmSto.zMappingsPid.search(cur);
              }
            }
            QSTuple cur = null;
            while ((cur != cx) && (null != cx)) {
              cur = (QSTuple) cx;
              cx = update(cur, mpg);
            }
            if (null != cx) {
              continue;
            }
          }

          if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CONTACT.flag)) {
            final String dat = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull();
            if ((null != dat) && (0 < dat.indexOf('@'))) {
              emails.put(getFirstEmail(dat), mpg.stamp);
            }
          } else if (0 != (CcmTag.CATS.getWord(mpg).i64() & (Cats.CHAT.flag | Cats.MSG.flag))) {
            ///// Need reference.
            QIfs.QSeqIf ref = mpg.getValue(CcmSto.CcmTag.TAGSREFS);
            if (!(ref instanceof QWord) || (0 == ((QWord) ref).i64())) {
              ref = null;
            } else {
              QIfs.QItemIf check = peek(((QWord) ref).i64(), false);
              if ((null == check)
                  || !(check instanceof QSTuple)
                  || (0
                      == (CcmTag.CATS.getWord(mpg).i64() & (Cats.CHAT.flag | Cats.CONTACT.flag)))) {
                ref = asInternal ? ref : null;
              }
            }
            if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CHAT.flag)) {
              final String label = CcmTag.LABEL.getWord(mpg).toString();
              if ((null != label) && (0 < label.indexOf('@'))) {
                chats.put(getFirstEmail(label), mpg.stamp);
                Long found = emails.get(label);
                if ((null == ref) && (null != found)) {
                  mpg.set(CcmTag.TAGSREFS, QWord.createQWordInt((long) found));
                }
              } else if (null == ref) {
                QWord ctrb = CcmTag.CNTRB.getWord(mpg);
                if (9999 < ctrb.i64()) {
                  mpg.set(CcmTag.TAGSREFS, ctrb);
                }
              }
            } else if (null == ref) {
              String label = CcmTag.LABEL.getWord(mpg).toString();
              if (0 < label.indexOf(':')) {
                String chat = label.substring(0, label.indexOf(':'));
                if (null != chats.get(chat)) {
                  mpg.set(CcmSto.CcmTag.TAGSREFS, QWord.createQWordInt(chats.get(chat)));
                }
              }
            }
          } else if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.DONE.flag)) {
            final String dat = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull();
            if ((null != dat) && dat.startsWith("ACK")) {
              final String[] acks = dat.split("\n");
              mpg = null;
              for (int i0 = 1; i0 < acks.length; ++i0) {
                try {
                  long pid = Long.parseLong(acks[i0]);
                  QIfs.QItemIf ix = Dib2Root.ccmSto.zMappingsPid.search(pid);
                  if ((null != ix)
                      && (ix instanceof QSTuple)
                      && (0 != (CcmTag.CATS.getWord((QSTuple) ix).i64() & Cats.MSG.flag))
                      && ((QSTuple) ix).getValue(CcmTag.RECV).toString().contains("*:")) {
                    ((QSTuple) ix).set(CcmTag.RECV, QWord.V_1);
                  }
                } catch (Exception e0) {
                  break;
                }
              }
            }
          }
          if (null != mpg) {
            Dib2Root.ccmSto.zMappingsPid.add4Handle(mpg);
            Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(mpg, CcmTag.LABEL).getShash(), mpg);
          }
        }
      }
    } catch (Exception e0) {
      return null;
    }
    if (0 < trash.length()) {
      QSeq t0 = QSeq.createQSeq(trash.toString());
      if (null != stack[stack.length - 1]) {
        stack = Arrays.copyOf(stack, stack.length + 1);
      }
      for (int i0 = stack.length - 1; i0 >= -1; --i0) {
        if (0 > i0) {
          stack[0] = t0;
          break;
        }
        if (null != stack[i0]) {
          stack[i0 + 1] = t0;
          break;
        }
      }
    }
    return stack;
  }

  static QSTuple[] exportData(QSeqIf[] xStack, int cStack, long catFilter, long bHidden) {
    int cMap = 0;
    QSTuple[] out = new QSTuple[Dib2Root.ccmSto.zVariables.size() + cStack + 1000];
    catFilter = (0 == catFilter) ? ~0 : catFilter;
    final QWord me = QWord.createQWordInt(Dib2Root.ccmSto.mUserPid);
    final QSeq meDat = QSeq.createQSeq(Dib2Root.ccmSto.mUserAddr);
    QSTuple tup = createEntry(QWord.V_DOT, Cats.VAR.flag, meDat);
    // For keeping lastID info:
    tup.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(tup.stamp)));
    tup.setShashOrIgnore(Dib2Root.ccmSto.mUserPid);
    tup.set(CcmTag.CNTRB, me);
    out[cMap++] = tup;
    if (0 < cStack) {
      out[cMap++] =
          createEntry(
              QWord.V_WILD,
              Cats.REF.flag,
              (cStack == xStack.length) ? xStack : Arrays.copyOf(xStack, cStack));
    }
    if (0 != (catFilter & Cats.VAR.flag)) {
      if (1 == (bHidden & 1)) {
        for (String key : qHidden.keySet()) {
          out = (cMap >= out.length) ? Arrays.copyOf(out, 2 * out.length) : out;
          String val = StringFunc.hex4Bytes(qHidden.get(key), true);
          if (null == val) {
            continue;
          }
          out[cMap++] =
              createEntry(
                  QWord.createQWord(key, true),
                  Cats.HIDDEN.flag | Cats.VAR.flag,
                  QWord.createQWord(val, true));
        }
      }
      for (String key : Dib2Root.ccmSto.zVariables.keySet()) {
        out = (cMap >= out.length) ? Arrays.copyOf(out, 2 * out.length) : out;
        String val = Dib2Root.ccmSto.zVariables.get(key).toString();
        if (null == val) {
          continue;
        }
        out[cMap++] =
            createEntry(QWord.createQWord(key, true), Cats.VAR.flag, QWord.createQWord(val, true));
      }
    }
    int c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
    if (0 > c0) {
      out = Arrays.copyOf(out, -c0);
      // Should now turn positive unless something skipped (and ignored) due to race condition:
      c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
      if (0 > c0) {
        // Mark it as incomplete:
        out[out.length - 1] = createEntry(QOpMain.zzWIPSYM, Cats.VAR.flag, QWord.V_NULL);
      }
    }
    for (int i0 = 0; i0 < c0; ++i0) {
      final QItemIf ctrb = out[i0].items[CcmTag.CNTRB.ordinal()];
      if ((ctrb instanceof QWord) && (0 == ((QWord) ctrb).qValLong)) {
        out[i0].items[CcmTag.CNTRB.ordinal()] = me;
      }
    }
    return (0 <= c0) ? Arrays.copyOf(out, c0) : out;
  }

  public static QSTuple[] ccmCleanup(QSTuple[] xyMpgs, long replaceCats, QWord ctrbFallback) {
    final long stampMax = DateFunc.currentTimeNanobisLinearized(true);
    final LinkedHashMap<Long, QSTuple> badIds = new LinkedHashMap<Long, QSTuple>();
    final LinkedHashMap<Long, QSTuple> goodIds = new LinkedHashMap<Long, QSTuple>();
    final ConcurrentHashMap<String, Long> contacts = new ConcurrentHashMap<String, Long>();
    final ConcurrentHashMap<String, String> contactEmails = new ConcurrentHashMap<String, String>();
    final ConcurrentHashMap<String, String> chats = new ConcurrentHashMap<String, String>();
    final long minTime62 = DateFunc.hash62oDate("1970-02-02");
    for (int iMpg = 0; iMpg < xyMpgs.length; ++iMpg) {
      QSTuple mpg = xyMpgs[iMpg];
      if (null == mpg) {
        continue;
      }
      if (null == CcmTag.CATS.getValue(mpg)) {
        mpg.set(
            CcmTag.CATS,
            QWord.createQWordInt((-1L == replaceCats) ? Cats.NOTE.flag : Cats.TRASH.flag));
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      if (0 >= label.length()) {
        mpg.set(CcmTag.LABEL, QWord.createQWordInt(mpg.stamp));
      }
      String dat = ((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull();
      if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CHAT.flag)) {
        // final String label = tup.getValue(CcmTag.LABEL).toString();
        if ((0 >= label.indexOf('@')) && dat.contains(":ADMIN:") && !dat.contains(":GROUP:")) {
          if (!dat.contains(":NAME:")) {
            dat = ":NAME: " + label + '\n' + dat;
          } else if (!dat.contains(label)) {
            dat += "\n:OLD: " + label;
          }
          dat = dat.replace(":ADMIN:", ":ADMX:");
          if (dat.contains(":OTHER:")) {
            // Group chat:
            dat = dat.replace(":OTHER:", ":GROUP:");
            dat = dat.replace(":EMAIL:", ":INIT:");
            int ix = dat.indexOf(":ADMX:") + 6;
            for (; (dat.charAt(ix) < '0') || ('9' < dat.charAt(ix)); ++ix)
              ;
            int ia = ix;
            for (; ('0' <= dat.charAt(ix)) && (dat.charAt(ix) <= '9'); ++ix)
              ;
            mpg.set(CcmTag.LABEL, QWord.createQWord("G" + dat.substring(ia, ix), true));
          } else {
            String s0 = CcmSto.getFirstEmail(dat);
            if (null != s0) {
              mpg.set(CcmTag.LABEL, QWord.createQWord(s0, true));
            }
          }
          mpg.set(CcmTag.DAT, QSeq.createQSeq(dat));
        }
        chats.put(label, CcmTag.LABEL.getWord(mpg).toString());
      }
      QWord tim = CcmTag.TIME.getWord(mpg);
      if ((null == tim) || (tim.getShash() < minTime62)) {
        if (CcmTag.CATS.getWord(mpg).i64() > (Cats.HIDDEN.flag << 1)) {
          mpg.set(
              CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(mpg.stamp)));
        }
      }

      ///// Check ID

      final long stamp0 = mpg.stamp;
      if (stamp0 <= (1L << 32)) {
        mpg.stamp = DateFunc.currentTimeNanobisLinearized(true);
      } else if (stampMax <= stamp0) {
        mpg.stamp = (stamp0 & 0xffffffffffffffL) | (1L << 56);
      }
      // Has to be unique ...
      while ((null != goodIds.get(mpg.stamp))
          || (null != badIds.get(mpg.stamp))
          || (null != Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp))) {
        if (0 != (CcmTag.CATS.getWord(mpg).i64() & replaceCats)) {
          QIfs.QItemIf px = Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
          if (px instanceof QSTuple) {
            if (null != update((QSTuple) px, mpg)) {
              xyMpgs[iMpg] = null;
              break;
            }
          }
        }
        mpg.stamp += 2;
      }
      if (null == xyMpgs[iMpg]) {
        continue;
      }
      if ((stamp0 != mpg.stamp) && (0 != stamp0)) {
        badIds.put(stamp0, mpg);
        QIfs.QItemIf tx = CcmTag.TRASHED.getValue(mpg);
        String trash = (dat.contains(":ID:")) ? "" : (":ID: " + stamp0 + "\n");
        if (tx instanceof QSeq) {
          trash += ((QSeq) tx).toStringFull();
        }
        if (2048 <= trash.length()) {
          trash = trash.substring(0, 2000) + "...";
        }
        mpg.set(CcmTag.TRASHED, QSeq.createQSeq(trash));
      }
      goodIds.put(mpg.stamp, mpg);

      if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CONTACT.flag)) {
        String email = CcmSto.getFirstEmail(((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull());
        if (null != email) {
          contacts.put(CcmTag.LABEL.getWord(mpg).toString(), mpg.stamp);
          contactEmails.put(CcmTag.LABEL.getWord(mpg).toString(), email);
        }
      }
    }
    int count = xyMpgs.length;
    ConcurrentHashMap<String, String> email2Labels =
        new ConcurrentHashMap<String, String>(contacts.size());
    for (Map.Entry<String, String> contact : contactEmails.entrySet()) {
      email2Labels.put(contact.getValue(), contact.getKey());
    }
    for (int i0 = count - 1; i0 >= 0; --i0) {
      final QSTuple tup = xyMpgs[i0];
      if (null == tup) {
        xyMpgs[i0] = xyMpgs[count - 1];
        --count;
        continue;
      }
      final QItemIf ctrb = tup.items[CcmTag.CNTRB.ordinal()];
      if ((ctrb instanceof QWord) && (1 < ((QWord) ctrb).qValLong)) {
        final long v0 = ((QWord) ctrb).qValLong;
        QSTuple ref = badIds.get(v0);
        if (null == ref) {
          if ((v0 <= (1L << 32)) || (stampMax <= v0)) {
            tup.items[CcmTag.CNTRB.ordinal()] = ctrbFallback;
          }
        } else {
          tup.items[CcmTag.CNTRB.ordinal()] = QWord.createQWordInt(ref.stamp);
        }
      }
      QItemIf refs = CcmTag.TAGSREFS.getValue(tup);
      if ((null == refs) || !(refs instanceof QSeq) || (0 >= ((QSeq) refs).size())) {
        if (0 != (CcmTag.CATS.getWord(tup).i64() & Cats.CHAT.flag)) {
          final String label = CcmTag.LABEL.getWord(tup).toString();
          String ref = email2Labels.get(label);
          if (null == ref) {
            ref = contactEmails.get(label);
            if (null != ref) {
              ref = email2Labels.get(ref);
            }
          }
          if (null != ref) {
            tup.set(CcmTag.TAGSREFS, QWord.createQWordInt(contacts.get(ref)));
          }
        }
      } else {
        final QWordIf rx = ((QSeq) refs).at(0);
        if (rx instanceof QWord) {
          long r2 = (rx.getShash() < (1L << 62)) ? rx.getShash() : ((QWord) rx).i64();
          QWordIf[] all = ((QSeq) refs).atoms();
          if (0 == r2) {
            all[0] = ctrbFallback;
            tup.set(CcmTag.TAGSREFS, QSeq.createQSeq(all));
          } else if (null != badIds.get(r2)) {
            all[0] = QWord.createQWordInt(badIds.get(rx.getShash()).stamp);
            tup.set(CcmTag.TAGSREFS, QSeq.createQSeq(all));
          }
        }
      }

      if (0 != (CcmTag.CATS.getWord(tup).i64() & Cats.MSG.flag)) {
        final String label = CcmTag.LABEL.getWord(tup).toString();
        if (0 >= label.indexOf(':')) {
          String chat = contactEmails.get(label);
          if (null != chat) {
            if (null == chats.get(chat)) {
              if (count >= xyMpgs.length) {
                xyMpgs = Arrays.copyOf(xyMpgs, xyMpgs.length + contacts.size() + 1);
              }
              xyMpgs[count++] =
                  CcmSto.createEntry(QWord.createQWord(chat, true), Cats.CHAT.flag, QSeq.NIL);
              chats.put(chat, chat);
            }
          } else {
            chat = chats.get(label);
          }
          if (null != chat) {
            tup.set(
                CcmTag.LABEL,
                QWord.createQWord(
                    chat + ':' + BigSxg.sxg4Long(CcmTag.TIME.getValue(tup).getShash()), true));
            tup.set(CcmTag.TRASHED, QWord.createQWord(label, true));
          }
        }
      }
    }
    return Arrays.copyOf(xyMpgs, count);
  }

  // =====
}
