// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.UiFontSize0;

public final class UiFunc {

  public static boolean hasAscenderNmz(String str) {
    for (int i0 = str.length() - 1; i0 >= 0; --i0) {
      final char ch = str.charAt(i0);
      if (' ' >= ch) {
        continue;
      } else if (0x3000 <= ch) {
        return true;
      }
      final int sizeInfo =
          ((0x1000 > ch)
                  ? UiFontSize0.kFontWidthAscDesc_00[ch]
                  : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
              >> 8;
      if (0 == sizeInfo) {
        if (ch >= 0x800) {
          return true;
        }
      } else if (0 != (sizeInfo & UiFontSize0.bAscender)) {
        return true;
      }
    }
    return false;
  }

  public static boolean hasDescenderNmz(String str) {
    for (int i0 = str.length() - 1; i0 >= 0; --i0) {
      final char ch = str.charAt(i0);
      if (' ' >= ch) {
        continue;
      } else if (0x3000 <= ch) {
        continue;
      }
      final int sizeInfo =
          ((0x1000 > ch)
                  ? UiFontSize0.kFontWidthAscDesc_00[ch]
                  : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
              >> 8;
      if (0 == sizeInfo) {
        continue;
      } else if (0 != (sizeInfo & UiFontSize0.bDescender)) {
        return true;
      }
    }
    return false;
  }

  public static int boundWidthMono(String str, int height) {
    return (int) (Dib2Constants.UI_FONT_NMZ_MONO_ADV * (long) str.length() * height)
        >> Dib2Constants.UI_FONT_NMZ_SHIFT;
  }

  public static int boundWidthMonoMax(String str, int height) {
    return (int) (Dib2Constants.UI_FONT_NMZ_MONO_MAX * (long) str.length() * height)
        >> Dib2Constants.UI_FONT_NMZ_SHIFT;
  }

  public static int boundWidthNmz(String str, int height) {
    int out = 0;
    for (int i0 = str.length() - 1; i0 >= 0; --i0) {
      final char ch = str.charAt(i0);
      if (' ' > ch) {
        continue;
      } else if (0x3000 <= ch) {
        out += Dib2Constants.UI_FONT_NMZ_N_ADV;
        continue;
      }
      final int sizeInfo =
          (((0x1000 > ch)
                      ? UiFontSize0.kFontWidthAscDesc_00[ch]
                      : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
                  >> 8)
              & 0x1f;
      if (0 == sizeInfo) {
        if (ch >= 0x800) {
          out += Dib2Constants.UI_FONT_NMZ_N_ADV;
        }
      } else if ((sizeInfo * 6)
          > (((0x1000 > ch)
                  ? UiFontSize0.kFontWidthAscDesc_00[ch]
                  : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
              & 0xff)) {
        out +=
            (((((0x1000 > ch)
                                ? UiFontSize0.kFontWidthAscDesc_00[ch]
                                : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
                            & 0xff)
                        * 16)
                    << (Dib2Constants.UI_FONT_NMZ_SHIFT - 4))
                / 100;
      } else {
        out += (sizeInfo << (Dib2Constants.UI_FONT_NMZ_SHIFT - 4));
        // + (1 << (Dib2Constants.UI_FONT_NMZ_SHIFT - 5));
      }
    }
    return (int) ((out * (long) height) >> Dib2Constants.UI_FONT_NMZ_SHIFT);
  }

  public static int boundWidthMax(String str, int height) {
    int out = 0;
    for (int i0 = str.length() - 1; i0 >= 0; --i0) {
      final char ch = str.charAt(i0);
      if (' ' > ch) {
        continue;
      } else if (0x3000 <= ch) {
        out += (Dib2Constants.UI_FONT_NMZ_MONO_ADV * 100) >> Dib2Constants.UI_FONT_NMZ_SHIFT;
      }
      final int sizeInfo =
          ((0x1000 > ch)
                  ? UiFontSize0.kFontWidthAscDesc_00[ch]
                  : UiFontSize0.kFontWidthAscDesc[ch >> 12][ch & 0xfff])
              & 0xff;
      if (0 == sizeInfo) {
        if (ch >= 0x800) {
          out += (Dib2Constants.UI_FONT_NMZ_MONO_ADV * 100) >> Dib2Constants.UI_FONT_NMZ_SHIFT;
        }
      } else {
        out += sizeInfo;
      }
    }
    return (int) (((out * (long) height) + 50) / 100);
  }
}
