// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import java.util.Arrays;
import java.util.regex.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.BigSxg;

/**
 * 'Quick' string utils: text segmentation and sorting, with 'shash' as sortable hash value, to be
 * used in QMMap/QVal: 'shash' + original string as required.
 */
public final class ShashFunc {

  /* Shash (sortable hash) as bit list:
               ...... ...0 (last bit:) 0 iff value is fully encoded within its shash
   0000 0000 0000 ... control ... (value 0 as "")
   0000 0000 0001 ... QFLIT delimiter/ separator (punctuation etc.)
   0000 0000 0010 ... QSLIT structurual literal as hidden separator ...
   0000 0000 0011 ... RFU (struc +)
   0000 0000 0100 ... Bitlist TMP
   00..  (>)   .. ... Date Hash62 or (including lower values) tick reference
   01.. .......    negative float (double precision), bits toggled
   10.. .......    positive float (double precision), flipped sign bit
   110. .......    String (last 3 bits support encoding the value if last bit = 0)
  */

  /*
  Cmp. icu-project.org:
  -- Primary weights can be 1 to 4 bytes in length. (If a character has a 3-byte
  CE primary weight, we'll call it a 3-byter, for example)
  -- They have the restriction that no weight can be a proper initial sequence
  of another. Eg, if X has weight 85 42, then no three or four byte weight
  can start with 85 42.
  -- It is important that the most frequent characters have short weights.
  So, for example, in U5.2 we have {space, A-Z} being 1-byters.
  -- The first bytes are important, and are allocated to special ranges.
  [Java uses double bytes/ ...: Special case: 0x80..., trailing 0x00! for concatenation]
   */

  public static enum ValType {
    X,
    FLIT,
    SLIT,
    DATE,
    NUM,
    LIT;

    public final char marker;

    public static final ValType[] vals = ValType.values();
    public static final char kMarkerMax = (char) ((0xe0 + vals.length) << 8);

    private ValType() {
      marker = (char) ((0xe0 + ordinal()) << 8);
    }
  };

  public static final long SHASH_FLIT = 1L << 52;
  public static final long SHASH_SLIT = 2L << 52;
  public static final long SHASH_AS_DATE = 4L << 52;
  public static final long SHASH_NUM_NEG = 1L << 62;
  public static final long SHASH_NUM_POS = 2L << 62;
  public static final long SHASH_LIT = 3L << 62;

  public static final char SHASH_CHAR_OFFS = (char) 0xE100;

  private static final String REGEX_SPACE_NL = "[\\s\\p{Z}]";
  static final Pattern PATTERN_SPACE_NL = Pattern.compile(REGEX_SPACE_NL);
  static final Pattern PATTERN_SPACE_NL_SEQ = Pattern.compile(REGEX_SPACE_NL + "+");
  static final Pattern PATTERN_SPACE_BEGIN = Pattern.compile("^\\s+");
  static final Pattern PATTERN_SPACE_END = Pattern.compile("\\s+$");
  public static final String REGEX_LINE_BREAK =
      "\\r?[\\n\\u0085\\u2028\\u2029]"; // "\\r?[\\n\\v\\u0085\\u2028\\u2029]";
  public static final Pattern PATTERN_LINE_BREAK_TAB =
      Pattern.compile("\\r?[\\n\u0085\u2028\u2029\\t]"); // "\\r?[\\n\\v\\u0085\\u2028\\u2029\\t]"
  public static final Pattern PATTERN_WORD_CONNECTOR =
      Pattern.compile("[\\p{L}\\p{M}\\p{N}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]+");
  public static final Pattern PATTERN_WORD_BASIC = Pattern.compile("[\\p{L}\\p{M}\\p{N}]+");
  public static final Pattern PATTERN_WORD_SYMBOL = Pattern.compile("[\\p{L}\\p{M}\\p{N}\\p{S}]+");
  public static final Pattern PATTERN_SYMBOLS = Pattern.compile("\\p{S}+");
  // public static final Pattern PATTERN_LETTERS_CASED = Pattern.compile( "\\p{L&}+" );
  public static final Pattern PATTERN_PUNCTUATION = Pattern.compile("\\p{P}+");
  public static final Pattern PATTERN_CONTROLS_UNI = Pattern.compile("\\p{Cc}+");
  public static final Pattern PATTERN_CONTROLS_ANSI = Pattern.compile("\\p{Cntrl}+");
  public static final Pattern PATTERN_DIGITS = Pattern.compile("\\p{Nd}+");
  public static final Pattern PATTERN_DIGITS_BASIC = Pattern.compile("\\p{Digit}+");
  public static final Pattern PATTERN_HEXS = Pattern.compile("\\p{XDigit}+");
  public static final Pattern PATTERN_NUMERICS = Pattern.compile("\\p{N}+");

  // private static final String REGEX_CURRENCY = "\\p{Sc}";
  private static final String REGEX_NUMBER =
      "((([\\+\\-])|([0-9]\\.)|([1-9]))[0-9_\u00B7\\'\\.\\,]*[0-9])|([0-9])";
  private static final String REGEX_NUMBER_SEP = "[_\u00B7\\']";
  private static final Pattern PATTERN_NUMBER_SEP = Pattern.compile(REGEX_NUMBER_SEP);
  private static final String REGEX_UNIT_SUFFIX = "_[\\./A-z0-9\\p{Sc}]+";
  private static final Pattern PATTERN_UNIT = Pattern.compile(REGEX_UNIT_SUFFIX);
  private static final String REGEX_DIGITS_TEL_ETC = "[\\+\\#0][0-9\\-\\*]+\\#?";
  private static final Pattern PATTERN_DIGITS_TEL_ETC = Pattern.compile(REGEX_DIGITS_TEL_ETC);
  private static final String REGEX_OID_DIGITS_ETC = "\\%[0-9A-Za-z_\\^\\~]+";
  private static final Pattern PATTERN_OID_DIGITS_ETC = Pattern.compile(REGEX_OID_DIGITS_ETC);
  // private static final String REGEX_NUMBER_UNIT = REGEX_NUMBER + REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_NUMBER = Pattern.compile(REGEX_NUMBER);
  private static final String REGEX_FLOAT_DEC =
      "[\\+\\-]?[0-9_\u00B7\\'\\.\\,]*[0-9][eE][\\+\\-]?[0-9]+[\\~0-9]*" // ,
      ; // + REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_FLOAT_DEC = Pattern.compile(REGEX_FLOAT_DEC);
  private static final String REGEX_FLOAT_HEX =
      "[\\+\\-]?0[xX][0-9A-Fa-f_\u00B7\\'\\.\\,]*[0-9A-Fa-f]([pP][\\+\\-]?[0-9]+[\\~0-9]*)?"; // +
  // REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_FLOAT_HEX = Pattern.compile(REGEX_FLOAT_HEX);
  private static final String REGEX_FLOAT_RXX =
      "[\\+\\-]?0[bBdDeEoxXyz#][0-9A-Za-z_#\u00B7\\'\\.\\,]*[0-9A-Za-z]([a-zEP#][\\+\\-]?[0-9A-Za-z]+)?"; // +
  private static final Pattern PATTERN_FLOAT_RXX = Pattern.compile(REGEX_FLOAT_RXX);
  private static final String REGEX_DATE =
      "\\-?[0-9]+\\-[0-1][0-9]\\-[0-9][0-9]T?[\\.0-9\\:\\+\\-]*";
  private static final Pattern PATTERN_DATE = Pattern.compile(REGEX_DATE);
  public static final Pattern PATTERN_DATE_D =
      Pattern.compile("[0-9][0-9]\\.[0-1][0-9]\\.[12]?[0-9]?[0-9][0-9]");
  public static final Pattern PATTERN_TIME = Pattern.compile("[0-9]+\\:[0-9][0-9](\\:[0-9][0-9])?");
  /** Include '_' for IDs in programming and '^'/'~' for OIDs/base64 */
  private static final String REGEX_WORD =
      "[_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*";
  // private static final String REGEX_WORD =
  // "[_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*";
  private static final Pattern PATTERN_WORD = Pattern.compile(REGEX_WORD);
  // Literal 'with meaning' or as ID etc., base64 x-chars explicit:
  // private static final String REGEX_SEMEME = "[\\w\\p{L}\\p{S}\\p{N}\\_\\^\\~\\p{Po}][\\-
  // \\w\\p{L}\\p{S}\\p{N}_\\^\\~\\p{Po}]*";
  // private static final Pattern PATTERN_SEMEME = Pattern.compile( REGEX_SEMEME );
  private static final String CHARS_NUM_FIRST = "+-0123456789#";
  private static final String CHARS_STRUC = "()[]{}<>"; // (cmp. Unicode 'Ps'/'Pe' open/close)
  // private static final String CHARS_STRUC_LINE = "./=;|-*:";
  private static final String CHARS_QUOTE_LEFT =
      "'\"\u00ab\u2018\u201b\u201c\u201f\u2039"; // (cmp. Unicode 'Pi' initial)
  // private static final String CHARS_QUOTE_RIGHT = "'\"\u00bb\u2019\u201d\u203a"; // (cmp. Unicode
  // 'Pf' final)
  private static final String CHARS_STRUC_MOD = "?%!&@"; // '$' moved to currency
  private static final String CHARS_FORMAT = "*_+^~"; // markdown, '#' moved to numeric
  private static final String CHARS_MARKER = CHARS_FORMAT + CHARS_STRUC + CHARS_STRUC_MOD;
  private static final String CHARS_MARKER_PLUS_LEFT = ".:" + CHARS_MARKER;
  static final String CHARS_MARKER_DOLLAR = "$#" + CHARS_MARKER;

  private static boolean isCurrency(char ch0) {
    return ('$' == ch0) || ((0xa2 <= ch0) && (ch0 <= 0xa5)) || ((0x20a0 <= ch0) && (ch0 < 0x20cf));
  }

  public static final long FLAG_VALUE_CAPS__4 = 4;
  public static final long FLAG_VALUE_CAPS_ALL__6 = 6;
  public static final long FLAG_VALUE_CAPS_FIRST__4 = 4;
  /** Make shashBits directly usable for QMap ... */
  public static final int FLAG_INDEXED__1 = 1;

  private static char[] zColl64Char4Key = null;
  private static char[] zColl64CharUpper4Key;
  private static byte[] zColl64Key4Char;

  private static void getCollArrays() {
    if (null == zColl64Char4Key) {
      Object[] aa = StringFunc.getCollArrays();
      zColl64Char4Key = (char[]) aa[0];
      zColl64CharUpper4Key = (char[]) aa[1];
      zColl64Key4Char = (byte[]) aa[2];
    }
  }

  public static boolean isLiteral(long shash) {
    return (shash >>> 32) > (SHASH_LIT >>> 32);
  }

  public static boolean isNumeric(long shash) {
    final long upper = shash >>> 32;
    return ((SHASH_NUM_NEG >>> 32) <= upper) && (upper < (SHASH_LIT >>> 32));
  }

  public static boolean isDate(long shash) {
    final long upper = shash >>> 32;
    return ((SHASH_AS_DATE >>> 32) <= upper) && (upper < (SHASH_NUM_NEG >>> 32));
  }

  public static boolean isSememe(long shash) {
    return (SHASH_AS_DATE >>> 32) <= (shash >>> 32);
  }

  /** For simple strings. Last bit is ignored, bit 1/2 is used as case marker. */
  public static String string4ShashBitsLiteral(long bits) {
    getCollArrays();
    if (0 == bits) {
      return "";
    }
    long out = bits;
    int len = 10;
    out >>>= FLAG_INDEXED__1;
    long markerCase8End = FLAG_VALUE_CAPS_ALL__6 >>> FLAG_INDEXED__1;
    for (; len > 0; --len) {
      if (markerCase8End < (out & 0x3fL)) {
        break;
      }
      markerCase8End = 0;
      out >>>= 6;
    }
    char[] aOut = new char[len];
    for (--len; len >= 0; --len) {
      aOut[len] = zColl64Char4Key[(int) (out & 0x3f)];
      out >>>= 6;
    }
    if ((0 == (bits & FLAG_VALUE_CAPS__4))
        || (10 <= aOut.length)
        || (0 >= aOut.length)) { // || QMap.isVoid( handle )) {
      return new String(aOut);
    }
    // First or all char's uppercase:
    final int cUpper =
        (FLAG_VALUE_CAPS_ALL__6 == (bits & FLAG_VALUE_CAPS_ALL__6)) ? aOut.length : 1;
    for (int i0 = 0; i0 < cUpper; ++i0) {
      aOut[i0] = zColl64CharUpper4Key[zColl64Key4Char[aOut[i0]]];
    }
    return new String(aOut);
  }

  public static long shashBits4DoubleD4(String xOptRepr, double addValueD4) {
    // German decimal point etc.:
    if ((null != xOptRepr) && (0 <= xOptRepr.indexOf(','))) {
      if (xOptRepr.indexOf('.') > xOptRepr.lastIndexOf(',')) {
        xOptRepr = xOptRepr.replace(",", "");
      } else {
        xOptRepr = xOptRepr.replace(".", "").replace(",", ".");
      }
    }
    double val =
        (null == xOptRepr)
            ? addValueD4
            : (BigSxg.doubleD4oString(xOptRepr, Double.NaN) + addValueD4);
    //    final boolean notMax = Math.abs(val) < Double.MAX_VALUE;
    //    if (notMax) {
    //      val = Math.nextAfter(val, (0 <= val) ? Double.POSITIVE_INFINITY :
    // Double.NEGATIVE_INFINITY);
    //    }
    long bits = Double.doubleToRawLongBits(val);
    if (0 > val) {
      bits = -bits;
    }
    final long exp = ((bits & 0x7ff0000000000000L) >>> 52);
    long significand = (bits & 0x000fffffffffffffL);
    int low = (int) (significand & 0xfL);
    if ((0 != low) && (0x000ffffffffffff0L >= significand)) {
      if (0 > val) {
        if (2 < (low & 0x3)) {
          significand = (significand | 0x3L) + 1;
        } else {
          significand &= ~0x3L;
        }
      } else {
        if (0xd <= low) {
          significand = (significand | 0xfL) + 1;
        } else if (1 >= (low & 0x3)) {
          significand &= ~0x3L;
        } else {
          significand = (significand | 0x3L) + 1;
        }
      }
    }
    return (long) ((0 > val) ? (1L << 62) : (1L << 63))
        | (exp << (52 - 1))
        | ((significand >>> 1) & ~1L);
  }

  public static double doubleD4oShashBits(long shashBits) {
    long bits = shashBits << 1;
    if ((0x7feffffffffffffcL <= bits) && (bits <= 0x7fefffffffffffffL)) {
      return Double.MAX_VALUE;
    }
    final long exp52 = bits & 0x7ff0000000000000L;
    final long significand = (bits & 0x000fffffffffffffL);
    final boolean neg = (0 == (shashBits >> 63));
    bits = exp52 | significand;
    bits = (neg ? (-bits) : bits);
    return Double.longBitsToDouble(bits);
  }

  public static long shashBits4PunctFS(String piece) {
    getCollArrays();
    final int len = (7 < piece.length()) ? 7 : piece.length();
    long out = (long) SHASH_FLIT;
    if (0 >= len) {
      return out;
    }
    for (int i0 = 0; i0 < len; ++i0) {
      final char ch0 = piece.charAt(i0);
      if (0 < StringFunc.XSTRUCS.indexOf(ch0)) {
        out = (long) SHASH_SLIT;
      }
    }
    final char ch0 = piece.charAt(0);
    final byte key = (zColl64Key4Char.length > ch0) ? zColl64Key4Char[ch0] : (byte) 4;
    out |= (long) ((key - 1) & 0x3) << 50;
    for (int i0 = 0, shift = 50 - 7; i0 < len; ++i0, shift -= 7) {
      if (0 <= shift) {
        out |= (piece.charAt(i0) & 0x7fL) << shift;
        continue;
      }
      out |= (piece.charAt(i0) & 0x7f) >>> (-shift);
    }
    return out;
  }

  public static long shashBits4String(String str, boolean forceLiteral) {
    getCollArrays();
    int shift = (64 - 3 - 6);
    if (0 == str.length()) {
      return 0;
    } else if (!forceLiteral && (1 == str.length() && (0x2b0 > str.charAt(0)))) {
      final char ch0 = str.charAt(0);
      if (('0' <= ch0) && (ch0 <= '9')) {
        return shashBits4DoubleD4(null, (ch0 - '0') * Dib2Constants.INT_D4_FACT);
      }
      if ((zColl64Key4Char.length > ch0)
          && (zColl64Key4Char['0'] <= zColl64Key4Char[ch0])
          && (zColl64Key4Char[ch0] < 63)) {
        long flag = 0;
        if (zColl64Char4Key[zColl64Key4Char[ch0]] != ch0) {
          if (zColl64CharUpper4Key[zColl64Key4Char[ch0]] == ch0) {
            flag = FLAG_VALUE_CAPS_FIRST__4;
          }
        }
        return SHASH_LIT | (((long) zColl64Key4Char[ch0]) << shift) | flag;
      }
    }
    long out = (long) SHASH_LIT;
    int len = (10 > str.length()) ? str.length() : 10;
    long flag = 0;
    boolean isWord = false;
    boolean isStruc = false;
    for (int i0 = 0; i0 < len; ++i0, shift -= 6) {
      final char ch0 = str.charAt(i0);
      if (isStruc || (0 < StringFunc.XSTRUCS.indexOf(ch0))) {
        isStruc = true;
      } else if (!isWord && (0x2b0 > ch0)) {
        if ((('a' <= ch0) && (ch0 <= 'z')) || (('A' <= ch0) && (ch0 <= 'Z'))) {
          isWord = true;
        } else if (('0' <= ch0) && (ch0 <= '9')) {
          isWord = true;
        } else if (0xc0 <= ch0) {
          isWord = true;
        }
      }
      if ((zColl64Key4Char.length > ch0) && (63 > zColl64Key4Char[ch0])) {
        if (zColl64Char4Key[zColl64Key4Char[ch0]] != ch0) {
          if (zColl64CharUpper4Key[zColl64Key4Char[ch0]] == ch0) {
            if ((0 != flag) || (0 == i0)) {
              flag = (flag != 0) ? FLAG_VALUE_CAPS_ALL__6 : FLAG_VALUE_CAPS_FIRST__4;
            }
          }
        }
        out |= (long) zColl64Key4Char[ch0] << shift;
      } else {
        if (3 <= shift) {
          byte[] shashBytes = StringFunc.coll64xBytes(str, i0);
          for (i0 = 0; (i0 < shashBytes.length) && (0 <= shift); ++i0, shift -= 6) {
            out |= (shashBytes[i0] & 0x3fL) << shift;
          }
          if ((i0 < shashBytes.length) && (-4 <= shift)) {
            out |= (shashBytes[i0] & 0x3f) >>> (-shift);
          }
        } else {
          out |= 63L << shift;
        }
        break;
      }
    }
    if (!isStruc && !isWord) {
      isWord = PATTERN_WORD.matcher(str).find();
    }
    if (isStruc || !isWord) {
      return shashBits4PunctFS(str);
    }
    return out | flag;
  }

  public static long shashBits4Ansi(String str) {
    getCollArrays();
    int shift = (64 - 3 - 6);
    if (0 == str.length()) {
      return 0;
    }
    long out = (long) SHASH_LIT;
    int len = (10 > str.length()) ? str.length() : 10;
    long flag = 0;
    for (int i0 = 0; i0 < len; ++i0, shift -= 6) {
      final char ch0 = str.charAt(i0);
      if ((zColl64Key4Char.length > ch0)
          && (63 > zColl64Key4Char[ch0])
          && (0 < zColl64Key4Char[ch0])) {
        if (zColl64Char4Key[zColl64Key4Char[ch0]] != ch0) {
          if (zColl64CharUpper4Key[zColl64Key4Char[ch0]] == ch0) {
            if ((0 != flag) || (0 == i0)) {
              flag = (flag != 0) ? FLAG_VALUE_CAPS_ALL__6 : FLAG_VALUE_CAPS_FIRST__4;
            }
          }
        }
        out |= (long) zColl64Key4Char[ch0] << shift;
      } else {
        return shashBits4DoubleD4(null, Double.NaN);
      }
    }
    return out | flag;
  }

  private static char classifyPiece(String piece) {
    if ((null == piece) || (0 >= piece.length())) {
      return 0;
    }
    boolean num0 = // !forceString &&
        (0 <= CHARS_NUM_FIRST.indexOf(piece.charAt(0)));
    //  double frac = 0;
    // String shash = "";
    boolean punct = false;
    // Units:
    String units = null;
    if (num0) {
      int iUnit = piece.lastIndexOf('_');
      if ((0 < iUnit) && (iUnit < (piece.length() - 1)) && (' ' < piece.charAt(iUnit + 1))) {
        if (PATTERN_UNIT.matcher(piece.substring(iUnit)).matches()
            && !PATTERN_NUMBER.matcher(piece.substring(iUnit)).matches()) {
          units = piece.substring(iUnit + 1);
          piece = piece.substring(0, iUnit);
        }
      }
    }

    if (num0 && PATTERN_FLOAT_HEX.matcher(piece).matches()) {
      if ((0 > piece.indexOf('p')) && (0 > piece.indexOf('P'))) {
        piece += "p0";
      }
    } else if (num0 && (PATTERN_DATE.matcher(piece).matches())
        || PATTERN_DATE_D.matcher(piece).matches()) {
      // if (classify) {
      return ValType.DATE.marker;
      // shash = shashStr4Date(piece);
    } else if (num0 && PATTERN_DIGITS_TEL_ETC.matcher(piece).matches()) {
      num0 = false;
    } else if (num0 && PATTERN_OID_DIGITS_ETC.matcher(piece).matches()) {
      num0 = false;
    } else if (num0 && PATTERN_FLOAT_DEC.matcher(piece).matches()) {
    } else if (num0 && PATTERN_NUMBER.matcher(piece).matches()) {
    } else if (num0 && PATTERN_FLOAT_RXX.matcher(piece).matches()) {
    } else if (PATTERN_WORD.matcher(piece).find()) { // floatError ||
      num0 = false;
    } else {
      num0 = false;
      punct = true;
    }
    if (num0) {
      // if (0 >= shash.length()) {
      // German decimal point etc.:
      if (0 <= piece.indexOf(',')) {
        if (piece.indexOf('.') > piece.lastIndexOf(',')) {
          piece = piece.replace(",", "");
        } else {
          piece = piece.replace(".", "").replace(",", ".");
        }
      }
      piece = PATTERN_NUMBER_SEP.matcher(piece).replaceAll("");
      long shx = shashBits4DoubleD4(piece, 0);
      if (Double.isNaN(doubleD4oShashBits(shx))) {
        return ValType.LIT.marker;
      }
      return ValType.NUM.marker;
    }
    // if (!num0) {
    if (punct) {
      return ValType.FLIT.marker;
    } else {
      if (null != units) {
        piece += "_" + units;
        // TODO
      }
    }
    return 0;
  }

  public static String string4ShashBits(long shash) {
    if (0 == shash) {
      return "";
    }
    // final char ch0 = (char) (0xe000 | (int) (shash >>> 48 + 3));
    if (!isLiteral(shash)) {
      if (!isNumeric(shash)) {
        //      if (1 >= len) {
        //        return "";
        //      }
        if (!isDate(shash)) {
          //          if (SHASH_ERR_MAX > ch0) {
          //            return (SHASH_STR_OFFS == ch0) ? "0aNaN" : "ERROR";
          //          }
          // For STRUC, PUNCT, ...:
          char[] out = new char[7];
          int len = 0;
          for (int shift = 50 - 7; shift >= 0; shift -= 7, ++len) {
            out[len] = (char) ((shash >>> shift) & 0x7f);
            if (' ' > out[len]) {
              break;
            }
          }
          return new String(out, 0, len);
        }
        // TODO Handle units
        //        return DateFunc.date4SlotSecond16Approx_OLD(
        //            (shash & ((SHASH_DATE_0 << 1) - 1)) - SHASH_DATE_0);
        return DateFunc.date4Hash62(shash, '.');
      }
      double val = doubleD4oShashBits(shash);
      return BigSxg.rxx4DoubleD4(val, 10);
    }
    return string4ShashBitsLiteral(shash);
  }

  private static int outWithStructMarker(String[] out, int cOut, String struct) {
    if (2 == struct.length()
        && (' ' > struct.charAt(1))
        && (0
            <= (""
                    + StringFunc.QUOTE_START
                    + StringFunc.QUOTE_END
                    + StringFunc.LIST_START
                    + StringFunc.LIST_END)
                .indexOf(struct.charAt(1)))) {
      out[cOut++] = ValType.SLIT.marker + struct.substring(1);
      return cOut;
    }
    boolean struc0 = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf(struct.charAt(1));
    for (int i0 = 2; i0 < struct.length(); ++i0) {
      boolean struc = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf(struct.charAt(i0));
      if (struc != struc0) {
        if (struc) {
          final String tmp = struct.substring(1, i0);
          out[cOut++] = // (PATTERN_WORD.matcher( tmp ).matches() ? SHASH_LITERAL : SHASH_PUNCT)
              // + tmp;
              ValType.FLIT.marker + tmp;
          out[cOut++] = ValType.SLIT.marker + struct.substring(i0);
          return cOut;
        }
        out[cOut++] = ValType.SLIT.marker + struct.substring(1, i0);
        final String tmp = struct.substring(i0);
        out[cOut++] =
            ""
                + // (PATTERN_WORD.matcher( tmp ).matches() ? SHASH_LITERAL : SHASH_PUNCT) + tmp;
                ValType.FLIT.marker
                + tmp;
        return cOut;
      }
    }
    if (struc0) {
      out[cOut++] = ValType.SLIT.marker + struct.substring(1);
      return cOut;
    }
    out[cOut++] = struct;
    return cOut;
  }

  /**
   * Split text into 'atoms' with simplified markers for numbers, bit lists, punctuation, assuming
   * '[[' and ']]' ... LIST_START, QUOTE_START, etc. as markers for lists. Markers: SHASH_STRUC,
   * SHASH_PUNCT, SHASH_DATE, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING, (SHASH_LITERAL)
   *
   * @param xText
   * @return
   */
  private static String[] splitTextAppend(String[] out, int cOut, String xText) {
    String[] in = xText.split(" ", -1);
    boolean first = true;
    for (String sliceFull : in) {
      if (!first && (0 < cOut)) {
        if ((null != out[cOut - 1]) && (ValType.FLIT.marker == out[cOut - 1].charAt(0))) {
          out[cOut - 1] += ' ';
        } else {
          out[cOut++] = "" + ValType.FLIT.marker + ' ';
        }
      }
      first = false;
      String slice = sliceFull;
      while (0 < slice.length()) {
        if ((cOut + 4) >= out.length) {
          out = Arrays.copyOf(out, 2 * out.length);
        }
        int cut = 0;
        char ch0 = slice.charAt(0);
        for (; cut < slice.length(); ++cut) {
          if (false // .
              || (('a' <= ch0) && (ch0 <= 'z')) // .
              || (('0' <= ch0) && (ch0 <= '9')) // .
              || (('A' <= ch0) && (ch0 <= 'Z')) // .
              || (' ' > ch0) // .
              || (true // .
                  && !isCurrency(ch0) // .
                  && (0
                      > (CHARS_MARKER_PLUS_LEFT + CHARS_QUOTE_LEFT + '.')
                          .indexOf(slice.charAt(cut))))) {
            break;
          }
        }
        if (cut >= slice.length()) {
          cOut = outWithStructMarker(out, cOut, ValType.FLIT.marker + slice);
          slice = "";
          continue;
        }
        String piece = slice;
        slice = "";
        String part = piece;
        String pre = "";
        boolean num = (0 < CHARS_NUM_FIRST.indexOf(ch0));
        if (0 < cut) {
          if ((1 == cut)
              && (1 < piece.length())
              && (('+' == piece.charAt(0)) || ('-' == piece.charAt(0)))
              && ('0' <= piece.charAt(1))
              && ('9' >= piece.charAt(1))) {
            cut = 0;
            num = true;
          } else {
            // Potential further splitting later on:
            pre = ValType.FLIT.marker + piece.substring(0, cut);
            part = piece.substring(cut);
            ch0 = part.charAt(0);
          }
        }
        cut = -1;
        boolean date = false;
        if (num) {
          num = false;
          Matcher mx;
          if ((mx = PATTERN_FLOAT_HEX.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
            num = true;
          } else if ((mx = PATTERN_FLOAT_RXX.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
            num = true;
          } else if ((mx = PATTERN_DATE.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
            date = true;
          } else if ((mx = PATTERN_FLOAT_DEC.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
            num = true;
          } else if ((mx = PATTERN_NUMBER.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
            int cut2 = 0;
            if ((mx = PATTERN_TIME.matcher(part)).find() && (0 == mx.start())) {
              cut2 = mx.end();
            } else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher(part)).find() && (0 == mx.start())) {
              cut2 = mx.end();
            } else if ((mx = PATTERN_OID_DIGITS_ETC.matcher(part)).find() && (0 == mx.start())) {
              cut2 = mx.end();
            }
            num = cut2 < cut;
            cut = num ? cut : cut2;
          } else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
          } else if ((mx = PATTERN_OID_DIGITS_ETC.matcher(part)).find() && (0 == mx.start())) {
            cut = mx.end();
          }
          if (num && (mx = PATTERN_UNIT.matcher(part.substring(cut))).find() && (0 == mx.start())) {
            cut += mx.end();
          }
        } else if ((1 < part.length() && ('\'' == part.charAt(1)))) {
          switch (ch0) { // TODO
            case 'P': // packed base64x
            case 'I': // id/ indirect
            case 'X': // octets
              cut = part.lastIndexOf('\'');
              if (1 >= cut) {
                cut = -1;
              } else {
                part = ValType.X.marker + part;
                cut += 2;
              }
            default:
              ;
          }
        }
        if (0 > cut) {
          Matcher mx = null;
          cut = (mx = PATTERN_WORD.matcher(part)).find() ? mx.start() : part.length();
          int cutx = (mx = PATTERN_NUMERICS.matcher(part)).find() ? mx.start() : cut;
          if (cut <= cutx) {
            cutx = (mx = PATTERN_DIGITS.matcher(part)).find() ? mx.start() : cut;
            if (cut <= cutx) {
              cutx = (mx = PATTERN_WORD.matcher(part)).find() ? mx.start() : -1;
            }
          }
          cut = cutx;
          if (0 > cut) {
            pre = (0 < pre.length()) ? pre : ("" + ValType.FLIT.marker);
            pre += part;
            cOut = outWithStructMarker(out, cOut, pre);
            continue;
          }
          if (0 < cut) {
            pre = (0 < pre.length()) ? pre : ("" + ValType.FLIT.marker);
            pre += part.substring(0, cut);
            part = part.substring(cut);
          }
          cut = mx.end() - cut;
          if (SHASH_CHAR_OFFS <= ch0) {
            part = ValType.LIT.marker + part;
            ++cut;
          }
        } else if (date) {
          num = false;
          part = ValType.DATE.marker + part;
          ++cut;
        } else if (num && (0 < pre.length()) && (pre.endsWith("+") || pre.endsWith("-"))) {
          part = pre.charAt(pre.length() - 1) + part;
          ++cut;
          pre = pre.substring(0, pre.length() - 1);
        }
        if (num) {
          if ((0 < pre.length()) && isCurrency(pre.charAt(pre.length() - 1))) {
            part = pre.charAt(pre.length() - 1) + part;
            ++cut;
            pre = pre.substring(0, pre.length() - 1);
          }
          part = ValType.NUM.marker + part;
          ++cut;
        }
        if (1 < pre.length()) {
          cOut = outWithStructMarker(out, cOut, pre);
          if (0 > cut) {
            cut = 0;
          }
        }
        if ((cut >= part.length()) || (0 >= cut)) {
          out[cOut++] = part;
          continue;
        }
        slice = part.substring(cut);
        if (0xf800 <= part.charAt(0)) {
          out[cOut++] = ValType.LIT.marker + part.substring(0, cut);
        } else {
          out[cOut++] = part.substring(0, cut);
        }
      }
    }
    if (cOut < out.length) {
      out[out.length - 1] = "\ue000" + (char) cOut;
    }
    for (int i0 = 0; i0 < cOut; ++i0) {
      char ch0;
      if ((0 >= out[i0].length() || ((ch0 = out[i0].charAt(0)) >= 0xf800))
          || (ValType.SLIT.marker == ch0)) {
        continue;
      }
      ///// Due to additional cutting, the pieces might end up having a different type:
      final String outx =
          ((ValType.X.marker <= ch0) && (ch0 <= ValType.kMarkerMax))
              ? (out[i0].substring(1))
              : out[i0];
      if (0 >= outx.length()) {
        out[i0] = "";
        continue;
      }
      final char x0 = classifyPiece(outx);
      // if (ch0 != ((null == x0) ? outx.charAt(0) : x0.charAt(0))) {
      out[i0] = ((0 == x0) || (ValType.LIT.marker == x0)) ? outx : ("" + x0 + outx);
    }
    return out;
  }

  /**
   * Transform text string (with quote markers STX/ETX etc.) into marked text atoms. Markers as
   * first char: SHASH_STRUC, SHASH_PUNCT, SHASH_DATE, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING,
   * also SHASH_LITERAL if needed: Missing marker = SHASH_LITERAL => simple word.
   */
  public static String[] markedAtoms4String(String xStr) {
    String str = xStr;
    String[] out = new String[10 + xStr.length() / 4];
    int cOut = 0;
    int level = -1;
    String combine = "";
    String[] chunks = str.split("" + StringFunc.QUOTE_START, -1);
    for (String chunk : chunks) {
      if ((cOut + 5) >= out.length) {
        out = Arrays.copyOf(out, 2 * out.length);
      }
      ++level;
      if (0 < level) {
        combine += StringFunc.QUOTE_START;
        int i0 = -1;
        while (0 < level) {
          i0 = chunk.indexOf(StringFunc.QUOTE_END, i0 + 1);
          if (0 > i0) {
            combine += chunk;
            chunk = "";
            break;
          }
          --level;
        }
        if (0 < level) {
          continue;
        }
        if (0 <= i0) {
          combine += chunk.substring(0, i0 + 1);
          chunk = chunk.substring(i0 + 1);
        }
        out[cOut++] = combine;
        combine = "";
      }
      if (0 < chunk.length()) {
        out = splitTextAppend(out, cOut, chunk.replace(StringFunc.QUOTE_END, ' '));
        cOut =
            (out[out.length - 1].startsWith("\ue000")) ? out[out.length - 1].charAt(1) : out.length;
      }
    }
    if (0 < combine.length()) {
      out[cOut++] = "" + combine + StringFunc.QUOTE_END;
    }
    return Arrays.copyOf(out, cOut);
  }

  /**
   * Transform array of marked atoms into array with list markers: balance lists, remove list SEPs,
   * remove outer STX/ ETX pairs from ATOMs.
   *
   * @param out
   * @return
   */
  public static int lists4MarkedAtoms(String[] out) {
    int iIn = 0;
    int iOut = 0;
    int level = 0;
    for (; iIn < out.length; ++iIn) {
      final String in = out[iIn];
      if (0 >= in.length()) {
        continue;
      }
      char ch0 = in.charAt(0);
      if (ch0 == ValType.SLIT.marker) {
        if (0 < in.indexOf(StringFunc.LIST_START)) {
          ++level;
        } else if ((0 < in.indexOf(StringFunc.LIST_END)) && (0 < level)) {
          --level;
        }
        out[iOut++] = in.substring(1);
        continue;
      } else if (ch0 == ValType.FLIT.marker) {
        if (0 < level) {
          // Drop it:
          continue;
        }
        out[iOut++] = in.substring(1);
        continue;
      } else if ((ch0 == ValType.X.marker) || (ch0 == ValType.NUM.marker)) {
        out[iOut++] = in.substring(1);
        continue;
      }
      if (iOut != iIn) {
        out[iOut] = in;
      }
      ++iOut;
    }
    final int cOut = iOut;
    iOut = 0;
    for (iIn = 0; iIn < cOut; ++iIn, ++iOut) {
      final String outx = out[iIn];
      if ((StringFunc.QUOTE_START == outx.charAt(0)) && outx.endsWith("" + StringFunc.QUOTE_END)) {
        out[iOut] = outx.substring(1, outx.length() - 1);
        if (0 >= out[iOut].length()) {
          --iOut;
        }
      } else if (iOut != iIn) {
        out[iOut] = out[iIn];
      }
    }
    return iOut;
  }

  // =====
}
