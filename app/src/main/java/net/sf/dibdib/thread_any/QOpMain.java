// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import com.gitlab.dibdib.picked.common.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.QEnumIf;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_net.*;
import net.sf.dibdib.thread_ui.*;

/**
 * Operators as functions (on Strings, doubles, bitlists, lists, ...). First 4 char's must be
 * significant. First string of optional symbols is empty if operator symbol is the default, rather
 * than the name. Boolean value = lowest bit of bitlist: TRUE = -1 or odd number, FALSE = 0 or even
 * number. Bitlist functions operate bitwise and are thus also used for booleans.
 */
public enum QOpMain implements QIfs.QEnumIf {
  // =====

  // var L lastX, T term, Q quickMem, E error

  NOP(0, 0, "no operation"),
  zzWIPCALC, // Work in Progress: calculating values
  zzWIPSYM, // RFU (Work in Progress: symbolic calculation)
  //  zzNULL,

  ///// Internal

  //  zzPUSH(-1, 1, "dummy for keeping operand"),

  ///// Math (atomic)

  ABS(1, "POSV", "absolute value: [X0 X1 ...] => [ABS(X0) ABS(X1) ...] "),
  ACOS(1, "acos"),
  ADD(2, "\u2214", "sum of 2 values each [Y0 Y1 ...] [X0 X1 ...] => "),
  AND(2, "", "&", "\u2227", "binary AND"),
  ANDT(2, "", "&&", "truthy AND"),
  ASIN(1, "asin"),
  ATAN(1, "atan"),
  CEIL(1, "integer above"),
  COMP(2, "comparison: Y X -> -1/0/1"),
  COS(1, "cos"),
  COSH(1, "cosh"),
  DEG(1, "radians to degrees"),
  DIV(2, "\u00f7", "\u2215", "division for 2 values (each)"),
  DROUND(1, "round to internal decimal places"),
  E(0, "Euler"),
  FALSE(0, "return 0 as even value (= boolean FALSE)"),
  FLOOR(1, "integer below"),
  FRAC(1, "fractional part"),
  // GCD
  GRAD(1, "radians to gradiens"),
  I,
  IDIV(2, "", "//", "integer division"),
  IMPLIES,
  INF(0, "infinity"),
  INT(1, "non-fractional part"),
  IST(1, "!!", "truthy conversion"),
  // LCM
  // LOG(2, "base Y logarithm"),
  LOG10(1, "base 10 logarithm"),
  LN(1, "natural logarithm"),
  MAX2(2, "maximum of 2 values (paired)"),
  MIN2(2, "minimum of 2 values (paired)"),
  MOD(2, "+%", "modulo (positive remainder)"),
  MUL(2, "\u00d7", "\u2217", "multiply: product of 2 values (each)"),
  NADD(1, "", "!+", "\u2213", "\u00b1", "CHS", "additive inverse (unary minus), change sign"),
  NAN(0, "error value NaN (not a number)"),
  NAND(2, "!&", "\u22bc", "\u2206", "binary NAND"),
  NEGV, // ( 1, "", "\u00af", "negative value" ),
  NMULT(1, "", "!*", "\u215f", "\u00b9", "INV", "multiplicative inverse, reciprocal"),
  NOR(2, "!|", "binary NOR"),
  NOT(1, "!~", "\u00ac", "negated bits, binary NOT"),
  NOTT(1, "!.", "truthy NOT"),
  OR(2, "", "|", "\u2228", "binary OR"),
  ORT(2, "", "||", "truthy OR"),
  PERCENT(1, "", "%", "percentage value (/100)"),
  PI(0, "\u03c0", "pi"),
  POWER(2, "", "**", "power"), // Do not use '^': => units/ variables/ mappings.
  PRED(1, "predecessor"),
  RADD(1, "RAD", "radians, from degrees"),
  RADG(1, "radians, from gradiens"),
  RND1(0, "random number"),
  REM(2, "-%", "remainder, returning the sign of the first value"),
  ROOT,
  ROUND(1, "rounded value"),
  SIGN(1, "value -> -1/0/1"),
  SIN(1, "sin"),
  SINH(1, "sinh"),
  SHL(2, "", "<<", "\u226a", "\u00ab", "shift left"),
  SHRA(2, "", ">>", "\u226b", "\u00bb", "arithmetic shift right"),
  SHRL(2, ">>>", "logical shift right"),
  SQRT(1, "square root"),
  SQUARE(1, "squared value"),
  SUB(2, "\u2212", "subtraction with 2 values (each)"),
  SUCC(1, "successor"),
  TAN(1, "tan"),
  TANH(1, "tanh"),
  TAU(0, "\u03c4", "tau (= 2 * pi)"),
  TRUE(0, "return -1 as odd value (= boolean TRUE)"),
  TRUNC(2, "truncate"),
  XOR(2, "|%", "\u22bb", "\u2207", "binary XOR"),

  ///// Any atomic type

  CHOICE(3, "OPT", "conditional value: Z Y X => X ? Y : Z"),

  // '-1' ==> '1' if QWord fully supports dates
  DTDT(-1, "convert date to Julian Day"),
  DTTS(-1, "convert J2000 ticks in seconds to ISO date (approx.)"),
  DTTY(-1, "convert sidereal years to ISO date (approx.)"),
  DTYD(-1, "convert Julian Day to sidereal years (approx.)"),

  EQ(2, "", "=", "==", "is equal"),
  GE(2, "", ">=", "\u2265", "is greater than or equal"),
  GT(2, "", ">", "is greater than"),
  // del IDAT(1, "integer representation of date"),
  ISTYPE, //  ( 2, "is same type (compare type of 2 values)" ),
  LE(2, "", "<=", "\u2264", "is less than or equal"),
  LENGTH(1, "number of characters"),
  LT(2, "", "<", "is less than"),
  MINUS(2, "", "-", "subtract value or char's"),
  NE(2, "", "!=", "<>", "\u2260", "is not equal"),
  PART(2, "", "/", "divide or cut off"),
  PLUS(2, "", "+", "add value or char's"),
  RX(2, "", "~~", "index of match for regular expression"),
  RXG(3, "", "~/", "n-th group of match for regular expression"),
  RXN(3, "", "~+", "index of n-th match for regular expression"),
  RXT(3, "", "~&", "n-th match for regular expression"),
  // SECT, // MSEC,
  // SLSD, // date to slot second
  SPLITAT(2, 2, "SUB1", "split atom at index"),
  TIMES(2, "", "*", "multiply or repeat, by value"),
  UTF16,
  UTF32,
  UTF8,
  // YDAT,
  // YNUM,

  ///// Sequence/ vector/ matrix

  LIMIT,

  ALL, // ( -2, "return -1 if all members match P: [L] P -> Xbool" ),
  AT, // (-2, "", ".", "INDX"  [L] I => L[I]
  ATKEY, // ".:" associative
  CONC, // ( -2, "", ",", "CONS", "concatenate" ),
  CONST(-1, "value of constant/ unit"),
  CONVT(-2, "convert value Y: Y (markerTo markerFrom ) => Y'"),
  FOLD, // L/R
  DUP(-1, 2, "duplicate: X => X X"),
  EDUPTO, // ( -2, "DUPI", "duplicate to: ... X I -> X ... X" ),
  EMAP, // ( -2, "operator X (with additional args) as scalar on each member: ... X [L] -> [L']" ),
  EMPTY,
  EPICK, // dup item n to top ... ->
  EROLL, // move item n to top ... ->
  EXPAND,
  HAS, // ( -2, "has as member: X [L] -> Ybool" ),
  INTRS, // INTERSECTION,
  IN, // ( -2, "ELEMENT", "is member of: [L] X -> Ybool" ),
  // INDEX, => AT
  MDROP, // ( -2, "drop first/last n members in list: [L] n -> [L']" ),
  MCROSS, // cross product
  // MDIA, matrix with matching diagonal
  // MDIAG, // diagonal of matrix
  MDIV, // ( -2, "matrix division"),
  MDOT, // scalar product
  MEAN,
  MEDIAN,
  MEIG, // eigenvalues
  MEYE, // identity matrix
  // MFLIP,
  MINV, // ( -2, "matrix inversion"),
  // MINP pseudo-inverse
  MMUL, // ( -2, "matrix multiplication"),
  MODE, // modal
  MONE, // matrix with n ones
  MROT, // ( 1, "rotate matrix" ),
  MSTD, // standard deviation
  MTRP, // .' ( 1, "transpose matrix" ),
  // MTRX  ..'
  MZERO, // matrix with n zeros
  NIP, // drop second
  OF, // I [L] => L[I]
  ONEOF, // ( -2, "return -1 if exactly one member matches P: [L] P -> Xbool" ),
  OVER, // dup second to top
  PRODUCT(-1, "\u220f", "product of sequence of values"),
  RANDOM(-1, "sequence of random numbers"),
  RANGE(-1, "sequence of numbers"), // ".." (1, "end/ start end/ start end step
  RXSEL, // ( -2, "filter list with pattern P: [L] P -> [L']" ),
  RHO,
  RLDOWN, // ( -3, "roll down: X Y Z -> Y Z X" ),
  RLUP, // ( -3, "roll up: X Y Z -> Z X Y" ),
  ROT, // ( -3, "ROTATE", "rotate: X Y Z -> Z Y X" ),
  SELECT, // ( 2, "compress list by list of booleans" ),
  SEQ, // ( 2, "create running sequence" ),
  SIZE, // COUNT
  SIZS, // DIMS
  SLICE,
  SMALL, // ( -1, "is empty list or single value" ),
  SOME, // ( -2, "return -1 if some members match P: [L] P -> Xbool" ),
  SORT,
  SUBSET,
  SUPSET,
  SUM(-1, "\u2211", "sum of sequence of values"),
  SWAP(-2, 2, "EXCH", "swap top 2 values: Y X -> X Y"),
  TAKE, // ( -2, "take first n members: [L] n -> [L']" ),
  TO, // ".="
  TOKEY, // associative
  TUCK, // dup below, top to third
  UNION,
  ZLENGTH, // length of full literal
  ZRX(-2, "", "`~", "index of match for regular expression"),
  ZRXG(-3, "", "`/", "n-th group of match for regular expression"),
  ZRXN(-3, "", "`+", "index of n-th match for regular expression"),
  ZRXSPLIT(-2, "RXS", "split literal on regex"),
  ZRXT(-3, "", "`&", "n-th match for regular expression"),
  ZSPLIT(-2, "SUBSTR", "split literal by range"),
  ZZ, // (1, "`", "quote: turn sequence into atomic string (literal item)"),

  ///// TODO Not necessarily thread safe

  //  zzNEGTSAFE, // marker

  ///// Combinator (may consume the whole stack)

  BRANCH, // [P1] [P0] V => $[V?P0:P1]
  COND, // // [P2] [P1] [P0] => $[P0] ? P1 : P2
  IF, // [P2] [P1] [P0] => $[P0] ? $[P1] : $[P2]
  FILTER, // WHERE COMPR
  FORALL,
  // LOOP,
  // RECURSE, RCLIN, RCTAIL, ...
  REDUCE, // FOLD
  RUN, // EXEC? (PROG)
  UNLESS, // [P1] [P0] => $[P0] ? $[] : $[P1]
  WHEN, // [P1] [P0] => $[P0] ? $[P1] : $[]
  XDIP, // ( 2, "execute and reverse: X [P] -> R X" ),
  XDUP, // ( 1, "execute and save: [P] -> [P] R" ),
  XMAP, // ( 2, "execute on each member: ... [L] [P] -> [L']" ),
  WHILE, // ( 2, "execute D while C: ... [D] [C] -> R" ),

  ///// Not pure ...

  CLEAR(-1, 0, "CLR", "CLX", "DROP", "POP", "\u0008", "\u00a2", "clear entry/ top value"),
  CLOSE,
  CLR1(-1, 0, "drop top value"),
  CLR2(-2, 0, "drop top 2 values"),
  CLR3(-3, 0, "drop top 3 values"),
  CLRN(-1, 0, "drop top plus n values"),
  // CLRALL(0, 0, "clear all 'volatile' data (stack + memory)"),
  DATE(0, "current date"),
  EXISTS,
  MMC(-1, 0, "MC", "clear memory value"),
  MMAT, // get variable's N-th subvalue
  MMCA(0, 0, "CLRM", "clear memory (all values)"),
  MMKL, // get value for key within variable
  MMKS, // put value for key into variable
  MMLD(-1, "", "@", "MR", "RCM", "load value from memory"), // "$"
  MMSTO(-2, "", "MS", "DEF", "store value in memory"), // ":"
  MMTO, // put variable as variable's N-th subvalue
  OPEN,
  RCQ(0, "RCL", "recall stashed value Q"),
  RCL(0, "LASTX", "recall previous stack value (L)"),
  SEED(-1, "use as seed for random generator"),
  SEEK,
  STQ(-1, "stash, store as Q"),
  TICS(0, "current time in sec since J2000"),
  TICX(0, "CLOCK", "current (Unix) time in sec"),

  ///// TODO External 0: generic (... ==> SETQ)

  ABOUT(QOpUi.ABOUT, 0, "show license"),
  BAK2EXT(QOpIo.BAK2EXT, 0, "backup data to external storage area"),
  BASSEC(QOpUi.BASSEC, -1, "secondary number base"),
  ESCAPE(0, 0, "escape, reset app's state"),
  EXIT(0, 0, "QUIT", "end program"),
  HELP(QOpUi.HELP, 0, "show help page"),
  LANG(QOpUi.LANG, -1, "switch language"),
  SETQ, // ( -2, "Set option/ configuration value"),
  UICOD(QOpUi.UICOD, -1, "Set UI offset for Unicode characters"), // ==> SETQ
  VIEW(QOpUi.VIEW, -1, "set view (filter/ category)"),

  ///// External 1: mappings

  // DUMP(0, /* "LSALL",*/ "display all"),
  INIT(-1, "initialize: access code e-mail password (host user)."),
  PW(-1, "set overall password"),
  PWAC(-1, "set app's access code"),
  QAT, // "^."
  QATKEY, // "^:"
  QDEL(-1, "QCLR", "delete mapping for given PID"),
  QDFC(-1, "set default value for categories"),
  QFIX, // rename double entries ...
  QHOST(-1, "set values for IMAP/SMTP host: (host user (portImap portSmtp))q"),
  QID(-1, "QPID", "get PIDs for label and current cat: name -> PID"),
  QLOAD(-1, "get data for given PID"),
  QME(-1, "set my e-mail address"),
  // QNEXT(-1, "get next PID for same label"),
  // QQL(-1, "@^", "get data for mapping's label and current cat"),
  // QSTORE(-3, "store new mapping: data cats label ->"),
  QS(-2, ":^", "store mapping for current cats: data label -> PID"),
  QTO,
  QTOKEY,
  QUP(-2, "update/ replace data of mapping: PID data ->"),
  QUPCAT(-2, "change mapping's categories: PID cats ->"),
  // VWCAT( -1, "set cat for viewing data") ==> ... CAT SETQ
  // VWFILTER,

  //  zzEXEC(-1, -1, "GO", "pop operator and apply: ... [OP] -> R"),

  /////

  zzSLOW, // marker

  FACT(1, "", ":!", "FACTORIAL", "factorial"),

  // zzFLOAD,
  // zzFSAVE,

  FDECD, // (-1, "Decode/ decrypt to file, from .dib"),
  FENCD, // (-1, "Encode/ encrypt file as .dib"),

  READ,
  WRITE,

  ARCHIVE, // ( 0, "archive old entries" ),
  EXPALL, // (-1, "export all data (incl. keys!) as plain CSV/TSV (careful!)."),
  EXPORT, // (-1, "export data as plain CSV/TSV to file etc."),
  IMPORT, // (-1, "import data from file etc."),
  SAVTO(QOpFeed.FSAVE, -1, "save all data as encoded copy to named file"),

  ///// Only for networking

  QMACK(-1, "acknowledge invitation or messages for chat"),
  QMINVIT(QOpNet.INVIT, -1, "send invitation or acknowledge contact (exch. keys)"),
  QMRCV(QOpNet.RCV, -1, "activate message receiver for given seconds"),
  QMSEND(-2, "send message: text chat -> (OR: msg '.' ->)");

  public final QEnumIf delegated;

  public final int cArgs;
  public final boolean zipped;
  public final int cReturnValues;
  public final String[] optionals;
  public final String description;
  public final QIfs.QWordIf name;

  public static char[] functSymbols;
  public static QOpMain[] functEnums;

  private static Random zRandom = new Random(42);
  private static final double TRUE_D4 = -Dib2Constants.INT_D4_FACT;

  /** For registering special cases such as "EXEC" ... */
  public static ConcurrentHashMap<String, QEnumIf> opsInternal =
      new ConcurrentHashMap<String, QIfs.QEnumIf>();

  /////

  private QOpMain() {
    delegated = null;
    cArgs = -1;
    zipped = true;
    cReturnValues = 0;
    optionals = new String[0];
    description = null;
    name = QWord.createQWord(name(), true);
  }

  private QOpMain(QEnumIf xmDelegation, int xcArgs, String xmDescription) {
    delegated = xmDelegation;
    cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
    zipped = false;
    cReturnValues = 0;
    optionals = new String[0];
    description = xmDescription;
    name = QWord.createQWord(name(), true);
  }

  private QOpMain(int xcArgs, int xcReturnValues, String... xmOptionals) {
    delegated = null;
    cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
    zipped = (0 < xcArgs) && (1 >= xcReturnValues);
    cReturnValues = xcReturnValues;
    optionals =
        ((null != xmOptionals) && (1 <= xmOptionals.length))
            ? Arrays.copyOf(xmOptionals, xmOptionals.length - 1)
            : xmOptionals;
    description =
        ((null != xmOptionals) && (1 <= xmOptionals.length))
            ? (xmOptionals[xmOptionals.length - 1])
            : null;
    name = QWord.createQWord(name(), true);
  }

  private QOpMain(int xcArgs, String... xmOptionals) {
    delegated = null;
    cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
    zipped = (0 < xcArgs);
    cReturnValues = 1;
    optionals =
        ((null != xmOptionals) && (1 <= xmOptionals.length))
            ? Arrays.copyOf(xmOptionals, xmOptionals.length - 1)
            : xmOptionals;
    description =
        ((null != xmOptionals) && (1 <= xmOptionals.length))
            ? (xmOptionals[xmOptionals.length - 1])
            : null;
    name = QWord.createQWord(name(), true);
  }

  public static QIfs.QEnumIf[] create() {
    TreeMap<Integer, QOpMain> map = new TreeMap<Integer, QOpMain>();
    for (QOpMain funct : QOpMain.values()) {
      for (String opt : funct.optionals) {
        if (1 == opt.length()) {
          map.put(opt.charAt(0) & 0xffff, funct);
        }
      }
    }
    functSymbols = new char[map.size()];
    functEnums = new QOpMain[functSymbols.length];
    int cnt = 0;
    for (Integer ch : map.keySet()) {
      functSymbols[cnt] = (char) (int) ch;
      functEnums[cnt++] = map.get(ch);
    }
    return values();
  }

  /////

  public String getOperator() {
    return ((1 < optionals.length) && (0 >= optionals[0].length())) ? optionals[1] : null;
  }

  public String getOperatorOrName() {
    return ((1 < optionals.length) && (0 >= optionals[0].length())) ? optionals[1] : name();
  }

  public String getDescription() {
    StringBuilder out = new StringBuilder(100);
    if (null == description) {
      out.append('.');
    }
    out.append(name() + " (" + cArgs + ")  \t");
    for (String opt : optionals) {
      if (0 < opt.length()) {
        out.append(" " + opt + ' ');
      }
    }
    if (null != description) {
      out.append("\t" + description);
    }
    return out.toString();
  }

  public static double getNextRandom() {
    return zRandom.nextDouble();
  }

  //  public static QSeq convertDat(double numD4, QOpMain conv) {
  //    QSeq out0 = QWord.NaN;
  //    if (!Double.isNaN(numD4)) {
  //      long vx = ((long) numD4) / 3;
  //      final long xmint = vx / 100 % 100;
  //      final long xsec = vx % 100;
  //      vx /= 10000;
  //      String str = "";
  //      switch (conv) {
  //        case DATI:
  //          str =
  //              String.format(
  //                  "%d-%02d-%02dT%02d:%02d",
  //                  (int) vx / 10000, ((int) vx / 100) % 100, (int) vx % 100, xmint, xsec);
  //          break;
  //        case DATT:
  //          str = DateFunc.dateLocal4Millis(((long) numD4) / (Dib2Constants.INT_D4_FACT_LONG /
  // 1000));
  //          break;
  //          // case DATSL_DEPR:
  //          // str = DateFunc.date4SlotSecondApprox_OLD(numD4);
  //          // break;
  //        default:;
  //      ;
  //      }
  //      out0 = QWord.createQWord(str, false);
  //    }
  //    return out0;
  //  }

  public static QSeq getConst(String xName) {
    String name = xName;
    if ((null == name) || (0 >= name.length())) {
      return QWord.NaN;
    }
    // TODO BigSxg
    if ("YS".equals(xName)) {
      return QWord.createQWord(Dib2Constants.YEAR_SECONDS_YEAR);
    }
    if (('?' == name.charAt(0)) || ('*' == name.charAt(0))) {
      name = name.substring(1).toLowerCase(Locale.ROOT);
      final int max = 100;
      QIfs.QWordIf[] out = ('*' == xName.charAt(0)) ? new QIfs.QWordIf[max] : null;
      int cnt1 = 1;
      for (Codata nx : Codata.values()) {
        if (nx.name().toLowerCase(Locale.ROOT).contains(name) && (cnt1 < max)) {
          if (null == out) {
            if (nx.name().equals(xName.substring(1))) {
              return QSeq.createQSeq(
                  nx.quantity
                      + ((0 >= nx.unit.length()) ? "" : ": " + nx.unit)
                      + ((0 == nx.uncertainty) ? "" : (" (uncert.: " + nx.uncertainty + ")")));
            }
            continue;
          }
          out[-1 + (cnt1++)] = QWord.createQWordAscii(nx.name());
        }
      }
      for (Units nx : Units.values()) {
        if (nx.name().toLowerCase(Locale.ROOT).contains(name) && (cnt1 < max)) {
          if (null == out) {
            if (nx.name().equals(xName.substring(1))) {
              return QSeq.createQSeq(": " + nx.units);
            }
            continue;
          }
          out[-1 + (cnt1++)] = QWord.createQWordAscii(nx.name());
        }
      }
      return (1 >= cnt1) ? QWord.NaN : QSeq.createQSeq(Arrays.copyOf(out, cnt1 - 1));
    }
    Codata outDat = null;
    try {
      outDat = Codata.valueOf(name);
      return QWord.createQWord(outDat.value);
    } catch (Exception e0) {
    }
    Units outUnit = null;
    try {
      outUnit = Units.valueOf(name);
    } catch (Exception e0) {
      name = name.toUpperCase(Locale.ROOT);
      for (Units unit : Units.values()) {
        if (unit.name().toUpperCase(Locale.ROOT).equals(name)) {
          outUnit = unit;
          break;
        }
      }
    }
    if (null == outUnit) {
      return null;
    }
    QIfs.QWordIf[] units = QSeq.words4String(outUnit.units.replace(' ', '_'));
    QIfs.QWordIf[] valUnits = new QIfs.QWordIf[1 + units.length];
    System.arraycopy(units, 0, valUnits, 1, units.length);
    valUnits[0] = QWord.createQWord(outUnit.val);
    return QSeq.createQSeq(valUnits);
  }

  public static QSeq convert(QSeq value, QSeq markers) {
    Object out = null;
    String mkTo = markers.at(0).toString();
    String mkFrom = markers.at(1).toString();
    if (!mkTo.toUpperCase(Locale.ROOT).startsWith("DT")) {
      return QSeq.createQSeq("Not implemented yet");
    }
    mkTo = mkTo.substring(2).toUpperCase(Locale.ROOT);
    mkTo = ("C".equals(mkTo)) ? "." : mkTo;
    mkFrom = mkFrom.toUpperCase(Locale.ROOT);
    mkFrom = mkFrom.startsWith("DT") ? mkFrom.substring(2) : mkFrom;
    if ((value instanceof QWord) && (((QWord) value).isDate())) {
      out = DateFunc.convert4Hash62(value.getShash(), mkTo);
    } else if ((value instanceof QWord) && (((QWord) value).isNumeric())) {
      long hash62 = ((QWord) value).qValLong;
      if (0 == hash62) {
        hash62 = DateFunc.convert2Hash62(((QWord) value).d4() * Dib2Constants.INT_D4_F_INV, mkFrom);
      }
      out = DateFunc.convert4Hash62(hash62, mkTo);
    } else {
      final long hash62 = DateFunc.convert2Hash62(value.toStringFull(), mkFrom);
      out = DateFunc.convert4Hash62(hash62, mkTo);
    }
    if (out instanceof Long) {
      return QWord.createQWordDateOrTickRef((Long) out);
    } else if (out instanceof Double) {
      return QWord.createQWord((Double) out);
    } else if (out instanceof String) {
      return QSeq.createQSeq((String) out);
    }
    return QWord.NaN;
  }

  public static QSeq doRegEx(
      JResult pooled, String str, String pattern, double inxD4, QOpMain kind) {
    QSeq out0 = QWord.NaN;
    try {
      int inx = (int) (inxD4 * Dib2Constants.INT_D4_F_INV);
      final Pattern pat = Pattern.compile(pattern);
      final Matcher mat = pat.matcher(str);
      if ((kind == QOpMain.RXG) || (kind == QOpMain.ZRXG)) {
        if (mat.find()) {
          out0 = QWord.createQWord(mat.group(inx), true);
        }
        return out0;
      }
      int count = (0 <= inx) ? inx : 99999;
      int i0 = -1;
      while (mat.find()) {
        ++i0;
        if (i0 >= count) {
          break;
        }
      }
      if (0 > inx) {
        count = i0 + inx + 1;
        i0 = count - 1;
        if (0 <= count) {
          mat.reset();
          i0 = -1;
          while (mat.find()) {
            ++i0;
            if (i0 >= count) {
              break;
            }
          }
        }
      }
      if (i0 < count) {
        out0 = QWord.NaN;
      } else if ((kind == RXT) || (kind == ZRXT)) {
        out0 = QSeq.createQSeq(mat.group());
      } else {
        out0 = QWord.createQWordD4(mat.start() * Dib2Constants.INT_D4_FACT);
      }
    } catch (Exception e) {
      out0 = QWord.NaN;
    }
    return out0;
  }

  /** Handle numeric cases. Return null if n.a. */
  public QSeq calcD4(QSeq[] argsD4) {
    if (cArgs > argsD4.length) {
      return null;
    }
    final double vAD4 = (0 >= cArgs) ? 0.0 : ((QWord) argsD4[0]).d4();
    final double vBD4 = (1 >= cArgs) ? 0.0 : ((QWord) argsD4[1]).d4();
    double out0 = Double.NaN;
    int len;
    try {
      long va = 0;
      long vb = 0;
      switch (this) {
        case ABS:
          out0 = (0 > vAD4) ? (-vAD4) : vAD4;
          break;
        case ACOS:
          out0 = Math.acos(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case ADD:
          out0 = vAD4 + vBD4;
          break;
        case AND:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va & vb) * Dib2Constants.INT_D4_FACT;
          break;
        case ANDT:
          out0 = ((0 != (long) vAD4) && (0 != (long) vBD4)) ? TRUE_D4 : 0;
          break;
        case ASIN:
          out0 = Math.asin(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case ATAN:
          out0 = Math.atan(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case CEIL:
          out0 = Math.ceil(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case COMP:
          out0 =
              MiscFunc.equalRounded(vAD4, vBD4)
                  ? 0.0
                  : ((vAD4 > vBD4) ? Dib2Constants.INT_D4_FACT : -Dib2Constants.INT_D4_FACT);
          break;
        case COS:
          out0 = Math.cos(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case COSH:
          out0 = Math.cosh(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case DEG:
          out0 = vAD4 * 180.0 / Math.PI;
          break;
        case DIV:
          out0 = (vAD4 * Dib2Constants.INT_D4_FACT / vBD4);
          break;
        case DROUND:
          out0 = (0.0 <= vAD4) ? vAD4 : -vAD4;
          if ((1.0e-6 * Dib2Constants.INT_D4_FACT) > out0) {
            out0 = 0;
          } else if (out0 < 1.0e12) {
            out0 = Math.round(vAD4 * 128) * (1.0 / 128);
          } else {
            out0 = vAD4;
          }
          break;
        case E:
          out0 = Math.E * Dib2Constants.INT_D4_FACT;
          break;
        case EQ:
          out0 = MiscFunc.equalRounded(vAD4, vBD4) ? TRUE_D4 : 0.0;
          break;
        case FACT:
          va = (long) (vAD4 * Dib2Constants.INT_D4_F_INV);
          if (200 >= va) {
            out0 = va;
            for (int i0 = (int) va - 1; i0 >= 2; --i0) {
              out0 *= i0;
            }
            out0 *= Dib2Constants.INT_D4_FACT;
          } else {
            out0 = Double.NaN;
          }
          break;
        case FALSE:
          out0 = 0.0;
          break;
        case FLOOR:
          out0 = Math.floor(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case FRAC:
          va = ((long) (vAD4 * Dib2Constants.INT_D4_F_INV)) * Dib2Constants.INT_D4_FACT_LONG;
          out0 = vAD4 - va;
          break;
        case GE:
          out0 = (MiscFunc.equalRounded(vAD4, vBD4) || (vAD4 >= vBD4)) ? TRUE_D4 : 0.0;
          break;
          // case GET:
        case GRAD:
          out0 = vAD4 * 200.0 / Math.PI;
          break;
        case GT:
          out0 = (!MiscFunc.equalRounded(vAD4, vBD4) && (vAD4 > vBD4)) ? TRUE_D4 : 0.0;
          break;
        case IDIV:
          va = (long) MiscFunc.roundForRxxUsage(vAD4) / (long) MiscFunc.roundForRxxUsage(vBD4);
          out0 = va * Dib2Constants.INT_D4_FACT;
          break;
        case INF:
          out0 = Double.POSITIVE_INFINITY;
          break;
        case INT:
          va = ((long) (vAD4 * Dib2Constants.INT_D4_F_INV)) * Dib2Constants.INT_D4_FACT_LONG;
          out0 = va;
          break;
        case IST:
          out0 = (0 != (long) vAD4) ? TRUE_D4 : 0;
          break;
        case LE:
          out0 = (MiscFunc.equalRounded(vAD4, vBD4) || (vAD4 <= vBD4)) ? TRUE_D4 : 0.0;
          break;
        case LOG10:
          out0 = Math.log10(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case LN:
          out0 = Math.log(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case LT:
          out0 = (!MiscFunc.equalRounded(vAD4, vBD4) && (vAD4 < vBD4)) ? TRUE_D4 : 0.0;
          break;
        case MAX2:
          out0 = (vAD4 >= vBD4) ? vAD4 : vBD4;
          break;
        case MIN2:
          out0 = (vAD4 <= vBD4) ? vAD4 : vBD4;
          break;
        case MOD:
          va = (long) (vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) (((0 <= vBD4) ? (long) vBD4 : -(long) vBD4) * Dib2Constants.INT_D4_F_INV);
          out0 = (va % vb);
          out0 = ((0 <= out0) ? out0 : (out0 + vb)) * Dib2Constants.INT_D4_FACT;
          break;
        case MUL:
          out0 = vAD4 * vBD4 * Dib2Constants.INT_D4_F_INV;
          break;
        case NADD:
          out0 = -vAD4;
          break;
        case NAN:
          out0 = Double.NaN;
          break;
        case NAND:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          // Up to 64 - 22 = 42 bits:
          out0 = (((~(va & vb)) << 22) >>> 22) * Dib2Constants.INT_D4_FACT;
          break;
        case NE:
          out0 = MiscFunc.equalRounded(vAD4, vBD4) ? 0.0 : TRUE_D4;
          break;
        case NMULT:
          out0 = Dib2Constants.INT_D4_FACT * Dib2Constants.INT_D4_FACT / vAD4;
          break;
        case NOR:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          // Up to 64 - 22 = 42 bits:
          out0 = (((~(va | vb)) << 22) >>> 22) * Dib2Constants.INT_D4_FACT;
          break;
        case NOT:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          // Up to 64 - 22 = 42 bits:
          out0 = (((~va) << 22) >>> 22) * Dib2Constants.INT_D4_FACT;
          break;
        case NOTT:
          out0 = (0 != (long) vAD4) ? 0 : TRUE_D4;
          break;
        case OR:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va | vb) * Dib2Constants.INT_D4_FACT;
          break;
        case ORT:
          out0 = ((0 != (long) vAD4) || (0 != (long) vBD4)) ? TRUE_D4 : 0;
          break;
        case PERCENT:
          out0 = vAD4 / 100.0;
          break;
        case PI:
          out0 = Math.PI * Dib2Constants.INT_D4_FACT;
          break;
        case POWER:
          out0 =
              Math.pow(vAD4 * Dib2Constants.INT_D4_F_INV, vBD4 * Dib2Constants.INT_D4_F_INV)
                  * Dib2Constants.INT_D4_FACT;
          break;
        case PRED:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va - 1) * Dib2Constants.INT_D4_FACT;
          break;
        case RADD:
          out0 = vAD4 * Math.PI / 180.0;
          break;
        case RADG:
          out0 = vAD4 * Math.PI / 200.0;
          break;
        case RANDOM:
          len = (int) (vAD4 * Dib2Constants.INT_D4_F_INV);
          ///// Check memory
          try {
            QWord[] result = new QWord[len];
            for (int i0 = 0; i0 < len; ++i0) {
              result[i0] = QWord.createQWordD4(getNextRandom() * Dib2Constants.INT_D4_FACT);
            }
            return QSeq.createQSeq(result);
          } catch (Throwable e) {
            out0 = Double.NaN;
          }
          break;
        case RANGE:
          len = argsD4[0].size();
          {
            double num = (1 >= len) ? 0.0 : argsD4[0].atomAt(0).d4();
            final double e0 = (2 >= len) ? Dib2Constants.INT_D4_FACT : argsD4[0].atomAt(2).d4();
            len = (int) ((argsD4[0].atomAt((1 < len) ? 1 : 0).d4() - num) / e0 + 0.99999);
            len = (0 >= len) ? 1 : len;
            ///// Check memory
            try {
              final QWord[] words = new QWord[len];
              for (int i0 = 0; i0 < len; ++i0) {
                words[i0] = QWord.createQWordD4(num);
                num += e0;
                num = MiscFunc.roundForRxxUsage(num);
              }
              return QSeq.createQSeq(words);
            } catch (Throwable e) {
              out0 = Double.NaN;
            }
          }
          break;
        case REM:
          va = (long) (vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) (vBD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va % vb) * Dib2Constants.INT_D4_FACT;
          break;
        case RND1:
          out0 = getNextRandom() * Dib2Constants.INT_D4_FACT;
          break;
        case ROUND:
          out0 = Math.round(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case SEED:
          va = (long) (vAD4 * Dib2Constants.INT_D4_F_INV);
          zRandom = new Random(va);
          out0 = va * Dib2Constants.INT_D4_FACT;
          break;
        case SIGN:
          out0 =
              MiscFunc.equalRounded(vAD4, 0.0)
                  ? 0.0
                  : ((0.0 < vAD4) ? Dib2Constants.INT_D4_FACT : -Dib2Constants.INT_D4_FACT);
          break;
        case SIN:
          out0 = Math.sin(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case SINH:
          out0 = Math.sinh(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
          // case SIZE:
        case SHL:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          out0 = ((0 <= vb) ? (va << vb) : (va >>> -vb)) * Dib2Constants.INT_D4_FACT;
          break;
        case SHRA:
        case SHRL:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          if (0 > vb) {
            out0 = (va << -vb) * Dib2Constants.INT_D4_FACT;
          } else {
            out0 = ((SHRA == this) ? (va >> vb) : (va >>> vb)) * Dib2Constants.INT_D4_FACT;
          }
          break;
        case SQRT:
          out0 = Math.sqrt(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case SQUARE:
          out0 = vAD4 * vAD4 * Dib2Constants.INT_D4_F_INV;
          break;
        case SUB:
          out0 = vAD4 - vBD4;
          break;
        case SUCC:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va + 1) * Dib2Constants.INT_D4_FACT;
          break;
        case TAN:
          out0 = Math.tan(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case TANH:
          out0 = Math.tanh(vAD4 * Dib2Constants.INT_D4_F_INV) * Dib2Constants.INT_D4_FACT;
          break;
        case TAU:
          out0 = Math.PI * 2 * Dib2Constants.INT_D4_FACT;
          break;
          // case TICK:
        case TRUE:
          out0 = TRUE_D4;
          break;
        case TRUNC:
          out0 =
              ((0.0 <= vAD4)
                      ? Math.floor(vAD4 * Dib2Constants.INT_D4_F_INV)
                      : Math.ceil(vAD4 * Dib2Constants.INT_D4_F_INV))
                  * Dib2Constants.INT_D4_FACT;
          break;
        case XOR:
          va = (long) MiscFunc.roundForRxxUsage(vAD4 * Dib2Constants.INT_D4_F_INV);
          vb = (long) MiscFunc.roundForRxxUsage(vBD4 * Dib2Constants.INT_D4_F_INV);
          out0 = (va ^ vb) * Dib2Constants.INT_D4_FACT;
          break;
        default:
          return null;
      }
    } catch (Exception e) {
      out0 = Double.NaN;
    }
    out0 = MiscFunc.roundForRxxUsage(out0);
    return QWord.createQWordD4(out0);
  }

  /** Handle scalars and simple cases. */
  public Object calc(QSeq[] args) {
    if ((null == args) || (cArgs > args.length)) {
      return null;
    }
    final JResult pooled = JResult.get8Pool();
    if ((2 == cArgs) && args[0].atom().isNumeric() && args[1].atom().isNumeric()) {
      switch (this) {
        case MINUS:
          return SUB.calc(args);
        case PLUS:
          return ADD.calc(args);
        case PART:
          return DIV.calc(args);
        case TIMES:
          return MUL.calc(args);
        default:
          ;
      }
    }
    QSeq out0 = null;
    QSeq out1 = null;
    // boolean todo = false;
    String str = "";
    String str2;
    double num;
    try {
      switch (this) {
          //    case ALL:
          //      todo = true;
          //      break;
          //    case AT:
          //      todo = true;
          //      break;
        case CHOICE:
          num = ((QWord) args[2]).d4();
          if (!Double.isNaN(num) && (0 != (1 & (long) (num * Dib2Constants.INT_D4_F_INV)))) {
            return args[1];
          }
          return args[0];
        case CLEAR:
        case CLR1:
        case CLR2:
        case CLR3:
          return null;
          //        case CONC:
          //          todo = true;
          //          break;
        case CONST:
          return getConst(args[0].toStringFull());
        case CONVT:
          return convert(args[0], args[1]);
        case DATE:
          return QWord.createQWordDate(DateFunc.date4Millis());
        case DTDT:
        case DTTS:
        case DTTY:
        case DTYD:
          return convert(
              args[0],
              QSeq.createQSeq(this.name().substring(0, 3) + ' ' + this.name().substring(3)));
        case DUP:
          return new QSeq[] {args[0], args[0]};
          //    case EDUPTO:
          //      todo = true;
          //      break;
          //    case EMPTY:
          //      todo = true;
          //      break;
          //        case EQ:
          //          todo = true;
          //          break;
          //    case EXISTS:
          //      todo = true;
          //      break;
          //    case EXPAND:
          //      todo = true;
          //      break;
          //    case RXSEL:
          //      todo = true;
          //      break;
          //        case GE:
          //          todo = true;
          //          break;
          //        case GT:
          //          todo = true;
          //          break;
          //        case INTRS:
          //          todo = true;
          //          break;
          //        case ISTYPE:
          //          todo = true;
          //          break;
          // case IDAT:
          // str = args[0].toStringFull().replaceAll("[^0-9T]", "").replace('T', '.');
          //        long v0 = DateFunc.leapMinute256ForDate( date );..
          // return QSeq.createQSeq(str);
          //        case IN:
          //          todo = true;
          //          break;
          //        case LE:
          //          todo = true;
          //          break;
        case LENGTH:
          str = args[0].toStringFull();
          return QWord.createQWordInt(str.length());
          //        case LT:
          //          todo = true;
          //          break;
          //          //    case EMAP:
          //          //      todo = true;
          //          //      break;
          //          //    case MSEC:
          //          //      todo = true;
          //          //      break;
          //        case NE:
          //          todo = true;
          //          break;
        case NOP:
          return null;
          //    case OF:
          //      todo = true;
          //      break;
          //    case ONEOF:
          //      todo = true;
          //      break;
        case PLUS:
          if ((args[0] instanceof QWord) && (args[1] instanceof QWord)) {
            return QWord.createQWord((args[0].toString() + args[1].toString()), true);
          }
          return args[0].append(args[1]);
          //    case RLDOWN:
          //      todo = true;
          //      break;
          //    case RLUP:
          //      todo = true;
          //      break;
          //    case ROT:
          //      todo = true;
          //      break;
        case PRODUCT:
          num = Dib2Constants.INT_D4_FACT;
          for (int i0 = args[0].size() - 1; i0 >= 0; --i0) {
            final QIfs.QWordIf v0 = args[0].at(i0);
            final double nx = (v0 instanceof QWord) ? ((QWord) v0).d4() : Double.NaN;
            if (Double.isNaN(nx)) {
              return null;
            }
            num = num * nx * Dib2Constants.INT_D4_F_INV; // * Dib2Constants.INT_D4_F_INV);
          }
          return QWord.createQWordD4(num);
        case RXG:
        case RXN:
        case RXT:
          return doRegEx(
              pooled, args[0].toStringFull(), args[1].toStringFull(), ((QWord) args[2]).d4(), this);
        case RX:
          return doRegEx(pooled, args[0].toStringFull(), args[1].toStringFull(), 0.0, this);
          //    case SELECT:
          //      todo = true;
          //      break;
          //    case SIZE:
          //      todo = true;
          //      break;
          //    case SLICE:
          //      todo = true;
          //      break;
          //    case SMALL:
          //      todo = true;
          //      break;
          //    case SOME:
          //      todo = true;
          //      break;
          //    case SORT:
          //      todo = true;
          //      break;
        case SPLITAT:
          num = ((QWord) args[1]).d4() * Dib2Constants.INT_D4_F_INV;
          str = args[0].toStringFull();
          out0 = QSeq.createQSeq(str.substring(0, (int) num));
          out1 = QSeq.createQSeq(str.substring((int) num));
          return new QSeq[] {out0, out1};
          //    case SUPSET:
          //      todo = true;
          //      break;
        case SUM:
          num = 0.0;
          for (int i0 = args[0].size() - 1; i0 >= 0; --i0) {
            final QIfs.QWordIf v0 = args[0].at(i0);
            final double nx = (v0 instanceof QWord) ? ((QWord) v0).d4() : Double.NaN;
            if (Double.isNaN(nx)) {
              return null;
            }
            num += nx;
          }
          return QWord.createQWordD4(num);
        case SWAP:
          return new QSeq[] {args[1], args[0]};
          //    case TAKE:
          //      todo = true;
          //      break;
        case TICS:
          return QWord.createQWordD4(
              DateFunc.currentTimeNanobisLinearized(false)
                  * Dib2Constants.INT_D4_FACT
                  * (1.0 / (1 << 30)));
        case TICX:
          return QWord.createQWordD4(
              DateFunc.currentTimeMillisLinearized() * Dib2Constants.INT_D4_FACT / 1000);
        case TIMES:
          str = args[0].toStringFull();
          double factor = ((QWord) args[1]).d4() * Dib2Constants.INT_D4_F_INV;
          if ((2 <= factor) && (factor <= 10000)) {
            int i0 = (int) factor;
            StringBuilder out = new StringBuilder(i0 * str.length() + 2);
            for (; i0 > 0; --i0) {
              out.append(str);
            }
            str = out.toString();
            out0 = QSeq.createQSeq(str);
          }
          return out0;
          //    case UNION:
          //      todo = true;
          //      break;
        case ZRXG:
        case ZRXN:
        case ZRXT:
          return QOpMain.doRegEx(
              pooled,
              args[0].toStringFull(),
              args[1].toStringFull(),
              (int) ((QWord) args[2]).d4() * Dib2Constants.INT_D4_F_INV,
              this);
        case ZRX:
          return QOpMain.doRegEx(pooled, args[0].toStringFull(), args[1].toStringFull(), 0.0, this);
        case ZSPLIT:
          str2 = args[1].toStringFull();
          str = args[0].toStringFull();
          {
            String[] a0 = str.split(str2, -1);
            out0 = QSeq.createQSeq(a0[0]);
            out1 = QSeq.createQSeq(a0[1]);
            return QSeq.createFlat(out0, out1);
          }
        default:
          out0 = calcD4(args);
      }
      //      if (todo) {
      //        out0 = QSeq.createQSeq("(function not implemented)");
      //      }
    } catch (Exception e) {
      return null;
    }
    return out0;
  }
  // =====

  @Override
  public long getShash() {
    //    return ('C' << 32) | ('A' << 24) | ('L' << 16) | ('C' << 8) | ordinal();
    return ShashFunc.shashBits4Ansi(name());
  }
}
