// Copyright (C) 2018, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import com.gitlab.dibdib.picked.common.*;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.*;
import java.util.*;
import net.sf.dibdib.config.*;

/** Utilities for the String class and byte[]. Cmp. http://www.unicode.org, RFC 1345. */
public final class StringFunc {
  // =====

  /** Assuming a 1:1 mapping for bytes ... :-( */
  public static final Charset CHAR8 = Charset.forName("ISO-8859-1");
  /** Trying to go for UTF-8 as much as possible ... :-( */
  public static final Charset CHAR16UTF8 = Charset.forName("UTF-8");

  ///// Control char assignments: UI, markers

  public static final char ESCAPE = '\u001b';
  public static final char EOF = '\u001a';
  public static final char PAR_X = '\u0014';
  public static final char SECT_X = '\u0015';
  public static final char CR = '\r';
  public static final char FF = '\u000c';
  public static final char ALT = '\u000b';
  public static final char LF = '\n';
  public static final char TAB = '\t';
  public static final char BULLET_X = '\u0007'; // BELL
  public static final char DELTA_X = '\u007f';

  public static final char ZOOM_OUT = '\u001e';
  public static final char ZOOM_IN = '\u001d';
  public static final char PUSH = EOF; // ==> finish, re-draw
  public static final char XCUT = '\u0019';
  public static final char XCOPY = '\u0018';
  public static final char XPASTE = '\u0017';
  public static final char PSHIFT = '\u0013';
  public static final char SHIFT = '\u0012';
  public static final char SCROLL_RIGHT = '\u0011';
  public static final char SCROLL_DOWN = '\u0010';
  public static final char SCROLL_UP = '\u000f';
  public static final char SCROLL_LEFT = '\u000e';
  public static final char BACKSP = '\u0008';
  public static final char MOVE_RIGHT = '\u0005';
  public static final char MOVE_DOWN = '\u0004';
  public static final char MOVE_UP = '\u0003';
  public static final char MOVE_LEFT = '\u0002';
  public static final char STEP = '\u0001'; // ==> execute single step/ send

  public static final char LF0 = '\u2029';
  public static final char BULLET = '\u2022';
  public static final char NBH0 = '\u2010';
  public static final char ZW0 = '\u200b';
  public static final char PAR = '\u00b6';
  public static final char SH0 = '\u00ad';
  public static final char SECT = '\u00a7';
  public static final char NBS0 = '\u00a0';

  public static final char SH = '\u001f';
  public static final char NBS = '\u001c';
  public static final char NBH = '\u0016';
  // public static final char TEMPLATE_END = SCROLL_RIGHT; // DLE
  public static final char TEMPLATE_END = SCROLL_DOWN; // SI (SO SI = stop last level's template)
  public static final char TEMPLATE_START = SCROLL_UP; // SO (SO template SI = apply template)
  public static final char ZW = '\u0006';
  public static final char LIST_END = MOVE_LEFT;
  public static final char LIST_START = MOVE_RIGHT; // (not within TEMPLATE OR QUOTE)
  public static final char QUOTE_END = MOVE_DOWN;
  public static final char QUOTE_START = MOVE_UP; // (not within TEMPLATE)

  public static final String XCHARS = "" + NBS0 + SECT + SH0 + PAR + ZW0 + NBH0 + LF0 + DELTA_X;
  public static final String XDELIMS = "" + CR + FF + LF + TAB + SH + NBS + NBH + ZW;
  public static final String XSTRUCS =
      ""
          + TEMPLATE_START
          + TEMPLATE_END
          + LIST_START
          + LIST_END
          + QUOTE_START
          + QUOTE_END
          + "\u0000\u0001";

  public static final char[] HEX = "0123456789ABCDEF".toCharArray();
  public static final byte[] NIBBLE = {
    9, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, // -
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
  };

  /////

  public static final char[] kControlAsButton = {
    // 0, SEND, LEFT, UP, DOWN, RIGHT,
    0, '>', '\u2190', '\u2191', '\u2193', '\u2192',
    // 0, 0, BACKSP, TAB
    0, 0, '\u226e', '\u21a0',
    // LF, VT, FF, CR
    '\u21b4', '\u21e9', 0, '\u21b2',
    // SCR-L, SCR-UP, SCR-DOWN, SCR-R, SHIFT
    '\u21d0', '\u21d1', '\u21d3', '\u21d2', '\u21e7',
    // PSHIFT, 0, 0, NBH, PASTE, COPY, CUT
    '\u21d6', 0, 0, '-', '\u21ca', '\u21c9', '\u21c8',
    // EOF, ESC, NBS
    '\u2584', '\u2297', ' ',
    // ZOOM-IN, ZOOM-OUT, SH
    '\u2295', '\u2299', '\u2194',
  };

  private static final byte[] kAlpha1345Caps =
      ( // -
          /* ---!"#$%&'()*+,-./0123456789:;<=>?----- U:alpha, OO: blank, Ox: RFU
          /* */ "INMSPAYCZXHJFTV0123456789BRKEDQ")
          .getBytes(StringFunc.CHAR8);
  private static final char[] kAlpha1345Cap2Sym = new char['Z' + 1];
  private static final HashMap<String, Character> kRfc1345ToChar = new HashMap<String, Character>();
  private static final HashMap<Character, String> kChar2Rfc1345 = new HashMap<Character, String>();

  /*
  private static byte[] Pc8BordersOffsB0_2_UnicodePage25 = {
      0x91, 0x92, 0x93, 0x02, 0x24, 0x61, 0x62, 0x56,
      0x55, 0x63, 0x51, 0x57, 0x5d, 0x5c, 0x5b, 0x10,
      0x14, 0x34, 0x2c, 0x1c, 0x00, 0x3c, 0x5e, 0x5f,
      0x5a, 0x54, 0x69, 0x66, 0x60, 0x50, 0x6c, 0x67,
      0x68, 0x64, 0x65, 0x59, 0x58, 0x52, 0x53, 0x6b,
      0x6a, 0x18, 0x0c, 0x88, 0x84, 0x8c, 0x90, 0x80,
  };
  */

  private static char[] Cp125xOffs7f_2_Unicode = {
    0x0394, // 0x7F DELTA/ HOUSE
    0x20AC, // 0x80 EURO SIGN
    0x067E, // 0x81 A81
    0x201A, // 0x82 SINGLE LOW-9 QUOTATION MARK
    0x0192, // 0x83 LATIN SMALL LETTER F WITH HOOK
    0x201E, // 0x84 DOUBLE LOW-9 QUOTATION MARK
    0x2026, // 0x85 HORIZONTAL ELLIPSIS
    0x2020, // 0x86 DAGGER
    0x2021, // 0x87 DOUBLE DAGGER
    0x02C6, // 0x88 MODIFIER LETTER CIRCUMFLEX ACCENT
    0x2030, // 0x89 PER MILLE SIGN
    0x0160, // 0x8A LATIN CAPITAL LETTER S WITH CARON
    0x2039, // 0x8B SINGLE LEFT-POINTING ANGLE QUOTATION MARK
    0x0152, // 0x8C LATIN CAPITAL LIGATURE OE
    0x0686, // 0x8D A8D
    0x0698, // 0x8E A8E
    0x0688, // 0x8F A8F
    0x06AF, // 0x90 A90
    0x2018, // 0x91 LEFT SINGLE QUOTATION MARK
    0x2019, // 0x92 RIGHT SINGLE QUOTATION MARK
    0x201C, // 0x93 LEFT DOUBLE QUOTATION MARK
    0x201D, // 0x94 RIGHT DOUBLE QUOTATION MARK
    0x2022, // 0x95 BULLET
    0x2013, // 0x96 EN DASH
    0x2014, // 0x97 EM DASH
    0x02DC, // 0x98 SMALL TILDE
    0x2122, // 0x99 TRADE MARK SIGN
    0x0161, // 0x9A LATIN SMALL LETTER S WITH CARON
    0x203A, // 0x9B SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
    0x0153, // 0x9C LATIN SMALL LIGATURE OE
    0x200C, // 0x9D ZWNJ
    0x200D, // 0x9E ZWJ
    0x0178, // 0x9F LATIN CAPITAL LETTER Y WITH DIAERESIS
  };

  private static Collator coll = null;

  ///// Trying to cover for Unicode's spread regarding Latin scripts, Greek, IPA,
  ///// transliterations, and phonemic alphabets.

  private static byte[] coll64Key4Char = new byte[0x3e2];

  private static char[] coll64CharUpper4Key = new char[64];

  private static char[] coll64Char4Key =
      new char[] {
        0, // < (skip)
        0x5f, // _/' ' // TODO 0x1 (SEND) at end of word as marker for operators
        0x2d, // -
        0x3a, // :/, // 0x2c, // , 0x3b, // ; 0x3a, // :  0x2e,
        0x2e, // .
        // 0x21, // ! 0x3f, // ?
        // 0x2f, // /  0x5e, // ^   0x27, // ' 0x22, // " // 0xab, // « // 0xbb, // »
        // 0x40, // @
        // 0x24, // $
        // 0x2a, // *
        // 0x26, // &
        // 0x23, // #
        // 0x25, // %
        // 0x2b, // +
        // 0x3d, // = 0x7c, // |

        0x30, // 0
        0x31, // 1
        0x32, // 2
        0x33, // 3
        0x34, // 4
        0x35, // 5
        0x36, // 6
        0x37, // 7
        0x38, // 8
        0x39, // 9  // 0xbc, // 1/4 // 0xbd, // 1/2 // 0xbe, // 3/4

        ///////// L I    U  H  G  C  A
        0x294, //  P A 6 .  .  h  .  ?
        0x061, // a . 5  a  A  A  a  a
        0x062, // b B    b  B  B  b  b
        0x063, // c C    g  G  g  v  g
        0x0F0, // . D    x  .  .  g  d
        0x064, // d .    d  D  d  d  e
        0x065, // e 3 9  h  H  E  e  z
        0x25B, // . E    .  .  .  e: e:
        0x066, // f .    w  W  fx .  y:
        0x067, // g .    .  .  st .  t'
        0x263, // , G 7  z  Z  Z  zh zh
        0x266, // . H    .  .  v  z  i
        0x127, // . h'   h' H' .  .  l
        0x068, // h .    .  .  .  .  .
        0x3B8, // . T    t' T' c  .  x
        0x069, // i I 1  .  .  I  i  .
        0x06A, // j J    y  Y  .  j  c
        0x281, // . K    .  .  .  .  k
        0x06B, // k .    k  K  K  k  h
        0x283, // . S    sh .  .  .  dz
        0x06C, // l L    l  L  .  l  g'
        0x3bb, // . L'   m  .  l  .  ch
        0x06D, // m M    dh M  M  m  m
        0x06E, // n N    n  N  N  n  y
        0x0F1, // ny .   zh s  x  .  n
        0x06F, // o O    s  .  O  o  sh
        0x295, // . Q    ?' O' .  .  o
        0x070, // p .    p  P  p  p  ch
        0x071, // q .    s' S' kx .  p
        0x27e, //   R    q  Q  r  .  j
        0x072, // r .    r  R  .  r  r'
        0x073, // s .    th S  s  s  s
        0x259, // . @    @  .  .  .  v
        0x074, // t 8    t  T  T  t  t
        0x075, // u U 0  i' .  Y  u  r
        0x076, // v V    .  .  .  .  c'
        0x3c6, // . F    .  .  f  f  .
        0x077, // w W    .  .  .  x  w
        0x078, // x .    .  .  .  c  p'
        0x3c7, // . X    .  .  j  ch .
        0x079, // y Y    u' .  .  sh .
        0x3c8, // . &    .  .  q  sc q
        0x07A, // z .    s' .  .  "  .
        0x292, // . Z    .  .  .  y  .
        0x3c9, // o: .   .  .  w  '  o:
        0x0b5, // % |    .  .  sx eh f
        0x265, // . 4    .  .  .  ju u
        0x28C, // . 2    .  .  .  ja .
        0x100 // > ... @63
      };

  ///// IPA/ TIPA

  public static char[] ipa4Tipa_Offs30 =
      new char[] {
        '\u0289', '\u0268', '\u028c', '\u025c', '\u0265', '\u0250', '\u0252', '\u0264',
        '\u0275', '\u0258',
        '\u02d0', '\u02d1', '\u02c8', ' ', '\u02cc', ' ',
        '\u0259', '\u0251', '\u03b2', '\u0255', '\u00f0',
        '\u025b', '\u0278', '\u0263', '\u0266', '\u0269', '\u029d', '\u0281', '\u028e',
        '\u0271', '\u014b', '\u0254', '\u0294', '\u0295', '\u027e', '\u0283', '\u03b8',
        '\u028a', '\u028b', '\u026f', '\u03c7', '\u028f', '\u0292',
      };

  public static String collTipa =
      ""
          + "P a b c D d e E f g "
          + "G H h'h T i j K k S "
          + "l L'm n nyo Q p q R "
          + "r s @ t u v F w x X "
          + "y & z Z o:| 4 2 "
      //
      ;

  public static String collTipa4Ugaritic =
      ""
      + "a b c D d e f "
      + "G h' T j k S "
      + "l L'm n nyo Q p q R "
      + "r s @ t u "
      + "y z "
      //
      ;

  public static String collTipa4UxIpa =
      ""
          + "a P P b o c d d "
          + "e @ e E E E E j "
          + "g g G G G 4 h'H "
          + "i i i L'L'L'L'm "
          + "m m nyn n t o o:"
          + "F K K K R R R R "
          + "R K s S S S S t "
          + "t u u u 2 w L'y "
          + "z z Z Z P Q Q Q "
          + "Q b e g h j K l "
          + "q P Q D D D T T "
      //
      ;

  private static String collTipa4UxGreek =
      ""
          // 0x294, // h
          + "P P : : . . G G . . "
          + ". : : . j . . . . . "
          + ". . a . e H i . o . u o:i "
          + "a " // A
          + "b " // B
          + "c " // g
          + "d " // d
          + "e " // E
          //// f .  fx
          //// g .  st
          + "G " // Z
          + "H " // v
          + "T " // c
          + "i " // I
          + "k " // K
          + "L'" // l
          + "m " // M
          + "n " // N
          + "ny" // x
          + "o " // O
          + "p " // p
          //// q .  kx
          + "R " // r
          + "r " // .
          + "s " // s
          + "t " // T
          + "u " // Y
          + "F " // f
          + "X " // j
          + "& " // q
          + "o:" // w
          //// | . sx
          + "i u a e H i u "
          + "a " // A
          + "b " // B
          + "c " // g
          + "d " // d
          + "e " // E
          //// f .  fx
          //// g .  st
          + "G " // Z
          + "H " // v
          + "T " // c
          + "i " // I
          + "k " // K
          + "L'" // l
          + "m " // M
          + "n " // N
          + "ny" // x
          + "o " // O
          + "p " // p
          //// q .  kx
          + "R " // r
          + "r " // .
          + "s " // s
          + "t " // T
          + "u " // Y
          + "F " // f
          + "X " // j
          + "& " // q
          + "o:" // w
          //// | . sx
          + "i u o u o:: : : : u u : . . . . g g f f q q | | "
      //
      ;

  private static int[] collDelta = new int[0x100];
  private static byte[][] coll64CollMin4Key = new byte[64][];
  private static int collMinLenDigit = 2;
  private static int collMinLenLetter = 2;

  static {
    for (int i0 = kAlpha1345Caps.length - 1; i0 >= 0; --i0) {
      kAlpha1345Cap2Sym[kAlpha1345Caps[i0]] = (char) (0x21 + i0);
    }
    for (int i0 = Rfc1345.kRfc1345.length - 2; i0 >= 0; i0 -= 2) {
      kRfc1345ToChar.put(Rfc1345.kRfc1345[i0], Rfc1345.kRfc1345[i0 + 1].charAt(0));
      kChar2Rfc1345.put(Rfc1345.kRfc1345[i0 + 1].charAt(0), Rfc1345.kRfc1345[i0]);
    }
  }

  private static synchronized void populateCollData() {
    if (null != coll) {
      return;
    }
    try {
      // Multilingual, using ICU's uca_rules.txt:
      RuleBasedCollator ucaDucet = new RuleBasedCollator("& \\u0001 = \\u0002");
      coll = ucaDucet;
    } catch (ParseException e) {
      coll = null;
    }
    final Locale loc = Dib2Root.app.locale;
    if ((null == coll) || (null != loc)) {
      coll = Collator.getInstance((null != loc) ? loc : Locale.CANADA);
      // Case-independent:
      coll.setStrength(Collator.PRIMARY);
    }

    {
      final byte[] collA = coll.getCollationKey("a").toByteArray();
      final byte[] collB = coll.getCollationKey("b").toByteArray();
      final byte[] collC = coll.getCollationKey("c").toByteArray();
      final byte[] coll0 = coll.getCollationKey("0").toByteArray();
      final byte[] coll1 = coll.getCollationKey("1").toByteArray();
      final byte[] coll2 = coll.getCollationKey("2").toByteArray();
      for (collMinLenLetter = 1; collMinLenLetter < collA.length; ++collMinLenLetter) {
        if (collA[collMinLenLetter - 1] != collB[collMinLenLetter - 1]) {
          break;
        }
      }
      int max4min = collMinLenLetter;
      for (collMinLenLetter = 1; collMinLenLetter < collC.length; ++collMinLenLetter) {
        if (collC[collMinLenLetter - 1] != collB[collMinLenLetter - 1]) {
          break;
        }
      }
      collMinLenLetter = (collMinLenLetter < max4min) ? max4min : collMinLenLetter;
      for (collMinLenDigit = 1; collMinLenDigit < coll0.length; ++collMinLenDigit) {
        if (coll0[collMinLenDigit - 1] != coll1[collMinLenDigit - 1]) {
          break;
        }
      }
      max4min = collMinLenDigit;
      for (collMinLenDigit = 1; collMinLenDigit < coll2.length; ++collMinLenDigit) {
        if (coll2[collMinLenDigit - 1] != coll1[collMinLenDigit - 1]) {
          break;
        }
      }
      collMinLenDigit = (collMinLenDigit < max4min) ? max4min : collMinLenDigit;
    }
    collMinLenLetter = (collMinLenLetter < collMinLenDigit) ? collMinLenDigit : collMinLenLetter;
    boolean letter = false;
    for (int i0 = 1; i0 < 63; ++i0) {
      coll64Key4Char[coll64Char4Key[i0]] = (byte) i0;
      byte[] cx = coll.getCollationKey("" + coll64Char4Key[i0]).toByteArray();
      coll64CharUpper4Key[i0] = coll64Char4Key[i0];
      if (letter) {
        if (0x80 > coll64Char4Key[i0]) {
          coll64CharUpper4Key[i0] = (char) (coll64Char4Key[i0] - 0x20);
          coll64Key4Char[coll64Char4Key[i0] - 0x20] = (byte) i0;
        } else {
          String up = StringFunc.toUpperCase("" + coll64Char4Key[i0]);
          if (1 == up.length()) {
            char ch = up.charAt(0);
            if ((ch != coll64Char4Key[i0]) && (0x80 <= ch) && (ch < coll64Key4Char.length)) {
              coll64Key4Char[ch] = (byte) i0;
              coll64CharUpper4Key[i0] = ch;
            }
          }
        }
        coll64CollMin4Key[i0] = Arrays.copyOf(cx, collMinLenLetter);
      } else {
        coll64CollMin4Key[i0] = Arrays.copyOf(cx, collMinLenDigit);
      }
      if ('9' == coll64Char4Key[i0]) {
        letter = true;
      }
    }
    coll64CharUpper4Key[63] = '^';

    ///// OLD data for Unicode blocks:
    for (int c0 = 0x100; c0 <= 0xff00; c0 += 0x100) {
      byte[] tmp = coll.getCollationKey("" + (char) c0).toByteArray();
      collDelta[c0 >>> 8] = -1;
      for (int i0 = 0; i0 < tmp.length; ++i0) {
        if (tmp[i0] == (byte) (c0 >>> 8)) {
          // tmp should have trailing '00' bytes.
          collDelta[c0 >>> 8] = (c0 & 0xff) - (tmp[i0 + 1] & 0xff);
          break;
        }
      }
    }

    ///// Needs to be sorted.
    coll64CollMin4Key[0] = new byte[collMinLenDigit];
    coll64CollMin4Key[2] = coll64CollMin4Key[1].clone();
    ++coll64CollMin4Key[2][coll64CollMin4Key[2].length - 1];
    coll64CollMin4Key[1] = new byte[collMinLenDigit];
    coll64CollMin4Key[1][collMinLenDigit - 1] = 1;
    coll64CollMin4Key[63] = coll64CollMin4Key[coll64Key4Char['z']].clone();
    ++coll64CollMin4Key[63][0];
    for (int i0 = 3; i0 <= 63; ++i0) {
      if (0x80 > coll64Char4Key[i0]) {
        if (0 > MiscFunc.compareUnsigned(coll64CollMin4Key[i0 - 1], coll64CollMin4Key[i0])) {
          continue;
        }
        for (int ix = i0 - 1; 0x80 <= coll64Char4Key[ix]; --ix) {
          if (0 <= MiscFunc.compareUnsigned(coll64CollMin4Key[ix], coll64CollMin4Key[i0])) {
            coll64CollMin4Key[ix] =
                Arrays.copyOf(coll64CollMin4Key[i0], coll64CollMin4Key[i0].length + 1);
            --coll64CollMin4Key[ix][coll64CollMin4Key[ix].length - 2];
            coll64CollMin4Key[ix][coll64CollMin4Key[ix].length - 1] = (byte) (0xff + ix - 63);
          }
        }
        continue;
      }
      coll64CollMin4Key[i0] = coll64CollMin4Key[i0 - 1].clone();
      if (0xff <= (coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1] & 0xff)) {
        ++coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 2];
        coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1] = 0;
      } else {
        ++coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1];
      }
      if (0 > MiscFunc.compareUnsigned(coll64CollMin4Key[i0 - 1], coll64CollMin4Key[i0])) {
        if ((63 <= i0)
            || (0 > MiscFunc.compareUnsigned(coll64CollMin4Key[i0], coll64CollMin4Key[i0 + 1]))) {
          continue;
        }
      }
      coll64CollMin4Key[i0] =
          Arrays.copyOf(
              coll64CollMin4Key[i0 - 1],
              coll64CollMin4Key[i0 - 1].length
                  + ((collMinLenLetter < coll64CollMin4Key[i0 - 1].length) ? 0 : 1));
      coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1] = (byte) (0xff + i0 - 63);
    }
    coll64CollMin4Key[63] =
        Arrays.copyOf(
            coll64CollMin4Key[62],
            coll64CollMin4Key[62].length
                + ((collMinLenLetter < coll64CollMin4Key[62].length) ? 0 : 1));
    coll64CollMin4Key[63][coll64CollMin4Key[63].length - 1] = (byte) 0xff;

    int vStart = coll64CollMin4Key[1][coll64CollMin4Key[1].length - 1];
    int iEnd = coll64Key4Char['0'];
    int vEnd = coll64CollMin4Key[iEnd][coll64CollMin4Key[iEnd].length - 1];
    vEnd += (vEnd <= vStart) ? 256 : 0;
    int delta = (vEnd - vStart) / (iEnd + 3);
    vStart += 2 + 2 * delta;
    delta = (vEnd - vStart) / (iEnd + 3);
    int stretch = (2 + (iEnd + 3) / (vEnd + 1 - vStart));
    while (vEnd <= (vStart + 1 + iEnd * delta + (iEnd / stretch))) {
      ++stretch;
      delta -= stretch / iEnd;
    }
    byte[] base = coll64CollMin4Key[1].clone();
    for (int i0 = 2; i0 < coll64Key4Char['0']; ++i0) {
      boolean pull = false;
      if (0 < MiscFunc.compareUnsigned(coll64CollMin4Key[i0 - 1], coll64CollMin4Key[i0])) {
        coll64CollMin4Key[i0] = coll64CollMin4Key[i0 - 1];
        pull = true;
      }
      final byte vx = coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1];
      int max = vStart + i0 * delta + (i0 / stretch);
      if (256 <= (max + delta + 1)) {
        max -= 256;
        if (2 <= base.length) {
          ++base[base.length - 2];
          base[base.length - 1] = 0;
        }
      }
      if ((vx > (max + 1)) || (pull & ((max - 2 * delta) > vx))) {
        coll64CollMin4Key[i0] = base.clone();
        coll64CollMin4Key[i0][coll64CollMin4Key[i0].length - 1] = (byte) max;
        if (0 < MiscFunc.compareUnsigned(coll64CollMin4Key[i0 - 1], coll64CollMin4Key[i0])) {
          coll64CollMin4Key[i0] = coll64CollMin4Key[i0 - 1];
        }
      }
    }

    coll64Key4Char[' '] = 1;
    coll64Key4Char['-'] = 2;
    coll64Key4Char[':'] = 3;
    final int offsetTipa = coll64Key4Char['A'] - 1;

    ///// Override higher by lower values, map to lowercase.
    //  for (char ch = (char) 0x1e; ch != 0x1f; ch -= (ch != 2) ? 1 : -(0x250 - 2 - 1)) {
    for (char ch = (char) 0x24f; ch > ' '; --ch) {
      if (0 >= coll64Key4Char[ch]) {
        final String s0 = "" + ch;
        final byte[] cx = coll.getCollationKey(s0).toByteArray();
        final int iz = MiscFunc.binSearchUnsigned(coll64CollMin4Key, cx);
        int ix = (0 <= iz) ? iz : ((-2 <= iz) ? 1 : (-iz - 2));
        if ((0xc0 <= ch)
            && (ch < 0x100)
            && (ix < coll64Key4Char['A'])
            && (0xf7 != ch)
            && (0xd7 != ch)) {
          // 'Normalize':
          ix = coll64Key4Char['O'];
        } else if ((0x7f <= ch) && (ch < 0xc0)) {
          ix = coll64Key4Char[':'];

          ///// Skip special cases ... (63 as marker for special handling)
          //        } else if (0xdf == ch) {
          //          // 'sharp S'
          //          ix = coll64Key4Char['S'];
          //        } else if ((0xde == ch) || (0xfe == ch)) {
          //          ix = 62;
        } else if (0 >= ix) {
          ix = ((' ' > ch) || (0 == iz)) ? 0 : 1;
        } else if (63 <= ix) {
          coll64Key4Char[ch] = 63;
          continue;
        }
        final byte[] cmp =
            Arrays.copyOf(
                coll64CollMin4Key[ix],
                coll64CollMin4Key[ix].length + coll64CollMin4Key[coll64Key4Char['0']].length);
        System.arraycopy(
            coll64CollMin4Key[coll64Key4Char['0']],
            0,
            cmp,
            coll64CollMin4Key[ix].length,
            cmp.length - coll64CollMin4Key[ix].length);
        coll64Key4Char[ch] = (0 <= MiscFunc.compareUnsigned(cx, cmp)) ? 63 : (byte) ix;
      }
    }
    // 0x300..0x370: combining => 0
    for (char ch = 0x2b0; ch < 0x300; ++ch) {
      coll64Key4Char[ch] = (byte) 1;
    }

    ///// Greek
    for (int i0 = 0, i1 = 0x370; i0 < collTipa4UxGreek.length(); i0 += 2, ++i1) {
      final char ch = collTipa4UxGreek.charAt(i0);
      if ((' ' == collTipa4UxGreek.charAt(i0 + 1)) && ('a' <= ch)) {
        coll64Key4Char[i1] = coll64Key4Char[ch];
        continue;
      }
      final String xtipa = collTipa4UxGreek.substring(i0, i0 + 2);
      final int ix = collTipa.indexOf(xtipa);
      coll64Key4Char[i1] = (0 > ix) ? coll64Key4Char[ch] : (byte) (ix / 2 + offsetTipa);
    }

    ///// IPA
    for (int i0 = 0, i1 = 0x250; i0 < collTipa4UxIpa.length(); i0 += 2, ++i1) {
      final char ch = collTipa4UxIpa.charAt(i0);
      if ((' ' == collTipa4UxIpa.charAt(i0 + 1)) && ('a' <= ch)) {
        coll64Key4Char[i1] = coll64Key4Char[ch];
        continue;
      }
      final String xtipa = collTipa4UxIpa.substring(i0, i0 + 2);
      final int ix = collTipa.indexOf(xtipa);
      coll64Key4Char[i1] = (0 > ix) ? coll64Key4Char[ch] : (byte) (ix / 2 + offsetTipa);
    }

    ///// Fallback values
    for (char ch = 0x100; ch < 0x250; ++ch) {
      if (coll64Key4Char['A'] > coll64Key4Char[ch]) {
        coll64Key4Char[ch] = (byte) (coll64Key4Char['A'] - 1);
      }
    }
    for (char ch = 0x370; ch < coll64Key4Char.length; ++ch) {
      if (0 == coll64Key4Char[ch]) {
        coll64Key4Char[ch] = (byte) (coll64Key4Char['A'] - 1);
      }
    }
  }

  public static Object[] getCollArrays() {
    if (null == coll) {
      populateCollData();
    }
    return new Object[] {coll64Char4Key, coll64CharUpper4Key, coll64Key4Char};
  }

  public static char char4Rfc1345(String rfc) {
    Character out = kRfc1345ToChar.get(rfc);
    return (null == out)
        ? (0 >= rfc.length() ? (char) 0 : rfc.charAt(rfc.length() - 1))
        : (char) out;
  }

  public static String rfc1345oChar(char ch) {
    return kChar2Rfc1345.get(ch);
  }

  private static int findChars(String chars, char[] in, int start) {
    final int last = in.length - chars.length();
    final char ch = chars.charAt(0);
    for (int i0 = start; i0 <= last; ++i0) {
      if (ch == in[i0]) {
        if (1 >= chars.length()) {
          return i0;
        }
        int ix = 1;
        for (; ix < chars.length(); ++ix) {
          if (chars.charAt(ix) != in[i0 + ix]) {
            break;
          }
        }
        if (ix >= chars.length()) {
          return i0 + chars.length() - 1;
          //      } else if (immediate) {
          //        return -1;
        }
      }
    }
    return -1;
  }

  private static int findChar(char ch, char[] in, int start) {
    final int len = in.length;
    for (int i0 = start; i0 < len; ++i0) {
      if (ch == in[i0]) {
        return i0;
      }
    }
    return -1;
  }

  private static int group4Rfc1345_start = 0;
  private static char[] group4Rfc1345_0x2600 = {
    0x2606, 0x260f, 0x2610, 0x2611, 0x2612, 0x2620, 0x2622, 0x262e, 0x262f, 0x2639, 0x263a, 0x2640,
    0x2641, 0x2642, 0x2690, 0x2692, 0x2695, 0x2696,
  };

  public static String group4Rfc1345(char xChar) {
    StringBuilder out = new StringBuilder(128 + 10);
    if (0 >= group4Rfc1345_start) {
      for (int i0 = 1; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
        if (Rfc1345.kRfc1345[i0].charAt(0) == ' ') {
          group4Rfc1345_start = i0 - 1;
          break;
        }
      }
    }
    if (' ' < xChar) {
      char mnemonicFirst = 0;
      int i0 = group4Rfc1345_start + 1;
      if ('0' > xChar) {
        mnemonicFirst = xChar;
      } else {
        for (; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
          final char c0 = Rfc1345.kRfc1345[i0].charAt(0);
          if (c0 == xChar) {
            mnemonicFirst = Rfc1345.kRfc1345[i0 - 1].charAt(0);
            break;
          }
        }
      }
      if (' ' < mnemonicFirst) {
        out.append(mnemonicFirst);
      }
      int loop = 1;
      if (('a' <= mnemonicFirst) && (mnemonicFirst <= 'z')) {
        loop = 2;
        out.append((char) (mnemonicFirst - 0x20));
      } else if (('A' <= mnemonicFirst) && (mnemonicFirst <= 'Z')) {
        loop = 2;
        out.append((char) (mnemonicFirst + 0x20));
      }
      if (0 < mnemonicFirst) {
        int stop = i0;
        int start = i0 + 1;
        for (; loop > 0; --loop) {
          for (i0 = start; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
            if ((Rfc1345.kRfc1345[i0].charAt(0) == mnemonicFirst)
                && (Rfc1345.kRfc1345[i0 + 1].charAt(0) != mnemonicFirst)) {
              out.append(Rfc1345.kRfc1345[i0 + 1].charAt(0));
            }
          }
          for (i0 = group4Rfc1345_start + 2; i0 < stop; i0 += 2) {
            if ((Rfc1345.kRfc1345[i0].charAt(0) == mnemonicFirst)
                && (Rfc1345.kRfc1345[i0 + 1].charAt(0) != mnemonicFirst)) {
              out.append(Rfc1345.kRfc1345[i0 + 1].charAt(0));
            }
          }
          mnemonicFirst += ('a' <= mnemonicFirst) ? -0x20 : 0x20;
        }
      }
    }
    char base = ' ';
    switch (xChar) {
      case ' ':
        base = ' ' + 1;
        break;
      case '.':
        base = 'Z' + 1;
        break;
      case '\\':
        for (char ch : group4Rfc1345_0x2600) {
          out.append(ch);
        }
        base = 0x25a0;
        break;
      default:
        if ('A' > xChar) {
          for (int i0 = 0; i0 < Rfc1345.kBasic80.length; ++i0) {
            if (('0' > Rfc1345.kBasic80[i0]) && ((xChar & 0xf) == (Rfc1345.kBasic80[i0] & 0xf))) {
              base = Rfc1345.kBasic80[i0 + 1];
              break;
            }
          }
        }
    }
    if ((' ' < base) || (0x80 > xChar)) {
      int count = 100 - out.length();
      for (; (count > 0) && (base < 0x80); --count, ++base) {
        switch (base) {
          case '0':
            base = '9' + 1;
            break;
          case 'A':
            base = 'Z' + 1;
            break;
          case 'a':
            base = 'z' + 1;
            break;
          case DELTA_X:
            base = NBS0 + 1;
            break;
            //        case SH0:
            //          base = SH0 + 1;
            //          break;
            //        case 0xc0:
            //          base = 0xd7;
            //          break;
            //        case 0xd9:
            //          base = 0xf7;
            //          break;
            //        case 0xf9:
            //          base = 0x100;
            //          break;
          default:
            ;
        }
        out.append(base);
      }
      int i0 = 0;
      for (; i0 < Rfc1345.kBasic80.length; ++i0) {
        if (base <= Rfc1345.kBasic80[i0]) {
          i0 = (i0 < Rfc1345.kBasic80.length) ? i0 : 0;
          break;
        }
      }
      for (boolean all = false; count > 0; --count) {
        if (0x80 <= Rfc1345.kBasic80[i0]) {
          if (all) {
            all = false;
            for (char ch0 = (char) (Rfc1345.kBasic80[i0 - 2] + 1);
                ch0 < Rfc1345.kBasic80[i0];
                ++ch0) {
              out.append(ch0);
              --count;
            }
          }
          out.append(Rfc1345.kBasic80[i0]);
        } else if (0 == Rfc1345.kBasic80[i0]) {
          all = true;
        }
        ++i0;
        i0 = (i0 < Rfc1345.kBasic80.length) ? i0 : 0;
      }
    } else {
      base = (char) ((xChar + 1) & 0x3fff);
      base = (0x100 > base) ? (char) (0x1ff - base) : base;
      for (int count = out.capacity() - out.length(); count > 0; --count, ++base) {
        out.append(base);
      }
    }
    return out.toString();
  }

  private static char char4Rfc1345(char[] rfc, int start, int end) {
    char[] tmp = new char[end - start];
    System.arraycopy(rfc, start, tmp, 0, end - start);
    return char4Rfc1345(new String(tmp));
  }

  public static int dropZeros(char[] str, int len) {
    int i0 = 0;
    for (; i0 < len; ++i0) {
      if (0 == str[i0]) {
        break;
      }
    }
    int put = i0++;
    for (; i0 < len; ++i0) {
      if (0 != str[i0]) {
        str[put++] = str[i0];
      }
    }
    return put;
  }

  public static int replaceRfc1345(char[] xyMnem, int start, int end) {
    int out = start;
    for (int i0 = start; i0 < end; ++i0) {
      if (' ' == xyMnem[i0]) {
        final char ch0 = char4Rfc1345(xyMnem, out, i0);
        if (0 == ch0) {
          out = i0;
        } else {
          xyMnem[out++] = ch0;
        }
        for (; out <= i0; ++out) {
          xyMnem[out] = 0;
        }
      }
    }
    final char ch0 = char4Rfc1345(xyMnem, out, end);
    if (0 == ch0) {
      return end;
    }
    xyMnem[out] = ch0;
    return out + 1;
    //  for (++ out; out <= end; ++ out) {
    //    xyMnem[ out ] = 0;
    //  }
  }

  public static int replaceAlpha1345(char[] sub, int start, int end) {
    boolean replace = false;
    boolean alpha = false;
    int put = start;
    for (int i0 = start; i0 < end; ++i0) {
      if ('_' == sub[i0]) {
        if (replace) {
          sub[start] = (put <= start) ? (char) 0 : char4Rfc1345(sub, start, put);
          put = (put <= start) ? start : ++start;
        } else if (((i0 + 1) < end) && ('O' == sub[i0 + 1])) {
          ++i0;
          if ((++i0) < end) {
            switch (sub[i0]) {
              case 0:
                break;
              case 'O':
                sub[put++] = ' ';
                break;
              default:
                sub[put++] = '.';
            }
          }
        }
        replace = !replace;
        start = put;
      } else if (!replace) {
        sub[put++] = sub[i0];
      } else if (alpha) {
        alpha = false;
        sub[put++] = sub[i0];
      } else if ('U' == sub[i0]) {
        alpha = true;
      } else if (('Z' >= sub[i0]) && ('A' <= sub[i0])) {
        sub[put++] = kAlpha1345Cap2Sym[sub[i0]];
      } else {
        sub[put++] = sub[i0];
      }
    }
    if (replace && (put > start)) {
      sub[start] = char4Rfc1345(sub, start, put);
      return start + 1;
      //    put=++start;
    }
    //  start=put;
    //  for (; put < end; ++ put) {
    //    sub[ put ] = 0;
    //  }
    //  return start;
    return put;
  }

  // private static String str4Alpha1345( char[] sub, int start, int end ) {
  //  StringBuilder out = new StringBuilder( end - start );
  //  StringBuilder tmp = new StringBuilder( 5 );
  //  boolean alpha = false;
  //  for (int i0 = start; i0 < end; ++ i0) {
  //    if (alpha) {
  //      alpha = false;
  //      tmp.append( sub[ i0 ] );
  //    } else if ('U' == sub[ i0 ]) {
  //      alpha = true;
  //    } else if ('O' == sub[ i0 ]) {
  //      if (0 < tmp.length()) {
  //        out.append( char4Rfc1345( tmp.toString() ) );
  //        tmp = new StringBuilder( 5 );
  //      }
  //    } else if (('Z' >= sub[ i0 ]) && ('A' <= sub[ i0 ])) {
  //      tmp.append( kCap2Sym[ sub[ i0 ] ] );
  //    } else {
  //      tmp.append( sub[ i0 ] );
  //    }
  //  }
  //  if (0 < tmp.length()) {
  //    out.append( char4Rfc1345( tmp.toString() ) );
  //    tmp = new StringBuilder( 5 );
  //  }
  //  return out.toString();
  // }

  public static String string4Alpha1345(String sub) {
    char[] a0 = sub.toCharArray();
    int len = replaceAlpha1345(a0, 0, a0.length);
    //  int len = dropZeros( a0, a0.length );
    return new String(a0, 0, len);
  }

  private static final long REP_ZERO = 1;
  private static final long REP_QUOTE = 2;
  private static final long REP_SEQ = 4;
  private static final long REP_LF = 8;
  private static final long REP_PRINTABLE = 0x10;

  public static void replaceCtrlNOld(char[] text, int start, int end, long flagsZeroQuoteSeqLfPrn) {
    for (int i0 = start; i0 < end; ++i0) {
      if (' ' <= text[i0]) {
        if (0x7f > text[i0]) {
          continue;
        } else if (ZW0 > text[i0]) {
          if (0xA0 > text[i0]) {
            text[i0] = Cp125xOffs7f_2_Unicode[text[i0] - 0x7f];
            continue;
          } else if ((SH0 != text[i0]) && (NBS0 != text[i0])) {
            continue;
          }
        } else if (LF0 < text[i0]) {
          continue;
        }
      }
      switch (text[i0]) {
        case 0:
          text[i0] = (0 != (REP_ZERO & flagsZeroQuoteSeqLfPrn)) ? ' ' : (char) 0;
          break;
        case TAB:
          break;
        case CR: // TODO '\r\n' -> ' '
        case LF:
          text[i0] = (0 != (REP_LF & flagsZeroQuoteSeqLfPrn)) ? ' ' : (char) 0;
        case FF:
        case LF0:
          text[i0] = ' ';
          break;
        case BULLET_X:
          text[i0] = BULLET;
          break;
        case PAR_X:
          text[i0] = PAR;
          break;
        case SECT_X:
          text[i0] = SECT;
          break;
        case DELTA_X:
          text[i0] = Cp125xOffs7f_2_Unicode[0];
          break;
        case NBS:
        case NBS0:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ' ' : NBS;
          break;
        case SH:
        case SH0:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ZW0 : SH;
          break;
        case ZW:
        case ZW0:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ZW0 : ZW;
          break;
        case NBH:
        case NBH0:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '-' : NBH;
          break;
        case QUOTE_START:
          text[i0] = (0 != (REP_QUOTE & flagsZeroQuoteSeqLfPrn)) ? '\u00a8' : text[i0];
          break;
        case QUOTE_END:
          text[i0] = (0 != (REP_QUOTE & flagsZeroQuoteSeqLfPrn)) ? '\u00a8' : text[i0];
          break;
        case LIST_START:
          text[i0] =
              (0 != (REP_SEQ & flagsZeroQuoteSeqLfPrn))
                      || (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn))
                  ? '\u00af'
                  : text[i0];
          break;
        case LIST_END:
          text[i0] =
              (0 != (REP_SEQ & flagsZeroQuoteSeqLfPrn))
                      || (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn))
                  ? '\u00af'
                  : text[i0];
          break;
        case TEMPLATE_START:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00aa' : text[i0];
          break;
        case TEMPLATE_END:
          text[i0] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00aa' : text[i0];
          break;
        default:
          text[i0] = (' ' <= text[i0]) ? text[i0] : '_';
      }
    }
  }

  public static String makePrintable(String data) {
    if (null == data) {
      return "^";
    }
    int len = data.length();
    char[] out = data.toCharArray();
    replaceCtrlNOld(out, 0, len, 0xff);
    return new String(out, 0, len);
  }

  public static void replaceCtrlNOld(char[] text, int start, int end) {
    replaceCtrlNOld(text, start, end, 0);
  }

  public static boolean equalsRoughly(String str0, String str1) {
    if ((null == str0) || (null == str1)) {
      return str0 == str1;
    }
    int i0 = 0, i1 = 0;
    char[] tmp = new char[1];
    for (; (i0 < str0.length()) && (i1 < str1.length()); ++i0, ++i1) {
      final char ch0 = str0.charAt(i0);
      final char ch1 = str1.charAt(i1);
      if (ch0 == ch1) {
      } else if ((' ' >= ch0) && (' ' >= ch1)) {
      } else if ((0 <= XCHARS.indexOf(ch0)) && (0 <= XCHARS.indexOf(ch1))) {
      } else if (0 <= XCHARS.indexOf(ch0)) {
        if (' ' <= ch1) {
          break;
        }
      } else if (0 <= XCHARS.indexOf(ch1)) {
        if (' ' <= ch0) {
          break;
        }
      } else if (' ' >= ch0) {
        tmp[0] = ch0;
        replaceCtrlNOld(tmp, 0, 1, ~0L);
        if (ch1 != tmp[0]) {
          --i1;
        }
      } else if (' ' >= ch1) {
        tmp[0] = ch1;
        replaceCtrlNOld(tmp, 0, 1, ~0L);
        if (ch0 != tmp[0]) {
          --i0;
        }
      } else {
        break;
      }
    }
    return !((i0 < str0.length()) && (i1 < str1.length()));
  }

  private static char[] char4Escape_map = new char['Z' + 1];

  public static char char4Escape(char c0, char c1) {
    char out =
        ( //
            ('^' == c0) // depr
                || ('*' == c0))
            ? c1
            : c0;
    if (('Z' >= out) && ('@' <= out)) {
      out = (char) (out - '@');
    } else if ('a' <= out) {
      if (10 != char4Escape_map['N']) {
        char4Escape_map['A'] = 7;
        char4Escape_map['B'] = 8;
        char4Escape_map['E'] = 27;
        char4Escape_map['F'] = 12;
        char4Escape_map['N'] = 10;
        char4Escape_map['R'] = 13;
        char4Escape_map['T'] = 9;
        char4Escape_map['V'] = 11;
      }
      out = char4Escape_map[out - ('a' - 'A')];
    } else {
      switch (out) {
        case '{':
        case '}':
          out = 0x80;
          break;
        case '<':
          out = '{';
          break;
        case '>':
          out = '}';
          break;
        default:
          ;
      }
    }
    return ('^' == c0) ? ((char) (0x80 | out)) : out;
  }

  public static boolean ipa4Tipa(char[] xy, int start, int end) {
    if (null == coll) {
      populateCollData();
    }
    final int offsetTipa = coll64Key4Char['A'] - 1;
    boolean dropZeros = false;
    for (int i0 = start; i0 < end; ++i0) {
      int ix = collTipa.indexOf(xy[i0]);
      if (0 <= ix) {
        int ie = ix;
        ix = -1;
        if ((i0 + 1) < end) {
          char cmp2 = xy[i0 + 1];
          for (; ie < collTipa.length(); ie += 2) {
            if (collTipa.charAt(ie) != xy[i0]) {
              break;
            }
            if (collTipa.charAt(ie + 1) == cmp2) {
              ix = ie;
              break;
            }
          }
        }
        if (0 <= ix) {
          xy[i0] = coll64Char4Key[ix / 2 + offsetTipa];
          xy[++i0] = 0;
          dropZeros = true;
          continue;
        }
      }
      if (('0' > xy[i0]) || (xy[i0] > 'Z')) {
        if ('"' != xy[i0]) {
          continue;
        }
        xy[i0] = '<';
      }
      xy[i0] = ipa4Tipa_Offs30[xy[i0] - '0'];
    }
    return !dropZeros;
  }

  /** Replace mnemonics in string. '_' rfc1345 (without '_') '_', '__' -> '_', '_ ' -> '' ... */
  // '_' rfc1345 (without '_') '_',
  // '__' -> '_', '_ ' -> '',
  // '_[*' rfc1345 (any) ']_', '_[_' alpha1345 ']_',
  // '_//' quote '//_': for escaping mnemonics,
  // '_{{' quasi-quote '}}_' : '_{{' > STX, '}}_' > ETX
  // '_[[' list ']]_' : '_[[' > EOT, ']]_' > ENQ
  // '_<' template '>_' : '_<' > SO, '>_' > SI
  // '_#' NN/NNNN unicode hex '_': ,
  // '_^^' tipa '_',
  // '_^' n/r/t/@/A/... control char  ('^*': +0x80) '_'
  // Note: RFC1345 does not use '_' in combination with '{}[]()<>/' or '#...'.
  public static String string4Mnemonics(String mnem) {
    if (0 > mnem.indexOf('_')) {
      return mnem;
    }
    final char[] out = mnem.toCharArray();
    int len = out.length;
    int nextQuotedChunk = -1;
    for (int i0 = 0; i0 < len; ++i0) {
      if ('_' != out[i0]) {
        continue;
      }
      if (i0 >= nextQuotedChunk) {
        len = mnem.indexOf("_//", i0); // quote
        if (0 > len) {
          len = out.length;
        } else if (i0 < len) {
          ++len;
        } else {
          ///// Skip the quote.
          out[i0++] = 0;
          out[i0++] = 0;
          out[i0++] = 0;
          len = mnem.indexOf("//_", i0);
          if (i0 <= len) {
            i0 = len;
            out[i0++] = 0;
            out[i0++] = 0;
            out[i0] = 0;
          }
          len = 1 + mnem.indexOf("_//", i0); // next one
          if (0 >= len) {
            len = out.length;
          }
        }
        nextQuotedChunk = len;
      }
      if ((i0 + 1) >= out.length) {
        continue;
      }
      out[i0++] = 0;
      int ix;
      int v0;
      switch (out[i0]) {
        case '_':
          continue;
        case ' ':
          out[i0] = 0;
          continue;
        case '#': // unicode hex
          out[i0] = 0;
          if (0 > (ix = findChar('_', out, i0))) {
            continue;
          } else if ((4 + 1) < (ix - i0)) {
            continue;
          }
          v0 = 0;
          for (++i0; i0 < ix; ++i0) {
            final int b0 = 0xff & NIBBLE[out[i0] & 0x1f];
            v0 = (v0 << 4) | b0;
            out[i0] = 0;
          }
          out[i0 - 1] = (char) v0;
          out[i0] = 0;
          continue;
        case '^': // control char or tipa
          out[i0] = 0;
          if (0 > (ix = findChar('_', out, i0))) {
            continue;
          }
          if ('^' == out[i0 + 1]) {
            out[++i0] = 0;
            ipa4Tipa(out, i0 + 1, ix);
          } else {
            out[i0] = char4Escape(out[i0 + 1], out[ix - 1]);
            out[++i0] = 0;
            // Potential override is okay:
            out[ix - 1] = 0;
          }
          i0 = ix;
          out[i0] = 0;
          continue;
        case '{': // quote
          //        if (0 > (ix = findChars( "}_", out, i0 ))) {
          //          continue;
          //        }
          //        out[ i0 ] = TEMPLATE_START;
          //        i0 = ix;
          //        out[ i0 - 1 ] = TEMPLATE_END;
          //        out[ i0 ] = 0;
          //        continue;
        case '<': // template
        case '[': // RFC1345 or list
          //        if (0 > (ix = findChars( "]_", out, i0 ))) {
          if (0 > (ix = findChars("" + (char) (out[i0] + 2) + "_", out, i0))) {
            continue;
          }
          if ((out[i0 + 1] == out[i0]) || ('<' == out[i0])) {
            if (out[ix - 2] == out[ix - 1]) {
              out[ix - 2] = 0;
            }
            // Might not be the matching one!:
            out[ix - 1] =
                ('<' == out[i0]) ? TEMPLATE_END : (('[' == out[i0]) ? LIST_END : QUOTE_END);
            out[ix] = 0;
            if (out[i0 + 1] == out[i0]) {
              out[i0 + 1] = 0;
            }
            out[i0] =
                ('<' == out[i0]) ? TEMPLATE_START : (('[' == out[i0]) ? LIST_START : QUOTE_START);
            ++i0;
            continue;
          } else if ('[' != out[i0]) {
            //          || (('_' != out[ i0 + 1 ]) && ((ix - i0) > 6))) {
            continue;
          }
          out[ix--] = 0;
          out[i0++] = 0;
          break;
        default:
          if (' ' > out[i0]) {
            out[i0] = 0;
            continue;
          }
          // RFC1345 wo '_'
          if (0 > (ix = findChar('_', out, i0))) {
            continue;
          }
      }
      if ('_' == out[i0]) {
        i0 = replaceAlpha1345(out, i0, ix);
      } else { // '*', or wo '_'
        i0 = replaceRfc1345(out, i0, ix);
      }
      for (; i0 < ix; ++i0) {
        out[i0] = 0;
      }
      out[i0] = 0;
    }
    final int len2 = dropZeros(out, len);
    return new String(out, 0, len2);
  }

  public static String mnemonics4String(String text, boolean leaveLfTab, boolean leaveExotic) {
    int i0 = 0;
    int lenLine = 0;
    for (; i0 < text.length(); ++i0, ++lenLine) {
      final char ch = text.charAt(i0);
      if ((78 < lenLine) && !leaveLfTab) {
        break;
      }
      if ((' ' > ch)) {
        if ('\n' == ch) {
          lenLine = -1;
        } else if ('\t' != ch) {
          break;
        }
        if (leaveLfTab) {
          continue;
        }
        // TODO: STX ETX SLS etc.
        break;
      } else if ((0x7f <= ch) || ('_' == ch)) { // (0 <= "_{}[]".indexOf( ch ))) {
        break;
      }
    }
    if (i0 >= text.length()) {
      return text;
    }
    char[] tmp = new char[1];
    StringBuilder out = new StringBuilder(text.length() * 2);
    out.append(text.substring(0, i0));
    int last = i0;
    final int len = text.length();
    for (; i0 < len; ++i0, ++lenLine) {
      char ch = text.charAt(i0);
      boolean breakLine = (77 < lenLine) && !leaveLfTab;
      if (!breakLine && (70 <= lenLine)) {
        breakLine = ((' ' > ch) || (0x7f <= ch) || ('_' == ch)); // (0 <= "_{}[]".indexOf( ch )));
      }
      if (breakLine) {
        for (int stop = ((i0 - 40) >= last) ? (i0 - 20) : last; i0 > stop; --i0) {
          if (' ' >= ch) {
            break;
          }
        }
        out.append(text.substring(last, i0));
        last = i0;
        out.append('\n');
        lenLine = -1;
        --i0;
        continue;
      }
      if ((' ' > ch)) {
        if (('\n' == ch) || ('\t' == ch)) {
          if (leaveLfTab) {
            lenLine = ('\n' == ch) ? -1 : lenLine;
            continue;
          }
          out.append(text.substring(last, i0));
          last = i0 + 1;
          if ('\n' == ch) {
            out.append("_^n_\n");
          } else if (40 > lenLine) {
            out.append("_^t_");
            lenLine += 3;
            continue;
          } else {
            out.append("_^t_\n");
          }
          lenLine = -1;
          continue;
        }
        if (!leaveExotic) {
          tmp[0] = ch;
          replaceCtrlNOld(tmp, 0, 1, 0x7);
          if (' ' < tmp[0]) {
            ch = tmp[0];
          }
        }
      } else if (0x7f > ch) {
        if ('_' == ch) {
          out.append(text.substring(last, i0 + 1));
          last = i0 + 1;
          ++lenLine;
          out.append('_');
          continue;
        }
        continue;
      }
      out.append(text.substring(last, i0));
      last = i0 + 1;
      if (' ' < ch) {
        String repl = kChar2Rfc1345.get(ch);
        if ((null != repl) && (1 < repl.length()) && (0 > repl.indexOf('_'))) {
          out.append('_');
          out.append(repl);
          out.append('_');
          lenLine += 1 + repl.length();
          continue;
        }
      }
      out.append("_#");
      if (0x100 <= ch) {
        out.append(HEX[(ch & 0xf000) >>> 12]);
        out.append(HEX[(ch & 0xf00) >>> 8]);
      }
      out.append(HEX[(ch & 0xf0) >>> 4]);
      out.append(HEX[ch & 0xf]);
      out.append('_');
      lenLine += (0x100 <= ch) ? 6 : 4;
    }
    out.append(text.substring(last, i0));
    return out.toString();
  }

  public static int binSearch(String[] sortedList, String key) {
    int min = 0;
    int max = sortedList.length - 1;
    while (min <= max) {
      final int i0 = (min + max) >> 1;
      final int cmp = sortedList[i0].compareTo(key);
      if (cmp < 0) {
        min = i0 + 1;
      } else if (cmp > 0) {
        max = i0 - 1;
      } else {
        return i0;
      }
    }
    return -1 - min;
  }

  public static int indexOf(String[] list, String key) {
    for (int i0 = 0; i0 < list.length; ++i0) {
      if ((list[i0] == key) || list[i0].equals(key)) {
        return i0;
      }
    }
    return -1;
  }

  /** Convert text with newline formatting for automatic flow */
  public static String flowText(String text) {
    return text.replace("\t", "    ")
        .replace("\r", "")
        .replaceAll(" *\\n *", "\t")
        .replace("\t\t", "\n\n")
        .replace("\n\t", "\n\n")
        .replace('\t', ' ');
  }

  /**
   * For sorted strings:
   *
   * @param sorted0
   * @param sorted1
   * @return
   */
  public static boolean containsAll(String[] sorted0, String... sorted1) {
    int i0 = sorted0.length - 1;
    int i1 = sorted1.length - 1;
    for (; (i0 >= 0) && (i1 >= 0); --i0) {
      int cmp = (i0 > 0) ? 1 : 0;
      for (; cmp > 0; --i0) {
        cmp = sorted1[i1].charAt(0) - sorted0[i0].charAt(0);
      }
      cmp = (0 == cmp) ? sorted0[i0].compareTo(sorted1[i1]) : cmp;
      if (0 > cmp) {
        return false;
      } else if (0 == cmp) {
        --i1;
      }
    }
    return (i1 < 0);
  }

  /**
   * For sorted strings:@param sorted0
   *
   * @param sorted1
   * @return
   */
  public static boolean containsOne(String[] sorted0, String... sorted1) {
    int i0 = sorted0.length - 1;
    int i1 = sorted1.length - 1;
    for (; (i0 >= 0) && (i1 >= 0); --i0) {
      int cmp = (i0 > 0) ? 1 : 0;
      for (; cmp > 0; --i0) {
        cmp = sorted1[i1].charAt(0) - sorted0[i0].charAt(0);
      }
      cmp = (0 == cmp) ? sorted0[i0].compareTo(sorted1[i1]) : cmp;
      if (0 > cmp) {
        ++i0;
        --i1;
      } else if (0 == cmp) {
        return true;
      }
    }
    return false;
  }

  public static String toLowerCase(String str) {
    boolean change = false;
    boolean ascii = true;
    int i0;
    for (i0 = str.length() - 1; i0 >= 0; --i0) {
      final char c0 = str.charAt(i0);
      if (('A' <= c0) && (c0 <= 'Z')) {
        change = true;
        break;
      } else if (0x80 <= c0) {
        ascii = false;
        break;
      }
    }
    if (!ascii) {
      return str.toLowerCase(Locale.ROOT);
    }
    if (change) {
      final char[] out = str.toCharArray();
      for (; i0 >= 0; --i0) {
        final char c0 = str.charAt(i0);
        out[i0] = (('A' <= c0) && (c0 <= 'Z')) ? (char) (c0 | 0x20) : c0;
      }
      return new String(out);
    }
    return str;
  }

  public static String toUpperCase(String str) {
    boolean change = false;
    boolean ascii = true;
    int i0;
    for (i0 = str.length() - 1; i0 >= 0; --i0) {
      final char c0 = str.charAt(i0);
      if (('a' <= c0) && (c0 <= 'z')) {
        change = true;
        break;
      } else if (0x80 <= c0) {
        ascii = false;
        break;
      }
    }
    if (!ascii) {
      return str.toUpperCase(Locale.ROOT);
    }
    if (change) {
      final char[] out = str.toCharArray();
      for (; i0 >= 0; --i0) {
        final char c0 = str.charAt(i0);
        out[i0] = (('a' <= c0) && (c0 <= 'z')) ? (char) (c0 ^ 0x20) : c0;
      }
      return new String(out);
    }
    return str;
  }

  /**
   * Poor Java, having fallen into both the localization trap and the UTF-16 trap ... :-( Having
   * char[] cloned is sad. And having to use 'ANSI' as a misnomer makes me really sad :-) Hope for
   * the best and use 'new String(byte[], StringFunc.CHAR8)' instead of this !
   *
   * @param iso_8859_1 This seems to be what Java refers to when all I need is a simple 1:1 byte
   *     mapping ...
   * @return
   */
  public static String string4Ansi(byte[] iso_8859_1) {
    final int len = iso_8859_1.length;
    final char[] out = new char[len];
    for (int i0 = 0; i0 < len; ++i0) {
      out[i0] = (char) (iso_8859_1[i0] & 0xff);
    }
    return new String(out);
  }

  /**
   * Poor Java, having fallen into both the localization trap and the UTF-16 trap ... :-( And having
   * to use 'ANSI' as a misnomer makes me really sad :-) Hope for the best and use
   * '.getBytes(StringFunc.CHAR8)' instead of this !
   *
   * @param literal256 String of bytes/ 8-bit char values.
   * @return
   */
  public static byte[] bytesAnsi(String literal256) {
    final int len = literal256.length();
    final byte[] out = new byte[len];
    for (int i0 = 0; i0 < len; ++i0) {
      out[i0] = (byte) literal256.charAt(i0);
    }
    return out;
  }

  public static String string4Cp125x(byte[] ansi) {
    final int len = ansi.length;
    final char[] out = new char[len];
    for (int i0 = 0; i0 < len; ++i0) {
      final char ch = (char) (ansi[i0] & 0xff);
      if ((0 <= ch) && (0x7f > ch)) {
        out[i0] = ch;
        continue;
      } else if (0xa0 > ch) {
        out[i0] = Cp125xOffs7f_2_Unicode[ch - 0x7f];
      } else {
        out[i0] = ch;
      }
    }
    return new String(out);
  }

  /**
   * For the sake of being less strict ...
   *
   * @param utf8 UTF-8 bytes (possibly 'messed up' with CP125x) or null
   * @return literal (as much as possible) or null
   */
  public static String string4Utf8(byte[] utf8) {
    if (null == utf8) {
      return null;
    }
    final int len1 = utf8.length - 1;
    final char[] out = new char[len1 + 1];
    boolean fast = true;
    for (int i0 = 0; i0 < utf8.length; ++i0) {
      out[i0] = (char) utf8[i0];
      if (0 > utf8[i0]) {
        fast = false;
        break;
      }
    }
    if (fast) {
      return new String(out);
    }
    try {
      String str = new String(utf8, "UTF-8");
      if (Arrays.equals(utf8, str.getBytes(CHAR16UTF8))) {
        return str;
      }
    } catch (UnsupportedEncodingException e) {
      // Better not mess it up with any system localization coincidences ...
      // -- out = new String( utf8 );
    }
    ///// Trying to make the most of it, assuming a dominance of some CP125x.
    int cCp125x = 0;
    int cUtf8 = 0;
    int i0 = 0;
    for (; i0 < len1; ++i0) {
      if (0 <= utf8[i0]) {
        continue;
      }
      if (0 <= utf8[++i0]) {
        cCp125x += 2;
        continue;
      }
      final char ch1 = (char) (utf8[i0] & 0xff);
      final char ch = (char) (utf8[i0 - 1] & 0xff);
      if ((0xc2 >= ch) || (0xf4 <= ch) || (0xc0 <= ch1)) {
        // Not a leading/ continuation byte.
        ++cCp125x;
        --i0;
        continue;
      }
      ++cUtf8;
      for (; (i0 < len1) && (0 > utf8[i0]); ++i0) {
        if (0xc0 <= (utf8[i0] & 0xff)) {
          --i0;
          break;
        }
      }
    }
    if (cCp125x > cUtf8) {
      ///// If it were UTF-8, it would be really messed up. Therefore, we assume CP125x.
      return string4Cp125x(utf8);
    }
    int combined = 0;
    i0 = 0;
    for (; i0 < len1; ++i0) {
      if (0 <= utf8[i0]) {
        // Leaving 0x7F as special case - since things are messed up anyway.
        out[i0 - combined] = (char) utf8[i0];
        continue;
      }
      final char ch = (char) (utf8[i0] & 0xff);
      final char ch1 = (char) (utf8[i0 + 1] & 0xff);
      if ((0xe0 > ch) && (0x80 <= ch1)) {
        out[i0 - combined] = (char) (((ch & 0x1f) << 6) | (ch1 & 0x3f));
        if ((0xc0 <= ch) && (0xa0 <= out[i0 - combined])) {
          ++combined;
          ++i0;
          continue;
        }
      } else if ((0xf0 > ch) && (0x80 <= ch1) && ((i0 + 2) <= len1) && (0 > utf8[i0 + 2])) {
        out[i0 - combined] =
            (char) (((ch & 0xf) << 12) | ((ch1 & 0x3f) << 6) | (utf8[i0 + 2] & 0x3f));
        if ((0x800 <= out[i0 - combined]) && (0xd800 > out[i0 - combined])) {
          combined += 2;
          i0 += 2;
          continue;
        }
      }
      // Rather misinterpret exotic char's than dropping CP125x text pieces ...
      if (0xa0 > ch) {
        out[i0 - combined] = Cp125xOffs7f_2_Unicode[ch - 0x7f];
      } else {
        out[i0 - combined] = ch;
      }
    }
    if (i0 == len1) {
      final char ch = (char) (utf8[i0] & 0xff);
      if ((0xa0 > ch) && (0x7f <= ch)) {
        out[i0 - combined] = Cp125xOffs7f_2_Unicode[ch - 0x7f];
      } else {
        out[i0 - combined] = ch;
      }
      ++i0;
    }
    return new String(out, 0, i0 - combined);
  }

  /**
   * Just for the sake of having it - use '.getBytes(StringFunc.STR16X)' instead of this !
   *
   * @param literal Text string /** @return UTF-8 encoding, as far as possible
   */
  public static byte[] bytesUtf8(String literal) {
    if (null == literal) {
      return null;
    }
    return literal.getBytes(StringFunc.CHAR16UTF8);
  }

  /**
   * @param hexLiteral String of hex digits, optionally marked as X'...'
   * @return matching or empty byte array (not null)
   */
  public static byte[] bytes4Hex(String hexLiteral) {
    if ((null == hexLiteral) || (0 >= hexLiteral.length())) {
      return new byte[0];
    }
    // For "X'" as marker:
    int offs = ('X' == hexLiteral.charAt(0)) ? 1 : 0;
    // Excluding possible "'" at the end:
    byte[] out = new byte[hexLiteral.length() / 2 - offs];
    char[] arr = hexLiteral.toCharArray();
    int i0 = out.length - 1 + offs;
    for (int i1 = 2 * i0 + 1; i0 >= offs; --i0) {
      byte b1 = NIBBLE[arr[i1--] & 0x1f];
      byte b0 = NIBBLE[arr[i1--] & 0x1f];
      out[i0 - offs] = (byte) ((b0 << 4) | b1);
    }
    return out;
  }

  public static byte[] bytes4Hex(byte[] hexAscii) {
    if ((null == hexAscii) || (0 >= hexAscii.length)) {
      return new byte[0];
    }
    int i0 = -1 + (hexAscii.length + 1) / 2;
    byte[] out = new byte[i0 + 1];
    for (int i1 = hexAscii.length - 1; i0 >= 0; --i0, --i1) {
      byte b1 = NIBBLE[hexAscii[i1] & 0x1f];
      i1 = (i1 >>> 1) << 1;
      byte b0 = NIBBLE[hexAscii[i1] & 0x1f];
      out[i0] = (byte) ((b0 << 4) | b1);
    }
    return out;
  }

  public static String string4HexUtf8(String hex2) {
    return string4Utf8(bytes4Hex(hex2));
  }

  public static String string4HexUtf16(char[] hex4, int start, int end) {
    if ((null == hex4) || (end <= start)) {
      return "";
    }
    // For "X'" as marker:
    int offs = ('X' == hex4[start]) ? 1 : 0;
    // Excluding possible "'" at the end:
    char[] out = new char[(end - start) / 4 - offs];
    int i1 = ((end - start) / 4) * 4 + start + offs * 2 - 1;
    i1 = (i1 >= end) ? (end - 1) : i1;
    for (int i0 = out.length - 1; i0 >= 0; --i0) {
      byte b3 = NIBBLE[hex4[i1--] & 0x1f];
      byte b2 = NIBBLE[hex4[i1--] & 0x1f];
      byte b1 = NIBBLE[hex4[i1--] & 0x1f];
      byte b0 = NIBBLE[hex4[i1--] & 0x1f];
      out[i0] = (char) ((b0 << 12) | (b1 << 8) | (b2 << 4) | b3);
    }
    return new String(out);
  }

  public static String hex4Bytes(byte[] data, int offset, int end, boolean marked) {
    if (null == data) {
      return "";
    }
    int offs2 = marked ? 2 : 0;
    char[] out = new char[2 * (end - offset) + (marked ? 3 : 0)];
    if (marked) {
      out[out.length - 1] = '\'';
      out[1] = '\'';
      out[0] = 'X';
    }
    for (int i0 = (end - offset) - 1; i0 >= 0; --i0) {
      final byte v0 = data[i0 + offset];
      out[i0 * 2 + offs2] = HEX[(v0 & 0xf0) >>> 4];
      out[i0 * 2 + 1 + offs2] = HEX[v0 & 0xf];
    }
    return new String(out);
  }

  public static String hex4Bytes(byte[] data, boolean marked) {
    if (null == data) {
      return "";
    }
    return hex4Bytes(data, 0, data.length, marked);
  }

  public static byte[] hexAscii4Bytes(byte[] data) {
    if (null == data) {
      return new byte[0];
    }
    byte[] out = new byte[2 * data.length];
    int i0 = data.length - 1;
    for (int i1 = 2 * i0 + 1; i0 >= 0; --i0) {
      final byte v0 = data[i0];
      out[i1--] = (byte) HEX[v0 & 0xf];
      out[i1--] = (byte) HEX[(v0 & 0xf0) >>> 4];
    }
    return out;
  }

  public static String hexUtf8(String literal, boolean marked) {
    return hex4Bytes(bytesUtf8(literal), marked);
  }

  public static String hexUtf16(String literal) {
    char[] out = new char[4 * literal.length()];
    for (int i0 = literal.length() - 1; i0 >= 0; --i0) {
      final char v0 = literal.charAt(i0);
      out[4 * i0] = HEX[(v0 & 0xf000) >>> 12];
      out[4 * i0 + 1] = HEX[(v0 & 0xf00) >>> 8];
      out[4 * i0 + 2] = HEX[(v0 & 0xf0) >>> 4];
      out[4 * i0 + 3] = HEX[v0 & 0xf];
    }
    return new String(out);
  }

  public static String string4Array(String[] array, String separator) {
    // Assuming '[...]' notation:
    //  String out = array.toString();
    //  return out.substring( 1, out.length() - 1 ).replace( ", ", separator );
    if (null == array) {
      return "";
    }
    StringBuilder out = new StringBuilder(array.length * 5);
    String sep = "";
    for (String el : array) {
      out.append(sep);
      sep = separator;
      out.append(el);
    }
    return out.toString();
  }

  public static String nameNormalize(String str, long bExtendedPunctSlash_Blank) {
    // Empty string not allowed:
    if (str.length() <= 0) {
      return ".";
    }
    // Basic case:
    if (str.matches("[0-9A-Za-z\\._]+") && (0 != (0x2 & bExtendedPunctSlash_Blank))) {
      return str;
    }
    if (6 == bExtendedPunctSlash_Blank) {
      // Expected to be ASCII char's:
      if (str.matches("[0-9A-Za-z\\._/]+") && !str.contains("//")) {
        return str;
      }
      String sx = str.trim();
      return sx.replace(' ', '_')
          .replace(',', '/')
          .replaceAll("[^0-9A-Za-z\\._/]", ".") // .
          .replaceAll("[_\\./]+/[_\\./]*", "/");
    } else if (2 == bExtendedPunctSlash_Blank) {
      // Expected to be ASCII char's:
      if (str.matches("[0-9A-Za-z\\._]+")) {
        return str;
      }
      String sx = str.trim();
      return sx.replace(' ', '_').replaceAll("[^0-9A-Za-z\\._]", ".");
    }
    int i0 = str.length() - 1;
    int found = 0;
    ///// Initially, just check without changing it.
    for (; i0 >= 0; --i0) {
      final char c0 = str.charAt(i0);
      if (('0' <= c0) && (c0 <= 'z')) {
        if (('9' >= c0) || ('a' <= c0)) {
          continue;
        } else if (('A' <= c0) && (c0 <= 'Z')) {
          continue;
        }
        if (0 <= "!#$*<>?[]^{}".indexOf(c0)) {
          break;
        }
        continue;
      } else if ((0x80 <= c0) && (0 != (0x1 & bExtendedPunctSlash_Blank))) {
        found |= 0x1;
        continue;
      } else if (' ' >= c0) {
        if ((' ' > c0) || (0 == (0x100 & bExtendedPunctSlash_Blank))) {
          break;
        }
        continue;
      } else if (0 <= "!#$*<>?[]^{}".indexOf(c0)) {
        break;
      }
      found |= ('0' > c0) ? 0x2 : 0;
      found |= ('/' == c0) ? 0x4 : 0;
    }
    if ((0 > i0) && (found == (found & bExtendedPunctSlash_Blank))) {
      return str;
    }
    char[] build = str.toCharArray();
    for (i0 = 0; i0 < build.length; ++i0) {
      final char c0 = str.charAt(i0);
      if (('0' <= c0) && (c0 <= 'z')) {
        if (0 <= "!#$*<>?[]^{}".indexOf(c0)) {
          build[i0] = '.';
          continue;
        }
      } else if (0x80 <= c0) {
        build[i0] = (0 == (0x1 & bExtendedPunctSlash_Blank)) ? '.' : build[i0];
        continue;
      } else if (' ' >= c0) {
        if ((' ' > c0) || (0 == (0x100 & bExtendedPunctSlash_Blank))) {
          build[i0] = '.';
        }
        continue;
      } else if (0 <= "!#$*<>?[]^{}".indexOf(c0)) {
        build[i0] = '.';
        continue;
      }
      if (('0' > c0) && (0 == (0x2 & bExtendedPunctSlash_Blank))) {
        build[i0] = '.';
      } else if (('/' == c0) && (0 == (0x4 & bExtendedPunctSlash_Blank))) {
        build[i0] = '.';
      }
    }
    return // (0 >= count) ? "." :
    new String(build);
  }

  public static String csvField4Text(String str) {
    if ((str.indexOf('\t') < 0) && (str.indexOf('\n') < 0)) {
      return str;
    }
    return str.replace('\t', '.').replace('\n', '.');
  }

  public static String[] sortStd(String[] strings) {
    CollationKey[] keys = new CollationKey[strings.length];
    for (int i0 = 0; i0 < strings.length; ++i0) {
      keys[i0] = coll.getCollationKey(strings[i0]);
    }
    Arrays.sort(keys);
    String[] out = new String[strings.length];
    for (int i0 = 0; i0 < out.length; ++i0) {
      out[i0] = keys[i0].getSourceString();
    }
    return out;
  }

  public static byte[] coll64xBytes(String xStr, int xOffset) {
    if (null == coll) {
      populateCollData();
    }
    String str =
        (Dib2Constants.SHASH_MAX * 2 >= xStr.length())
            ? xStr
            : xStr.substring(0, Dib2Constants.SHASH_MAX * 2);
    byte[] out = new byte[str.length() + xOffset];
    int len = xOffset;
    for (int i0 = 0; i0 < str.length(); ++i0) {
      char ch = str.charAt(i0);
      if ((coll64Key4Char.length > ch) && (63 != coll64Key4Char[ch])) {
        if (0 != coll64Key4Char[ch]) {
          out[len++] = (byte) (0x40 | coll64Key4Char[ch]);
        }
        continue;
      }
      byte[] tmp =
          coll.getCollationKey(str.substring(i0))
              .toByteArray(); // coll128oString( str.substring( i0 ) );
      int tlen = tmp.length;
      for (; tlen > 0; --tlen) {
        if ((0 != tmp[tlen - 1]) && (tmp[tlen - 1] != 1)) { // (byte) 0x80)) {
          break;
        }
      }
      if ((len + tlen * 8 / 6 + 2) >= out.length) {
        out = Arrays.copyOf(out, len + tlen * 8 / 6 + 2);
      }
      int ix = 0;
      while ((tlen >= collMinLenDigit) || (tlen >= collMinLenLetter)) {
        ix = MiscFunc.binSearchUnsigned(coll64CollMin4Key, tmp);
        ix = (0 <= ix) ? ix : ((-1 == ix) ? 0 : (-ix - 2));
        if ((coll64Key4Char['0'] > ix) || (ix >= 63)) {
          break;
        }
        out[len++] = (byte) (0x40 | ix);
        final int cut = (coll64Key4Char['9'] >= ix) ? collMinLenDigit : collMinLenLetter;
        tlen -= cut;
        if (0 < tlen) {
          System.arraycopy(tmp, cut, tmp, 0, tlen);
        }
      }
      if (63 > ix) {
        continue;
      }
      //    boolean escape = ((0 >= ix) || (63 <= ix)); // || ((i0 + 1) >= str.length()));
      //    if (!escape && !MiscFunc.equalsRange( tmp, 0, coll64CollMin4Key[ ix ].length,
      // coll64CollMin4Key[ ix ] )) {
      out[len++] = (byte) (0x40 | 63);
      tlen = (Dib2Constants.SHASH_MAX * 2 < tlen) ? Dib2Constants.SHASH_MAX * 2 : tlen;
      int shift = 2;
      for (int it = 0; it < tlen; ++it, shift += 2) {
        if (6 < shift) {
          ++len;
          shift = 2;
        }
        out[len++] |= (byte) ((tmp[it] >>> shift) & 0x3f);
        out[len] = (byte) (0x40 | (((tmp[it] << (6 - shift))) & 0x3f));
      }
      if (0 != out[len]) {
        ++len;
      }
      break;
    }
    return (len == out.length) ? out : Arrays.copyOf(out, len);
  }

  public static byte[] coll64xBytes(byte[] xStr, int xOffset) {
    if (null == coll) {
      populateCollData();
    }
    int len0 = xStr.length;
    len0 = ((Dib2Constants.SHASH_MAX * 2) <= len0) ? len0 : (Dib2Constants.SHASH_MAX * 2);
    byte[] out = new byte[len0 + xOffset];
    int len = xOffset;
    for (int i0 = 0; i0 < len0; ++i0) {
      int ch = xStr[i0] & 0xff;
      if ((0xe0 <= ch) || ((0x80 <= ch) && ((i0 + 1) >= len0))) {
        return coll64xBytes(string4Utf8(xStr), xOffset);
      } else if (0x80 <= ch) {
        ch = ((ch & 0x3f) << 6) | (xStr[++i0] & 0x3f);
      }
      if ((coll64Key4Char.length > ch) && (63 != coll64Key4Char[ch])) {
        if (0 != coll64Key4Char[ch]) {
          out[len++] = (byte) (0x40 | coll64Key4Char[ch]);
        }
        continue;
      }
      return coll64xBytes(string4Utf8(xStr), xOffset);
    }
    return Arrays.copyOf(out, len);
  }

  private static String asciiCompressed4String_xBase64 = null;

  public static byte[] asciiCompressed4Bytes(byte[] string) {
    byte[] out = new byte[2 * string.length];
    int cnt = 0;
    if (null == asciiCompressed4String_xBase64) {
      char[] ax = new char[62];
      for (char ch = ' '; cnt < 62; ch = (char) ((0x7e == ch) ? 0xdf : (ch + 1))) {
        if (0 > Dib2Constants.base64XString.indexOf(ch)) {
          ax[cnt++] = ch;
        }
      }
      asciiCompressed4String_xBase64 = new String(ax);
      cnt = 0;
    }
    for (int i0 = 0; i0 < string.length; ++i0) {
      final byte v0 = string[i0];
      int ix;
      if ((' ' <= v0) && (0 <= (ix = Dib2Constants.base64XString.indexOf((char) v0)))) {
        out[cnt++] = (byte) ix;
        continue;
      } else if ((' ' <= v0) && (v0 < 0x7f)) {
        ix = asciiCompressed4String_xBase64.indexOf((char) v0);
        out[cnt++] = (byte) (ix | 0x40);
        continue;
      } else if ((0xc2 == (v0 & 0xfe))
          && ((i0 + 1) < string.length)
          && (0xc0 > (string[i0 + 1] & 0xff))) {
        ///// UTF-8.
        char cx = (char) (((v0 & 3) << 6) | (string[i0 + 1] & 0x3f));
        if (0 <= (ix = asciiCompressed4String_xBase64.indexOf(cx))) {
          out[cnt++] = (byte) (ix | 0x40);
          ++i0;
          continue;
        }
      }
      out[cnt++] = (byte) ((v0 >= 0) ? 0x7f : 0x7e);
      out[cnt++] = (byte) (v0 & 0x7f);
    }
    return Arrays.copyOf(out, cnt);
  }

  public static byte[] bytes4AsciiCompressed(byte[] ascii) {
    byte[] out = new byte[2 * ascii.length];
    int cnt = 0;
    if (null == asciiCompressed4String_xBase64) {
      asciiCompressed4Bytes(new byte[0]);
    }
    for (int i0 = 0; i0 < ascii.length; ++i0) {
      final byte v0 = ascii[i0];
      if (0x40 > v0) {
        out[cnt++] = (byte) Dib2Constants.base64XChars[v0 & 0x3f];
      } else if (0x7e > v0) {
        char cx = asciiCompressed4String_xBase64.charAt(v0 & 0x3f);
        if (0x80 > cx) {
          out[cnt++] = (byte) cx;
        } else {
          byte[] utf8 = bytesUtf8("" + cx);
          out[cnt++] = utf8[0];
          out[cnt++] = utf8[1];
        }
      } else {
        out[cnt++] = (byte) ((0x7f == v0) ? 0 : 0x80);
        ++i0;
        if (i0 < ascii.length) {
          out[cnt - 1] |= ascii[i0];
        }
      }
    }
    return Arrays.copyOf(out, cnt);
  }

  // =====
}
