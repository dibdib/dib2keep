// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import com.gitlab.dibdib.picked.common.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;
import net.sf.dibdib.config.*;

// =====

public final class MiscFunc {

  public static final StringBuffer logBuffer = new StringBuffer(22000);

  // =====

  // public static void checkPlatform() {
  // Check:
  //  BigInteger P;
  //  P = new BigInteger( 64, 1, new Random( 42 ) );
  //  Dib2Root.log( "receiver", "random check > 16: " + P );
  //  P = new BigInteger( 64, 1, new Random( 42 ) );
  //  Dib2Root.log( "receiver", "random check > 16: " + P );
  //  P = new BigInteger( 8, 1, new Random( 42 ) );
  //  Dib2Root.log( "receiver", "random check < 16: " + P );
  //  P = new BigInteger( 8, 1, new Random( 42 ) );
  //  Dib2Root.log( "receiver", "random check < 16: " + P );
  // }

  public static final char[] BITS = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, // /
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, // /
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // /
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // /
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // /
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // /
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // /
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, // /
  };

  // =====

  public static String readStream(InputStream is) {
    try {
      byte[] text = new byte[90000];
      int cnt = 0;
      while (cnt < (text.length - 200)) {
        int cx = is.read(text, cnt, text.length - cnt - 100);
        if (0 >= cx) {
          break;
        }
        cnt += cx;
      }
      text[cnt++] = '\n';
      return new String(text, 0, cnt);
    } catch (IOException ex) {
      ExceptionAdapter.throwAdapted(ex, MiscFunc.class, "arg = InputStream");
      return null;
    } finally {
      if (null != is) {
        try {
          is.close();
        } catch (IOException ex) {
        }
      }
    }
  }

  public static double roundForRxxUsage(double val) {
    double check = (0.0 <= val) ? val : -val;
    if ((1.0e-6 <= check) && (check < 1.0e12)) {
      long bits = Double.doubleToRawLongBits(val);
      long significand = (bits & 0x000fffffffffffffL);
      if (0 == (significand & 0xfL)) {
        return val;
      } else if (0xffL == (significand & 0xffL)) {
        return Math.nextAfter(
            val, (0 <= val) ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY);
      } else if (0xfcL == (significand & 0xfcL)) {
        bits |= 0x3L;
      } else if (0 == (significand & 0xfcL)) {
        bits &= ~0x3L;
        return Double.longBitsToDouble(bits);
      } else if (0xfff8L == (significand & 0xfff8L)) {
        bits |= 0x7L;
      } else if (0 == (significand & 0xfff8L)) {
        bits &= ~0x7L;
        return Double.longBitsToDouble(bits);
      } else if (0xfffff0L == (significand & 0xfffff0L)) {
        bits |= 0xfL;
      } else if (0 == (significand & 0xfffff0L)) {
        bits &= ~0xfL;
        return Double.longBitsToDouble(bits);
      } else if (0 == (significand & 0x1L)) {
        bits |= 1L;
      }
      val = Double.longBitsToDouble(bits);
      return Math.nextAfter(val, (0 <= val) ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY);
    }
    return val;
  }

  public static boolean equalRounded(double v0, double v1) {
    if ((v0 == v1)
        || (-Double.MAX_VALUE / 2 >= v0)
        || (-Double.MAX_VALUE / 2 >= v1)
        || (Double.MAX_VALUE / 2 <= v0)
        || (Double.MAX_VALUE / 2 <= v1)) {
      return (v0 == v1);
    }
    // Some 44 bits ...
    final double epsilon = (1.0 / (1L << 44));
    final double absDiff = (v0 <= v1) ? (v1 - v0) : (v0 - v1);
    final double sumAbs = ((0.0 <= v0) ? v0 : -v0) + ((0.0 <= v1) ? v1 : -v1);
    if (v0 == 0.0 || v1 == 0.0 || (sumAbs < Double.MIN_NORMAL)) {
      return absDiff < (Double.MIN_NORMAL / 2);
    }
    return (absDiff / sumAbs) <= epsilon;
  }

  public static String base64X4Bitlists(int[] acBits, long... vals) {
    int len = 0;
    for (int b0 : acBits) {
      len += b0 / 6;
    }
    char[] out = new char[len];
    int inx = len;
    for (int iv = vals.length - 1; iv >= 0; --iv) {
      long val = vals[iv];
      for (int i0 = acBits[iv] / 6 - 1; i0 >= 0; --i0) {
        int b64 = 0x3f & (int) val;
        --inx;
        out[inx] = Dib2Constants.base64XChars[b64];
        val = val >>> 6;
      }
    }
    if (0 != inx) {
      return null;
    }
    return new String(out);
  }

  public static String base64Cd4Bitlists(int[] acBits, long... vals) {
    int len = 0;
    for (int b0 : acBits) {
      len += b0 / 6;
    }
    int[] out = new int[len];
    int inx = len;
    for (int iv = vals.length - 1; iv >= 0; --iv) {
      long val = vals[iv];
      for (int i0 = acBits[iv] / 6 - 1; i0 >= 0; --i0) {
        int b64 = 0x3f & (int) val;
        --inx;
        out[inx] = b64;
        val = val >>> 6;
      }
    }
    if (0 != inx) {
      return null;
    }
    int checkDigit = CdDammFunc.checkDigit(out, 6);
    char[] ac = new char[len + 1];
    ac[len] = Dib2Constants.base64XChars[checkDigit];
    for (int i0 = 0; i0 < len; ++i0) {
      ac[i0] = Dib2Constants.base64XChars[out[i0]];
    }
    return new String(ac);
  }

  private static byte[] base64x4Bytes_b64 = new byte[64];

  public static byte[] base64x4Bytes(byte[] string) {
    if (0 == base64x4Bytes_b64[0]) {
      for (int i0 = 0; i0 < 64; ++i0) {
        base64x4Bytes_b64[i0] = (byte) (Dib2Constants.base64XChars[i0]);
      }
    }
    byte[] out = new byte[string.length + (string.length + 2) / 3];
    final int len = string.length - 3;
    int cnt = 0;
    int iStr = 0;
    for (; iStr < len; iStr += 3) {
      out[cnt++] = base64x4Bytes_b64[((string[iStr] & 0xff) >>> 2)];
      out[cnt++] =
          base64x4Bytes_b64[(((string[iStr] & 0x3) << 4) | ((string[iStr + 1] & 0xff) >>> 4))];
      out[cnt++] =
          base64x4Bytes_b64[(((string[iStr + 1] & 0xf) << 2) | ((string[iStr + 2] & 0xff) >>> 6))];
      out[cnt++] = base64x4Bytes_b64[(string[iStr + 2] & 0x3f)];
    }
    if (iStr < len) {
      out[cnt++] = base64x4Bytes_b64[((string[iStr] & 0xff) >>> 2)];
      out[cnt++] =
          base64x4Bytes_b64[
              ((string[iStr] & 0x3) << 4)]; // | ((0 < pad.length) ? (pad[ 0 ] & 0xf) : 0))];
      ++iStr;
    }
    if (iStr < len) {
      out[cnt - 1] =
          base64x4Bytes_b64[((string[iStr] & 0x3) << 4) | ((string[iStr + 1] & 0xff) >>> 4)];
      out[cnt++] = base64x4Bytes_b64[((string[iStr + 1] & 0xf) << 2)];
    }
    return out;
  }

  /**
   * FNV-1a, also for short keys. Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
   *
   * @param bits >= 16, <= 32
   */
  public static int hash32_fnv1a(int bits, String... pieces) {
    //  final int prime = 0x1000193; // 16777619;
    final int offs = 0x811c9dc5; // (int) 2166136261L;
    int hash = offs;
    for (String dat : pieces) {
      final byte[] data = StringFunc.bytesUtf8(dat);
      for (byte b0 : data) {
        hash ^= 0xff & b0;
        // hash *= prime;
        hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
      }
    }
    if (bits == 32) {
      return hash;
    }
    // XOR folding:
    return ((hash >>> bits) ^ hash) & ((1 << bits) - 1);
  }

  /**
   * FNV-1a, also for short keys. Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
   *
   * @param bits >= 32, <= 64
   */
  public static long hash64_fnv1a(byte[] data, int bits) {
    //  final long prime = 0x100000001b3L; // 1099511628211L;
    final long offs = 0xcbf29ce484222325L; // any non-zero: 14695981039346656037
    long hash = offs;
    for (byte b0 : data) {
      hash ^= 0xff & b0;
      // hash *= prime;
      hash += (hash << 1) + (hash << 4) + (hash << 5) + (hash << 7) + (hash << 8) + (hash << 40);
    }
    if (bits == 64) {
      return hash;
    }
    // XOR folding:
    return ((hash >>> bits) ^ hash) & ((1L << bits) - 1L);
  }

  public static long hash32_partialString(String... parts) {
    long hash = 0;
    for (String content : parts) {
      int len = content.length();
      int hash0 =
          (4096 > len)
              ? hash32_fnv1a(32, content) // ,
              : hash32_fnv1a(
                  32,
                  content.substring(0, 1024),
                  content.substring(len / 2, len / 2 + 1024),
                  content.substring(len - 1024));
      hash ^= hash0 & ((1L << 32) - 1);
    }
    return hash;
  }

  public static void writeFile(String path, byte[] dat, int offset, int length, byte[] xHeader) {
    RandomAccessFile file = null;
    try {
      file = new RandomAccessFile(path, "rw");
      file.seek(0);
      if (xHeader != null) {
        file.write(xHeader);
      }
      file.write(dat, offset, length);
      file.setLength(file.getFilePointer());
    } catch (IOException ex) {
      ExceptionAdapter.throwAdapted(ex, MiscFunc.class, "path = " + path);
    } finally {
      if (null != file) {
        try {
          file.close();
        } catch (IOException ex) {
        }
      }
    }
  }

  public static byte[] readFile(String xPath, int xExpectedFileStruc) {
    RandomAccessFile file = null;
    try {
      file = new RandomAccessFile(xPath, "r");
      byte[] tlv = new byte[2];
      byte[] dat;
      int offs = 0;
      if (0 >= xExpectedFileStruc) {
        dat = new byte[(int) file.length()];
      } else {
        file.read(tlv);
        if (Dib2Constants.MAGIC_BYTES[0] == tlv[0]) {
          ///// Old format.
          byte[] xlen = len4880((int) file.length());
          offs = xlen.length + 1;
          dat = new byte[1 + xlen.length + (int) file.length()];
          dat[0] = (byte) Dib2Constants.RFC4880_EXP2;
          System.arraycopy(xlen, 0, dat, 1, xlen.length);
        } else {
          dat = new byte[(int) file.length()];
        }
        file.seek(0);
      }
      file.read(dat, offs, dat.length - offs);
      return dat;
    } catch (IOException ex) {
      ExceptionAdapter.throwAdapted(ex, MiscFunc.class, "path = " + xPath);
      return null;
    } finally {
      if (null != file) {
        try {
          file.close();
        } catch (IOException ex) {
        }
      }
    }
  }

  /**
   * OLD: little endian.
   *
   * @param val >= 0
   * @return
   */
  public static byte[] int2Tlv(int val) {
    if (32 > val) {
      return new byte[] {(byte) val};
    }
    if (0x100 > val) {
      return new byte[] {0x21, (byte) val};
    }
    byte[] little =
        new byte[] {
          (byte) (val & 0xff),
          (byte) ((val >>> 8) & 0xff),
          (byte) ((val >>> 16) & 0xff),
          (byte) (val >>> 24)
        };
    if (0x10000 > val) {
      return new byte[] {0x22, little[0], little[1]};
    }
    if (0x1000000 > val) {
      return new byte[] {0x23, little[0], little[1], little[2]};
    }
    return new byte[] {0x24, little[0], little[1], little[2], little[3]};
  }

  public static byte[] len4880(int len) {
    return (192 > len)
        ? new byte[] {(byte) len} // ,
        : ((8283 >= len)
            ? new byte[] {(byte) ((((len - 192) >> 8) & 0xff) + 192), (byte) (len - 192)} // ,
            : new byte[] {
              (byte) 0xff, (byte) (len >> 24), (byte) (len >> 16), (byte) (len >> 8), (byte) len
            });
  }

  /**
   * RFC 4880: header len 2 if packet len < 192, 3 if <= 8283, 6 otherwise.
   *
   * @param packet
   * @param offsetLv
   * @return
   */
  public static int getPacketBodyLen(byte[] packet, int offsetLv) {
    final byte[] p = packet;
    final int o = offsetLv;
    return (192 > (p[o] & 0xff))
        ? (p[o] & 0xff) // ,
        : ((223 >= (p[o] & 0xff))
            ? (((((p[o] & 0xff) - 192) << 8) | (p[o + 1] & 0xff)) + 192) // ,
            : (((p[o + 1] & 0xff) << 24)
                | ((p[o + 2] & 0xff) << 16)
                | ((p[o + 3] & 0xff) << 8)
                | (p[o + 4] & 0xff)));
  }

  public static int getPacketHeaderLen(byte[] packet, int offsetTag) {
    return (192 > (packet[offsetTag + 1] & 0xff))
        ? 2 // ,
        : ((223 >= (packet[offsetTag + 1] & 0xff)) ? 3 : 6);
  }

  /**
   * Cmp. RFC 4880. E.g. for tag 11 (literal): one-byte format ('b' binary/ 'u' UTF8),
   * len-of-filename, filename, 4-byte time stamp, data.
   *
   * @param xTag e.g. 0x80 | 0x40 | 11=literal, | 62=experimental2
   * @param xName4Literal (currently NULL)
   * @param xyData
   * @param xOffs4Reuse >2 for keeping data in xyData[], -1 for new byte[]
   * @return new data starting at 0 or at data[0]
   */
  public static byte[] packet4880X(
      int xTag, byte[] xName4Literal, byte[] xyData, int xOffs4Reuse, int xEnd) {
    // TODO: additional len if (xName4Literal != NULL)
    int len = ((0 > xEnd) ? xyData.length : xEnd) - xOffs4Reuse;
    byte[] qlen = len4880(len);
    byte[] out = (2 > xOffs4Reuse) ? new byte[len + qlen.length + 1] : xyData;
    int offs = (2 > xOffs4Reuse) ? 0 : (xOffs4Reuse - qlen.length - 1);
    out[0] = (byte) offs;
    out[offs] = (byte) xTag;
    System.arraycopy(qlen, 0, out, offs + 1, qlen.length);
    if (2 > xOffs4Reuse) {
      System.arraycopy(xyData, xOffs4Reuse, out, 1 + qlen.length, len);
    }
    return out;
  }

  /**
   * OLD: little endian.
   *
   * @param len
   * @return
   */
  public static byte[] lvLen(long len) {
    // bits: 00xxxxxx/ 010xxxxx
    if (len <= ((3 << 5) - 1)) {
      return new byte[] {(byte) len};
    }
    // bits: 0xxxxxxx
    if (len <= ((1 << 15) - 2)) {
      return new byte[] {(byte) (len & 0x7f), (byte) (len >> 7)};
    }
    // bits: 10xxxxxx, 110xxxxx, ...
    int lead = 0x80;
    byte[] out = new byte[9];
    int inx = 8;
    for (int shift = 57; shift >= 22; shift -= 7) {
      out[inx] = (byte) ((len >>> shift) & 0xff);
      lead += lead >> 1;
    }
    out[0] |= lead;
    return out;
  }

  public static long long4Bytes(byte[] dat, int offs, int count) {
    long out = 0;
    for (int i0 = 0; i0 < count; ++i0) {
      out = (out << 8) | (dat[offs + i0] & 0xff);
    }
    return out;
  }

  public static int bytes4Long(byte[] out, int offset, long val, int size) {
    for (int i0 = size - 1; i0 >= 0; --i0) {
      out[i0 + offset] = (byte) val;
      val >>>= 8;
    }
    return size;
  }

  /**
   * Compress using Java's Deflater.
   *
   * @param xyData data[offset] should contain part of magic byte (>= 0x30)
   * @return new data starting at 0 or at data[0]
   */
  public static byte[] compress(int xTag, byte[] xyData, int xOffs4Reuse, int to) {
    Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
    int len = to - xOffs4Reuse;
    byte[] full = len4880(len);
    deflater.setInput(xyData, xOffs4Reuse, len);
    deflater.finish();
    int outOffs = 22;
    byte[] out = new byte[2 * xyData.length + 2 * outOffs];
    len = deflater.deflate(out, outOffs, out.length - outOffs);
    deflater.end();
    byte[] mb2 = Dib2Constants.MAGIC_BYTES;
    byte[] time = len4880((int) (DateFunc.currentTimeMillisLinearized() / 1000));
    byte[] mbNStamp = new byte[] {mb2[0], mb2[1], (byte) xTag, (byte) (full.length + time.length)};
    mbNStamp = Arrays.copyOf(mbNStamp, mbNStamp.length + mbNStamp[3]);
    System.arraycopy(full, 0, mbNStamp, 4, full.length);
    System.arraycopy(time, 0, mbNStamp, 4 + full.length, time.length);
    byte[] header = (1 < xTag) ? mbNStamp : new byte[xTag];
    if ((len >= (xyData.length - xOffs4Reuse)) && (0 < xOffs4Reuse)) {
      // No compression:
      out = Arrays.copyOfRange(xyData, xOffs4Reuse, to);
      len = out.length;
      full = new byte[] {0};
    }
    byte[] ll = len4880(len + header.length);
    if (1 == header.length) {
      // Old format (magic bytes not used).
      byte[] tlv0 = int2Tlv(to - xOffs4Reuse);
      byte[] tlv1 = (len >= 32) ? int2Tlv(len) : new byte[] {0x21, (byte) len};
      xyData[0] = (byte) xOffs4Reuse;
      System.arraycopy(tlv0, 0, xyData, xOffs4Reuse, tlv0.length);
      // 0x50 + 0x2x: marker for binary data + len-of-len:
      xyData[xOffs4Reuse + tlv0.length] = (byte) (0x50 + tlv1[0]);
      System.arraycopy(tlv1, 1, xyData, xOffs4Reuse + 1 + tlv0.length, tlv1.length - 1);
      System.arraycopy(out, outOffs, xyData, xOffs4Reuse + tlv0.length + tlv1.length, len);
      return xyData; // tlv0.length + tlv1.length + len;
    } else if (0 < header.length) {
      int offs = (xOffs4Reuse - header.length - 1 - ll.length);
      if (0 <= offs) {
        System.arraycopy(out, outOffs, xyData, xOffs4Reuse, len);
        out = xyData;
        outOffs = xOffs4Reuse;
      } else {
        out = Arrays.copyOf(out, outOffs + len);
        offs = outOffs - header.length - 1 - ll.length;
      }
      out[0] = (byte) offs;
      out[offs++] = (byte) Dib2Constants.RFC4880_EXP2;
      System.arraycopy(ll, 0, out, offs, ll.length);
      System.arraycopy(header, 0, out, offs + ll.length, header.length);
    } else {
      if (10 < xOffs4Reuse) {
        System.arraycopy(out, outOffs, xyData, xOffs4Reuse, len);
        out = xyData;
        outOffs = xOffs4Reuse;
      }
      out[0] = (byte) (outOffs - ll.length - full.length);
      System.arraycopy(full, 0, out, outOffs - ll.length - full.length, full.length);
      System.arraycopy(ll, 0, out, outOffs - ll.length, ll.length);
    }
    return out;
  }

  public static byte[] decompress(byte[] xData, int from, int to) {
    int offs = from;
    int full = 6 * (to - from);
    if (xData[offs] == (byte) Dib2Constants.RFC4880_EXP2) {
      offs +=
          (192 > (xData[offs + 1] & 0xff))
              ? 2 // ,
              : ((223 >= (xData[offs + 1] & 0xff)) ? 3 : 6);
    }
    if (0x27 > (xData[from] & 0xff)) {
      // Old format.
      return null;
    } else if (Dib2Constants.MAGIC_BYTES[0] == xData[offs]) {
      // Skip markers:
      for (int i0 = 0; i0 <= Dib2Constants.MAGIC_BYTES.length; ++i0, ++offs) {
        if ('z' == xData[offs]) {
          ++offs;
          break;
        }
      }
      full = getPacketBodyLen(xData, offs + 1);
      if (0 >= full) {
        return Arrays.copyOfRange(xData, from + 2, to);
      }
      offs += 1 + xData[offs];
    }
    Inflater inflater = new Inflater(true);
    inflater.setInput(xData, offs, to - offs);
    byte[] out = new byte[full];
    int done = 0;
    try {
      done = inflater.inflate(out);
      while (!inflater.finished()) {
        out = Arrays.copyOf(out, 2 * out.length);
        int add = inflater.inflate(out, done, out.length - done);
        if (0 >= add) {
          break;
        }
        done += add;
      }
    } catch (DataFormatException e) {
      return xData;
    }
    inflater.end();
    //  if (done < (data.length / 2 - 32)) {
    return Arrays.copyOf(out, done);
  }

  public static int bitsCount(long bitList, int max) {
    int count = 0;
    for (int i0 = 0; i0 < max; i0 += 8) {
      count += BITS[0xff & (int) (bitList >>> i0)];
    }
    return count;
  }

  private static final long bitsCount_m1 = 0x5555555555555555L;
  private static final long bitsCount_m2 = 0x3333333333333333L;
  private static final long bitsCount_m4 = 0x0f0f0f0f0f0f0f0fL;
  private static final long bitsCount_m8 = 0x00ff00ff00ff00ffL;
  private static final long bitsCount_m16 = 0x0000ffff0000ffffL;
  private static final long bitsCount_m32 = 0x00000000ffffffffL;

  /** Sideways ... */
  public static int bitsCount(long bitList) {
    bitList = (bitList & bitsCount_m1) + ((bitList >>> 1) & bitsCount_m1);
    bitList = (bitList & bitsCount_m2) + ((bitList >>> 2) & bitsCount_m2);
    bitList = (bitList & bitsCount_m4) + ((bitList >>> 4) & bitsCount_m4);
    bitList = (bitList & bitsCount_m8) + ((bitList >>> 8) & bitsCount_m8);
    bitList = (bitList & bitsCount_m16) + ((bitList >>> 16) & bitsCount_m16);
    bitList = (bitList & bitsCount_m32) + ((bitList >>> 32) & bitsCount_m32);
    return (int) bitList;
  }

  public static int bitsParity(long bitList) {
    return 1
        & BITS[
            (0xff & (int) bitList)
                ^ (0xff & (int) (bitList >>> 8))
                ^ (0xff & (int) (bitList >>> 16))
                ^ (0xff & (int) (bitList >>> 24))
                ^ (0xff & (int) (bitList >>> 32))
                ^ (0xff & (int) (bitList >>> 40))
                ^ (0xff & (int) (bitList >>> 48))
                ^ (0xff & (int) (bitList >>> 56))];
  }

  public static long bitsMaskPower2(long value) {
    --value;
    value |= value >>> 1;
    value |= value >>> 2;
    value |= value >>> 4;
    value |= value >>> 8;
    value |= value >>> 16;
    value |= value >>> 32;
    return value;
  }

  public static boolean equalsRange(byte[] left, int from, int to, byte[] right) {
    if (((to - from) != right.length) || (to > left.length)) {
      return false;
    }
    int iL = from;
    for (int iR = 0; iL < to; ++iL, ++iR) {
      if (left[iL] != right[iR]) {
        return false;
      }
    }
    return true;
  }

  public static int compareUnsigned(byte[] left, byte[] right) {
    int len = (left.length > right.length) ? right.length : left.length;
    int cmp = 0;
    for (int i0 = 0; (i0 < len) && (0 == cmp); ++i0) {
      cmp = (left[i0] & 0xff) - (right[i0] & 0xff);
    }
    if (0 == cmp) {
      cmp = left.length - right.length;
    }
    return cmp;
  }

  public static int compareSigned(byte[] left, byte[] right) {
    int len = (left.length > right.length) ? right.length : left.length;
    int cmp = 0;
    for (int i0 = 0; (i0 < len) && (0 == cmp); ++i0) {
      cmp = left[i0] - right[i0];
    }
    if (0 == cmp) {
      cmp = left.length - right.length;
    }
    return cmp;
  }

  public static int binSearchUnsigned(byte[][] sortedList, byte[] key) {
    int min = 0;
    int max = sortedList.length - 1;
    while (min <= max) {
      final int ix = (min + max) >> 1;
      final int cmp = compareUnsigned(sortedList[ix], key);
      if (cmp < 0) {
        min = ix + 1;
      } else if (cmp > 0) {
        max = ix - 1;
      } else {
        return ix;
      }
    }
    return -1 - min;
  }

  public static int binSearchSigned(byte[][] sortedList, byte[] key) {
    int min = 0;
    int max = sortedList.length - 1;
    while (min <= max) {
      final int ix = (min + max) >> 1;
      final int cmp = compareSigned(sortedList[ix], key);
      if (cmp < 0) {
        min = ix + 1;
      } else if (cmp > 0) {
        max = ix - 1;
      } else {
        return ix;
      }
    }
    return -1 - min;
  }

  public static int indexOf(byte[] strUtf8, byte[] subStr, int offs) {
    for (int i0 = offs; i0 < strUtf8.length; ++i0) {
      if (strUtf8[i0] == subStr[0]) {
        int i1 = 0;
        for (; i1 < subStr.length; ++i1) {
          if (strUtf8[i0 + i1] != subStr[i1]) {
            break;
          }
        }
        if (i1 >= subStr.length) {
          return i0;
        }
      }
    }
    return -1;
  }

  public static int indexOf(byte[] strUtf8, byte[] subStr) {
    return indexOf(strUtf8, subStr, 0);
  }

  public static byte[] appendResize(byte[] xyArray, int[] xyPos, byte[] xToAppend) {
    int newlen = xyArray.length;
    while ((xyPos[0] + xToAppend.length) >= newlen) {
      newlen *= 2;
    }
    if (newlen > xyArray.length) {
      xyArray = Arrays.copyOf(xyArray, newlen);
    }
    System.arraycopy(xToAppend, 0, xyArray, xyPos[0], xToAppend.length);
    xyPos[0] += xToAppend.length;
    xyArray[xyPos[0]++] = '\n';
    return xyArray;
  }

  public static byte[] appendClone(byte[] str, int offs, byte[] app) {
    if ((str == null) || (str.length <= 0)) {
      return app;
    } else if ((app == null) || (app.length <= 0)) {
      return str;
    }
    offs = (offs >= 0) ? offs : str.length;
    byte[] out = new byte[offs + app.length];
    System.arraycopy(str, 0, out, 0, (offs < str.length) ? offs : str.length);
    System.arraycopy(app, 0, out, offs, app.length);
    return out;
  }

  public static byte[] replaceClone(byte[] strUtf8, byte chOld, byte chNew) {
    int i0 = strUtf8.length - 1;
    for (; i0 >= 0; --i0) {
      if (strUtf8[i0] == chOld) {
        break;
      }
    }
    if (i0 < 0) {
      return strUtf8;
    }
    byte[] out = new byte[strUtf8.length];
    System.arraycopy(strUtf8, 0, out, 0, strUtf8.length);
    for (; i0 >= 0; --i0) {
      if (out[i0] == chOld) {
        out[i0] = chNew;
      }
    }
    return out;
  }

  // =====
}
