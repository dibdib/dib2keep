// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.Arrays;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;

/** Shared storage for multivalued mappings. */
// Also used as node ...
public final class QMMap implements QItemIf { // Cloneable, QItemIf {
  // =====

  // Not yet.
  private static QMMap[] roots = null; // new QMMap[16];

  private static final QItemIf[] zEmpty = new QItemIf[0];

  /** null for getShash(), others for getAsKey(keyGroup). */
  private final QIfs.QEnumIf keyGroup;

  /** Tree elements: QSto for nodes (0 or 16 elements) or QList, or QItem. */
  private QItemIf[] mRefs = zEmpty;

  private long mBitList = 0;

  private static final char[] BITS = MiscFunc.BITS.clone();

  private static long getKey(QItemIf item, QIfs.QEnumIf keyGroup) {
    return ((null != keyGroup) && !(item instanceof QBunch))
        ? ((QIfs.QTupleIf) item).getAsKey(keyGroup)
        : item.getShash();
  }

  private static int calcIndex4Bitlist(int count, long xBitList) {
    int iRef = 0;
    final long bitCut = (xBitList & ((1L << count) - 1));
    for (int i0 = 0; i0 < count; i0 += 8) {
      iRef += BITS[0xff & (int) (bitCut >>> i0)];
    }
    return iRef;
  }

  // =====
  private static class QBunch implements QItemIf {
    // =====

    private QItemIf[] bunch;
    private int high;
    private final long shash;

    private QBunch(long xShash) {
      bunch = new QItemIf[4];
      high = -1;
      shash = xShash;
    }

    private boolean shrink() {
      int checked = bunch.length - 16;
      for (; (4 <= checked) && (checked < bunch.length); ++checked) {
        if (null != bunch[checked]) {
          high = checked;
          checked = -1;
          break;
        }
      }
      if (20 <= checked) {
        bunch = Arrays.copyOf(bunch, bunch.length - 16);
        if (high >= bunch.length) {
          high = -1;
        }
      }
      if (16 < bunch.length) {
        return false;
      }
      for (int i0 = 0; i0 < bunch.length; ++i0) {
        if (null != bunch[i0]) {
          return false;
        }
      }
      bunch = new QItemIf[4];
      high = -1;
      return true;
    }

    private boolean remove(int inx) {
      bunch[inx] = null;
      if (inx >= (bunch.length - 16)) {
        return shrink();
      } else if ((inx < high) && (high < bunch.length)) {
        bunch[inx] = bunch[high];
        bunch[high] = null;
        high = -1;
      }
      return false;
    }

    private boolean remove(QItemIf xm) {
      for (int i0 = 0; i0 < bunch.length; ++i0) {
        if (xm.equals(bunch[i0])) {
          return remove(i0);
        } else if ((null != bunch[i0]) && (high < i0)) {
          high = i0;
        }
      }
      return false;
    }

    private int append(QItemIf xm) {
      high = bunch.length;
      bunch = Arrays.copyOf(bunch, bunch.length + 16);
      bunch[high] = xm;
      return high;
    }

    private int add(QItemIf xm) {
      int inx = -1;
      for (int i0 = 0; i0 < bunch.length; ++i0) {
        if (null == bunch[i0]) {
          inx = (0 <= inx) ? inx : i0;
        } else if (xm.equals(bunch[i0])) {
          return i0;
        } else if (high < i0) {
          high = i0;
        }
      }
      if (0 <= inx) {
        bunch[inx] = xm;
        return inx;
      }
      return append(xm);
    }

    private QItemIf put(QItemIf xm) {
      int inx = -1;
      for (int i0 = 0; i0 < bunch.length; ++i0) {
        if (null == bunch[i0]) {
          inx = (0 <= inx) ? inx : i0;
        } else if (xm.equals(bunch[i0])) {
          QItemIf out = bunch[i0];
          bunch[i0] = xm;
          return out;
        } else if (high < i0) {
          high = i0;
        }
      }
      if (0 <= inx) {
        bunch[inx] = xm;
      } else {
        append(xm);
      }
      return null;
    }

    @Override
    public long getShash() {
      return shash;
    }
  }
  // =====

  private QMMap(QIfs.QEnumIf xmKeyGroup, int size) {
    // Top level has 64-10*6 = 4 bits:
    mRefs = (0 == size) ? zEmpty : new QItemIf[(0 >= size) ? (1 << (64 - 10 * 6)) : size];
    keyGroup = xmKeyGroup;
  }

  public static synchronized QMMap create(QIfs.QEnumIf xmKeyGroup) {
    QMMap out = new QMMap(xmKeyGroup, -1);
    if (null != roots) {
      int i0 = roots.length;
      for (; i0 > 0; --i0) {
        if (null != roots[i0 - 1]) {
          break;
        }
      }
      if (roots.length <= i0) {
        roots = Arrays.copyOf(roots, roots.length + 16);
      }
      roots[i0] = out;
    }
    return out;
  }

  private static int countBits(QMMap xNode) {
    int count = 0;
    for (int shift = 56; shift >= 0; shift -= 8) {
      count += BITS[(int) (xNode.mBitList >>> shift) & 0xff];
    }
    return count;
  }

  static void checkBits(QMMap xNode, boolean top) {
    int cBits = countBits(xNode);
    QItemIf[] refs = xNode.mRefs;
    if ((64 > refs.length) && !top) {
      for (int i0 = refs.length - 1; i0 >= 0; --i0) {
        if ((cBits <= i0) != (null == refs[i0])) {
          ExceptionAdapter.doAssert(false, QMMap.class, "Inconsistent MMap");
        }
      }
    }
    for (int i0 = refs.length - 1; i0 >= 0; --i0) {
      if (null != refs[i0]) {
        --cBits;
      }
    }
    if (0 != cBits) {
      ExceptionAdapter.doAssert(false, QMMap.class, "Inconsistent MMap " + cBits);
    }
  }

  private static synchronized QItemIf searchLeaf(QMMap xNode, long xKey) {
    QMMap node = xNode;
    for (int shift = 60; shift >= 0; shift -= 6) {
      final QItemIf[] refs = node.mRefs;

      // checkBits(node, (60 <= shift));

      final int part6 = (int) ((xKey >>> shift) & 0x3f);
      if (0 == (node.mBitList & (1L << part6))) {
        return null;
      }
      final int iRef =
          ((64 > refs.length) && (60 > shift)) ? calcIndex4Bitlist(part6, node.mBitList) : part6;
      final QItemIf found = (refs.length > iRef) ? refs[iRef] : null;
      if (found instanceof QMMap) {
        node = (QMMap) found;
        continue;
      }
      return found;
    }
    return null;
  }

  private static synchronized QMMap removeEmpty(QMMap xNode, long xKey) {
    QMMap node = xNode;
    QMMap lastNode = xNode;
    int lastInx = -1;
    for (int shift = 60; shift >= 0; shift -= 6) {
      final QItemIf[] refs = node.mRefs;

      // checkBits(node, (60 <= shift));

      final int part6 = (int) ((xKey >>> shift) & 0x3f);
      if (0 == node.mBitList) {
        node.mRefs = zEmpty;
        lastNode.mRefs[lastInx] = null;
        final int px = (int) ((xKey >>> (shift + 6)) & 0x3f);
        lastNode.mBitList &= ~(1L << px);
        if ((64 > lastNode.mRefs.length) && ((60 - 6) > shift)) {
          if (0 == lastNode.mBitList) {
            return removeEmpty(xNode, xKey);
          }
          System.arraycopy(
              lastNode.mRefs,
              lastInx + 1,
              lastNode.mRefs,
              lastInx,
              lastNode.mRefs.length - lastInx - 1);
          lastNode.mRefs[countBits(lastNode)] = null;
        }
        return node;
      }
      final int iRef =
          ((64 > refs.length) && (60 > shift)) ? calcIndex4Bitlist(part6, node.mBitList) : part6;
      final QItemIf found = (refs.length > iRef) ? refs[iRef] : null;
      if (found instanceof QMMap) {
        lastNode = node;
        node = (QMMap) found;
        lastInx = iRef;
        continue;
      }
      return null;
    }
    return null;
  }

  private static synchronized QItemIf searchNext(QMMap xNode, long xKey, long recursionMaster) {
    QMMap node = xNode;
    long key = (-1L != xKey) ? xKey : 0;
    for (int shift = 60; shift >= 0; shift -= 6) {
      final QItemIf[] refs = node.mRefs;
      int part6 = (int) ((key >>> shift) & 0x3f);
      while (0 == (node.mBitList & (1L << part6))) {
        ++part6;
        if (0x3f < part6) {
          for (; 60 > shift; ) {
            shift += 6;
            part6 = 1 + (int) ((key >>> shift) & 0x3f);
            if (0x3f >= part6) {
              final long old = key;
              key = (((key >>> (shift + 6)) << 6) | part6) << shift;
              return ((key >>> 1) < (old >>> 1)) ? null : searchNext(xNode, key, recursionMaster);
            }
          }
          return null;
        }
        key = (((key >>> (shift + 6)) << 6) | part6) << shift;
      }
      final int iRef =
          ((64 > refs.length) && (60 > shift)) ? calcIndex4Bitlist(part6, node.mBitList) : part6;
      final QItemIf found = (refs.length > iRef) ? refs[iRef] : null;
      if (found instanceof QMMap) {
        node = (QMMap) found;
        continue;
      }
      ExceptionAdapter.doAssert((null != found), QMMap.class, "Search error");
      if ((-1L == recursionMaster) || (-1L == key)) {
        return (recursionMaster == key) ? null : found;
      } else if ((key >>> 1) < (recursionMaster >>> 1)) {
        return null;
      } else if ((key == recursionMaster) && (key == xKey)) {
        key = ((key >>> shift) + 1) << shift;
        return searchNext(xNode, key, recursionMaster);
      }
      return (key == recursionMaster) ? null : found;
    }
    return null;
  }

  // Use shash of xKey if (null == xOptObj).
  private static synchronized QItemIf removeCcur(QMMap xyNode, long xKey, QItemIf xOptObj) {
    QMMap node = xyNode;
    for (int shift = 60; shift >= 0; shift -= 6) {
      final QItemIf[] refs = node.mRefs;
      final int part6 = (int) ((xKey >>> shift) & 0x3f);

      // checkBits(node, (60 <= shift));

      if (0 == (node.mBitList & (1L << part6))) {
        return null;
      }
      final int iRef =
          ((64 > refs.length) && (60 > shift)) ? calcIndex4Bitlist(part6, node.mBitList) : part6;
      final QItemIf found = (refs.length > iRef) ? refs[iRef] : null;
      if (found instanceof QMMap) {
        node = (QMMap) found;
        continue;
      } else if (null == found) {
        return null;
      } else if (found instanceof QBunch) {
        if (null != xOptObj) {
          if (!((QBunch) found).remove(xOptObj)) {
            return null;
          }
        } else {
          final QItemIf[] list = ((QBunch) found).bunch;
          QItemIf out = null;
          for (int i0 = 0; i0 < list.length; ++i0) {
            if ((null != list[i0])
                && (0 == QIfs.compareShashPartial(xKey, getKey(list[i0], xyNode.keyGroup)))) {
              out = list[i0];
              list[i0] = null;
            }
          }
          if (!((QBunch) found).shrink()) {
            return out;
          }
        }
      } else if (null != xOptObj) {
        if (!xOptObj.equals(found)) {
          return null;
        }
      } else if (0 != QIfs.compareShashPartial(xKey, getKey(found, xyNode.keyGroup))) {
        return null;
      }

      int countLeft = countBits(node) - 1;
      refs[iRef] = null;

      if ((0 >= countLeft) && (60 > shift)) {
        node.mBitList = 0;
        node.mRefs = zEmpty;
        removeEmpty(xyNode, xKey);
      } else {
        node.mBitList &= ~(1L << part6);
        if (60 > shift) {
          final boolean isSlower64 = (64 > refs.length) && ((refs.length - 4) > countLeft);
          final boolean forceSlower64 = (20 > countLeft) && (64 <= refs.length);
          if (isSlower64 || forceSlower64) {
            node.mRefs = new QItemIf[((countLeft >>> 2) + 1) << 2];
            for (int i0 = refs.length - 1; (countLeft > 0) && (i0 >= 0); --i0) {
              if (null != refs[i0]) {
                node.mRefs[--countLeft] = refs[i0];
              }
            }
            if (0 != countLeft) {
              ExceptionAdapter.doAssert(false, QMMap.class, "Inconsistent MMap");
            }
          } else if (64 > refs.length) {
            System.arraycopy(refs, iRef + 1, node.mRefs, iRef, node.mRefs.length - iRef - 1);
            node.mRefs[countLeft] = null;
          }
        }
      }
      return found;
    }
    return null;
  }

  private static synchronized long addOrPutCcur(
      int xShift0, QMMap xyNode, long xKey, QItemIf xmObj, long xb4HandleReplace) {
    QMMap node = xyNode;
    int shift = (0 <= xShift0) ? (xShift0 - 6) : 60;
    for (; shift >= 0; shift -= 6) {
      final QItemIf[] refs = node.mRefs;
      final int part6 = (int) ((xKey >>> shift) & 0x3f);

      // checkBits(node, (60 <= shift));

      int iRef =
          ((64 > refs.length) && (60 > shift)) ? calcIndex4Bitlist(part6, node.mBitList) : part6;
      QItemIf found = (refs.length > iRef) ? refs[iRef] : null;
      if (0 == (node.mBitList & (1L << part6))) {
        found = null;
      } else if (found instanceof QMMap) {
        node = (QMMap) found;
        continue;
      } else if (found instanceof QBunch) {
        if ((xKey == found.getShash()) || (0 >= shift)) {
          if (0 == (xb4HandleReplace & 2L)) {
            return ((QBunch) found).add(xmObj);
          }
          ((QBunch) found).put(xmObj);
          return xKey;
        }
      } else if (xmObj.equals(found)) {
        if (0 != (xb4HandleReplace & 2L)) {
          node.mRefs[iRef] = xmObj;
          return xKey;
        }
        return 0; // -1;
      }
      if (null == found) {
        node.mBitList |= (1L << part6);
        int count = 0;
        if ((64 > refs.length) && (60 > shift)) {
          for (int shx = 56; shx >= 0; shx -= 8) {
            count += BITS[(int) (node.mBitList >>> shx) & 0xff];
          }
          if (30 > count) {
            if (refs.length < count) {
              count += (4 > count) ? 0 : (((8 > count) ? 0 : 2) + (count & 1));
              node.mRefs = Arrays.copyOf(refs, count);
            }
            System.arraycopy(node.mRefs, iRef, node.mRefs, iRef + 1, node.mRefs.length - iRef - 1);
          } else {
            QItemIf[] old = node.mRefs;
            node.mRefs = new QItemIf[64];
            iRef = part6;
            count = 0;
            for (int i0 = 0; (i0 < 64) && (count < old.length); ++i0) {
              if (0 != (node.mBitList & (1L << i0))) {
                if (i0 == part6) {
                  node.mRefs[i0] = null;
                } else {
                  node.mRefs[i0] = old[count++];
                }
              }
            }
          }
        }
      }
      if ((null == found) || ((0 != (xb4HandleReplace & 2L)) && (0 == shift))) {
        node.mRefs[iRef] = xmObj;
        return xKey;
      } else if (0 < shift) {
        final long key = getKey(node.mRefs[iRef], node.keyGroup);
        if (key != xKey) {
          QMMap node2 = new QMMap(node.keyGroup, 2);
          addOrPutCcur(shift, node2, xKey, xmObj, xb4HandleReplace);
          addOrPutCcur(shift, node2, key, node.mRefs[iRef], xb4HandleReplace);
          node.mRefs[iRef] = node2;
          return xKey;
        }
      }
      if ((1L == (xb4HandleReplace & 3L)) && (xmObj instanceof QMutableIf)) {
        if (1 == (xKey & 0x3fL)) {
          for (int i0 = 0x5; i0 < 64; i0 += 4) {
            if (0 == (node.mBitList & (1L << i0))) {
              xKey = QIfs.encodeIndex(xKey, i0 >>> 2);
              ((QMutableIf) xmObj).setShashOrIgnore(xKey);
              return addOrPutCcur(-1, xyNode, xKey, xmObj, xb4HandleReplace);
            }
          }
        }
        // TODO Optimize.
        for (int inx = (64 >>> 2); true; ++inx) {
          xKey = QIfs.encodeIndex(xKey, inx);
          final QItemIf o1 = searchLeaf(xyNode, xKey);
          if ((null == o1) || (o1.getShash() != xKey)) {
            ((QMutableIf) xmObj).setShashOrIgnore(xKey);
            return addOrPutCcur(-1, xyNode, xKey, xmObj, xb4HandleReplace);
          }
        }
      }
      QBunch list = new QBunch(xKey);
      list.add(found);
      list.add(xmObj);
      node.mRefs[iRef] = list;
      return xKey;
    }
    return 0; // -1;
  }

  //////

  public QItemIf search(long xKey) {
    QItemIf out = searchLeaf(this, xKey);
    if (null == out) {
      return null;
    } else if (out instanceof QBunch) {
      final QItemIf[] list = ((QBunch) out).bunch;
      for (int i0 = 0; i0 < list.length; ++i0) {
        if ((null != list[i0])
            && (0 == QIfs.compareShashPartial(xKey, getKey(list[i0], keyGroup)))) {
          return list[i0];
        }
      }
      return null;
    }
    return (0 == QIfs.compareShashPartial(xKey, getKey(out, keyGroup))) ? out : null;
  }

  public QItemIf[] searchBunch(long xKey) {
    QItemIf out = searchLeaf(this, xKey);
    if (null == out) {
      return null;
    } else if (out instanceof QBunch) {
      final QItemIf[] list = ((QBunch) out).bunch;
      final QItemIf[] lx = new QItemIf[list.length];
      int c0 = 0;
      for (int i0 = 0; i0 < list.length; ++i0) {
        if ((null != list[i0])
            && (0 == QIfs.compareShashPartial(xKey, getKey(list[i0], keyGroup)))) {
          lx[c0++] = list[i0];
        }
      }
      return (0 >= c0) ? null : Arrays.copyOf(lx, c0);
    }
    return (0 == QIfs.compareShashPartial(xKey, getKey(out, keyGroup)))
        ? new QItemIf[] {out}
        : null;
  }

  private QItemIf searchNextKey(long xKey) {
    final QItemIf out = searchNext(this, xKey, xKey);
    if ((-1L == xKey) && (null == out)) {
      return search(-1L);
    }
    return out;
  }

  /**
   * For traversal, -1L can be used as starting point.
   *
   * @param xKey Key, exclusive if != -1.
   * @return Value of succeeding entry if it exists, or value for -1L or null.
   */
  public QIfs.QTupleIf searchNext(long xKey, QItemIf[] yOpt, int offset) {
    QItemIf out = searchNext(this, xKey, xKey);
    if ((-1L == xKey) && (null == out)) {
      out = search(-1L);
    }
    if (out instanceof QBunch) {
      final QBunch list = (QBunch) out;
      for (int i0 = 0; i0 < list.bunch.length; ++i0) {
        if (null != list.bunch[i0]) {
          out = list.bunch[i0];
          if ((null == yOpt) || (offset >= yOpt.length)) {
            break;
          }
          yOpt[offset++] = out;
        }
      }
    } else if (null != yOpt) {
      yOpt[offset++] = out;
    }
    if ((null != yOpt) && (offset < yOpt.length)) {
      yOpt[offset] = null;
    }
    return (out instanceof QIfs.QTupleIf) ? (QIfs.QTupleIf) out : null;
  }

  public int dump(QItemIf[] xyMap, int offs, int skip) {
    long shash = -1L;
    long last = shash;
    while (true) {
      QItemIf found = searchNextKey(shash);
      if (null == found) {
        break;
      }
      if ((null != xyMap) && (offs >= xyMap.length)) {
        xyMap = null;
      }
      if (!(found instanceof QBunch)) {
        if (null != xyMap) {
          xyMap[offs] = found;
        }
        offs += ((skip--) <= offs) ? 1 : 0;
      } else {
        final QBunch list = (QBunch) found;
        for (int i0 = 0; i0 < list.bunch.length; ++i0) {
          if (null != list.bunch[i0]) {
            found = list.bunch[i0];
            if ((null != xyMap) && (offs < xyMap.length)) {
              xyMap[offs] = found;
            }
            offs += ((skip--) <= offs) ? 1 : 0;
          }
        }
      }
      if (null == keyGroup) {
        shash = found.getShash();
      } else if (found instanceof QIfs.QTupleIf) {
        shash = ((QIfs.QTupleIf) found).getAsKey(keyGroup);
      } else {
        break;
      }
      if (-1L == shash) {
        break;
      }
      if (null == keyGroup) {
        ExceptionAdapter.doAssert(
            (((last >>> 1) <= (shash >>> 1)) && (last != shash)) || (-1L == last),
            this.getClass(),
            "Sort order in map " + shash + ' ' + last);
        last = shash;
      }
    }
    return (null == xyMap) ? -offs : offs;
  }

  public QItemIf searchMultivalued_OLD(long xKey, int xOffset) {
    QItemIf out = searchLeaf(this, xKey);
    if (!(out instanceof QBunch)) {
      return out;
    }
    final QItemIf[] list = ((QBunch) out).bunch;
    int inx = 0; // QItemIf.getStampIndex(xKey);
    int end = -1;
    for (; true; inx += (0 <= xOffset) ? 1 : -1) {
      inx = (0 > inx) ? (list.length - 1) : ((inx >= list.length) ? 0 : inx);
      if (null != list[inx]) {
        //          && (0 == QItemIf.compareShashPartial(xKey, list[inx].getKey((int) keyType
        // >> 1)))) {
        if (0 == xOffset) {
          return list[inx];
        }
        xOffset += (0 > xOffset) ? 1 : -1;
      }
      if (inx == end) {
        return null;
      } else if (0 > end) {
        end = inx;
      }
    }
  }

  // Return key value or 0/1 for error, possibly modified in case of conflict.
  long putCcur(long xKey, QItemIf xmVal, long xb4HandleReplace) {
    long out = addOrPutCcur(-1, this, xKey, xmVal, xb4HandleReplace);
    // ...
    return out;
  }

  public long add4Handle(QItemIf xmVal) {
    return addOrPutCcur(-1, this, xmVal.getShash(), xmVal, 1L);
  }

  public long add4Multi(long xKey, QItemIf xmVal) {
    return addOrPutCcur(-1, this, xKey, xmVal, 0L);
  }

  public long put4Handle(QItemIf xmVal) {
    return addOrPutCcur(-1, this, xmVal.getShash(), xmVal, 3L);
  }

  public long put4Multi(long xKey, QItemIf xmVal) {
    return addOrPutCcur(-1, this, xKey, xmVal, 2L);
  }

  public QItemIf removeCcur_OLD(long xKey) {
    return removeCcur(this, xKey, null);
  }

  public QItemIf remove(long xKey, QItemIf xmVal) {
    return removeCcur(this, xKey, xmVal);
  }

  @Override
  public long getShash() {
    return System.identityHashCode(this);
  }

  // =====
}
