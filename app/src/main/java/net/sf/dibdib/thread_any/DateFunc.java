// Copyright (C) 2016, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

// =====

import static net.sf.dibdib.config.Dib2Constants.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.BigSxg;

/** Hemera time (HT-Y/D): count of uniform sidereal years for varying length of days @ J2000. */

// Projected counts of ticks, days, and 'sidereal' years (year-seconds-years of 31558150 sec).
// Tick 0.0 = Y 0.0 = J2000.0 (JD 2451545.0 TT) minus 2000 year-seconds (31558150 sec), assuming
// that 1 BC/ 2 BC also had some 31558149.5 + x seconds per sidereal year.
// With a conservative value of 16 millis increasing length of day per millennium
// and a potential value of < 0.1 seconds for the length of year per millennium (HT16):
// => delta-T as ~10000 for Y0 and 63.8285 for Y2000.
// => Drifting 'sidereal' years (uniform sidereal years with fixed number of seconds).
// Era Day 0 (ED 0.0 = JD 1721033.0) is Sunday (noon), ~73500 secs after tick 0 (for HT16).
// JD 2451545.0 TT = 2000-01-01T11:58:55.816 = 11:58:56.1715 UT = 11:59:27.816 TAI
// = 2000-01-01Y12:00@12 = 2000-01-01Y00:00(@0) = Y 2000.0 = HT tick 2000.0 * 31558150.
// With HT-Y as YT: delta-Y = YT - UT12 = delta-T + delta-E:
// delta-Y = delta-T + 0 for 2000 and ~75000s for Y0.
// Y0.0 = JD 1721033.0 - delta-Y = 0000-01-01Y00:00 (-12:00 = @0)
// Terrestrial time representation for HT-Y has 29 days in February and a short Dec 31.

// The HT calendar is an 'adjusted' Julian calendar (with additional leap years) or
// a 364 day calendar (August reduced to 30 days) with exactly 52 or, in case of leap years,
// 53 weeks (February with 28+7 = 35 days):
// a) HT-J: Year starts with the first JD of the uniform sidereal year. Leap year if delta-Y
//    of preceding year is < 0.5d or > 1.5d.
// b) HT-W: Year starts on Sunday. Leap week if delta-Y of preceding year < 2d or > 9d.
// The precise length of the calendar second as (LOD (average day length) / 86400) is adjusted
// at the end of a cycle of leap years according to its preceding cycle.

public final class DateFunc {

  // =====

  /*
  =====
  Note:
  W3 recommendation for ISO date format (www.w3.org/TR/NOTE-datetime):
  Complete date:
  	YYYY-MM-DD (eg 1997-07-16)
  Complete date plus hours and minutes:
  	YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
  Complete date plus hours, minutes and seconds:
  	YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
  Complete date plus hours, minutes, seconds and a decimal fraction of a
  second
  	YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

  where:
   YYYY = four-digit year
   MM   = two-digit month (01=January, etc.)
   DD   = two-digit day of month (01 through 31)
   hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
   mm   = two digits of minute (00 through 59)
   ss   = two digits of second (00 through 59)
   s    = one or more digits representing a decimal fraction of a second
   TZD  = time zone designator (Z or +hh:mm or -hh:mm)
  =====
  */

  // ...
  public static int processMaxTimer = 1000;
  public static int processMicroSteps4Timer = 30000;

  private static long offsetNanos2000 = 0;
  private static boolean useJavaNanos = true;
  private static final AtomicLong minTimeNanobiSecCount2000 = new AtomicLong(0);

  private static int idStampHex = (((2017 - 2000) & 0xf0) << 14) | (((2017 - 2000) & 0xf) << 12);
  private static long idCount = 1;
  private static final long minTime2017 = (2017 - 2000) * 365L * 24 * 3600 * 1000686000;
  private static AtomicLong minTimeNanobiSec4Id = new AtomicLong(minTime2017);
  // SimpleDateFormat is not thread-safe! 'X' works only for Java v >= 7.
  private static final String DATE_FORMAT_ISO_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  // Not needed here:
  //  private static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
  //  private static final String DATE_FORMAT_SHORT = "yyMMdd'.'HHmmss";
  //  private static final String DATE_FORMAT_SHORT_TZ = "yyMMdd'.'HHmmss'.'SSSZ";

  // Do not waste time by constantly accessing TZ data:
  private static final SimpleDateFormat DATE_SDF = new SimpleDateFormat(DATE_FORMAT_ISO_Z);
  private static int timeZoneOffsetMillis = 0;

  /** Might need a fresh start later on, e.g. for daylight saving. */
  public static boolean timeZoneDone = false;

  /**
   * B = barycentric, Y = uniform sidereal, D = days (UT/ UT1), E = uniform atomic (terrestrial:
   * T-eph/TT), C = common (T = UTC, G = Greg, J = Julian).
   */
  public static final String timeSystemMarkers = "BCDEY";

  /**
   * A = atomic (TAI), E = Terrestrial_Time_Eph (T-eph), J = Julian, G = Gregorian, T = UTC/ ISO
   * Greg., C = common (J/G/T), H = Hemera_Time_HT-J, W = Hemera_Time_HT-W, Y =
   * Terrestrial_Hemera_Time_HT-Y.
   */
  public static final String dateMarkers = "ACEGHJTWY";

  // =====

  public static enum DateFormat {

    // =====

    ANNUS("annus", 'E', "Annus years for J2000 as 2000.0 (curr. def.: " + YEAR_ANNUS + " s/y."),
    // B("barycentric", 'B', "Barycentric (TCB)"),
    // BOXED("boxed", 'G', "UTC/ Gregorian boxed (numeric, proleptic)."),
    C("common", 'C', "Common/ civil (local) time (Gregorian/ Julian, with year -1 = 1 BC)."),
    D("JD", 'D', "Julian Day."),
    E("tdb", 'E', "Barycentric ephemeris/ terrestrial time (T-eph/TDB/TT/ET)."),
    F("nanobis", 'E', "Nanobi atomic seconds (2**-30 seconds) since TT2000E12 (J2000)."),
    G("greg", 'G', "(Proleptic) Gregorian with year -1 = 1 BC."),
    // H("htc", 'H', "Sidereal (Hemera Time) calendar with leap-days."),
    J("jul", 'J', "(Unified) Julian (1582-10-15T = 1582-10-05J) with year -1 = 1 BC."),
    K("weekday", '0', "Day of the week (0=Su, 6=Sa)."),
    L("hash", 'Y', "Sortable hash value of time (62 bit, atomic or projected atomic)."),
    N("ED", 'D', "Era Day ('Hemeras'), counting from JD 1721033 (2 BC)."),
    P("gps", 'E', "GPS weeks (1.5 seconds per tick)."),
    Q("ticks16", 'Y', "Era ticks (HT16) since Y0.0 (JD 1721033, projected from J2000)."),
    // R("tropical", 'R', "Tropical ... years."),
    S("seconds", 'E', "SI atomic seconds since TT2000E12 = JD 2451545.0 - delta-t."),
    T("utc", 'T', "Coordinated time (UTC/T)/ ISO format, with year 0 = 1 BC."),
    TAI("tai", 'E', "TAI time, using weighted average of atomic seconds since 1958-01-01."),
    // V("millis", 'T', "Unix time in milliseconds."),
    // W("htw", 'H', "Sidereal (Hemera Time) calendar with leap-weeks."),
    X("unix", 'C', "Unix ticks (seconds, possibly smeared)."),
    Y("years", 'Y', "Sidereal year-seconds-years (Y2000.0 = J2000.0)."),
    // Z("hty", 'Y', "Sidereal year-seconds-years time (2000-01-01Y00:00 =
    // J2000.0)."),
    SHORTLOCAL(null, 'G', "2=YYMM, 3=YYMMDD, 5=YYMMDD.HH:MM, ..., 9=YYMMDD.HH:MM:SS.+nanos.");

    public final String name;
    public final String nameUpper;
    public final String descr;
    public final char sysMarker;

    private DateFormat(String xmName, char xSysMarker, String xmDescr) {
      name = xmName;
      nameUpper = (null == xmName) ? null : xmName.toUpperCase(Locale.ROOT);
      sysMarker = xSysMarker;
      descr = xmDescr;
    }

    private static DateFormat find(String xMarkerOrName) {
      try {
        if (".".equals(xMarkerOrName)) {
          return C;
        }
        xMarkerOrName = xMarkerOrName.toUpperCase(Locale.ROOT);
        DateFormat out = (1 != xMarkerOrName.length()) ? null : DateFormat.valueOf(xMarkerOrName);
        if ((null != out) || (1 >= xMarkerOrName.length())) {
          return out;
        }
      } catch (Exception e0) {
        // NOP.
      }
      for (DateFormat f0 : DateFormat.values()) {
        if ((null != f0.nameUpper) && (f0.nameUpper.startsWith(xMarkerOrName))) {
          return f0;
        }
      }
      return null;
    }
  }

  // =====

  public static void checkTimeZone() {
    if (timeZoneDone) {
      // Leave the time zone for the current run, rather than messing up the internal time line.
      return;
    }
    timeZoneDone = true;
    timeZoneOffsetMillis =
        TimeZone.getDefault()
            .getOffset(System.currentTimeMillis()); // DATE_SDF.getTimeZone().getRawOffset();
    DATE_SDF.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  public static double eraTicks4J2000Ticks(double j2000Secs) {
    return j2000Secs + 2000.0 * YEAR_SECONDS_YEAR;
  }

  public static double j2000Ticks4EraTicks(double et) {
    return et - 2000.0 * YEAR_SECONDS_YEAR;
  }

  /** Convert J2000 SI ticks to 'year-seconds-years' of 31558150 sec (fixed), starting noon. */
  // Year 2000 starts at 12:00 UTC (= 00:00-12:00, => add 12 * 3600 for comparing Zulu time zone).
  public static double eraYears4J2000Ticks(double s) {
    return 2000.0 + (s / YEAR_SECONDS_YEAR);
  }

  public static double j2000Ticks4EraYears(double y) {
    return (y - 2000.0) * YEAR_SECONDS_YEAR;
  }

  /** Convert Julian Day (JD) to Era Day ('Hemeras': ED), both starting at noon. */
  // Julian Days as UT noon time. UT/UT1 = UTC + delta-UT = TT + delta-t.
  public static double eraDay4JulianDay(double jd) {
    return jd - TIME_Y0_JD;
  }

  /** Convert Era Day ('Hemeras': ED) to Julian Day (JD), both starting at noon. */
  // Era Days like Julian days as UT noon time. UT/UT1 = UTC + delta-UT = TT + delta-t.
  public static double julianDay4EraDay(double ed) {
    return ed + TIME_Y0_JD;
  }

  // Cmp. https://eclipse.gsfc.nasa.gov/SEhelp/deltat2004.html @2021-04-12.
  private static double deltaTimeEstE4oYear(double y) {
    // yx = year + (month - 0.5)/12
    double yx = y + 0.5 / 12;
    if ((1999.9 <= y) && (y <= 2001.2)) {
      ///// Higher precision for offsets around 2000.
      if (y == 2000.0) {
        return 638285.0;
      }
      return 1.0 * (long) (638285.0 + (y - 2000.0) * (640908.0 - 638285.0));
    }
    double u = (yx - 1820) / 100;
    if ((1961.0 <= yx) && (yx < 2150.0)) {
      double t = yx - 2000;
      if (yx >= 2050.0) {
        return 1.0 * (long) (1E4 * (-20 + 32.0 * u * u - 0.5628 * (2150 - yx)));
      } else if (yx < 2005.0) {
        if (1986.0 > yx) {
          t = yx - 1975;
          return 1.0 * (long) (1E4 * (45.45 + 1.067 * t - t * t / 260 - t * t * t / 718));
        }
        return 1.0
            * (long)
                (1E4
                    * (63.86
                        + 0.3345 * t
                        - 0.060374 * t * t
                        + 0.0017275 * t * t * t
                        + 0.000651814 * t * t * t * t
                        + 0.00002373599 * t * t * t * t * t));
      }
      return 1.0 * (long) (1E4 * (62.92 + 0.32217 * t + 0.005589 * t * t));
    } else if ((1680.0 <= y) && (y < 1970)) {
      // Rougher ...
      return 1.0
          * (long) (1E4 * (((1880 <= y) ? (y - 1880) * 32.0 / 80 : (1880 - y) * 26.0 / 200)));
    }
    // Long-term ...
    return 1E4 * (-20 + 32.0 * u * u);
  }

  /** Common estimation for DeltaTime = TT/TDT/ET - UT1, delta-UT = UT1 - UTC. */
  public static double deltaTimeEst4Year(double y) {
    return deltaTimeEstE4oYear(y) / 1E4;
  }

  /** Convert TAI seconds to J2000 (TT2000E12) SI ticks (S): TT@12 = TAI + 12h + 32.184s. */
  // JD 2451545.0 UT - delta-T = 2000-01-01E12:00 = 2000-01-01T11:58:56 = 11:59:27.816 TAI.
  public static double j2000Ticks4Atomic2000(double tai2000) {
    return tai2000 + TIME_DELTA_TAI - 12 * 3600.00;
  }

  /** DeltaTime = TT/TDT/ET - UT1, 'projected' value for HT16. */
  // delta-UT = UT1 - UTC (64.184 - 63.8285 for 2000). */
  // For projected calculations: year is sidereral year-seconds-year of 31558150 secs.
  public static double deltaTimeProjected4Year(double year) {
    final double lod =
        (year - TIME_LOD_REF_YEAR) / 1000.0 * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / 1000.0
            + (24.0 * 3600);
    return TIME_DELTA_T_2000_UT
        + (((TIME_LOD_2000_PROJ + lod) / 2) - (24.0 * 3600))
            * (((year - 2000) * YEAR_SECONDS_YEAR) / (24.0 * 3600));
  }

  /** DeltaTime = TT/TDT/ET - UT1, for 'currentTime' calculations, otherwise: use PROJ. */
  // delta-UT = UT1 - UTC (64.184 - 63.8285 for 2000). */
  public static double deltaTimeEst4J2000(double j2000Secs) {
    // TODO: Use leap second bit lists.
    // Projected values use HT-Y years, value good enough for TT years around 2000:
    final double year = 2000 + (int) (j2000Secs / YEAR_SECONDS_YEAR);
    if (1045 > year) {
      return deltaTimeProjected4Year(year);
    } else if (1681 >= year) {
      return (1679 <= year) ? 26.0 : (deltaTimeEst4Year(year) - 15.0);
    } else if (2008 > year) {
      return deltaTimeEst4Year(year);
    } else if ((2022 + 1 + 1) > year) {
      return (2015 > year) ? 66 : ((2017 > year) ? 67.5 : 69);
    }
    return TIME_DELTADELTA_CURR + deltaTimeProjected4Year(year - 1);
  }

  /** Estimated (not projected!) nanobi-seconds since/before 2000/2400/... noon (YT), 62 bits. */
  // => signed for ~1900..2100, unsigned (TODO) for ~2200..2300 :-)
  public static long nanobisEst4UnixMillis(long unixMillis) {
    final long millis2000 = unixMillis - TIME_G2000_UNIX_MILLIS;
    final long j2000k =
        ((long) (TIME_DELTA_T_2000_UTC - TIME_DELTA_T_2000_UT) * 1000)
            + (millis2000
                + (long) (1000 * deltaTimeEst4J2000(millis2000 / 1000))
                - 12 * 3600 * 1000);
    return ((j2000k / 1000) << 30) + ((j2000k % 1000) << 30) / 1000;
  }

  /** Estimated (not projected!) Unix milliseconds, from J2000 seconds. */
  public static long millisUnixEst4J2000(double j2000Secs) {
    return (long)
        ((j2000Secs * 1000 + TIME_G2000_UNIX_MILLIS)
            - 1000 * deltaTimeEst4J2000(j2000Secs)
            + 12 * 3600 * 1000
            - (TIME_DELTA_T_2000_UTC - TIME_DELTA_T_2000_UT) * 1000);
  }

  public static double eraTicksProj4UnixTicks(double ticks) {
    return eraTicksProj4EraDay(
        eraDay4JulianDay(
            (TIME_DELTA_T_2000_UTC - TIME_DELTA_T_2000_UT) / 86400 + julianDay4UnixTicks(ticks)));
  }

  public static double eraTicksEst4UnixTicks(double ticks) {
    if ((0 <= ticks) && (ticks <= TIME_MAX_AS_CURR_REASONABLE_UNIX_MILLIS / 1000)) {
      return nanobisEst4UnixMillis((long) (1000.0 * ticks)) * (1.0 / (1 << 30))
          + 2000.0 * YEAR_SECONDS_YEAR;
    }
    return eraTicksProj4UnixTicks(ticks);
  }

  private static synchronized void initNanoTimer() {
    long nanos = System.nanoTime();
    long millis = 100 + System.currentTimeMillis();
    final long nx = nanos;
    while (millis > System.currentTimeMillis()) {
      nanos = System.nanoTime();
    }
    final long e0 = 1 + System.currentTimeMillis() - (millis - 100);
    final long e1 = (nanos - nx) / (1000 * 1000);
    boolean looksGood = (50 < e1) && (e1 < e0);

    if (!looksGood) {
      useJavaNanos = false;
    } else {
      final long millis2000 = millis - TIME_G2000_UNIX_MILLIS;
      final long j2000k =
          millis2000
              + (long) (1000 * deltaTimeEst4J2000(millis2000 / 1000))
              - 12 * 3600 * 1000
              + (long) ((TIME_DELTA_T_2000_UTC - TIME_DELTA_T_2000_UT) * 1000);
      final long nanos2000Smeared = j2000k * 1000 * 1000;
      offsetNanos2000 = nanos - nanos2000Smeared;
      useJavaNanos = (((minTime2017 / 1024) * 996) < (nanos - offsetNanos2000));
    }
    long cmp = minTimeNanobiSecCount2000.get();
    if (cmp < minTime2017) {
      final long nanobisMin = nanobisEst4UnixMillis(millis - 1);
      minTimeNanobiSecCount2000.compareAndSet(cmp, nanobisMin);
    }
  }

  /** Rough nanobi-seconds since/before 2000/2400/... noon, based on currentTimeMillis(). */
  // Lowest bit indicates error, or conflict in case it is being used as ID.
  // Using lower bits for continuous count,
  // signed for ~1900..2100, unsigned (TODO) for ~2200..2300 :-)
  public static long currentTimeNanobisLinearized(boolean fastForLoops) {
    if (minTimeNanobiSecCount2000.get() < minTime2017) {
      initNanoTimer();
    }
    long minTimeVal = minTimeNanobiSecCount2000.incrementAndGet();
    if (fastForLoops) {
      return minTimeNanobiSecCount2000.incrementAndGet() & ~1L;
    }
    long ticksN = useJavaNanos ? 0 : nanobisEst4UnixMillis(System.currentTimeMillis());
    if (useJavaNanos) {
      final long nanos = System.nanoTime() - offsetNanos2000;
      ticksN =
          ((nanos / (1000 * 1000 * 1000)) << 30)
              + (((nanos % (1000 * 1000 * 1000)) << 30) / (1000 * 1000 * 1000));
    }
    final long out = (ticksN == minTimeVal) ? (ticksN + 1) : ticksN;

    if (((ticksN >>> 20) >= TIME_MAX_AS_CURR_REASONABLE_UNIX_MILLIS)
        || (0 >= minTimeVal)
        || (0 >= out)) {

      ///// Something is really bad ...

      if (useJavaNanos) {
        // Try again with millis:
        useJavaNanos = false;
        long v0 = currentTimeNanobisLinearized(false);
        if ((minTime2017 < v0) && (v0 <= nanobisEst4UnixMillis(50 + System.currentTimeMillis()))) {
          initNanoTimer();
        }
        return v0;
      }
      // This version was not meant to be used for more than 200 years :-)
      // Shift to unsigned values or next epoch (2400) ... Greetings from 200 years ago :-)
      if ((out > minTimeVal) && (0 < minTimeVal) && (0 < out)) {
        ///// Keep 'ticking'.
        if (((2017 - 2000) * 365 * 24 * 3600 * 1000686000) >= minTimeVal) {
          minTimeNanobiSecCount2000.compareAndSet(
              minTimeVal, ((2017 - 2000) * 365 * 24 * 3600 * 1000686000));
        }
        return minTimeNanobiSecCount2000.incrementAndGet() | 1L;
      }
      return minTimeVal | 1L;

    } else if (((minTimeVal >>> 30) - 2) <= (out >>> 30)) {

      ///// Looks good

      if ((minTimeVal + 3) >= out) {
        // Seems to be very fast ...
        return minTimeNanobiSecCount2000.getAndDecrement() & ~1L;
      } else if ((minTimeVal + 24) >= out) {
        return minTimeNanobiSecCount2000.incrementAndGet() & ~1L;
      }
      minTimeNanobiSecCount2000.compareAndSet(minTimeVal, out & ~1L);
      return out & ~1L;
    }

    ///// Bad timer or bad alignment?

    if (((minTimeVal >>> 30) - 99) <= (out >>> 30)) {
      ///// Keep 'ticking'.
      return minTimeNanobiSecCount2000.get() | 1L;
    } else if (((TIME_MAX_AS_CURR_REASONABLE_UNIX_MILLIS - TIME_G2000_UNIX_MILLIS)
            <= (minTimeVal >>> 20))
        && ((TIME_MIN_NANOBIS) < (out >>> 20))) {
      ///// The system seems to have corrected previous faulty values, but maybe not quite.
      long val = (minTimeVal + out) / 2 - (((24 + 12) * 3600L) << 30);
      val = (val >>> 20) << 20;
      minTimeNanobiSecCount2000.compareAndSet(minTimeVal, val);
      if (out <= val) {
        return out | 1L; // (out & ~TIME_SHIFTED) | TIME_SHIFTED_HOUR;
      }
      minTimeVal = val;
    }
    // Potential time zone shifting?
    if ((out >>> 30) >= ((minTimeVal >>> 30) - (24 + 12) * 3600)) {
      ///// Keep 'ticking'.
      return minTimeNanobiSecCount2000.get() | 1L;
    }
    return minTimeVal | 1L;
  }

  /** Millisecond ticks in Unix time (U). */
  public static long currentTimeMillisLinearized() {
    return millisUnixEst4J2000(currentTimeNanobisLinearized(false) * (1.0 / (1 << 30)));
  }

  /** Current time if nanos < 0. */
  public static double eraTicks4NanobisOrCurrent(long nanos) {
    nanos = (0 <= nanos) ? nanos : currentTimeNanobisLinearized(false);
    return eraTicks4J2000Ticks(nanos * (1.0 / (1 << 30)));
  }

  public static double currentTimeEraDay() {
    final long millis = System.currentTimeMillis();
    return eraDay4JulianDay(julianDay4UnixTicks(millis * 0.001));
  }

  public static long nanobis4EraTicks(double eticks) {
    return (long) (j2000Ticks4EraTicks(eticks) * (1 << 30));
  }

  public static long createId() {
    long out = minTimeNanobiSec4Id.addAndGet(2);
    long tim = currentTimeNanobisLinearized(true) - 10000;
    if (out < tim) {
      if (minTimeNanobiSec4Id.compareAndSet(out, tim)) {
        return tim & ~1L;
      }
    }
    return out & ~1L;
  }

  // Cmp. http://howardhinnant.github.io/date_algorithms.html @2021-04-11.
  private static long daysUnix4Greg(long y, int m, int d) {
    y -= (m <= 2) ? 1 : 0;
    final long epoch = (y >= 0 ? y : (y - 399)) / 400;
    final int yoe = (int) (y - epoch * 400); // [0, 399]
    final int doy = (153 * (m + (m > 2 ? -3 : 9)) + 2) / 5 + d - 1; // [0, 365]
    final int doe = yoe * 365 + yoe / 4 - yoe / 100 + doy; // [0, 146096]
    return epoch * 146097L + doe - 719468;
  }

  /** Era Days ('Hemeras') run with Julian Days, 0000-03-01G00+12 = ED 86.0 = JD 1721119.0 */
  // 1 BC as year 0.
  // Cmp. http://howardhinnant.github.io/date_algorithms.html @2021-04-11.
  public static long boxedGreg4EraDayAs12h(long z) {
    final int gregEra = (int) (400 * 365.25 - 3);
    z -= TIME_GREG_0_02_29_JD + 1 - TIME_Y0_JD;
    final long epoch = (z >= 0 ? z : (z - (gregEra - 1))) / gregEra;
    final int doe = (int) (z - epoch * gregEra); // [0, 146096]
    final int yoe = (doe - doe / 1460 + doe / 36524 - doe / (gregEra - 1)) / 365; // [0, 399]
    final long yx = yoe + epoch * 400L;
    final int doey = doe - (365 * yoe + yoe / 4 - yoe / 100); // [0, 365]
    final int em = (5 * doey + 2) / 153; // [0, 11]
    final int d = doey - (153 * em + 2) / 5 + 1; // [1, 31]
    final int m = em + (em < 10 ? 3 : -9); // [1, 12]
    final long y = (yx + ((m <= 2) ? 1 : 0));
    final long out = ((0 <= y) ? y : -y) * 10000L + m * 100 + d;
    return (0 <= y) ? out : -out;
  }

  /** Convert (proleptic) Gregorian date (G/T) to Era Day/ 'Hemeras': ED 0.0 = JD 1721033.0. */
  public static long eraDay4GregAt12h(long y, int m, int d) {
    return daysUnix4Greg(y, m, d) + 719468 + (TIME_GREG_0_02_29_JD + 1 - TIME_Y0_JD);
  }

  public static double julianDay4UnixTicks(double ticks) {
    return TIME_2000_JD + (ticks - TIME_G2000_UNIX_MILLIS / 1000.0 - 12 * 3600) / (24.0 * 3600);
  }

  /** Era ticks start on J2000 minus 2000 'sidereal' year-seconds-years, before JD 1721033. */
  public static double eraTicksProj4EraDay(double ed) {
    final double h2000 = ed - TIME_2000_ED;
    final double s_est =
        2000.0 * YEAR_SECONDS_YEAR
            + h2000
                * (86400 + TIME_LOD_2000_PROJ)
                / (2 - h2000 / YEAR_SECONDS_YEAR * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / 1000000.0);
    final double y_est = (long) (s_est / YEAR_SECONDS_YEAR);
    final double lod_est =
        24.0 * 3600
            + (y_est - TIME_LOD_REF_YEAR) * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / (1000.0 * 1000);
    double avg = (lod_est + TIME_LOD_2000_PROJ) / 2;
    final long y = 2000 + (long) (h2000 * avg / YEAR_SECONDS_YEAR);
    final double lod =
        24.0 * 3600 + (y - TIME_LOD_REF_YEAR) * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / (1000.0 * 1000);
    avg = (lod + TIME_LOD_0_PROJ) / 2;
    double out = ed * avg + TIME_DELTA_Y_Y0;

    ///// Fix fractional part, round-trip capable:
    double cmp = eraDayProj4EraTicks(out);
    double e0 = (ed - cmp) * 86400;
    out = Math.nextUp(out + e0 / 2);
    cmp = eraDayProj4EraTicks(out);
    if ((Math.nextAfter(cmp, Double.NEGATIVE_INFINITY) <= ed) && (ed <= Math.nextUp(cmp))) {
      return out;
    }
    e0 = (0 <= e0) ? e0 : (-e0);
    double min = out - e0 - lod;
    double max = out + e0 + lod;
    for (; min < max; min = Math.nextUp(min)) {
      out = (min + max) / 2;
      cmp = eraDayProj4EraTicks(out);
      min = (cmp <= ed) ? out : min;
      max = (cmp >= ed) ? out : max;
    }
    return out;
  }

  /** Era days like Julian days as UT noon time. UT/UT1 = UTC + delta-UT = TT + delta-t. */
  public static double eraDayProj4EraTicks(double ticks) {
    final long year = (long) (ticks / YEAR_SECONDS_YEAR);
    final double lod =
        24.0 * 3600
            + ((year - TIME_LOD_REF_YEAR) * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / (1000.0 * 1000));
    final double avg = (lod + TIME_LOD_0_PROJ) / 2;
    final double out = (ticks - TIME_DELTA_Y_Y0) / avg;
    final long day = (long) out;
    // Fraction counts for last day:
    return day + (out - day) * lod / avg;
  }

  /** Convert (postleptic, unified) Julian date to Era Day/ 'Hemeras': ED 0.0 = JD 1721033.0. */
  public static long eraDay4JulianUnifiedAt12h(long y, int m, int d) {
    final long offs = y + ((0 <= y) ? 4712 : 4713);
    if (3 <= m) {
      return (long) (365 * offs + offs / 4) + 60 + (306 * (m - 3) + 5) / 10 + d - 1 - 1721033;
    }
    return (long) (365 * offs + offs / 4) + ((1 >= m) ? 0 : 31) + d - 1 - 1721033;
  }

  /** Era Days ('Hemeras') start on JD 1721033, 1 BC as year -1. */
  // ED 0.0 = JD 1721033.0
  // Cmp. http://howardhinnant.github.io/date_algorithms.html @2021-04-11.
  public static long boxedJulian4EraDayAs12h(long z) {
    final long eProleptic = (long) (365.25 * 4) * 10000;
    final long d = eProleptic + z + TIME_Y0_JD - 60;
    final long cycles = d / (int) (365.25 * 4);
    final int doc = (int) (d - cycles * (int) (365.25 * 4));
    final int doey = doc - (doc / 365) * 365;
    final int em = (5 * doey + 2) / 153; // [0, 11]
    final int dom = doey - (153 * em + 2) / 5 + 1; // [1, 31]
    final int m = em + (em < 10 ? 3 : -9); // [1, 12]
    final long y =
        (-4712 - (int) (eProleptic / 365.25)) + (cycles * 4 + (doc / 365) + ((m <= 2) ? 1 : 0));
    final long out = ((0 <= y) ? y : -y) * 10000L + m * 100 + dom;
    return (0 < y) ? out : (-out - 10000);
  }

  /** Era Days ('Hemeras') start on JD 1721033, 1 BC as year -1. */
  public static double boxedJulian4EraDay(double z) {
    z += 0.5;
    long days = (long) z;
    days = (long) (((0 <= z) || (days == z)) ? z : (z - 1));
    double out = boxedJulian4EraDayAs12h(days);
    z = ((z >= days) ? (z - days) : (days - z));
    return out + ((0 <= out) ? z : (-z));
  }

  /** Era Days ('Hemeras') start on JD 1721033, 1 BC as year 0. */
  public static double boxedGreg4EraDay(double z) {
    z += 0.5;
    long days = (long) z;
    days = (long) (((0 <= z) || (days == z)) ? z : (z - 1));
    double out = boxedGreg4EraDayAs12h(days);
    z = ((z >= days) ? (z - days) : (days - z));
    return out + ((0 <= out) ? z : (-z));
  }

  /** Era Day 0 (ED 0 = JD 1721033) is Sunday (noon), ~73500 secs after tick 0 (for HT16). */
  public static int weekday4EraDay(long z) {
    final int rem = (int) (z % 7);
    return (0 <= rem) ? rem : (7 + rem);
  }

  // Cmp. www.astro.uni.torun.pl.
  // Cum.: 100.0021383976 * eCent + 8.43550E-7 * eCent * eCent + 5.88E-11 * eCent * eCent * eCent.
  public static double tropYearLengthEst4J2000Ticks(double j2000Secs) {
    final double eCent = j2000Secs / (36525.0 * 24 * 3600.0);
    final double val = (365.242189669781 - 6.161870E-6 * eCent - 6.44E-10 * eCent * eCent) * 86400;
    if ((eCent <= -50.0) || (50.0 <= eCent)) {
      // Well, ...
      final double x0 = 365.2422 * 86400;
      final double weight = 100 - ((0 <= eCent) ? eCent : -eCent);
      return (0 >= weight) ? x0 : (val * weight + x0 * (50 - weight)) / 50;
    }
    return val;
  }

  /** J2000 starts at 12:00 TT (2000-01-01E12:00). */
  public static double j2000Ticks4TerrestrialTime(int y, int m, int d, double sec) {
    final long days = daysUnix4Greg(y, m, d) - daysUnix4Greg(2000, 1, 1);
    return days * (24.0 * 3600.00) + sec - 12.0 * 3600.0;
  }

  /** Convert GPS seconds (1.5 per tick) to J2000 SI ticks (based on current time if cycle < 0). */
  // TAI = GPST (P) + 19.000s, GPS ticks are 1.5 sec, starting 1980-01-06T0.
  public static double j2000Ticks4GpsSecs(int cycle, int week, double secOfWeek) {
    if (0 > cycle) {
      final long days = (long) (currentTimeNanobisLinearized(false) / ((1L << 30) * 24.0 * 3600.0));
      cycle = (int) ((days + 500 + 20 * 365) / (1024 * 7));
    }
    long day = (cycle * 1024 + week) * 7 + eraDay4GregAt12h(1980, 1, 6);
    return (julianDay4EraDay(day) - 2451545) * 24.0 * 3600.0
        + secOfWeek
        + 19
        + j2000Ticks4Atomic2000(0.0);
  }

  public static int month4Letters3(String mmm, int xOffset) {
    mmm = mmm.toUpperCase(Locale.ROOT);
    switch (mmm.charAt(xOffset)) {
      case 'J':
        return (0 < mmm.indexOf('L', xOffset))
            ? 7
            : ((0 < mmm.indexOf('A', xOffset)) ? 1 : ((0 < mmm.indexOf('N', xOffset)) ? 6 : 0));
      case 'F':
        return 2;
      case 'M':
        return (0 < mmm.indexOf('R', xOffset)) ? 3 : 5;
      case 'A':
        if (((xOffset + 1) < mmm.length()) && ('9' >= mmm.charAt(xOffset + 1))) {
          // TAI:
          return 0;
        }
        return (0 < mmm.indexOf('P', xOffset)) ? 4 : 8;
      case 'S':
        return 9;
      case 'O':
        return 10;
      case 'N':
        return 11;
      case 'D':
        return 12;
      case '.':
        return 0;
      default:
        if (0 <= dateMarkers.indexOf(mmm.charAt(xOffset))) {
          return 0;
        }
    }
    return -1;
  }

  /**
   * Set time value (nanobiSec2000 << 30) according to current time, or adjust overall timer.
   *
   * @param value The original value or -1 for current time.
   * @param minimum -1 for unmarked time value, >1 for potential change of overall timer.
   * @return
   */
  public static long alignTime(long value, long minimum) {
    long minTimeVal = currentTimeNanobisLinearized(true);
    final long hour = 3600L << 30;
    final long minute = 60L << 30;
    final long delta = 2 * minute;
    long current = currentTimeNanobisLinearized(false);
    long val = (0 <= value) ? value : current;
    long ref = (1 <= minimum) ? minimum : current;
    if (ref > (current + TIME_SHIFTED)) {
      // Bad ref?
      if (TIME_MAX_AS_CURR_REASONABLE_UNIX_MILLIS <= (ref >>> 20)) {
        ref = minTimeVal;
      }
      // Adjust overall timer?
      if (ref > current) {
        long vx = (ref > minTimeVal) ? ref : minTimeVal;
        minTimeNanobiSecCount2000.compareAndSet(minTimeVal, vx);
        minTimeVal = vx;
      }
    }
    if (val < (ref - delta)) {
      // Time zone changed?
      if (val >= (ref - 25 * hour)) {
        long out = val & ~TIME_SHIFTED;
        while (out < (ref - delta)) {
          out += hour;
        }
        if (out <= current) {
          return out | TIME_SHIFTED_HOUR;
        }
      }
      return (ref & ~TIME_SHIFTED_HOUR) | TIME_SHIFTED_UNKNOWN;
    } else if (val > (current + delta)) {
      if (val > (current + 25 * hour)) {
        return current | TIME_SHIFTED_UNKNOWN;
      }
      long out = val & ~TIME_SHIFTED;
      while (out > (current + delta)) {
        out -= hour;
      }
      return out | TIME_SHIFTED_HOUR;
    }
    return (0 <= minimum) ? val : (val & ~TIME_SHIFTED);
  }

  public static void alignId(long minId) {
    long id = minTimeNanobiSec4Id.get();
    if (minId > id) {
      long max = 24 * 3600 * (1L << 30) + currentTimeNanobisLinearized(false);
      if (minId < max) {
        minTimeNanobiSec4Id.compareAndSet(id, minId);
      }
    }
  }

  /**
   * Calculate limit or steps to limit.
   *
   * @param count
   * @param previousLimit
   * @return
   */
  public static long processTimer(int count, long previousLimit) {
    if (previousLimit <= 0) {
      return currentTimeMillisLinearized() + processMaxTimer;
    }
    long now = currentTimeMillisLinearized() + 2;
    if (now >= previousLimit) {
      return Long.MIN_VALUE + 1;
    }
    return (processMaxTimer / 2 * count) / (processMaxTimer + (int) (now - previousLimit));
  }

  private static String date4Boxed(
      long box,
      double secOfDay,
      double fracD4,
      char markerSys,
      char markerMin,
      char markerLeapSec,
      int cFracDigits) {
    final long boxPos = (0 <= box) ? box : -box;
    if (INT_D4_FACT_LONG <= fracD4) {
      final long e0 = (long) (fracD4 * INT_D4_F_INV);
      secOfDay += e0;
      fracD4 -= e0 * INT_D4_FACT;
      fracD4 = (0.0 >= fracD4) ? 0.0 : fracD4;
    }
    String year = "" + (box / 10000);
    if (4 > year.length()) {
      final String digits = "000" + boxPos / 10000;
      year = ((0 <= box) ? "" : "-") + digits.substring(digits.length() - 4);
    }
    final int m = (int) ((boxPos / 100) % 100);
    final int d = (int) (boxPos % 100);
    final int h = (int) secOfDay / 3600;
    final int min = (int) secOfDay / 60 - h * 60;
    final int s = (int) secOfDay % 60;
    return year
        + '-'
        + ((m < 10) ? "0" : "")
        + m
        + '-'
        + ((d < 10) ? "0" : "")
        + d
        + markerSys // 'T'
        + ((h < 10) ? "0" : "")
        + h
        + markerMin // ':'
        + ((min < 10) ? "0" : "")
        + min
        + (((0 == s) && (0 == fracD4))
            ? ""
            : markerLeapSec // ':'
                + ((s < 10) ? "0" : "")
                + s
                + BigSxg.rxxFraction4DoublePos(fracD4, INT_D4_FACT_LONG, 10, cFracDigits));
  }

  public static String date4Millis(long... msecOptional) {
    long v0 =
        ((null == msecOptional) || (0 >= msecOptional.length) || (0 > msecOptional[0]))
            ? currentTimeMillisLinearized()
            : msecOptional[0];
    if (!timeZoneDone) {
      checkTimeZone();
    }
    final double ed = eraDay4JulianDay(julianDay4UnixTicks(v0 / 1000.0));
    final double boxed = boxedGreg4EraDay(ed);
    final double frac = (0 <= boxed) ? (boxed - (long) boxed) : (-boxed + (long) -boxed);
    return date4Boxed((long) boxed, 0, (long) (frac * 86400 * INT_D4_FACT), 'T', ':', ':', 3);
  }

  public static String date4EraDay(double... dayOptional) {
    double v0 =
        ((null == dayOptional) || (0 >= dayOptional.length)) ? currentTimeEraDay() : dayOptional[0];
    final double boxed = boxedGreg4EraDay(v0);
    final double frac = (0 <= boxed) ? (boxed - (long) boxed) : (-boxed - (long) -boxed);
    return date4Boxed((long) boxed, 0, (long) (frac * 86400 * INT_D4_FACT), 'T', ':', ':', 3);
  }

  public static String dateGregProj4EraTicks(double ticks) {
    final double boxed = boxedGreg4EraDay(eraDayProj4EraTicks(ticks));
    final double frac = (0 <= boxed) ? (boxed - (long) boxed) : (-boxed + (long) -boxed);
    return date4Boxed((long) boxed, frac * 86400, 0, 'T', ':', ':', 0);
  }

  /** Use the projective (ticks) variant for HT16! This is for 'currentTime' calculations. */
  public static String dateEst4J2000(double j2000Secs) {
    return date4Millis(millisUnixEst4J2000(j2000Secs));
  }

  /** Convert J2000 SI ticks (S) to Terrestrial Time (ET = TDT/TT). */
  // 'Scientific' ticks (S) is the count of seconds since 2000-01-01T12 TT (J2000) = JD 2451545.0.
  public static String dateTerrestrialTime4J2000Ticks(double s) {
    s += 12.0 * 3600.0;
    final long vFloor = ((0.0 <= s) || (s == (long) s)) ? (long) s : (-1 + (long) s);
    final long ed2000 =
        (0 < vFloor) ? (vFloor / (24 * 3600)) : ((vFloor + 1 - 24 * 3600) / (24 * 3600));
    final long box =
        boxedGreg4EraDayAs12h(ed2000 + TIME_2000_ED); // ((int) ((2000L * 3652425) / 10000))+..);
    final long sod = (int) (vFloor - ed2000 * 24 * 3600);
    final double frac = s - vFloor;
    return date4Boxed(box, sod, frac * INT_D4_FACT, 'E', ':', ':', 0);
  }

  public static String dateLocal4Millis(long... msec) {
    long v0 =
        ((null == msec) || (0 >= msec.length) || (0 > msec[0]))
            ? currentTimeMillisLinearized()
            : msec[0];
    if (!timeZoneDone) {
      checkTimeZone();
    }
    // Local time (without sdf.setTimeZone(TimeZone.getTimeZone()):
    String date = date4Millis(v0 + timeZoneOffsetMillis);
    //  "+00:00":
    // date = date.substring(0, date.length() - 6);
    String tzone = "+";
    int tzMint = timeZoneOffsetMillis / (1000 * 60);
    if (0 > tzMint) {
      tzone = "-";
      tzMint = -tzMint;
    }
    tzone += String.format("%02d:%02d", (tzMint / 60), tzMint % 60);
    return date + tzone;
  }

  /** As YYMMDD.HH:MM... */
  public static String dateShort4Millis(long... msec) {
    long v0 =
        ((null == msec) || (0 >= msec.length) || (0 > msec[0]))
            ? currentTimeMillisLinearized()
            : msec[0];
    if (!timeZoneDone) {
      checkTimeZone();
    }
    // Local time (without sdf.setTimeZone(TimeZone.getTimeZone()):
    String dx = DATE_SDF.format(new Date(v0 + timeZoneOffsetMillis));
    dx = dx.substring(2, dx.length() - 9).replaceAll("[^0-9T\\:]", "").replace('T', '.');
    return dx;
  }

  /** 62 (-1) bit (sortable) hash: shifted tick value + TZ info + precision/ leap secs of day. */
  // date: 00 + sign (1=pos) + exp (-10,0,10,20) + 49 bits + flags9 + 0/1
  // flags9: ..0. Era ticks (7+1 bits value), ...1. TZ/error (2 bit leap sec (-1) + 6 bits)
  // TZ: 4..63: [-32,*30] Min., 1: hour wrong, 2,3: other error, 8: -12h = HT.
  public static long hash62oEraTicks(
      long ticks,
      long nanobisAdd,
      int tzMinutes, // -14*60..15*60, or 0
      int precisionLevel, // 1 leap seconds provided, 0 leap seconds unknown, -1 hour?, -2 error?
      int leapSecsOfDay) { // -1..2
    final long nx = (nanobisAdd >> 30);
    ticks += nx - 2000.0 * YEAR_SECONDS_YEAR;
    nanobisAdd -= nx;
    final long posTicks = (0 <= ticks) ? ticks : -ticks;
    tzMinutes =
        (((-14 * 60) > tzMinutes) || (0 == tzMinutes))
            ? (((0 <= precisionLevel) && (0 == leapSecsOfDay)) ? 0 : 32)
            : (tzMinutes + 16 * 60) / 30;
    tzMinutes = (0 <= precisionLevel) ? tzMinutes : -precisionLevel;
    int shift =
        ((1L << 56) <= posTicks)
            ? -10
            : (((1L << 46) <= posTicks) ? 0 : (((1L << 36) <= posTicks) ? 10 : 20));
    final int iShift = (0 <= ticks) ? (2 - shift / 10) : (1 + shift / 10);
    shift += 2;
    long top = (0 <= shift) ? ((ticks << shift) + (nanobisAdd >> (30 - shift))) : (ticks >> -shift);
    top = ((1L << 61) ^ (top & (~(0x1bL << 59) & ~0x7L))) | ((long) iShift << 59);
    if (0 != tzMinutes) {
      return (top & ~0x3ffL) | (tzMinutes << 4) | (((leapSecsOfDay + 1) & 0x3) << 2) | 0x2;
    }
    return (top & ~0x3L);
  }

  /** Convert specific 62 (-1) bit hash to Era ticks, optionally returning TZ and leap secs. */
  public static double eraTicks4Hash62(long hash, int... yOptTzErrorLeapsecs) {
    int iShift = (int) (hash >>> 59) & 0x7;
    iShift = (4 <= iShift) ? (iShift - 4) : (3 - iShift);
    final int shift = 20 - iShift * 10;
    final boolean hasTz = (0 != (hash & 2));
    int leapSecs = hasTz ? (((int) ((hash >>> 2) & 0x3L)) - 1) : 0;
    final int tzMinOrErr = hasTz ? (int) ((hash >>> 3) & 0x3fL) : 0;
    final int tzMint = (3 >= tzMinOrErr) ? 0 : (tzMinOrErr * 30 - 16 * 60);
    long shifted = hash & ((1L << 59) - 1) & ((0 != (hash & 2)) ? (~0x3ffL) : ~0L);
    shifted = (shifted | ((0 != (hash & (1L << 61))) ? 0 : (0x1fL << 59))) >> 2;
    double fact = (0 <= shift) ? (1.0 / (1L << shift)) : (1.0 * (1L << -shift));
    if (0 < yOptTzErrorLeapsecs.length) {
      yOptTzErrorLeapsecs[yOptTzErrorLeapsecs.length - 1] = leapSecs;
      yOptTzErrorLeapsecs[(yOptTzErrorLeapsecs.length >= 2) ? 1 : 0] =
          (3 >= tzMinOrErr) ? tzMinOrErr : 0;
      yOptTzErrorLeapsecs[0] = tzMint;
    }
    return 2000.0 * YEAR_SECONDS_YEAR + shifted * fact;
  }

  /** Convert specific 62 (-1) bit hash to date string. */
  // Marker 'Y' for format Z, lower case for local time, '.' for C local time.
  public static String date4Hash62(long hash, char xOptMarker) {
    xOptMarker = (('A' > xOptMarker) || ('c' == xOptMarker)) ? '.' : xOptMarker;
    boolean asLocal = ('Z' < xOptMarker) || ('.' == xOptMarker);
    xOptMarker =
        ('Z' < xOptMarker) ? (char) (((xOptMarker & 0x7f) | 0x60) - ('a' - 'A')) : xOptMarker;
    xOptMarker = ('Z' == xOptMarker) ? 'Y' : xOptMarker;
    final int[] tzErrorLeapsecs = new int[3];
    final double et = eraTicks4Hash62(hash, tzErrorLeapsecs);
    if ('E' == xOptMarker) {
      return dateTerrestrialTime4J2000Ticks(et - 2000.0 * YEAR_SECONDS_YEAR);
    }
    int tzMint = tzErrorLeapsecs[0];

    final double tic = et + tzMint * 60.0;
    final double days =
        (('Y' != xOptMarker) ? eraDayProj4EraTicks(tic) : (tic / (24.0 * 3600))) + 0.5;
    final long ed = (0.0 <= days) ? (long) days : (long) (-1.0 + days);
    long boxed = ('J' != xOptMarker) ? boxedGreg4EraDayAs12h(ed) : 0;
    if (('T' != xOptMarker) && ('Y' != xOptMarker)) {
      if ((15821015.0 > boxed) && ('G' != xOptMarker)) {
        boxed = boxedJulian4EraDayAs12h(ed);
      } else if (boxed < 10000) {
        // Astronomic Y0 as 1 B.C.
        boxed -= 10000;
      }
    }
    final double secOfDay = (days - ed) * (24.0 * 3600);
    final int err = tzErrorLeapsecs[1];
    final char markerError = ((0 < err) && (err <= 3)) ? '?' : ':';
    final char mk =
        (asLocal && (0 != tzMint) && ('.' != xOptMarker))
            ? (char) (xOptMarker + 'a' - 'A')
            : xOptMarker;
    String out = date4Boxed(boxed, secOfDay, 0.0, mk, markerError, ':', 0);
    if ((0 < err) && (err <= 3)) {
      int ix = out.indexOf(markerError);
      if (1 == err) {
        out = out.substring(0, ix - 2) + "??:" + out.substring(ix + 1);
      } else {
        out = out.substring(0, ix) + "???"; // + out.substring(ix + 3);
      }
    } else if ((0 != tzMint) && !asLocal) {
      out += (0 > tzMint) ? "-" : "+";
      final int min = (0 <= tzMint) ? tzMint : -tzMint;
      out += String.format("%02d:%02d", (min / 60), min % 60);
    }
    return out;
  }

  public static String prependCentury(String shortDate) {
    String yyyy = date4Millis().substring(0, 4);
    int pp = (yyyy.charAt(0) & 0xf) * 10 + (yyyy.charAt(1) & 0xf);
    if ((yyyy.charAt(2) & 0xf) > ((shortDate.charAt(0) & 0xf) + 5)) {
      ++pp;
    } else if ((yyyy.charAt(2) & 0xf) < ((shortDate.charAt(0) & 0xf) - 5)) {
      --pp;
    }
    final char digit = (2 > shortDate.length()) ? '0' : shortDate.charAt(1);
    final String fill = ((2 > shortDate.length()) || (digit < '0') || ('9' < digit)) ? "0" : "";
    return "" + pp + fill + shortDate;
  }

  /** Accepts YY*-(M)M-(D)D, YY*-(M)M-(D)D.(H)H:(m)m:(s)s.F* etc., w/o deltas. */
  // With '.' or lower case for local time. No delta-time calculations.
  public static double eraDay4Normalized(String date, int[] yOptTzMinutes) {
    char julianOpt = 0;
    int i0 = ('-' == date.charAt(0)) ? 1 : 0;
    int year = 0;
    char c0 = 0;
    for (; i0 < date.length(); ++i0) {
      c0 = date.charAt(i0);
      if (!(('0' <= c0) && (c0 <= '9'))) {
        break;
      }
      year = year * 10 + (c0 & 0xf);
    }
    int tzMint = 0;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    int month = c0 & 0xf;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    month = (('0' <= c0) && (c0 <= '9')) ? (month * 10 + (c0 & 0xf)) : month;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    i0 += (('0' <= c0) && (c0 <= '9')) ? 0 : 1;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    int day = c0 & 0xf;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    day = (('0' <= c0) && (c0 <= '9')) ? (day * 10 + (c0 & 0xf)) : day;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    if (('.' == c0) || ('Z' < c0)) {
      c0 -= ('Z' < c0) ? ('a' - 'A') : 0;
      checkTimeZone();
      tzMint = timeZoneOffsetMillis / (1000 * 60);
    }
    char marker = c0;
    julianOpt = (('J' == marker) || ('.' == marker) || ('C' == marker)) ? marker : 0;
    i0 += (('0' <= c0) && (c0 <= '9')) ? 0 : 1;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    int hour = c0 & 0xf;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    hour = (('0' <= c0) && (c0 <= '9')) ? (hour * 10 + (c0 & 0xf)) : hour;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    i0 += (('0' <= c0) && (c0 <= '9')) ? 0 : 1;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    int min = c0 & 0xf;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    min = (('0' <= c0) && (c0 <= '9')) ? (min * 10 + (c0 & 0xf)) : min;
    ++i0;
    c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
    int sec = 0;
    double frac = 0.0;
    if (('+' != c0) && ('-' != c0) && (0 != c0)) {
      i0 += (('0' <= c0) && (c0 <= '9')) ? 0 : 1;
      c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
      sec = c0 & 0xf;
      ++i0;
      c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
      sec = (('0' <= c0) && (c0 <= '9')) ? (sec * 10 + (c0 & 0xf)) : sec;
      ++i0;
      c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
      i0 += (('0' <= c0) && (c0 <= '9')) ? 0 : 1;
      c0 = (i0 < date.length()) ? date.charAt(i0) : 0;
      double fact = 0.1;
      for (; (i0 < date.length()) && ('-' < (c0 = date.charAt(i0))); ++i0) {
        frac += (c0 & 0xf) * fact;
        fact /= 10;
      }
    }
    if (('+' == c0) || ('-' == c0)) {
      tzMint = 0;
      boolean pos = ('+' == c0);
      ++i0;
      int count = 0;
      for (; i0 < date.length(); ++i0) {
        c0 = date.charAt(i0);
        if (('0' <= c0) && (c0 <= '9')) {
          tzMint = 10 * tzMint + (c0 & 0xf);
          ++count;
        }
      }
      for (count = 4 - count; 0 < count; --count) {
        tzMint *= 10;
      }
      tzMint = (tzMint / 100) * 60 + (tzMint % 100);
      tzMint = pos ? tzMint : -tzMint;
    }
    if (0 != tzMint) {
      hour -= tzMint / 60;
      min -= tzMint % 60;
    }
    if (null != yOptTzMinutes) {
      yOptTzMinutes[0] = tzMint;
    }
    if (('J' != julianOpt) && (0 != julianOpt) && ('-' != date.charAt(0))) {
      final int monthDay = month * 100 + day;
      if ((1582 < year) || ((1582 == year) && (monthDay >= 1015))) {
        julianOpt = 0;
      }
    }
    year = ('-' == date.charAt(0)) ? (-year) : year;
    year += ((0 > year) && ('G' == marker)) ? 1 : 0;
    final long ed =
        (0 == julianOpt)
            ? eraDay4GregAt12h(year, month, day)
            : eraDay4JulianUnifiedAt12h(year, month, day);
    return ed + (hour - 12) / 24.0 + (min * 60.0 + sec + frac) / (24.0 * 3600);
  }

  private static String dateNormalize(
      String date, char marker, boolean prependCentury, int xjYear, int xjMonth, int xjDay) {
    final int len = date.length();
    if ((xjMonth >= len) || (xjDay >= len)) {
      return "0000";
    }
    int ix = xjMonth;
    char cx = (ix < len) ? date.charAt(ix) : 0;
    ix += (('0' <= cx) && (cx <= '9')) ? 0 : 1;
    cx = (ix < len) ? date.charAt(ix) : 0;
    int month = cx & 0xf;
    ++ix;
    cx = (ix < len) ? date.charAt(ix) : 0;
    month = (('0' <= cx) && (cx <= '9')) ? (10 * month + (cx & 0xf)) : month;
    int max = ix;
    ix = xjDay;
    cx = (ix < len) ? date.charAt(ix) : 0;
    ix += (('0' <= cx) && (cx <= '9')) ? 0 : 1;
    cx = (ix < len) ? date.charAt(ix) : 0;
    int day = cx & 0xf;
    ++ix;
    cx = (ix < len) ? date.charAt(ix) : 0;
    day = (('0' <= cx) && (cx <= '9')) ? (10 * day + (cx & 0xf)) : day;
    max = (ix <= max) ? max : ix;
    String year = "";
    if ((xjYear >= len) || (0 > xjYear)) {
      year =
          ""
              + (long)
                      boxedGreg4EraDay(
                          eraDayProj4EraTicks(currentTimeNanobisLinearized(false) / (1 << 30)))
                  / 10000;
    } else if (prependCentury) {
      year = date.substring(xjYear, xjYear + (((xjYear + 1) < len) ? 2 : 1));
      year = prependCentury(year);
      max = (xjYear <= max) ? max : (xjYear + 1);
    } else {
      final boolean neg = ('-' == date.charAt(xjYear));
      ix = xjYear + (neg ? 1 : 0);
      for (; ix < len; ++ix) {
        cx = date.charAt(ix);
        if ((cx < '0') || ('9' < cx)) {
          break;
        }
      }
      year = date.substring(xjYear, ix);
      if ((4 > (ix - xjYear)) && !neg) {
        year = "0000".substring(ix - xjYear) + year;
      }
      max = (ix <= max) ? max : ix;
    }
    ++max;
    cx = (max < len) ? date.charAt(max) : '0';
    max += (('0' <= cx) && (cx <= '9')) ? 0 : 1;
    return year
        + '-'
        + ((10 > month) ? "0" : "")
        + month
        + '-'
        + ((10 > day) ? "0" : "")
        + day
        + ((max >= len)
            ? (('A' <= marker) ? ("" + marker) : "")
            : ("" + marker + date.substring(max)));
  }

  /** Accepts YYMMDD, DD/MM/YY, (M)M-(D)D/YY, (D)D-mmm-YY, mmm-(D)D-YY, and long forms YY*-MM-DD. */
  // Append '.replace("???", ":00").replace("??", "00")' if needed.
  public static String dateNormalize(String date) {
    String prefix = "";
    if (date.startsWith("-")) {
      if (6 >= date.length()) {
        ///// -Y+(MM(DD)?)?
        date = date + "-00000".substring(date.length());
      }
      prefix = "-";
      date = date.substring(1);
    } else if (6 >= date.length()) {
      if (date.matches("[0-9]+")) {
        ///// Y?Y(MM(DD)?)?
        if (0 != (1 & date.length())) {
          date = "0" + date;
        }
        date = date + "000000".substring(date.length());
        return dateNormalize(date, '.', true, 0, 2, 4);
      }
    }
    for (int i0 = 0; i0 < date.length(); ++i0) {
      char c0 = date.charAt(i0);
      char c1 = 0;
      char c2 = 0;
      if (('0' <= c0) && (c0 <= '9')) {
        continue;
      }
      if ((6 == i0) && (('.' == c0) || ('A' <= c0)) && "".equals(prefix)) {
        // YYMMDD.
        return dateNormalize(prefix + date, c0, true, 0, 2, 4);
      }
      int i1 = i0 + 1;

      ///// Months as letters?
      if (('A' <= c0) && (c0 <= 'z') && (3 >= i0)) {
        final int month = month4Letters3(date, i0);
        for (; i1 < date.length(); ++i1) {
          c1 = date.charAt(i1);
          if (('0' <= c1) && (c1 <= '9')) {
            break;
          }
        }
        if (0 == i0) {
          ///// mmm-DD-YY etc.
          for (int i2 = i1 + 1; i2 < date.length(); ++i2) {
            c2 = date.charAt(i2);
            if ((c2 < '0') || ('9' < c2)) {
              int i3 = i2 + 1;
              for (; i3 < date.length(); ++i3) {
                char c3 = date.charAt(i3);
                if ((c3 < '0') || ('9' < c3)) {
                  break;
                }
              }
              return dateNormalize(
                  prefix + date.substring(i1, i2) + '-' + month + '-' + date.substring(i2 + 1));
            }
          }
          ///// mmmDD..., ignore prefix.
          i0 = i1 + 2;
          final int len = date.length();
          if (i0 >= len) {
            return dateNormalize(
                "" + month + '-' + date.substring(i1, len), '.', true, -1, 0, (9 < month) ? 3 : 2);
          }
          final String mm = ((9 < month) ? "" : "0") + month;
          date = date.substring(i1, i0) + '-' + mm + '-' + date.substring(i0);
          return dateNormalize(date);
        }
        if (0 >= month) {
          ///// YY*...E/T/...
          return prefix + date;
        }
        ///// DDmmmYY etc.
        date = date.substring(0, i0) + '-' + month + '-' + date.substring(i1);
      }

      ///// With separator.
      for (i1 = i0 + 1; i1 < date.length(); ++i1) {
        c1 = date.charAt(i1);
        if ((c1 < '0') || ('9' < c1)) {
          break;
        }
      }
      int i2 = i1 + 1;
      for (; i2 < date.length(); ++i2) {
        c2 = date.charAt(i2);
        if ((c2 < '0') || ('9' < c2)) {
          break;
        }
      }
      if (('A' <= c2) && (c2 <= 'z')) {
        final int month = month4Letters3(date, (('A' <= c1) && (c1 <= 'z')) ? i1 : i2);
        if (0 >= month) {
          ///// YY*...E/T/...
          return prefix + date;
        }
        ///// DD-mmm-... etc., ignore prefix.
        for (; i2 < date.length(); ++i2) {
          c2 = date.charAt(i2);
          if (('0' <= c2) && (c2 <= '9')) {
            break;
          }
        }
        return dateNormalize("" + month + '-' + date.substring(0, i0) + '/' + date.substring(i2));
      } else if ((i2 < date.length()) && ('-' == c0) && ('-' == c1)) {
        // Y+-M+-D+.+
        return prefix + dateNormalize(date, c2, false, 0, i0 + 1, i1 + 1);
      }
      if ((2 < i0) || (i1 >= date.length())) {
        // YYYY...
        return prefix + date;
      }
      if (('/' == c1) && ('/' != c0)) {
        ///// MM-DD/YYYY etc.
        return prefix + dateNormalize(date, '.', (3 >= (i2 - i1)), i1 + 1, 0, i0 + 1);
      }
      ///// DD.MM etc.
      return prefix + dateNormalize(date, '.', (3 >= (i2 - i1)), i1 + 1, i0 + 1, 0);
    }
    int len = date.length();
    if (4 > len) {
      return "0000".substring(0, len);
    }
    date = prefix + date.substring(0, len - 4) + '-' + date.substring(len - 4);
    len = date.length();
    return dateNormalize(date, '.', false, 0, len - 4, len - 2);
  }

  public static long hash62oDate(String xOptDate) {
    if (null == xOptDate) {
      return hash62oEraTicks(
          (long) TIME_2000_HT_SECONDS, currentTimeNanobisLinearized(true), 0, 0, 0);
    }
    String date = xOptDate.trim();
    int shifted = (0 > date.indexOf('?')) ? 0 : (date.contains("???") ? -2 : -1);
    date = date.replace("???", ":00").replace("??", "00");
    int[] tz = new int[1];
    String dn = dateNormalize(date);
    if ((null == dn) || (5 > dn.length())) {
      return hash62oEraTicks(
          (long) TIME_2000_HT_SECONDS, currentTimeNanobisLinearized(true), 0, -2, 0);
    }
    double days = eraDay4Normalized(dn, tz);
    double ticks = eraTicksProj4EraDay(days);
    final long tr = ((long) (((0 <= ticks) ? 0.1 : -0.1) + ticks));
    final long nanobis = (long) ((ticks - tr) * (1024.0 * 1024.0 * 1024.0));
    return hash62oEraTicks(tr, nanobis, tz[0], shifted, 0);
  }

  public static long getOldPidBase(long... optionalTime) {
    String dateHour = date4Millis(optionalTime).substring(0, 15);
    int year =
        (dateHour.charAt(0) & 0xf) * 1000
            + (dateHour.charAt(1) & 0xf) * 100
            + (dateHour.charAt(2) & 0xf) * 10
            + (dateHour.charAt(3) & 0xf);
    int month = (dateHour.charAt(5) & 0xf) * 10 + (dateHour.charAt(6) & 0xf);
    int day2 = ((dateHour.charAt(8) & 0xf) * 10 + (dateHour.charAt(9) & 0xf)) << 1;
    int hour = ((dateHour.charAt(11) & 0xf) * 10 + (dateHour.charAt(12) & 0xf));
    day2 = day2 - ((12 > hour) ? 1 : 0);
    //    hour2 += ('3' <= dateHour.charAt(14)) ? 1 : 0;
    int stamp =
        (((year - 2000) & 0xf0) << 14) | (((year - 2000) & 0xf) << 12) | (month << 6) | day2;
    if (stamp <= idStampHex) {
      ++idCount;
    } else {
      idStampHex = stamp;
      idCount = 1;
    }
    if ((1 << 30) < idCount) {
      // Wow :-)
      return ((long) stamp) << 32;
    }
    return (((long) stamp) << 32) | (idCount & ((1L << 32) - 1));
  }

  public static Object convert4Hash62(long xHash62, String xFormat) {
    DateFormat format = DateFormat.find(xFormat);
    if (null == format) {
      return null;
    }
    final int[] tzMint = new int[1];
    final double ticks = eraTicks4Hash62(xHash62, tzMint);
    switch (format) {
      case ANNUS:
        return (ticks - 2000.0 * YEAR_SECONDS_YEAR) / YEAR_ANNUS + 2000.0;
        // case BOXED:
        // return boxedGreg4EraDay(eraDayProj4EraTicks(ticks));
      case D:
        return julianDay4EraDay(eraDayProj4EraTicks(ticks));
      case F:
        return new Long((long) ((ticks - 2000.0 * YEAR_SECONDS_YEAR) * (1 << 30)));
      case K:
        return new Double(
            (double) weekday4EraDay((long) eraDayProj4EraTicks(ticks + tzMint[0] * 60.0)));
      case L:
        return new Long(xHash62);
      case N:
        return eraDayProj4EraTicks(ticks);
      case P:
        return ((j2000Ticks4EraTicks(ticks)
                - 19
                - j2000Ticks4Atomic2000(0.0)
                + (eraDay4GregAt12h(2000, 1, 1) - eraDay4GregAt12h(1980, 1, 6)) * 24.0 * 3600))
            / (7 * 24.0 * 3600);
      case Q:
        return ticks;
      case S:
        return j2000Ticks4EraTicks(ticks);
      case X:
        return millisUnixEst4J2000(j2000Ticks4EraTicks(ticks)) / 1000.0;
      case Y:
        return ticks / YEAR_SECONDS_YEAR;
      default:
        ;
    }
    switch (format) {
      case C:
      case G:
      case J:
      case T:
        // case Z:
        // Including '.' !
        return date4Hash62(xHash62, ((1 == xFormat.length()) ? xFormat : format.name()).charAt(0));
        // case H:
        // TODO
        // break;
      case E:
        return dateTerrestrialTime4J2000Ticks(j2000Ticks4EraTicks(ticks));
      case TAI:
        return dateTerrestrialTime4J2000Ticks(
                j2000Ticks4EraTicks(ticks) - Dib2Constants.TIME_DELTA_TAI)
            .replace('E', 'A');
        // case W:
        // TODO
        // break;
      case SHORTLOCAL:
        return date4Hash62(xHash62, '.');
      default:
        ;
    }
    return "Y" + (ticks / YEAR_SECONDS_YEAR);
  }

  public static long convert2Hash62(Object xDate, String xFormatOpt) {
    DateFormat format = (null == xFormatOpt) ? null : DateFormat.find(xFormatOpt);
    if (null == format) {
      if (xDate instanceof Long) {
        return (Long) xDate;
      } else if (!(xDate instanceof String)) {
        return 1;
      }
      final String date = (String) xDate;
      return hash62oDate(date);
    }
    String fName = (DateFormat.TAI == format) ? "A" : format.name();
    double ticks = 0;
    if (xDate instanceof String) {
      int[] tz = new int[1];
      String dn = dateNormalize((String) xDate);
      if ((null == dn) || (5 > dn.length())) {
        return 1;
      }
      switch (format) {
        case C:
        case T:
          return hash62oDate(dn);
        case G:
        case J:
          if (0 >= dn.indexOf(fName.charAt(0))) {
            for (int i0 = 0; i0 < dn.length(); ++i0) {
              if ((('0' > dn.charAt(i0)) || (dn.charAt(i0) > '9')) && ('-' != dn.charAt(i0))) {
                dn = dn.substring(0, i0) + fName + dn.substring(i0 + 1);
              }
            }
          }
          return hash62oDate(dn);
        case E:
          ticks = eraDay4Normalized(dn, tz) * 24.0 * 3600 + Dib2Constants.TIME_DELTA_TERR12_Y0;
          break;
          // case H:
          // TODO
          // return 1;
        case TAI:
          ticks = eraDay4Normalized(dn, tz) * 24.0 * 3600 + Dib2Constants.TIME_DELTA_TERR12_Y0;
          ticks += Dib2Constants.TIME_DELTA_TAI;
          break;
          // case Z:
          // TODO
          // return 1;
        default:
          return 1;
      }
      if (0 != tz[0]) {
        ticks -= tz[0] * 60.0;
      }
    } else {
      double val = 0;
      if (xDate instanceof Long) {
        switch (format) {
          case L:
            return (Long) xDate;
          case F:
          default:
            val = (Long) xDate;
        }
      } else if (!(xDate instanceof Double)) {
        return 1;
      } else {
        val = (Double) xDate;
      }
      switch (format) {
        case ANNUS:
          ticks = (val - 2000.0) * YEAR_ANNUS + 2000.0 * YEAR_SECONDS_YEAR;
          break;
          // case BOXED:
          // ticks = (0 <= val) ? (val - (long) val) : (-val + (long) -val);
          // return hash62oDate(date4Boxed((long) val, 0, (long) (ticks * 86400), 'T', ':', ':',
          // 3));
        case D:
          val = eraDay4JulianDay(val);
          ticks = eraTicksProj4EraDay(val);
          break;
        case F:
          ticks = (val / (1 << 30)) + 2000.0 * YEAR_SECONDS_YEAR;
          break;
        case K:
          // TODO
          return 1;
        case L:
          return (long) val;
        case N:
          ticks = eraTicksProj4EraDay(val);
          break;
        case P:
          ticks =
              j2000Ticks4GpsSecs(
                      (int) val / 1024, (int) val % 1024, Math.floor(val) * 7 * 24.0 * 3600)
                  + 2000.0 * YEAR_SECONDS_YEAR;
          break;
        case Q:
          ticks = val;
          break;
        case S:
          ticks = eraTicks4J2000Ticks(val);
          break;
        case X:
          ticks = eraTicksEst4UnixTicks(val);
          break;
        case Y:
          ticks = val * YEAR_SECONDS_YEAR;
          break;
        default:
          return 1;
      }
    }
    final long tr = ((long) (((0 <= ticks) ? 0.1 : -0.1) + ticks));
    final long nanobis = (long) ((ticks - tr) * (1024.0 * 1024.0 * 1024.0));
    return hash62oEraTicks(tr, nanobis, 0, 0, 0);
  }

  // =====
}
