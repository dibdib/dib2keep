// Copyright (C) 2020, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_feed.UiValFeedTag;
import net.sf.dibdib.thread_ui.UiValTag;

public enum QOpGraph implements QIfs.QEnumIf {
  // =====

  ///// Context item

  POS,
  POSX,
  POSY,
  RBASE,
  RMOVE,
  LNWIDTH,
  LNCAP,
  LNJOIN,
  DASH,
  RGBCOLOR,
  FACE,
  STYLE,
  WEIGHT,
  HEIGHT,
  // LEADING,
  TXLF,
  ENTRY,

  ///// Advancing item

  LINE,
  ARC,
  CURVE,
  TEXT,
  TXSHLEFT,
  TXCTR,
  TXBOX,
  IMAGE,
// ,
;

  private static GraphContext[] zPoolScriptContext = new GraphContext[64];
  private static final AtomicInteger zcPoolScriptContext = new AtomicInteger(0);

  public static QIfs.QEnumIf[] create() {
    return values();
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }

  // =====
  public static final class GraphContext {
    // =====

    /** Negative if recycled */
    public int eLine;

    public int eBasePt10;
    public int xPt10;
    public int yPt10;
    public GraphContext prevContext;
    public int heightPt10;
    public int color;
    public int zHeightPx;

    private GraphContext init(GraphContext previousVals, boolean forFrame) {
      final int lineSpacing =
          // Dib2Root.uiVals.i32(
          forFrame
              ? UiValTag.UI_DISPLAY_BAR_HEIGHT.getInitial()
              : UiValFeedTag.UI_LINE_SPACING_PT10.i32(0);
      final int height =
          // Dib2Root.uiVals.i32(
          forFrame
              ? UiValTag.UI_FONT_SIZE_FRAME_PT10.getInitial()
              : UiValFeedTag.UI_FONT_SIZE_PT10.i32(0);
      if (null != previousVals) {
        xPt10 = previousVals.xPt10;
        yPt10 = previousVals.yPt10;
        eLine = previousVals.eLine;
        eBasePt10 = previousVals.eBasePt10;
        prevContext = previousVals.prevContext;
        heightPt10 = previousVals.heightPt10;
        color = previousVals.color;
        return this;
      }
      xPt10 = 0;
      heightPt10 = height;
      eLine = lineSpacing;
      if (forFrame) {
        eBasePt10 =
            ((lineSpacing
                    + ((heightPt10 * Dib2Constants.UI_FONT_NMZ_CAP_H) // ,
                        >> Dib2Constants.UI_FONT_NMZ_SHIFT))
                >> 1);
      } else {
        eBasePt10 =
            ((lineSpacing
                    + ((heightPt10 * Dib2Constants.UI_FONT_NMZ_X_HEIGHT) // ,
                        >> Dib2Constants.UI_FONT_NMZ_SHIFT))
                >> 1);
      }
      yPt10 =
          eBasePt10
              - ((heightPt10 * Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT);
      eBasePt10 -= yPt10;
      prevContext = previousVals;
      color = ColorNmz.ColorDistinct.FG__BLACK.argbQ(1);
      return this;
    }

    private GraphContext() {
      eLine = -1;
      //  initParameters( null, -1, false);
    }

    public GraphContext recycleMe() {
      // Not too many ...
      if ((0 <= eLine) && (1024 > zcPoolScriptContext.get())) {
        eLine = -1;
        xPt10 = -1;
        int inx = zcPoolScriptContext.getAndIncrement();
        if (inx >= zPoolScriptContext.length) {
          zPoolScriptContext = Arrays.copyOf(zPoolScriptContext, 2 * zPoolScriptContext.length);
        }
        zPoolScriptContext[inx] = this;
      }
      prevContext = null;
      return null;
    }
    // =====
  }

  public static GraphContext makeScriptContext(GraphContext previous, boolean forFrame) {
    while (0 < zcPoolScriptContext.get()) {
      int inx = zcPoolScriptContext.decrementAndGet();
      if (null != zPoolScriptContext[inx]) {
        return zPoolScriptContext[inx].init(previous, forFrame);
      }
    }
    return new GraphContext().init(previous, forFrame);
  }

  // =====
}
