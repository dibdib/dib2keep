// Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_any.StringFunc;

// =====

public final class QTemplates {

  // =====

  /** 32 bytes each (with 0x00 as default):
   * (0) key, sizeHigh, sizeLow, Family, Style, Variant,
   * (6) Skew degrees, ZFraction percent, Weight tenths, (Width), 0, Decoration, 0, 0, 0, 0,
   * (16) ColorStrokeControl, ColorStrokeRed, ColorStrokeGreen, ColorStrokeBlue,
   * ColorFillControl, ColorFillRed, ColorFillGreen, ColorFillBlue,
   * ColorBackgroundControl, ColorBackgroundRed, ColorBackgroundGreen, ColorBackgroundBlue, 0, ...
   */
  public static final int BYTES_PER_TEMPLATE = 32;

  /////

  public static final int FONT_INX_SIZE = 1;
  public static final int FONT_INX_FAMILY = 3;
  public static final int FONT_INX_STYLE = 4;
  public static final int FONT_INX_VARIANT = 5;
  public static final int FONT_INX_SKEW = 6;
  public static final int FONT_INX_ZF = 7;
  public static final int FONT_INX_WEIGHT = 8;
  public static final int FONT_INX_DECORATION = 11;
  public static final int FONT_INX_COLOR = 16;

  public static final byte FAMILY_MONOSPACE = 0x10;
  public static final byte FAMILY_SERIF = 0x20;
  public static final byte FAMILY_SANS_SERIF = 0x30;
  public static final byte FAMILY_CURSIVE = 0x40;
  public static final byte FAMILY_FANTASY = 0x50;
  public static final byte STYLE_ITALIC = 0x1;
  public static final byte STYLE_CONDENSED = (byte) 0x80;
  public static final byte VARIANT_SMALL_CAPS = 0x1;
  public static final byte SKEW_OBLIQUE = 10;
  public static final byte WEIGHT10_NORMAL = 40; // => 400
  public static final byte WEIGHT10_BOLD = 70; // => 700

  public static final byte LINE_UNDER = 0x1;
  public static final byte LINE_OVER = 0x2;
  public static final byte LINE_THROUGH = 0x4;
  public static final byte DECO_BLINK = 0x1;
  public static final byte DECO_SHADOW = 0x2;
  public static final byte DECO_OUTLINE = 0x4;

  public static final byte COLOR_EXTERNAL_VALUES = 0;
  public static final byte COLOR_AS_ARGB = 1;
  public static final byte COLOR_OPAQUE = 1;
  public static final byte COLOR_TRANSPARENT = (byte) 0xff;
  public static final byte COLOR_EXTERNAL_CONTRAST = 0x2;

  /////

  public byte[] templatesFont;
  public String[] templateNamesFont;

  public QTemplates() {}

  public QTemplates(byte[] xmTemplatesFont, String[] xmTemplateNamesFont) {
    templateNamesFont = xmTemplateNamesFont;
    templatesFont = xmTemplatesFont;
  }

  public static final byte[] kDefaultTemplatesFont = {
    // ""
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    // BLUE
    'B', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, (byte) 0xff, 1, 0, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    // GREEN
    'G', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, (byte) 0xaa, 0, 1, 0, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    // RED
    'R', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, (byte) 0xee, 0, 0, 1, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  };

  public static final String[] kDefaultTemplateNamesFont = {"", "BLUE", "GREEN", "RED"};

  public static QTemplates Dib2UiP_Templates_Default() {
    return new QTemplates(kDefaultTemplatesFont, kDefaultTemplateNamesFont);
  }

  @Override
  public String toString() {
    StringBuilder out = new StringBuilder(100);
    out.append('\'');
    for (int i0 = 0; i0 < templatesFont.length; i0 += BYTES_PER_TEMPLATE) {
      out.append(
          (' ' < templatesFont[i0])
              ? ("" + (char) templatesFont[i0])
              : StringFunc.hex4Bytes(new byte[] {templatesFont[i0]}, false));
    }
    out.append("':");
    for (int i0 = 0; i0 < templateNamesFont.length; ++i0) {
      out.append(templateNamesFont[i0]);
    }
    return out.toString();
  }

  // =====
}
