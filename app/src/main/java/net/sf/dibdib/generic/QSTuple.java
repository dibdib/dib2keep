// Copyright (C) 2021, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import static net.sf.dibdib.generic.QIfs.NIL_SEQS;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.Arrays;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;

// =====

/** Stack/ tuple/ array of QSeq values, also used as entry for QSto. */
public class QSTuple extends QIfs.QStamped implements Cloneable, QIfs.QSTupleIf {

  // =====

  public static final QSTuple[] NIL_STACKS = new QSTuple[0];

  public final QIfs.QTagIf[] tags;
  public QIfs.QSeqIf[] items;
  public volatile int jStart = 0;
  public volatile int jEnd = 0;
  private int zMinCap = 8;

  public QSTuple(QIfs.QTagIf[] xrTags, int xInitialSize) {
    tags = xrTags;
    items = (0 >= xInitialSize) ? NIL_SEQS : new QIfs.QSeqIf[xInitialSize];
    zMinCap = (xInitialSize < 8) ? 8 : xInitialSize;
  }

  @Override
  public QItemIf getValue(int index) {
    return items[jStart + index];
  }

  /** Return value according to ordinal() of xEnum, or stamp value if null. */
  public QIfs.QSeqIf getValue(QIfs.QEnumIf xEnum) {
    return (null == xEnum)
        ? QWord.createQWordInt(stamp)
        : (((jStart + xEnum.ordinal()) < jEnd) ? items[jStart + xEnum.ordinal()] : null);
  }

  /** Return shash of value/ stamp according to ordinal() of xEnum. */
  @Override
  public long getAsKey(QIfs.QEnumIf xKeyGroup) {
    return (null == xKeyGroup) ? stamp : items[jStart + xKeyGroup.ordinal()].getShash();
  }

  @Override
  public String getAsString(QTagIf xEnum) {
    return (null == xEnum)
        ? ("0#" + BigSxg.sxgChecked64(stamp))
        : xEnum.formatValue(items[jStart + xEnum.ordinal()]);
  }

  public void set(QIfs.QEnumIf xKey, QIfs.QSeqIf xmValue) {
    final int inx = jStart + xKey.ordinal();
    if ((inx < jEnd) || (null != xmValue)) {
      items[inx] = xmValue;
    }
  }

  @Override
  public String toString() {
    final int len = jEnd - jStart;
    if (null == items) {
      return null;
    } else if (0 == len) {
      return "[]";
    } else if (1 == len) {
      return (null != items[jStart]) ? items[jStart].toString() : "(null)";
    } else if (2 == len) {
      return "[" + items[jStart] + ", " + items[jStart + 1] + "]";
    }
    return "[" + items[jStart] + ", " + items[jStart + 1] + ", " + items[jStart + 2] + ", ..]";
  }

  public String toStringFull(char separator, int... delayed) {
    if (jStart >= jEnd) {
      return "";
    }
    final StringBuilder out0 = new StringBuilder((jEnd - jStart) * 10 + 10);
    final StringBuilder out1 = new StringBuilder((jEnd - jStart) * 10 + 10);
    int cSep = 0;
    int iD = 0;
    for (int i0 = jStart; i0 < jEnd; ++i0, ++cSep) {
      StringBuilder out = out0;
      if ((iD < delayed.length) && (delayed[iD] == i0)) {
        ++iD;
        out = out1;
        --cSep;
        out1.append(separator);
      }
      if ((null == items[i0]) || (QSeq.NIL == items[i0])) {
        continue;
      }
      for (; cSep > 0; --cSep) {
        out0.append(separator);
      }
      String dat;
      if ((null == tags) || (i0 >= tags.length)) {
        dat = items[i0].toString();
      } else {
        dat = tags[i0].formatValue(items[i0]);
      }
      if (0 != separator) {
        if (out == out1) {
          dat = dat.replace('\n', separator);
        } else {
          dat = dat.replace('\t', ' ');
        }
        dat = StringFunc.makePrintable(dat);
      }
      out.append(dat);
    }
    if (1 >= out1.length()) {
      return out0.toString();
    }
    for (--cSep; cSep > 0; --cSep) {
      out0.append(separator);
    }
    return out0.toString() + out1.toString();
  }

  /** clone() without checked exception. */
  @Override
  public Object clone() {
    QIfs.QSeqIf[] ix = items;
    final int j0 = jStart;
    final int j1 = jEnd;
    if (null == ix) {
      ix = NIL_SEQS;
    }
    if (j1 <= j0) {
      return new QSTuple(tags, 0);
    }
    final int size = ((j1 - j0) > ix.length) ? ix.length : (j1 - j0);
    QSTuple out = new QSTuple(tags, size);
    System.arraycopy(ix, j0, out.items, 0, size);
    out.jStart = 0;
    out.jEnd = size;
    return out;
  }

  public int size() {
    final int j0 = jStart;
    final int j1 = jEnd;
    if (j0 >= j1) {
      // Potential race condition, but 'good enough' for other threads peeking in:
      return jEnd - jStart;
    }
    return j1 - j0;
  }

  public QIfs.QSeqIf at(int xiFromBottom) {
    int j0 = jStart;
    int j1 = jEnd;
    QSeqIf[] a0 = items;
    if ((j0 >= j1) || (j1 >= a0.length)) {
      // Potential race condition, but second attempt 'good enough' for other threads:
      a0 = items;
      j0 = jStart;
      j1 = jEnd;
    }
    xiFromBottom += (0 > xiFromBottom) ? j1 : j0;
    if ((j0 > xiFromBottom) || (xiFromBottom >= j1) || (xiFromBottom >= a0.length)) {
      return null;
    }
    return a0[xiFromBottom];
  }

  public QIfs.QSeqIf peek(int xiFromTop) {
    int j0 = jStart;
    int j1 = jEnd;
    QSeqIf[] a0 = items;
    if ((j0 >= j1) || (j1 >= a0.length)) {
      // Potential race condition, but second attempt 'good enough' for other threads:
      a0 = items;
      j0 = jStart;
      j1 = jEnd;
    }
    xiFromTop = j1 - 1 - xiFromTop;
    if ((j0 > xiFromTop) || (xiFromTop >= j1) || (xiFromTop >= a0.length)) {
      return null;
    }
    return a0[xiFromTop];
  }

  public QIfs.QSeqIf remove(int xInx) {
    final int inx = xInx + ((0 > xInx) ? jEnd : jStart);
    if ((inx >= jEnd) || (jStart > inx) || (0 > inx)) {
      if ((inx < items.length) && (null != items[inx])) {
        items[inx] = null;
        return null;
      }
      ExceptionAdapter.throwAdapted(
          new ArrayIndexOutOfBoundsException(), getClass(), "inx = " + inx);
    }
    final QIfs.QSeqIf out = items[inx];
    items[inx] = null;
    if ((8 > inx) || ((jEnd >> 3) > inx)) {
      if (0 < (inx - jStart)) {
        System.arraycopy(items, jStart, items, jStart + 1, inx - jStart);
      }
      ++jStart;
      return out;
    }
    --jEnd;
    System.arraycopy(items, inx + 1, items, inx, jEnd - inx);
    if (items.length > zMinCap) {
      final int siz = size();
      if ((items.length > (zMinCap + 16)) && (siz < (items.length - 16 - (items.length >>> 2)))) {
        final int newSize = ((siz >>> 2) + 2) << 2;
        jEnd = siz;
        items = Arrays.copyOfRange(items, jStart, (newSize > zMinCap) ? newSize : zMinCap);
        jStart = 0;
      }
    }
    return out;
  }

  public QIfs.QSeqIf replace(int inx, QIfs.QSeqIf mxValue) {
    inx += (0 > inx) ? jEnd : jStart;
    if ((inx >= jEnd) || (jStart > inx) || (0 > inx)) {
      ExceptionAdapter.throwAdapted(
          new ArrayIndexOutOfBoundsException(), getClass(), "inx = " + inx);
    }
    QIfs.QSeqIf out = items[inx];
    items[inx] = mxValue;
    return out;
  }

  public int insert(int xInx, QIfs.QSeqIf /*OLD*/... values) {
    if (jStart >= jEnd) {
      jStart = jEnd = 0;
    }
    final int inx = xInx + ((0 > xInx) ? jEnd : jStart);
    if ((inx > jEnd) || (jStart > inx) || (0 > inx)) {
      ExceptionAdapter.throwAdapted(
          new ArrayIndexOutOfBoundsException(), getClass(), "inx = " + inx);
    }
    final int len = values.length;
    if ((jEnd + len) > items.length) {
      QIfs.QSeqIf[] itx =
          new QIfs.QSeqIf[(((jEnd - jStart + len + (zMinCap >>> 2)) >>> 2) + 2) << 2];
      System.arraycopy(items, jStart, itx, 0, inx - jStart);
      System.arraycopy(values, 0, itx, inx - jStart, len);
      System.arraycopy(items, inx, itx, inx - jStart + len, jEnd - inx);
      items = itx;
      jEnd = jEnd - jStart + len;
      jStart = 0;
      return inx;
    }
    if (inx < jEnd) {
      System.arraycopy(items, inx, items, inx + len, jEnd - inx);
    }
    jEnd += len;
    System.arraycopy(values, 0, items, inx, len);
    return inx - jStart;
  }

  public int push(QIfs.QSeqIf xmTop) {
    return insert(jEnd - jStart, xmTop);
  }

  public QIfs.QSeqIf pop(boolean deleteWipData) {
    QIfs.QSeqIf out;
    try {
      out = remove(-1);
    } catch (Exception e0) {
      return null;
    }
    if (deleteWipData && (out instanceof QSeq) && (QOpMain.zzWIPCALC == ((QSeq) out).at(0))) {
      final QIfs.QWordIf par1 = ((QSeq) out).at(2);
      if (par1 instanceof QWord) {
        final long wipKey = ((QWord) par1).i64();
        QToken.zWip.remove(wipKey);
        out = ((QSeq) out).at(1);
        Dib2Root.app.error = ExceptionAdapter.WARNING_INTERRUPTED;
      }
    }
    return out;
  }

  // =====
}
