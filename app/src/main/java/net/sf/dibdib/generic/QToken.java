// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.QOpUi;

public class QToken extends QTaskR {

  public static final QScript EMPTY = new QScript();
  public static final QScript ERROR = new QScript();

  public static ConcurrentHashMap<Long, QToken> zWip = new ConcurrentHashMap<Long, QToken>();

  public static final class QScript extends QToken {

    static AtomicInteger zcPoolScript = new AtomicInteger(0);
    static QScript[] zPoolScript = new QScript[64];

    public static QScript makeScriptEl(int no, QOpGraph tag, String parA, long parB) {
      QScript out = makeScript(no);
      out.op = tag;
      out.parN0 = parB;
      out.parS0 = parA;
      return out;
    }

    public static QScript makeScriptEl(int no, QOpGraph tag, String par) {
      QScript out = makeScript(no);
      out.op = tag;
      out.parS0 = par;
      return out;
    }

    public static QScript makeScriptEl(int no, QOpGraph tag, int... pars) {
      QScript out = makeScript(no);
      out.op = tag;
      out.parS0 = null;
      if (null == pars) {
        return out;
      }
      if (0 < pars.length) {
        out.parX = pars[0];
      }
      if (1 < pars.length) {
        out.parY = pars[1];
      }
      return out;
    }

    public static QScript makeScript(int no) {
      while (0 < zcPoolScript.get()) {
        int inx = zcPoolScript.decrementAndGet();
        if ((0 <= inx) && (null != zPoolScript[inx])) {
          zPoolScript[inx].iElement = no;
          return zPoolScript[inx];
        }
      }
      QScript out = new QScript();
      out.iElement = no;
      return out;
    }
  }
  /////

  private QToken() {
    stamp = DateFunc.createId();
  }

  public QToken(QEnumIf xOperator) {
    op = xOperator;
    stamp = DateFunc.createId();
  }

  //  public static QScript create() {
  //    final QScript out = new QScript();
  //    return out;
  //  }

  public static QToken createTask(QEnumIf xOperator, QSeqIf... xmArgs) {
    final QEnumIf op =
        ((xOperator instanceof QOpMain) && (null != ((QOpMain) xOperator).delegated))
            ? ((QOpMain) xOperator).delegated
            : xOperator;
    QToken out = new QToken(op);
    out.argX = (0 < xmArgs.length) ? xmArgs[0] : null;
    out.argY = (1 < xmArgs.length) ? xmArgs[1] : null;
    out.argZ = (2 < xmArgs.length) ? xmArgs[2] : null;
    out.argZ1 = (3 < xmArgs.length) ? xmArgs[3] : null;
    return out;
  }

  public static QToken createTask4UiEvent(char key) {
    QToken out = new QToken(QOpUi.zzKEY); // QOpFeed.zzCHECK4UI);
    out.argX = null;
    out.argY = null;
    out.argZ = null;
    out.argZ1 = null;
    out.parX = key;
    return out;
  }

  @Override
  public QItemIf getValue(int xIndex) {
    return (0 == xIndex) ? op : null;
  }

  @Override
  public long getAsKey(QEnumIf xEnum) {
    return (null == xEnum) ? stamp : 0;
  }

  @Override
  public String getAsString(QTagIf xEnum) {
    return (null == xEnum) ? op.name() : null;
  }

  public void reset4Recycle() {
    stamp = 0;
  }

  public long pushWip(long xmId4Result, Object xmoData) {
    wip = xmoData;
    parN0 = xmId4Result;
    zWip.put(stamp, this);
    return stamp;
  }

  public QSeq pushWip4Seq(Object xmoData) {
    wip = xmoData;
    final QWord id = QWord.createQWordInt(stamp);
    final QSeq sq = QSeq.createQSeq(QOpMain.zzWIPCALC, op, id);
    parN0 = sq.getShash();
    zWip.put(stamp, this);
    return sq;
  }

  public QScript recycleMe() {
    // Not too many ...
    if ((0 <= cScript) && (2048 > QScript.zcPoolScript.get())) {
      cScript = -1;
      iElement = -1;
      int inx = QScript.zcPoolScript.getAndIncrement();
      if (0 <= inx) {
        if (inx >= QScript.zPoolScript.length) {
          QScript.zPoolScript = Arrays.copyOf(QScript.zPoolScript, 2 * QScript.zPoolScript.length);
        }
        QScript.zPoolScript[inx] = (QScript) this;
        if (null != script) {
          for (int i0 = 0; i0 < script.length; ++i0) {
            if (null == script[i0]) {
              break;
            }
            ((QScript) script[i0]).recycleMe();
          }
        }
      }
    }
    script = null;
    return null;
  }

  @Override
  public long getShash() {
    return stamp;
  }

  @Override
  public void setShashOrIgnore(long shash) {
    this.stamp = shash;
  }

  public void shiftArgs() {
    argX = argY;
    argY = argZ;
    argZ = argZ1;
  }

  @Override
  public String toString() {
    if (null == argX) {
      return (0 <= cScript)
          ? ("*[" + cScript + "]")
          : ""
              + ((0 == parX) ? "" : "X=" + parX)
              + ((0 == parY) ? "" : " Y=" + parY)
              + ((0 == parN0) ? "" : " N0=" + parN0)
              + ((null == parS0) ? "" : " S0={" + parS0 + '}')
              + " "
              + op.name();
    }
    return ""
        + argX.toString()
        + ((null == argY) ? "" : (" " + argY.toString()))
        + ((null == argZ) ? "" : (" " + argZ.toString()))
        + " "
        + op.name(); // + '@' + getOpIndex();
  }
}
