// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;

public final class QIfs {

  /*
  (\n\t ):{name}(QSeq / \n ... \n)
  \n...\n...\n = \n:{0}...\n:{1}...\n:{2}, goes on stack
  :{:name} as ref, :{$name}QSeq as call, :"..." "..." as condensed stack values
  \n:<op>+ on stack and variables
  <p> print on current and succeeding page, with footnotes {FOOT...}
  <f\f> end page shifting page values
  \n:[
  */

  // Dibdib document DD:

  // DD => DDH ( [\n] ( QQuote / QuotedScript / Quote ) )*
  // QRefDef => ( [\n] ":{" QKey "*"? "}"  ;"*" as wildcard for sequence number

  // DDH => ":{DD" QDate "}" QPkg " " QVersion ( " " QWord )*
  // QPkg, QThread = QTag
  // QVersion => "v" [0-9\.]+
  // Quote =>  ":{}" ( [\n] ( ":{}" CKey* [\n] )? Text )*   ;quoted text
  // QuotedScript => QRefDef QScript  ;quoted
  // QQuote => ( QRefDef ( QContext / QStack ) ) / ( [\n] QRefDef? Text )  ;quasi-quoted text for QTree
  // QContext = QSeq  ;ctx definiton/ application: VAR, PREF, STATE, ...

  // QTree (post-order traversal), QList => "[" QListElement? ( [ \t\n]+ QListElement )*  "]"
  // QListElement => QSeq / QStoEntry / QScript / QTree
  // QScript => QQ ( ( QScriptElement / PartialTask )? ( [ \t\n]+ QScriptElement )* ) / QSeq / QStack EXEC
  // QScriptElement => QTask / QOp / QScript
  // Record = QList  ;with implied QSeq/QEnum tags
  // Struc = QList  ;pairwise, for QRecord with explicit tags
  // Map/ MMap (multivalued) ~ QList  ;pairwise, for relation, but implemented more efficiently
  // QMMap (simplified MMap) ~ QTree of QStoEntry  ;sorted by one of the QStoEntry elements
  // Store (set of QMMap) => "<" QStoEntry ( [ \t\n]+ QStoEntry)* ">"  ;container for efficient handling

  // QStoEntry (with implied QEnum tags) => QSeq / ( "<" QSeq ( [ \t\n]+ QSeq)* ">" )  ;sequence of QSeq
  // QSTuple( QTuple (immutable), QStack (LIFO), QArray) = QStoEntry
  // QTask (immutable), QToken (mutable)
  //     => QOp-0 / ( QSeq [ \t\n]+ QOp-1 ) / ( QSeq [ \t\n]+ QSeq [ \t\n]+ QOp-2 ) / ...
  //        / ( ( QSeq [ \t\n]+ )* QOp-X )
  // QSeq (immutable) => QSememe / ( '"' QSememe ( QSep QSememe )+ '"' )  ;similar to 'String'

  // QSememe => QWord ( "__" QWord )*  ( '{' QLiteral '}' )? ;allows tuples of words as unit, possibly quoting original text
  // QWord => ( QOp / QDate / QNumeric / QLiteral )
  // QOp => QKey / QOpSym / QQ / EXEC
  // QTag: '.'? ( '/' QKey )+
  // QSep => ( CSep / COpSym )* CSep+ ( '{' QLiteral '}' )?
  //       / ( QSep* EscapedCtrl+ QSep* )
  // QKey => [A-Z][A-Z0-9_]*
  // QOpSym => COpSym+
  // QQ = "{" ;= "QQ" as operator
  // EXEC = "}" ;= "EXEC" as operator
  // QDate => ;applies syntactically also for version numbering
  //          [0-9] [0-9]+ '-'   [0-9]+ '-' [0-9\-\.\:T]+ [0-9] (...)?
  //        / [0-9]+       [/\-] [0-9]+ '/' [0-9\-\.\:]+  [0-9]
  //        / [0-9]+       '.'   [0-9]+ '.' [0-9\-\.\:]+  [0-9]
  // QNumeric => [0-9]... / Dozenal / Hex / Sexagesimal
  // QLiteral => ( [^\CKey\CSep\CSugar]* [a-z]~ [^\CKey\CSep\CSugar]* )
  //           / ( "'" ( Escaped* [^\CKey] Escaped? )* "'" )
  //           / "X'" [0-9A-Fa-f ]* "'"
  //           / "Y'" [0-9A-NP-Za-y]* "'"
  // Dozenal => "0d" ...
  // Hex => "0x" ...
  // Sexagesimal => "0z" ... / "0#" ...  ;without or with check digit
  // Escaped => EscapedCtrl / EscapedChar
  // EscapedCtrl => "\" [@-Z \`ftnr'"]
  // EscapedChar => "\" [\\ '"]

  // CKey => "\" / '"' / "'" / '<' / '>' / '[' / ']' / '{' / '}' / [\n\t] / [\x00..\x1f]
  // COpSym = [+\-\*%/...]
  // CSep => [ \n\t] / UnicodeSepMinus
  // CSugar => '@' / '$' / '?' / '#' / ';' / ...

  // Note: 'immutable' = WO = quasi-immutable, i.e. not to be changed after final step of init,
  //       mutable = RW, RO = quasi-readonly, i.e. being changed by owner, not outside.

  /////

  public static interface QItemIf {

    public long getShash();

    /** Implementable via Enum. */
    @Override
    public int hashCode();

    /** Implementable via Enum. */
    @Override
    public boolean equals(Object obj);
  }

  public static interface QMutableIf extends QItemIf {

    public void setShashOrIgnore(long shash);
  }

  /** Sequence of items/ words is an item. */
  public static interface QSeqIf extends QItemIf {}

  /** For special case of a sequence with only one word. */
  public static interface QWordIf extends QSeqIf {}

  /** Implementable via Enum. */
  public static interface QEnumIf extends QWordIf {

    /** Implementable via Enum. */
    public String name();

    /** Implementable via Enum. */
    public int ordinal();
  }

  public static interface QTagIf extends QEnumIf {
    /** Format a value according to its associated tag. */
    public String formatValue(QItemIf xValue);
  }

  public static interface QStackIf extends QSeqIf, QMutableIf {}

  public static interface QTupleIf extends QSeqIf {
    public QItemIf getValue(int xIndex);

    public long getAsKey(QEnumIf xEnum);

    public String getAsString(QTagIf xEnum);
  }

  public static interface QSTupleIf extends QStackIf, QTupleIf {}

  // public static interface QKeyIf extends QWordIf {}

  public static final QItemIf[] NIL_ITEMS = new QItemIf[0];
  public static final QSeqIf[] NIL_SEQS = new QSeqIf[0];
  public static final QWordIf[] NIL_WORDS = new QWordIf[0];
  public static final QEnumIf[] NIL_ENUMS = new QEnumIf[0];

  public static class QStamped implements QMutableIf {

    public long stamp;

    public void init() {
      stamp = DateFunc.createId();
    }

    @Override
    public long getShash() {
      return stamp;
    }

    @Override
    public void setShashOrIgnore(long stamp) {
      this.stamp = stamp;
    }
  }

  public static final class QSeqInt extends QStamped implements QSeqIf, Cloneable {

    private static final int[] zNil = new int[0];

    public int[] items = zNil;

    private QSeqInt() {}

    public QSeqInt(int size) {
      items = (0 >= size) ? zNil : new int[size];
    }

    @Override
    public QSeqInt clone() {
      QSeqInt out = new QSeqInt();
      out.stamp = stamp;
      out.items = items.clone();
      return out;
    }
  }

  public static interface QTagged extends QEnumIf {

    public long i64(long tick, Object... xOptInstance);

    public int i32(long tick, Object... xOptInstance);

    public double d4(long tick, Object... xOptInstance);

    public String strFull(long tick, Object... xOptInstance);
  }

  public static interface QComponentTagged extends QMutableIf {}

  public static final class QComponent implements QMutableIf {

    public QStamped initial;
    public volatile QStamped old;
    public volatile QStamped current;
    public QStamped future;

    public QComponent(QStamped xmaFields) {
      initial = xmaFields;
      future = current = old = initial;
    }

    @Override
    public long getShash() {
      return current.stamp;
    }

    @Override
    public void setShashOrIgnore(long shash) {
      if (future != current) {
        future.stamp = shash;
      }
      // Atomic:
      current = future;
    }

    public void tick(long xNanobis) {
      setShashOrIgnore(xNanobis);
    }

    public QStamped get(long tick) {
      // Atomic:
      final QStamped cur = current;
      if (cur == old) {
        return cur;
      }
      if (cur.stamp <= Dib2Root.app.qTickMin) {
        old = cur;
        // Race condition?
        while (old != current) {
          old = current;
        }
        return cur;
      }
      return (tick >= cur.stamp) ? cur : old;
    }
  }

  public static final QComponent[] NIL_COMPONENTS = new QComponent[0];

  // : *QVal: long -> QSeq, *QSeq, *QWord

  public static class QVal implements QSeqIf {
    public long shash;

    @Override
    public long getShash() {
      return shash;
    }

    public int compareTo(QItemIf other) {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  public static final QVal[] NIL_VALS = new QVal[0];

  public static class QSeqR extends QVal {

    /** (Partial) string value. */
    protected String zString = null;

    /** null iff instanceof QWord. */
    protected QWordIf[] mWords = NIL_WORDS;

    protected int jStart = 0;
    protected int jEnd = 0;
  }

  public static final class QSeqVals extends QStamped implements QSeqIf, Cloneable {

    public QVal[] seq = NIL_VALS;

    public QSeqVals clone() {
      QSeqVals out = new QSeqVals();
      out.stamp = stamp;
      out.seq = seq.clone();
      return out;
    }
  }

  /** Contains data for both QTask and QScript. */
  public abstract static class QTaskR extends QStamped implements QSTupleIf {

    public QEnumIf op = null;
    public QSeqIf argX = null;
    public QSeqIf argY = null;
    public QSeqIf argZ = null;
    public QSeqIf argZ1 = null;

    ///// Optimized ...

    public QTaskR[] script = null;
    public int iElement = -1;
    public int cScript = -1;
    // E.g. X in graphics, key ...:
    public int parX = 0;
    // E.g. count for WIP:
    public int parY = 0;
    public long parN0 = 0;
    public String parS0 = null;

    /** Transitional data for work in progress, feedback data. */
    public Object wip;
  }

  public static final QTaskR[] NIL_TASKS = new QTaskR[0];

  public static interface QRunnableIf extends Callable<QToken> {
    // =====

    /** Check whether token/ task can be handled. */
    // @return true iff applicable.
    boolean guard(QToken xTask, long... xParameters);

    /** Initial step: accept task, remember starting time, advance time stamps (shared objects). */
    // Advance state and time stamp of owned components of shared objects (c.current = c.future).
    // @return Estimation of processing steps, or 0 iff task not accepted.
    int start(long xTick, QToken xmTask);

    /** (Short) processing steps of possibly long process, synchronized. */
    // Processing depends on current time as indicated and on time stamp of original task.
    // @return Estimation of steps remaining (plus SYNC token)
    int step(long xTick);

    /** Additional or initial processing steps for 'async', not synchronized. */
    // @return Estimation of steps remaining, 1 for sync, -1 in case of error.
    int stepAsync();

    void removeWipData4Interrupts();

    /** Final step, just for returning the result. */
    // @return Result.
    @Override
    QToken call();

    // =====
  }

  public static class QObjectR extends QStamped {
    public QComponent[] components;

    public QObjectR(int size) {
      components = new QComponent[size];
    }

    public QObjectR(QComponent... xmComponents) {
      components = xmComponents;
    }

    public QStamped get(int inx, long tick) {
      return components[inx].get(tick);
    }
  }

  public static class QObject extends QObjectR {

    private QObject(int size) {
      super(size);
    }

    public static QObject create(int size) {
      QObject out = new QObject(size);
      out.stamp = DateFunc.createId();
      //    out.owner = xmOwner;
      //    out.components = new QIfs.QComponent[size];
      return out;
    }

    @Override
    public void setShashOrIgnore(long shash) {
      stamp = shash;
    }

    @Override
    public long getShash() {
      return stamp;
    }
  }

  /////

  public static interface GraphicsIf {

    public void setCanvasImage(
        int index, int width, int height, int colorBackground, int colorBorder);

    public void drawImage(int index, int left, int top);

    /** Assuming thickness of ~1/2pt. */
    public void drawLine(int startX, int startY, int stopX, int stopY);

    public void drawText(String text, int x, int y);

    public int getBoundsLeft(String txt);

    public int getBoundsRight(String txt);

    public int getBoundsMiddle(String txt);

    //  public void setBackground(int xColor);

    public void setClip(int left, int top, int right, int bottom);

    public void setColor(int xColor);

    public void setColorText(int xColor);

    public void setColorTool(int xColor);

    public int setMatching128Font(int xHeightPx, String font, int type);

    public int setMatching128FontHeight(int xHeightPx);

    public void show();
  }

  /////

  public static interface TsvCodecIf {

    byte[] create(char platform, Object... parameters);

    byte[] compress(byte[] xyData, int xOffset4Reuse, int to);

    byte[] decompress(byte[] xData, int length);

    /** To take advantage of background processing ... */
    // @param phrase Pass phrase.
    // @param headerInfo Null for creating key with xyIvData.
    // @param xyIvData Null for preparing a new key.
    // @return Key bytes.
    //
    // byte[] prepareKey( byte[] phrase, Object headerInfo, byte[] xyIvData );

    byte[] getKey(byte[] phrase, byte[] accessCode, byte[] salt, int iterations);

    String getKeyInfo(HashSet<String> entries);

    byte[] encodePhrase(byte[] phrase, byte[] accessCode);

    byte[] decodePhrase(byte[] encoded, byte[] accessCode);

    /** Header bytes: 0..1 magic bytes, 2 cipher/format, 3 encoder, 4.. counts etc. */
    byte[] encode(
        byte[] compressedData,
        int from,
        int to,
        byte[] key,
        byte[] iv16,
        int headerTagsOrKeyInfo,
        byte[] header,
        byte[] signatureKey)
        throws Exception;

    // Header bytes: 0..1 magic bytes, 2 version/format, 3 encoder, 4.. counts etc. */
    // byte[] encode4DerivedKey( byte[] compressedData, int from, int to, byte[] pass ) throws
    // Exception;

    byte[] decode(byte[] data, int from, int to, byte[] key, byte[] signatureKey) throws Exception;

    byte[] getInitialValue(int len);

    byte getMethodTag();

    // StringBuffer getLog();

  }

  public static interface PlatformIf {

    void log(String... aMsg);

    void invalidate();

    /**
     * Proper directory depending on platform.
     *
     * @param parameters "main" or "external" (=download) or "safe".
     * @return null if nothing suitable found (e.g. for "safe").
     */
    File getFilesDir(String... parameters);

    String[] getLicense(String[] xAdditionalVersionInfo, String... resources);

    boolean pushClipboard(String label, String text);

    String getClipboardText();

    void toast(String msg);

    Object parseUri(String path);

    InputStream openInputStreamGetType(Object xUri, String[] yType);
  }

  public static interface TriggerIf {

    /** Trigger independent task. */
    boolean triggerExt(QIfs.QRunnableIf prcs, QToken task);

    /** Prepare new thread if needed, then run the scheduler. Cmp. MainThreads. */
    boolean trigger(QToken xOptionalCmd);
  }

  /////

  public static long getShashPartial(long stamp) {
    if (0 == (stamp & 1)) {
      return stamp;
    } else if (0x1 == (stamp & 3)) {
      return stamp & ~0x3ffL;
    } else if (0x3 == (stamp & 7)) {
      return stamp & ~((1L << 21) - 1);
    }
    return stamp & ~((1L << 32) - 1);
  }

  public static int compareShashPartial(long stamp0, long stamp1) {
    if ((0x7 == (stamp0 & 0xf)) || (0x7 == (stamp1 & 0xf))) {
      stamp0 = stamp0 & ~((1L << 32) - 1);
      stamp1 = stamp1 & ~((1L << 32) - 1);
    } else if ((0x3 == (stamp0 & 7)) || (0x3 == (stamp1 & 7))) {
      stamp0 = stamp0 & ~((1L << 21) - 1);
      stamp1 = stamp1 & ~((1L << 21) - 1);
    } else if ((0x1 == (stamp0 & 3)) || (0x1 == (stamp1 & 3))) {
      stamp0 = stamp0 & ~0x3ffL;
      stamp1 = stamp1 & ~0x3ffL;
    }
    return (stamp0 == stamp1) ? 0 : ((stamp0 < stamp1) ? -1 : 1);
  }

  public static int getStampIndex(long stamp) {
    if (0 == (stamp & 1)) {
      return -1;
    } else if (0x1 == (stamp & 3)) {
      return (int) ((stamp >>> 2) & ((1L << 8) - 1));
    } else if (0x3 == (stamp & 7)) {
      return (int) ((stamp >>> 3) & ((1L << 18) - 1));
    }
    return (int) ((stamp >>> 4) & ((1L << 28) - 1));
  }

  // index 0 is not fixed
  public static long encodeIndex(long shashPartial, int index) {
    if ((1 << 8) > index) {
      return (index << 2) | 0x1 | (shashPartial & ~(((1L << 10) - 1)));
    } else if ((1 << 18) > index) {
      return (index << 3) | 0x3 | (shashPartial & ~(((1L << 21) - 1)));
    }
    return ((long) index << 4) | 0x7 | (shashPartial & ~(((1L << 32) - 1)));
  }
}
