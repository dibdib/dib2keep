// Copyright (C) 2019, 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.nio.charset.Charset;
import java.util.Locale;
import net.sf.dibdib.thread_any.*;

/** Serialization functions. */
public final class SerFunc {
  // =====

  ///// Message Pack

  // Encoding with reversed TLV/TCV, cmp. https://github.com/msgpack/msgpack
  // Values are big-endian !
  /** NUM (0,1,2..127): positive fixint 7, 0x00 - 0x7f */
  public static final short TAG_NUM0_POS = 0;
  /** MAP (0,2,4..30 elements): fixmap C4*2, 0x80 - 0x8f */
  public static final short TAG_MAP_C0 = 0x80;
  /** ARR (0,1,2..15 elements): fixarray C4, 0x90 - 0x9f */
  public static final short TAG_ARR_C0 = 0x90;
  /** STR (len<32): fixbytes L5, 0xa0 - 0xbf */
  public static final short TAG_BYTES_L0 = 0xa0;
  /** nil, 0xc0 */
  public static final byte TAG_nil = (byte) 0xc0;
  /** ARR2SET/INT2FALSE (indicator): false, 0xc2 */
  public static final byte TAG_ARR2SET_INT2FALSE = (byte) 0xc2;
  /** ARR2TRIPLES/INT2TRUE (indicator): true, 0xc3 */
  public static final byte TAG_ARR2TRIPLES_INT2TRUE = (byte) 0xc3;
  /** BIGINT (len<=0xff): bin L8, 0xc4 + L */
  public static final byte TAG_BIGINT_L1 = (byte) 0xc4;
  /** BIGFLOAT (len<=0xffff): bin L16, 0xc5 + L1 + L0 */
  public static final byte TAG_BIGFLOAT_L2 = (byte) 0xc5;
  /** NUM (float): float 64, 0xcb */
  public static final byte TAG_NUM64 = (byte) 0xcb;
  /** MSEC (timeStamp): uint 64, 0xcf */
  public static final byte TAG_MSEC = (byte) 0xcf;
  /** LMIN (leapMinutes): int 64, 0xd3 */
  public static final byte TAG_LEAPMIN = (byte) 0xd3;
  /** STR (len<=0xff): bytes L8, 0xd9 + L */
  public static final byte TAG_BYTES_L1 = (byte) 0xd9;
  /** STR (len<=0xffff): bytes L16, 0xda + L1 + L0 */
  public static final byte TAG_BYTES_L2 = (byte) 0xda;
  /** STR (len>0xffff): bytes L32, 0xdb + L3 + L2 + L1 + L0 */
  public static final byte TAG_BYTES_L4 = (byte) 0xdb;
  /** ARR (count<=0xffff): array L16, 0xdc .. */
  public static final byte TAG_ARR_C2 = (byte) 0xdc;
  /** ARR (count>0xffff): array L32, 0xdd .. */
  public static final byte TAG_ARR_C4 = (byte) 0xdd;
  /** MAP (count<=0xffff): map L16, 0xde .. */
  public static final byte TAG_MAP_C2 = (byte) 0xde;
  /** MAP (count>0xffff): map L32, 0xdf .. */
  public static final byte TAG_MAP_C4 = (byte) 0xdf;
  /** NUM (-32..-1): negative fixint 5, 0xe0 - 0xff */
  public static final short TAG_NUM0_NEG = 0xe0;

  ///// Netstring

  public static final byte NETSTRING_SEP_LEN = ':';
  public static final byte NETSTRING_TERM = ',';

  ///// Netstream

  public static final byte NETSTREAM_SEP_BLOCKLEN = ';';
  public static final byte NETSTREAM_BLOCK_TERM = '.';

  ///// Message Pack

  public static long getTcvOffsetLength(byte[] data, int start, int iTag, int count) {
    int cC = 0;
    int cV = 0;
    for (; count > 0; --count, --iTag) {
      if (iTag < start) {
        return -1;
      }
      cC = 0;
      cV = 0;
      if (TAG_NUM0_NEG < data[iTag]) {
        continue;
      } else if (0xc0 > (data[iTag] & 0xff)) {
        final int t0 = data[iTag] & 0xff;
        if (TAG_BYTES_L0 <= t0) {
          cV = (t0 & 0x1f);
          iTag -= cV;
        } // else // TODO
        continue;
      }
      switch (data[iTag]) {
        case TAG_nil:
          break;
        case TAG_BYTES_L1:
          cC = 1;
          break;
        case TAG_BYTES_L2:
          cC = 2;
          break;
        case TAG_BYTES_L4:
          cC = 4;
          break;
          // case ... TODO
        default:
          ;
      }
      if (iTag < cC) {
        return -1;
      }
      if (0 < cC) {
        cV = data[iTag - 1] & 0xff;
      }
      if (1 < cC) {
        cV = (cV << 8) | (data[iTag - 2] & 0xff);
      }
      if (2 < cC) {
        cV = (cV << 16) | ((data[iTag - 3] & 0xff) << 8) | (data[iTag - 4] & 0xff);
      }
      iTag -= cV + cC;
    }
    return ((cV & -1L) << 32) | ((iTag + 1) & -1L);
  }
  
  ///// Netstring/ Netstream
  
  public static int encodeNetstreamLen(byte[] xyBuf, int xOffset, int xLen, long bNetstreamFinalblock) {
    byte[] len = StringFunc.bytesAnsi("" + xLen);
    System.arraycopy(len, 0, xyBuf, xOffset, len.length);
    xyBuf[xOffset + len.length] = (0 == (1L & bNetstreamFinalblock)) ? NETSTRING_SEP_LEN : NETSTREAM_SEP_BLOCKLEN;
    return xOffset + len.length + 1;
  }
  
  // =====
}
