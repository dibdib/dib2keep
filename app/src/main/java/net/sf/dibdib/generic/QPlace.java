// Copyright (C) 2018, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;

/** For token flow/ relay model, based on Petri net (Arc weight = 1)
 * with typed/ guarded Places for data tokens.
 * (Places impose guards on their preceding Transitions).
 * Elements of net ('Context'):
 * - (Q)Process (synchronized split): incoming (Q)Places with single Arcs + single Transition.
 * - (Q)Dispatcher (merged choice): single (unguarded) incoming Place + identity Transitions.
 * - (Q)Store (merged source): single guarded Place + copy Transitions with returning Arc.
 * - Plus (Q)Terminators (source, sink) for workflow: used for gateways/ bridges/ external transitions.
 */
public class QPlace {
  // =====

  public static boolean qExitRequested = false;

  private QToken[] mTokens = new QToken[4];
  private volatile int iInToken = 0;
  private volatile int iOutToken = 0;

  public QToken peek() {
    return ((iInToken == iOutToken) && (null == mTokens[iOutToken])) ? null : mTokens[iOutToken];
  }

  /**
   * For guarded transitions, to be overridden.
   *
   * @param xToken Instance of (sub-) class.
   * @return true if the Token is acceptable.
   */
  public boolean matches(QToken xToken) {
    return true;
  }

  /** To be run on sender's thread. */
  public int push(QToken xmToken) {
    if (null == xmToken) {
      return iInToken;
    }
    if ((iInToken == iOutToken) && (16 < mTokens.length) && (iInToken < mTokens.length / 2)) {
      mTokens = Arrays.copyOf(mTokens, mTokens.length / 2);
    }
    int iIn = (iInToken + 1) % mTokens.length;
    int old = iInToken;
    if (iIn == iOutToken) {
      // Temporarily block the other outgoing data.
      iInToken = iIn;
      if (iOutToken <= iInToken) {
        ///// Ok, it is blocked.
        iIn = mTokens.length;
        QToken[] ox = Arrays.copyOf(mTokens, mTokens.length * 2);
        ox[iIn] = xmToken;
        mTokens = ox;
        iInToken = old;
      } else {
        ///// The other thread was faster.
        mTokens[old] = xmToken;
      }
    } else {
      // This one first, explicitly using the volatile variable.
      mTokens[iInToken] = xmToken;
      iInToken = iIn;
    }
    return old;
  }

  /** To be run on dispatcher's/ receiver proc's thread. */
  public QToken pull() {
    int iIn = iInToken;
    QToken[] old = mTokens;
    if (iIn == iOutToken) {
      return null;
    }
    if (null == old[iOutToken]) {
      iOutToken = 0;
    }
    if (qExitRequested) {
      for (;
          (iOutToken < iIn)
              && ((null == old[iOutToken])
                  || (null == old[iOutToken].op)
                  || !old[iOutToken].op.name().contains("zzEXIT"));
          ++iOutToken) {
        mTokens[iOutToken] = null;
      }
    }
    for (; (iOutToken < iIn) && (null == old[iOutToken]); ++iOutToken) {}
    if (iOutToken == iIn) {
      return null;
    }
    QToken out = old[iOutToken];
    mTokens[iOutToken] = null;
    iOutToken = (iOutToken + 1) % mTokens.length;
    return out;
  }

  /** To be run on dispatcher's/ receiver proc's thread. */
  public void flush(boolean resize) {
    iOutToken = iInToken;
    for (int i0 = mTokens.length - 1; i0 >= 0; --i0) {
      mTokens[i0] = null;
      if (resize && (i0 < iOutToken)) {
        iOutToken = i0;
        iInToken = i0;
        final boolean isBlocked = (iOutToken <= iInToken);
        if (isBlocked && ((i0 * 2) < mTokens.length) && (4 < mTokens.length)) {
          mTokens = Arrays.copyOf(mTokens, mTokens.length / 2);
        }
      }
    }
  }

  // =====
}
