// Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import com.gitlab.dibdib.picked.common.ColorOklab;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/** Compromising 'better' color space (~450/ ~540/ ~650+) with real life values. */
// Cmp. (sashat.me/2017/01/11/list-of-20-simple-distinct-colors)
// https://sashamaps.net/docs/resources/20-colors,
// https://www.w3schools.com/colors,
// https://matplotlib.org/stable/tutorials/colors/colors.html,
// https://simple.wikipedia.org/wiki/List_of_colors,
// https://colors.artyclick.com/color-name-finder,
// https://clarkvision.com/articles.
public enum ColorNmz implements QIfs.QEnumIf {

  /* 000 00 000 */ BLACK(0x000000),
  /* 000 00 033 */ ONYX_GRAY(0x0a0a0a),
  /* 000 00 050 */ RAISIN_GRAY(0x202020),
  /* 000 00 055 */ MIDNIGHT_GRAY(0x2b2b2b),
  /* 000 00 065 */ GRAPHITE_GRAY(0x474747),
  /* 000 00 071 */ BASALT_GRAY(0x5c5c5c),
  /* 000 00 073 */ SLATE_GRAY(0x656565),
  /* 000 00 077 */ BRIDGE_GRAY(0x777777),
  /* 000 00 079 */ PURE_GRAY(0x808080),
  /* 000 00 079 */ GRANITE_GRAY(0x7e7e7e),
  /* 000 00 080 */ BATTLESHIP(0x848484),
  /* 000 00 084 */ PIPELINE_GRAY(0x999999),
  /* 000 00 086 */ QUICKSILVER(0xa6a6a6),
  /* 000 00 087 */ LIGHTBOX_GRAY(0xadadad),
  /* 000 00 087 */ GRAY20(0xa9a9a9),
  /* 000 00 089 */ ASH_GRAY(0xb9b9b9),
  /* 000 00 090 */ SILVER_SAND(0xc0c0c0),
  /* 000 00 092 */ SILVER(0xc7c7c7),
  /* 000 00 092 */ PASTEL_GRAY(0xcccccc),
  /* 000 00 095 */ GAINSBORO(0xdcdcdc),
  /* 000 00 096 */ PLATINUM(0xe4e4e4),
  /* 000 00 098 */ WHITESMOKE(0xf5f5f5),
  /* 000 00 099 */ WHITE(0xffffff),

  CLARET__XKCD(0x680018),
  ORANGE__XKCD(0xf97306),
  EMERALD__XKCD(0x01a049),
  TEAL__XKCD(0x029386),
  MAUVE__XKCD(0xae7181), // e0b0ff

  /* 000 23 086 */ ROSA(0xfe86a4),
  /* 000 59 060 */ SHIRAZ(0xaa0d33),
  /* 000 60 068 */ RED20(0xe6194b),
  /* 000 74 060 */ CHERRY(0xcf0234),
  /* 000 81 064 */ TORCH(0xff0040),
  /* 002 27 061 */ WINE(0x702838),
  /* 002 75 066 */ VIVID_CHERRY(0xff0840),
  /* 003 05 096 */ PINKLACE(0xffdde4),
  /* 004 50 071 */ AMARANTH(0xe52b50),
  /* 006 26 084 */ CARNATION(0xfd798f),
  /* 009 16 087 */ ROSE_PINK(0xe8919c),
  /* 010 34 044 */ AUBERGINE(0x3b0910),
  /* 010 57 052 */ FIREBRICK(0x800517),
  /* 012 65 054 */ CARMINE(0x9d0216),
  /* 012 70 058 */ SCARLET__XKCD(0xbe0119),
  /* 015 06 091 */ SUNSET_PINK(0xe3bbbd),
  /* 015 23 060 */ CLARET(0x67292d),
  /* 015 57 046 */ ROSEWOOD(0x65000b),
  /* 016 29 076 */ LOLLIPOP(0xcc5058),
  /* 019 17 063 */ RED_OXIDE(0x663334),
  /* 019 39 064 */ CRIMSON(0x9e2429),
  /* 020 09 093 */ PINK20(0xfabebe),
  /* 020 28 064 */ RASPBERRY(0x852f31),
  /* 020 42 065 */ WARATAH(0xaa2429),
  /* 021 22 067 */ LOTUS(0x863c3c),
  /* 021 68 065 */ FERRARI(0xf70d1a),
  /* 023 41 066 */ STRAWBERRY(0xb4292a),
  /* 023 41 070 */ PERSIAN_RED(0xcc3333),
  /* 025 04 091 */ DUSTY_PINK(0xdbbebc),
  /* 028 01 099 */ ROSEWHITE(0xfff6f5),
  /* 028 34 079 */ CORAL(0xfc5a50),
  /* 028 39 068 */ SIGNAL_RED(0xba312b),
  /* 030 26 084 */ SALMON(0xff796c),
  /* 031 61 048 */ BLOOD(0x770001),
  /* 032 33 026 */ DIESEL(0x130000),
  /* 032 41 072 */ HOMEBUSH_RED(0xd83a2d),
  /* 032 63 049 */ MAROON__X11(0x800000),
  /* 032 63 049 */ MAROON20(0x800000),
  /* 032 65 051 */ REDBERRY(0x8e0000),
  /* 032 77 060 */ RED(0xe50000),
  /* 032 79 062 */ PURE_RED(0xff0000),
  /* 034 39 071 */ SCARLET(0xcd392a),
  /* 034 51 042 */ MAHOGANY(0x4a0100),
  /* 035 04 096 */ SALMON_PALE(0xffe1db),
  /* 035 21 069 */ RED_GUM(0x8d4338),
  /* 035 27 081 */ TANGERINE(0xe96957),
  /* 036 13 087 */ BURNT_PINK(0xe19b8e),
  /* 036 15 088 */ BLUSH(0xf29e8e),
  /* 039 21 065 */ VENETIAN_RED(0x77372b),
  /* 039 59 060 */ MILANO(0xb81104),
  /* 040 43 074 */ TOMATO(0xef4026),
  /* 041 05 082 */ POSSUM(0xa18881),
  /* 042 24 079 */ RED_DUST(0xd0674f),
  /* 045 24 071 */ TERRACOTTA(0xa04c36),
  /* 051 02 095 */ GHOST_GUM(0xe8dad4),
  /* 051 34 045 */ BEAN(0x3d0c02),
  /* 054 14 086 */ SALMON_PINK(0xd99679),
  /* 054 37 076 */ MANDARINE(0xe45427),
  /* 057 03 096 */ APPLE_BLOSSOM(0xf2e1d8),
  /* 058 29 040 */ COFFEE_BEAN(0x290a01),
  /* 060 27 068 */ CUMIN(0x924321),
  /* 063 55 074 */ VERMILLION(0xff4000),
  /* 064 31 084 */ MANGO(0xff8040),
  /* 067 27 085 */ ROCKMELON(0xf6894b),
  /* 067 36 049 */ CHOCOLATE_RED(0x481400),
  /* 068 18 090 */ PEACH(0xffb07c),
  /* 068 34 080 */ ORANGE__A(0xe36c2b),
  /* 069 20 076 */ EARTH(0xa2653e),
  /* 070 45 065 */ AUBURN(0x9a3001),
  /* 073 20 046 */ ZINNWALDITE(0x2c1608),
  /* 073 31 084 */ ORANGE_DUSTY(0xf0833a),
  /* 073 39 060 */ CHESTNUT(0x742802),
  /* 074 21 085 */ PERSIAN_ORANGE(0xd99058),
  /* 075 31 073 */ SIENNA(0xa9561e),
  /* 076 07 095 */ SHELL_PINK(0xf9d9bb),
  /* 076 34 084 */ ORANGE20(0xf58231),
  /* 077 04 067 */ MUDSTONE(0x574e45),
  /* 077 30 076 */ COPPER(0xb66325),
  /* 078 10 095 */ PEACH_LIGHT(0xffd8b1),
  /* 078 10 095 */ APRICOT20(0xffd8b1),
  /* 079 14 078 */ MOCHA(0x9d7651),
  /* 079 22 050 */ CLINKER(0x371d09),
  /* 080 02 084 */ HOMEBUSH_GRAY(0xa29b93),
  /* 081 21 091 */ APRICOT(0xfeb56d),
  /* 084 30 051 */ CHOCOLATE(0x3d1c02),
  /* 084 42 072 */ CINNAMON(0xac4f06),
  /* 087 06 089 */ MERINO(0xc9b79e),
  /* 087 12 092 */ RAFFIA(0xebc695),
  /* 087 26 084 */ BUTTERSCOTCH(0xd38f43),
  /* 088 09 086 */ TAUPE(0xb9a281),
  /* 088 27 089 */ SAFFRON(0xf6aa51),
  /* 088 43 082 */ MARIGOLD(0xed7f15),
  /* 089 28 074 */ BROWN20(0x9a6324),
  /* 089 39 082 */ PUMPKIN(0xdd7e1a),
  /* 090 14 074 */ KHAKI__A(0x826843),
  /* 091 03 086 */ BIRCH_GRAY(0xaba498),
  /* 091 05 097 */ TEQUILA(0xf6e8d0),
  /* 091 11 088 */ OATMEAL(0xcaae82),
  /* 092 07 095 */ MAGNOLIA(0xf1debe),
  /* 092 17 074 */ BROWN_DIRT(0x836539),
  /* 092 53 083 */ ORANGE(0xff8000),
  /* 093 10 096 */ MOCCASIN(0xffe4b5),
  /* 094 01 071 */ LEAD_GRAY(0x5e5c58),
  /* 094 12 088 */ TOUPE(0xc7ac7d),
  /* 095 09 093 */ MANILLA(0xe5d0a7),
  /* 095 39 062 */ BROWN(0x653700),
  /* 096 41 083 */ INCA_GOLD(0xdf8c19),
  /* 097 13 091 */ SAND(0xdcc18b),
  /* 098 01 082 */ KOALA_GRAY(0x928f88),
  /* 098 02 093 */ PEARL_GRAY(0xd8d3c7),
  /* 098 06 082 */ OYSTER(0x998f78),
  /* 099 04 097 */ OFF_WHITE(0xf1e9d5),
  /* 099 11 090 */ SANDSTONE(0xd5bf8e),
  /* 099 17 088 */ TAN(0xd1b26f),
  /* 100 01 091 */ CLOUD_GRAY(0xc4c1b9),
  /* 101 14 087 */ CHAMOIS(0xbea873),
  /* 102 12 071 */ BRONZE_OLIVE(0x695d3e),
  /* 102 16 092 */ STRAW(0xe3c882),
  /* 103 07 096 */ CREAM(0xefe3be),
  /* 103 51 088 */ SUNFLOWER(0xffa709),
  /* 105 08 092 */ PARCHMENT(0xd4c9a3),
  /* 105 20 043 */ ACADIA(0x1b1404),
  /* 106 20 086 */ SUGAR_CANE(0xbca55c),
  /* 106 28 093 */ PRIMROSE(0xf5cf5b),
  /* 106 53 089 */ AMBER(0xfeb308),
  /* 106 55 087 */ GOLDEN_YELLOW(0xf5a601),
  /* 107 20 092 */ SAND__XKCD(0xe2ca76),
  /* 107 29 076 */ OLIVE_YELLOW(0x8e7426),
  /* 108 11 094 */ BEIGE(0xe6daa6),
  /* 108 50 078 */ BRONZE(0xa87900),
  /* 109 35 085 */ MUSTARD(0xc4a32e),
  /* 109 48 091 */ HOMEBUSH_GOLD(0xfcc51a),
  /* 110 28 093 */ CUSTARD(0xefd25c),
  /* 110 50 082 */ OCHRE(0xbf9005),
  /* 110 55 090 */ MARIGOLD__XKCD(0xfcc006),
  /* 110 56 087 */ WATTLE(0xe8af01),
  /* 110 58 090 */ GOLDEN(0xffc000),
  /* 111 36 076 */ HAZEL(0x8e7618),
  /* 112 07 098 */ PALE(0xfff9d0),
  /* 112 50 089 */ CANARY(0xe7bd11),
  /* 113 04 085 */ CEMENT(0xa5a391),
  /* 113 51 088 */ GOLD(0xdbb40c),
  /* 114 09 098 */ BEIGE20(0xfffac8),
  /* 114 13 095 */ FLUMMERY(0xe6df9e),
  /* 115 35 091 */ BUTTERCUP(0xe0cd41),
  /* 115 60 093 */ GOLD__X11(0xffd700),
  /* 116 10 099 */ EGGSHELL(0xfffcc4),
  /* 116 52 094 */ YELLOW20(0xffe119),
  /* 116 58 093 */ DANDELION(0xfedf08),
  /* 117 18 085 */ KHAKI(0xaaa662),
  /* 118 04 095 */ LILY_GREEN(0xe3e3cd),
  /* 119 05 064 */ BLACK_OLIVE(0x47473b),
  /* 119 07 091 */ SURF_GREEN(0xc8c8a7),
  /* 119 09 099 */ IVORY(0xffffcb),
  /* 120 10 099 */ ECRU(0xfeffca),
  /* 121 13 091 */ CHARTREUSE__A(0xc7c98d),
  /* 121 38 097 */ LEMON(0xfdff52),
  /* 121 46 097 */ SUNSHINE(0xfffd37),
  /* 122 07 082 */ BANKSIA(0x929479),
  /* 122 07 086 */ LICHEN(0xa7a98c),
  /* 122 25 038 */ OILBLACK(0x101000),
  /* 122 51 076 */ OLIVE20(0x808000),
  /* 122 57 097 */ YELLOW__XKCD(0xffff14),
  /* 122 65 096 */ YELLOW(0xffff00),
  /* 123 36 089 */ CITRONELLA(0xbfc83e),
  /* 124 06 071 */ TI_TREE(0x5d5f4e),
  /* 124 32 080 */ LIME_GREEN(0x89922e),
  /* 124 42 074 */ OLIVE(0x6e750e),
  /* 126 15 077 */ AVOCADO(0x757c4c),
  /* 128 35 078 */ WASABI(0x788a25),
  /* 129 05 071 */ SLATE(0x5e6153),
  /* 130 63 094 */ CHARTREUSE__XKCD(0xc1f80a),
  /* 131 34 085 */ KIKUYU(0x95b43b),
  /* 131 61 087 */ SLIME(0x99cc04),
  /* 131 68 094 */ CHARTREUSE(0xc0ff00),
  /* 132 14 074 */ SAGE_GREEN(0x677249),
  /* 132 41 093 */ LIME20(0xbfef45),
  /* 133 27 046 */ BLACKPINE(0x171f04),
  /* 133 35 095 */ PEAR(0xcbf85f),
  /* 134 05 066 */ ENVIRONMENT_GREEN(0x484c3f),
  /* 135 25 077 */ SWAMP(0x698339),
  /* 138 06 079 */ MIST_GREEN(0x7a836d),
  /* 138 43 069 */ LEAF(0x436a0d),
  /* 138 51 094 */ LIME__XKCD(0xaaff32),
  /* 139 13 064 */ RAINFOREST_GREEN(0x3d492d),
  /* 139 14 086 */ PALM_GREEN(0x99b179),
  /* 139 21 081 */ LETTUCE(0x7b9954),
  /* 141 44 092 */ KIWI(0x9cef43),
  /* 141 73 092 */ LIME(0x80ff00),
  /* 143 21 039 */ BLACKFOREST(0x0b1304),
  /* 143 25 095 */ PISTACHIO(0xc0fa8b),
  /* 146 09 075 */ EUCALYPTUS(0x66755b),
  /* 146 14 038 */ GORDONS(0x0b1107),
  /* 147 43 081 */ GRASS(0x5cac2d),
  /* 148 23 053 */ SEAWEED(0x1b2f11),
  /* 149 16 086 */ LICHEN__XKCD(0x8fb67b),
  /* 150 24 072 */ FERN_TREE(0x477036),
  /* 150 80 089 */ HIGHLIGHTER(0x40ff00),
  /* 153 01 083 */ GRAY(0x929591),
  /* 153 29 082 */ FERN(0x63a950),
  /* 155 03 095 */ GLACIER(0xd5e1d2),
  /* 156 21 066 */ MOSS_GREEN(0x33572d),
  /* 156 31 078 */ APPLE_GREEN(0x4e9843),
  /* 157 09 091 */ CRYSTAL_GREEN(0xadcca8),
  /* 157 45 056 */ HUNTERGREEN(0x0b4008),
  /* 158 23 059 */ MYRTLE(0x21421e),
  /* 158 72 068 */ GREEN__X11(0x008000),
  /* 158 80 076 */ GREEN(0x00b000),
  /* 158 91 086 */ NEON_GREEN(0x00ff00),
  /* 160 63 078 */ GREEN__XKCD(0x15b01a),
  /* 161 06 074 */ RIVERGUM(0x617061),
  /* 161 25 069 */ SHAMROCK(0x336634),
  /* 162 03 099 */ HONEYDEW(0xeeffee),
  /* 163 17 096 */ MINT_LIGHT(0xb6ffbb),
  /* 165 40 081 */ GREEN20(0x3cb44b),
  /* 167 23 066 */ PINE(0x2b5d34),
  /* 168 13 083 */ SERPENTINE(0x78a681),
  /* 168 21 095 */ MINT(0x9ffeb0),
  /* 170 70 087 */ RADIOACTIVE(0x00ff40),
  /* 172 24 046 */ PALM(0x082310),
  /* 173 02 081 */ STORM_GRAY(0x858f88),
  /* 173 17 096 */ MINT20(0xaaffc3),
  /* 174 06 091 */ OPALINE(0xafcbb8),
  /* 175 01 096 */ MIST_BLUE(0xe0e6e2),
  /* 175 17 060 */ HOLLY(0x21432d),
  /* 175 45 069 */ LA_SALLE(0x087830),
  /* 177 27 080 */ BEANSTALK(0x45a56a),
  /* 178 29 065 */ EMERALD(0x195f35),
  /* 181 20 077 */ VERTIGRIS(0x468a65),
  /* 181 34 053 */ COUNTY(0x01371a),
  /* 184 14 065 */ TRAFFIC_GREEN(0x305442),
  /* 184 17 059 */ EVERGLADE(0x1c402e),
  /* 184 57 088 */ PURE_EMERALD(0x00ff80),
  /* 184 57 088 */ SPEARMINT(0x00ff80),
  /* 185 09 097 */ AERO(0xc9ffe5),
  /* 186 09 062 */ ZUCCHINI(0x2e443a),
  /* 187 32 058 */ EVERGREEN(0x05472a),
  /* 189 40 066 */ DARTMOUTH(0x00693e),
  /* 190 42 070 */ HOMEBUSH_GREEN(0x017f4d),
  /* 191 09 058 */ BOTTLE_GREEN(0x253a32),
  /* 194 32 070 */ JADE(0x127453),
  /* 194 37 073 */ LAWN_GREEN(0x0d875d),
  /* 195 09 083 */ COOTAMUNDRA(0x759e91),
  /* 195 31 056 */ SHERWOOD(0x02402c),
  /* 196 29 050 */ BURNHAM(0x002e20),
  /* 200 34 066 */ SPRUCE(0x05674f),
  /* 200 50 089 */ TURQUOISE(0x00ffc0),
  /* 202 11 085 */ GREEN_ICE(0x78aea2),
  /* 203 01 091 */ SILVER_GRAY__A(0xbdc7c5),
  /* 206 04 088 */ SMOKE_BLUE(0x9eb6b2),
  /* 209 06 098 */ ICE(0xd6fffa),
  /* 209 42 078 */ PERSIAN_GREEN(0x00a693),
  /* 210 19 080 */ TEAL20(0x469990),
  /* 212 13 037 */ WOODSMOKE(0x041110),
  /* 212 21 079 */ SEA(0x3c9992),
  /* 212 37 082 */ TOPAZ(0x13bbaf),
  /* 214 02 099 */ FROSTED(0xeefffe),
  /* 214 07 079 */ BLUE_GUM(0x6a8a88),
  /* 216 12 086 */ HUON_GREEN(0x72b3b1),
  /* 216 32 071 */ SURFIE_TEAL(0x0c7a79),
  /* 216 47 090 */ AQUA__X11(0x00ffff),
  /* 216 47 090 */ CYAN(0x00ffff),
  /* 218 32 092 */ CYAN_BRIGHT(0x41fdfe),
  /* 218 35 073 */ TURQUOISE__A(0x098587),
  /* 220 24 063 */ MALACHITE(0x105154),
  /* 221 35 068 */ DIAMANTIA(0x006c74),
  /* 222 31 059 */ SHERPA(0x004950),
  /* 223 34 065 */ PETROL(0x005f6a),
  /* 226 13 087 */ HOSTA_BLUE(0x77bfc7),
  /* 227 21 076 */ ORIENTAL_BLUE(0x358792),
  /* 228 37 071 */ OCEAN(0x017b92),
  /* 231 16 051 */ FIREFLY(0x0e2a30),
  /* 232 28 088 */ CYAN20(0x42d4f4),
  /* 234 17 066 */ PEACOCK_BLUE(0x245764),
  /* 235 45 083 */ AZURE(0x00c0ff),
  /* 237 45 082 */ SKY_BAFF(0x00baff),
  /* 238 11 087 */ SKY_BLUE(0x7db7c7),
  /* 238 17 080 */ AQUA(0x5097ac),
  /* 238 25 079 */ PARADISE_BLUE(0x3499ba),
  /* 241 37 068 */ PEACOCK(0x016795),
  /* 242 17 059 */ TEAL(0x183f4e),
  /* 243 16 066 */ MOUNTAIN_BLUE(0x295668),
  /* 243 17 075 */ STORM_BLUE(0x3f7c94),
  /* 243 38 068 */ TROPICAL_BLUE(0x006698),
  /* 247 32 057 */ ASTROBLUE(0x013e62),
  /* 248 25 084 */ PICTON(0x45b1e8),
  /* 248 42 078 */ AZURE__XKCD(0x069af3),
  /* 249 30 080 */ IVY_BLUE(0x2c9ad7),
  /* 250 22 040 */ MIDNIGHT_BLUE(0x011522),
  /* 250 29 085 */ CRYSTAL_BAFF(0x40baff),
  /* 251 33 058 */ REGAL_BLUE(0x013f6a),
  /* 256 16 089 */ MALIBU(0x7dc8f7),
  /* 257 02 071 */ PEWTER(0x596064),
  /* 257 08 093 */ URANIAN(0xafdbf5),
  /* 257 25 040 */ BLACK_PEARL(0x001528),
  /* 259 05 094 */ TWINBED(0xbedbed),
  /* 259 07 072 */ SLATE__XKCD(0x516572),
  /* 259 16 090 */ SKY_BRIGHT(0x82cafc),
  /* 259 48 074 */ PURE_SKY(0x0080ff),
  /* 261 08 090 */ CERULEAN(0xa0c6e0),
  /* 262 12 075 */ METALLICBLUE(0x4f738e),
  /* 263 08 074 */ MARBLE(0x566d7e),
  /* 265 11 077 */ STEELBLUE(0x5a7d9a),
  /* 265 31 078 */ BLEU_FRANCE(0x318ce7),
  /* 265 33 053 */ MARINE(0x042e60),
  /* 265 33 064 */ YALE(0x0f4d92),
  /* 265 50 072 */ BRANDEIS(0x0070ff),
  /* 267 14 075 */ CADETBLUE(0x4e7496),
  /* 268 22 074 */ PERIWINKLE(0x3871ac),
  /* 269 04 061 */ CHARCOAL(0x363e45),
  /* 270 06 079 */ STEEL(0x738595),
  /* 270 15 086 */ RUDDY(0x76abdf),
  /* 270 17 071 */ DENIM(0x3b638c),
  /* 271 19 081 */ BLUEBELL(0x5b94d1),
  /* 273 34 072 */ WOAD(0x246bce),
  /* 275 47 057 */ SMALT(0x003399),
  /* 276 34 042 */ NAVY(0x01153e),
  /* 277 04 091 */ POWDER_BLUE(0xb7c8db),
  /* 277 28 066 */ HOMEBUSH_BLUE(0x215097),
  /* 279 29 064 */ COBALT(0x1e488f),
  /* 279 40 053 */ CATALINA(0x062a78),
  /* 280 54 063 */ BLUE__XKCD(0x0343df),
  /* 282 33 078 */ DODGER(0x3e82fc),
  /* 282 62 063 */ BLUE(0x0040ff),
  /* 283 40 040 */ NAVY_BLUE(0x001146),
  /* 285 26 067 */ ULTRAMARINE__XKCD(0x2c5098),
  /* 286 01 065 */ GRAPHITE(0x45474a),
  /* 288 34 057 */ LAPIS(0x15317e),
  /* 290 02 071 */ BASALT(0x585c63),
  /* 290 66 048 */ ZAFFRE(0x0014a8),
  /* 291 49 034 */ STRATOS(0x000741),
  /* 291 63 044 */ PHTHALO(0x000f89),
  /* 292 22 057 */ DELFT(0x1f305e),
  /* 293 13 090 */ PASTEL_BLUE(0xa2bffe),
  /* 293 74 034 */ NAVY20(0x000075),
  /* 293 77 035 */ NAVY__X11(0x000080),
  /* 293 97 045 */ PURE_BLUE(0x0000ff),
  /* 295 43 065 */ BLUEBLUE(0x2242c7),
  /* 295 45 062 */ PERSIAN_BLUE(0x1c39bb),
  /* 296 14 089 */ JORDY_BAFF(0xa0baff),
  /* 296 72 044 */ BLUE_COBALT(0x030aa7),
  /* 297 32 073 */ BLUE20(0x4363d8),
  /* 297 38 074 */ ULTRAMARINE(0x4166f5),
  /* 298 21 054 */ SPACE_CADET(0x1e2952),
  /* 298 41 062 */ SAPPHIRE(0x2138ab),
  /* 300 14 057 */ NAVY_BLUE__A(0x28304d),
  /* 301 24 061 */ CORAL_SEA(0x2b3873),
  /* 302 51 064 */ PALATINATE_BLUE(0x273be2),
  /* 304 20 059 */ ROYAL_BLUE(0x2c3563),
  /* 307 69 057 */ BLUEBONNET(0x1c1cf0),
  /* 308 30 079 */ CORNFLOWER(0x6a79f7),
  /* 311 23 072 */ LIBERTY(0x545aa7),
  /* 312 05 055 */ MIDNIGHT_BLUE__A(0x292a34),
  /* 318 01 054 */ BLACK__A(0x2a2a2c),
  /* 318 01 099 */ GHOST(0xf8f8ff),
  /* 318 19 037 */ EBONY(0x0c0b1d),
  /* 318 38 058 */ PICOTEE(0x2e2787),
  /* 321 30 073 */ IRIS(0x6258c4),
  /* 324 03 098 */ TITAN(0xf0eeff),
  /* 325 44 034 */ BLACK_ROCK(0x0d0433),
  /* 328 19 065 */ BLUEBERRY(0x4c4176),
  /* 329 10 076 */ WISTERIA(0x756d91),
  /* 329 59 050 */ BLUE_GEM(0x2c0e8c),
  /* 330 16 087 */ BILOBA(0xb2a1ea),
  /* 330 91 054 */ VIVID_IRIS(0x4000ff),
  /* 331 30 080 */ CROCUS(0x9070ea),
  /* 332 28 069 */ PASSIFLORA(0x6048a0),
  /* 337 91 056 */ ULTRABLUE(0x5800ff),
  /* 338 04 086 */ LILAC(0xa69fb1),
  /* 338 47 060 */ DAISY_BUSH(0x4f2398),
  /* 341 28 072 */ ROYAL_PURPLE(0x7851a9),
  /* 342 10 094 */ LILAC_PALE(0xe4cbff),
  /* 342 71 047 */ INDIGO(0x380282),
  /* 343 18 088 */ LAVENDER(0xc79fef),
  /* 343 20 089 */ LILAC_STRONG(0xcea2fd),
  /* 344 16 074 */ JACKARANDA(0x795f91),
  /* 347 93 060 */ PURE_INDIGO(0x8000ff),
  /* 348 14 093 */ LAVENDER20(0xe6beff),
  /* 349 29 077 */ AMETHYST(0x9b5fc0),
  /* 351 70 069 */ PURPLE__X11(0xa020f0),
  /* 352 81 065 */ VIOLET__XKCD(0x9a0eea),
  /* 353 39 030 */ JAGUAR(0x100118),
  /* 354 24 087 */ LILAC_BRIGHT(0xd891ef),
  /* 354 32 080 */ LILAC_RICH(0xb666d2),
  /* 354 83 055 */ PURPLISH_VIOLET(0x7000a8),
  /* 355 65 068 */ VIOLET_STRONG(0xa020d0),
  /* 356 61 065 */ PURPLE20(0x911eb4),
  /* 357 16 075 */ FRENCH_LILAC(0x86608e),
  /* 357 97 065 */ PURE_PURPLE(0xc000ff),
  /* 359 19 065 */ VIOLET(0x5d3a61),
  /* 359 89 061 */ PURPLE(0x9f00c5),
  /* 362 09 096 */ PEARL(0xffd8ff),
  /* 362 10 088 */ LILAC__X11(0xc8a2c8),
  /* 362 65 067 */ BARNEY(0xac1db8),
  /* 364 25 082 */ ORCHID(0xc875c4),
  /* 365 60 076 */ PURPLISH_PINK(0xe838e0),
  /* 365 64 076 */ MAGENTA20(0xf032e6),
  /* 365 67 047 */ DEEP_VIOLET(0x500050),
  /* 365 99 070 */ MAGENTA(0xff00ff),
  /* 366 45 045 */ EGGPLANT(0x380835),
  /* 367 12 057 */ MAROON(0x3f2b3c),
  /* 367 15 073 */ CYCLAMEN(0x83597d),
  /* 367 33 062 */ PALATINATE_PURPLE(0x682860),
  /* 367 85 070 */ FUCHSIA(0xed0dd9),
  /* 368 25 070 */ PURPLE__A(0x85467b),
  /* 372 02 099 */ PINKWHITE(0xfff4fc),
  /* 372 94 068 */ NEON_PINK(0xff00c0),
  /* 375 45 053 */ PLUM__XKCD(0x580f41),
  /* 375 60 046 */ BLACKBERRY(0x4d0135),
  /* 375 61 054 */ VELVET(0x750851),
  /* 376 57 044 */ BAROSSA(0x44012d),
  /* 377 04 091 */ RIBBON_PINK(0xd1bcc9),
  /* 377 83 061 */ MAGENTA__XKCD(0xc20078),
  /* 379 63 050 */ PURPURA(0x66023c),
  /* 382 44 059 */ PANSY(0x78184a),
  /* 383 27 086 */ PINK(0xff81c0),
  /* 383 88 066 */ CERISE(0xff0080),
  /* 385 33 070 */ ROUGE(0xa23b6c),
  /* 389 18 086 */ CADILLAC(0xe38aae),
  /* 390 27 077 */ ERICA_PINK(0xc55a83),
  /* 391 25 069 */ RUBY(0x8f3e5c),
  /* 391 66 051 */ BORDEAUX(0x7b002c),
  /* 392 29 063 */ MAGENTA__A(0x7b2b48),
  /* 392 76 060 */ RUBY__XKCD(0xca0147),
  /* 394 61 048 */ MAROON__XKCD(0x650021),
  /* 397 16 066 */ PLUM(0x6e3d4b),
  /* 398 11 093 */ PASTEL_PINK(0xffbacd),
  /* 399 14 077 */ MULBERRY(0xa06574),
  ;

  public final int rgb0;
  private int zRgb;
  private int zArgb;

  private static boolean darkMode = true;

  public static final double MAX_CHROMA_4_RGB = 0.32249096;
  public static final double MAX_CHROMA_INV = 1.0 / MAX_CHROMA_4_RGB;
  private static final double MAX_QCHROMA = Math.nextAfter(1.0, Double.NEGATIVE_INFINITY);

  private static double[] calibrateHueFromTo =
      new double[] {
        // RED (~640+ nm) leans too much (~630- nm) towards ORANGE on many displays:
        0.51719756, -0.1 + 0.51719756,
        // YELLOW needs a tiny touch of blue on many displays:
        1.9228, 0.05 + 1.9228,
        // Level off:
        2.1, 0 + 2.1,
        4.7, 0 + 4.7,
        // VIOLET is affected by the RED issue:
        5.65, -0.05 + 5.65,
      };

  // To be called only when idle :-)
  public static void setCalibrationHue(double... xmaPairsFromTo) {
    calibrateHueFromTo = xmaPairsFromTo;
    for (ColorNmz cx : ColorNmz.values()) {
      cx.zRgb = -1;
    }
  }

  public static void setDisplayMode(int mode) {
    darkMode = (0 == mode);
  }

  public static boolean isDarkMode() {
    return darkMode;
  }

  public static double invertLightness(double x) {
    return (1 - x + (x + 1) * Math.abs((x - 1) * (x - 0.5) * x));
  }

  /////

  private ColorNmz(int rgb) {
    this.rgb0 = rgb;
    zRgb = -1;
    zArgb = rgb | 0xff000000;
  }

  public int rgbCalibrated() {
    if (0 <= zRgb) {
      return zRgb;
    } else if ((6 > calibrateHueFromTo.length) || (BLACK == this) || (WHITE == this)) {
      zRgb = rgb0;
      zArgb = zRgb | 0xff000000;
      return zRgb;
    }
    // Atomic:
    double[] aCalib = calibrateHueFromTo;
    double[] hql = new double[3];
    double hue = hql4Rgb(hql, rgb0);
    if (0.001 >= hql[1]) {
      // Gray.
      zRgb = rgb0;
      zArgb = zRgb | 0xff000000;
      return zRgb;
    }
    double left = aCalib[aCalib.length - 2] - 2 * Math.PI;
    for (int i0 = 0; i0 <= aCalib.length; i0 += 2) {
      double right = (i0 >= aCalib.length) ? (aCalib[0] + 2 * Math.PI) : aCalib[i0];
      if ((left <= hue) && (hue <= right)) {
        double right2;
        double left2;
        if (0 >= i0) {
          right2 = aCalib[1];
          left2 = aCalib[aCalib.length - 1] - 2 * Math.PI;
        } else if (i0 < aCalib.length) {
          right2 = aCalib[i0 + 1];
          left2 = aCalib[i0 - 1];
        } else {
          right2 = aCalib[1] + 2 * Math.PI;
          left2 = aCalib[i0 - 1];
        }
        hue = left2 + (hue - left) * (right2 - left2) / (right - left);
        break;
      }
      left = right;
    }
    zRgb = rgb4Hql(hue, hql[1], hql[2]);
    zArgb = zRgb | 0xff000000;
    return zRgb;
  }

  /** Calibrated RGB value with full opacity. */
  public int argb() {
    if (0 > zRgb) {
      // (Re-)calculate:
      rgbCalibrated();
    }
    return zArgb;
  }

  public int argb4Dark(double opac, boolean asBackground) {
    if (0 > zRgb) {
      // (Re-)calculate:
      rgbCalibrated();
    }
    double[] hql = new double[3];
    double hue = hql4Rgb(hql, zRgb);
    double sat = hql[1];
    double lt = hql[2];
    if (0.75 > opac) {
      sat = sat - (sat * sat) * 0.35;
    }
    if (asBackground) {
      if (0.6 < lt) {
        lt = invertLightness(lt);
      }
      lt = (0.8 > opac) ? (0.85 * lt) : ((0.9 > opac) ? (0.92 * lt) : lt);
    } else if (lt < opac) {
      lt = (lt + opac) / 2;
    }
    return rgb4Hql(hue, sat, lt) | ((int) (opac * 255.99) << 24);
  }

  /////

  public static double waveLen4Hue(double hue) {
    // V 390-400 I 410-420 C 490 Y 575-580 XR 680-700
    // B 440-450+ G 540 O 600 R 640-650
    boolean neg = false;
    double h400 = hue * 200 / Math.PI;
    if (399.99 < h400) {
      h400 = ((400 < h400) && (h400 < 800)) ? (h400 - 400) : 0;
    }
    if (360.0 < h400) {
      neg = true;
      h400 -= 200;
    }
    if (32 > h400) {
      final double check = 32 * Math.PI / 200;
      if (hue < check) {
        double out = waveLen4Hue(check) + (32 - hue);
        return (700 <= out) ? 700 : out;
      }
    }
    double out = 667 - h400 * 187 / 256;
    if ((120 < h400) && (h400 < 320)) {
      final double e0 = 6.5 * (h400 - 120) / 200 * (h400 - 320) / 320;
      out += -18 * e0 * e0;
    }
    return neg ? -out : out;
  }

  public static double hue4WaveLen(double waveLen) {
    double wl = (0 > waveLen) ? (-waveLen) : waveLen;
    double h400 = (667 - wl) * 256 / 187;
    if (643.625 < wl) {
      h400 = ((643.625 + 32) < wl) ? 0 : (643.625 + 32 - wl);
    } else if ((120 < h400) && (h400 < 320)) {
      double e0 = 6.5 * (h400 - 120) / 200 * (h400 - 320) / 320;
      wl += (18 * e0 * e0);
      h400 = (667 - wl) * 256 / 187;
      double wc = waveLen4Hue(h400 * Math.PI / 200);
      wl = ((0 > waveLen) ? (-waveLen) : waveLen);
      h400 += (wc - wl) * 256 / 187;
      wc = waveLen4Hue(h400 * Math.PI / 200);
      h400 += (wc - wl) * 256 / 187;
      e0 = 6.5 * (h400 - 120) / 200 * (h400 - 320) / 320;
      wl = (18 * e0 * e0) + ((0 > waveLen) ? (-waveLen) : waveLen);
      h400 = (667 - wl) * 256 / 187;
    }
    if (0 > waveLen) {
      h400 = (200 <= h400) ? (h400 - 200) : (h400 + 200);
    }
    return h400 * Math.PI / 200;
  }

  public static final ColorOklab.Lab oklab_0 = new ColorOklab.Lab(0.0, 0, 0);
  public static final ColorOklab.Lab oklab_1 = new ColorOklab.Lab(1.0, 0, 0);

  public static ColorOklab.Lab lab4MaxChroma(double lt, double a, double b) {
    double max = MAX_CHROMA_4_RGB;
    if (0.0 >= lt) {
      return oklab_0;
    } else if (1.0 <= lt) {
      return oklab_1;
    } else if (0.97 < lt) {
      max = (1.0 - lt) / (1.0 - 0.97);
    } else if (0.3 > lt) {
      max = lt / 0.3;
    }
    double min = 0.001;
    double try0 = max * 0.8;
    for (;
        ((0.002 < Math.abs(a)) || (0.002 < Math.abs(b))) && ((max - min) > 0.001);
        try0 = (3 * max + min) / 4) {
      max -= 0.001;
      if (0.002 >= max) {
        return (0.5 < lt) ? oklab_1 : oklab_0;
      }
      boolean hit = false;
      if (Math.abs(a) > try0) {
        b = b * try0 / Math.abs(a);
        a = (0.0 <= a) ? try0 : -try0;
        hit = true;
      }
      if (Math.abs(b) > try0) {
        a = a * try0 / Math.abs(b);
        b = (0.0 <= b) ? try0 : -try0;
        hit = true;
      }
      if (!hit) {
        final double fact = (Math.abs(a) > Math.abs(b)) ? (max / Math.abs(a)) : (max / Math.abs(b));
        a *= fact;
        b *= fact;
        min += 0.001;
      }
      final ColorOklab.RGB rgbOk = ColorOklab.oklab_to_linear_srgb(new ColorOklab.Lab(lt, a, b));
      double rgbmin =
          (rgbOk.r < rgbOk.g)
              ? ((rgbOk.b < rgbOk.r) ? rgbOk.b : rgbOk.r)
              : ((rgbOk.b < rgbOk.g) ? rgbOk.b : rgbOk.g);
      double rgbmax =
          (rgbOk.r > rgbOk.g)
              ? ((rgbOk.b > rgbOk.r) ? rgbOk.b : rgbOk.r)
              : ((rgbOk.b > rgbOk.g) ? rgbOk.b : rgbOk.g);
      try0 -= 0.001;
      if (-0.002 > rgbmin) {
        if (0.002 < rgbmax) {
          try0 = try0 * rgbmax / (rgbmax - rgbmin);
        } else {
          max -= 0.001;
        }
        max = (try0 < max) ? try0 : (max - 0.001);
      } else if (1.002 < rgbmax) {
        try0 /= rgbmax;
        max = (try0 < max) ? try0 : (max - 0.001);
      } else {
        min = (try0 < min) ? (min + 0.001) : try0;
      }
    }
    return new ColorOklab.Lab(lt, a, b);
  }

  public static ColorOklab.Lab lab4MaxChroma(double lt, double hue) {
    // Adjust zero point:
    hue -= 0.00697;
    hue = (0 > hue) ? (hue + 2 * Math.PI) : hue;
    final double a = Math.cos(hue) * MAX_CHROMA_4_RGB;
    final double b = Math.sin(hue) * MAX_CHROMA_4_RGB;
    return lab4MaxChroma(lt, a, b);
  }

  public static ColorOklab.Lab lab4MaxChroma(double hue) {
    // Adjust zero point:
    hue -= 0.00697;
    hue = (0 > hue) ? (hue + 2 * Math.PI) : hue;
    final double a = Math.cos(hue) * MAX_CHROMA_4_RGB;
    final double b = Math.sin(hue) * MAX_CHROMA_4_RGB;
    double lt = 0.99;
    ColorOklab.Lab lab1 = null;
    for (double delta = -0.2; (lt > 0.3) && (delta < -0.005); lt -= 0.001) {
      final ColorOklab.Lab lab0 = (null != lab1) ? lab1 : lab4MaxChroma(lt, a, b);
      lab1 = lab4MaxChroma(lt + delta, a, b);
      if ((Math.abs(lab1.a) >= Math.abs(lab0.a)) && (Math.abs(lab1.b) >= Math.abs(lab0.b))) {
        lt += delta;
      } else {
        lab1 = null;
        lt -= delta;
        delta /= 4;
      }
    }
    return lab4MaxChroma(lt, a, b);
  }

  public static int rgb4Lab(double lightness, double a, double b) {
    ColorOklab.RGB rgbOk = ColorOklab.oklab_to_linear_srgb(new ColorOklab.Lab(lightness, a, b));
    double min =
        (rgbOk.r < rgbOk.g)
            ? ((rgbOk.b < rgbOk.r) ? rgbOk.b : rgbOk.r)
            : ((rgbOk.b < rgbOk.g) ? rgbOk.b : rgbOk.g);
    double max =
        (rgbOk.r > rgbOk.g)
            ? ((rgbOk.b > rgbOk.r) ? rgbOk.b : rgbOk.r)
            : ((rgbOk.b > rgbOk.g) ? rgbOk.b : rgbOk.g);
    if ((-0.004 >= min) || (1.004 <= max)) {
      ColorOklab.Lab lab = lab4MaxChroma(lightness, a, b);
      rgbOk = ColorOklab.oklab_to_linear_srgb(lab);
    }
    int red = (int) (0.9 + 256 * rgbOk.r);
    int green = (int) (0.9 + 256 * rgbOk.g);
    int blue = (int) (0.9 + 256 * rgbOk.b);
    red = (0xff <= red) ? 0xff : ((0 <= red) ? red : 0);
    green = (0xff <= green) ? 0xff : ((0 <= green) ? green : 0);
    blue = (0xff <= blue) ? 0xff : ((0 <= blue) ? blue : 0);
    return (red << 16) | (green << 8) | blue;
  }

  public static int rgb4Lab(ColorOklab.Lab lab) {
    return rgb4Lab(lab.L, lab.a, lab.b);
  }

  /** HQL as adjusted hue/chroma/lightness with range 0..2*PI, 0..1, 0..1. */
  public static double hql4Lab(double[] yHql, ColorOklab.Lab lab) {
    // Adjust zero point:
    double hue = 0.00697 + ColorOklab.hue4lab(lab);
    hue = ((Math.PI * 2) <= hue) ? (hue - (Math.PI * 2)) : hue;
    final double chroma = ColorOklab.chroma4lab(lab);
    if (null != yHql) {
      yHql[1] = Math.min(chroma * MAX_CHROMA_INV, MAX_QCHROMA);
      yHql[0] = (0.004 <= yHql[1]) ? hue : 0.0;
      yHql[2] = lab.L;
    }
    return hue;
  }

  public static int rgb4Hql(double hue, double chromaticRatio, double lightness) {
    // Adjust zero point:
    hue -= 0.00697;
    hue = (0 > hue) ? (hue + 2 * Math.PI) : hue;
    double a = Math.cos(hue) * chromaticRatio * MAX_CHROMA_4_RGB;
    double b = Math.sin(hue) * chromaticRatio * MAX_CHROMA_4_RGB;
    return rgb4Lab(lightness, a, b);
  }

  public static double hql4Rgb(double[] yHql, int xRgb) {
    final ColorOklab.Lab lab = ColorOklab.linear_srgb_to_oklab(ColorOklab.rgb4Code(xRgb));
    return hql4Lab(yHql, lab);
  }

  /** Convert RGB value to SCL value (=> range -1..0..1). */
  // S = spectral value (e.g. violet): log base 2, times 400 THz
  // C = chromatic ratio, negative for complementary value (e.g. lilac).
  // L = lightness, negative for explicit composite (e.g. purple).
  // C/L: +/+ for spectral light (e.g. violet), +/- for simple composites (RGB).
  public static double scl4Rgb(double[] yScl, int xRgb) {
    double s = 299792.458 / waveLen4Hue(hql4Rgb(yScl, xRgb));
    if (0 > s) {
      if (null != yScl) {
        yScl[1] = -yScl[1];
      }
      s = -s;
    }
    s = Math.log(s / 400) / Math.log(2);
    if (null != yScl) {
      yScl[0] = s;
    }
    return s;
  }

  /** Approximated composite, also for spectral values. */
  public static int rgb4Scl(double l2Wave, double chromaticRatio, double lightness) {
    final double waveLen = 299792.458 / (Math.pow(2, l2Wave) * 400);
    double hue = ColorNmz.hue4WaveLen(waveLen);
    hue += (0 <= chromaticRatio) ? 0 : Math.PI;
    final double lx = (0.0 <= lightness) ? lightness : -lightness;
    final double qx = (0.0 <= chromaticRatio) ? chromaticRatio : -chromaticRatio;
    return rgb4Hql(hue, qx, lx);
  }

  private static ColorNmz[][][][] box_box = null;

  public static ColorNmz[][][][] box() {
    if (null != box_box) {
      return box_box;
    }
    ConcurrentHashMap<Integer, HashSet<ColorNmz>> map =
        new ConcurrentHashMap<Integer, HashSet<ColorNmz>>();
    final int maxhue10 = 1 + (int) (Math.PI * 2 * 10);
    ColorNmz[][][][] out = new ColorNmz[maxhue10][][][];
    double[] hql = new double[3];
    for (ColorNmz col : ColorNmz.values()) {
      hql4Rgb(hql, col.rgb0);
      int inx =
          (((int) (hql[0] * 10)) << 16) | (((int) (hql[1] * 10)) << 8) | ((int) (hql[2] * 10));
      HashSet<ColorNmz> set = map.get(inx);
      if (null == set) {
        set = new HashSet<ColorNmz>();
        map.put(inx, set);
      }
      set.add(col);
    }
    ColorNmz[] a0 = new ColorNmz[0];
    for (int i2 = 0; i2 < maxhue10; ++i2) {
      out[i2] = new ColorNmz[10][][];
      for (int i1 = 0; i1 < 10; ++i1) {
        out[i2][i1] = new ColorNmz[10][];
        for (int i0 = 0; i0 < 10; ++i0) {
          HashSet<ColorNmz> set = map.get((i2 << 16) | (i1 << 8) | i0);
          out[i2][i1][i0] = (null == set) ? null : set.toArray(a0);
        }
      }
    }
    // Atomic:
    box_box = out;
    return out;
  }

  /////

  public static final ColorNmz[] Gray =
      new ColorNmz[] {
        /* 000 00 00  000000 */ BLACK,
        /* 000 00 33  0a0a0a */ ONYX_GRAY,
        /* 000 00 50  202020 */ RAISIN_GRAY,
        /* 000 00 55  2b2b2b */ MIDNIGHT_GRAY,
        /* 000 00 65  474747 */ GRAPHITE_GRAY,
        /* 000 00 71  5c5c5c */ BASALT_GRAY,
        /* 000 00 73  656565 */ SLATE_GRAY,
        /* 000 00 77  777777 */ BRIDGE_GRAY,
        /* 000 00 79  7e7e7e */ GRANITE_GRAY,
        /* 000 00 79  808080 */ PURE_GRAY,
        /* 000 00 80  848484 */ BATTLESHIP,
        /* 000 00 84  999999 */ PIPELINE_GRAY,
        /* 000 00 86  a6a6a6 */ QUICKSILVER,
        /* 000 00 87  adadad */ LIGHTBOX_GRAY,
        /* 000 00 89  b9b9b9 */ ASH_GRAY,
        /* 000 00 90  c0c0c0 */ SILVER_SAND,
        /* 000 00 92  c7c7c7 */ SILVER,
        /* 000 00 92  cccccc */ PASTEL_GRAY,
        /* 000 00 95  dcdcdc */ GAINSBORO,
        /* 000 00 96  e4e4e4 */ PLATINUM,
        /* 000 00 98  f5f5f5 */ WHITESMOKE,
        /* 000 00 99  ffffff */ WHITE,
      };

  public static final ColorNmz[] ColorStrong =
      new ColorNmz[] {
        /* 032 79 62  ff0000 */ PURE_RED,
        /* 121 65 96  ffff00 */ YELLOW,
        /* 158 91 86  00ff00 */ NEON_GREEN,
        /* 216 47 90  00ffff */ CYAN,
        /* 293 97 45  0000ff */ PURE_BLUE,
        /* 364 99 70  ff00ff */ MAGENTA,
      };

  /** Complementary strong colors. */
  public static final ColorNmz[] ColorWheel =
      new ColorNmz[] {
        /* 000 81 64  ff0040 */ TORCH,
        /* 032 79 62  ff0000 */ PURE_RED,
        /* 063 55 74  ff4000 */ VERMILLION,
        /* 092 53 83  ff8000 */ ORANGE,
        /* 110 58 90  ffc000 */ GOLDEN,
        /* 122 65 96  ffff00 */ YELLOW,
        /* 131 68 94  c0ff00 */ CHARTREUSE,
        /* 141 73 92  80ff00 */ LIME,
        /* 150 80 89  40ff00 */ HIGHLIGHTER,
        /* 158 91 86  00ff00 */ NEON_GREEN,
        /* 170 70 87  00ff40 */ RADIOACTIVE,
        /* 184 57 88  00ff80 */ SPEARMINT,
        /* 200 50 89  00ffc0 */ TURQUOISE,
        /* 216 47 90  00ffff */ CYAN,
        /* 235 45 83  00c0ff */ AZURE,
        /* 259 48 74  0080ff */ PURE_SKY,
        /* 282 62 63  0040ff */ BLUE,
        /* 293 97 45  0000ff */ PURE_BLUE,
        /* 330 91 54  4000ff */ VIVID_IRIS,
        /* 347 93 60  8000ff */ PURE_INDIGO,
        /* 357 97 65  c000ff */ PURE_PURPLE,
        /* 365 99 70  ff00ff */ MAGENTA,
        /* 372 94 68  ff00c0 */ NEON_PINK,
        /* 383 88 66  ff0080 */ CERISE,
      };

  public static final ColorNmz[] ColorSpectrum =
      new ColorNmz[] {
        /* 000 81 64  ff0040  675 */ TORCH,
        /* 032 79 62  ff0000  642 */ PURE_RED,
        /* 063 55 74  ff4000  620 */ VERMILLION,
        /* 092 53 83  ff8000  599 */ ORANGE,
        /* 106 53 89  feb308  588 */ AMBER,
        /* 122 65 96  ffff00  577 */ YELLOW,
        /* 141 73 92  80ff00  561 */ LIME,
        /* 158 91 86  00ff00  543 */ NEON_GREEN,
        /* 184 57 88  00ff80  518 */ PURE_EMERALD,
        /* 216 47 90  00ffff  490 */ CYAN,
        /* 235 45 83  00c0ff  477 */ AZURE,
        /* 259 48 74  0080ff  464 */ PURE_SKY,
        /* 293 97 45  0000ff  448 */ PURE_BLUE,
        /* 337 91 56  5800ff  420 */ ULTRABLUE,
        /* 347 93 60  8000ff  413 */ PURE_INDIGO,
        /* 359 19 65  5d3a61  404 */ VIOLET,
      };

  /** Double cone: lightness 0.3 (blackish) 0.452 (deep) ... (strong/0.7) ... 0.9/0.968 (bright) 0.99. */
  // Strong color variants have ..ff.. and ..00.., others have the 'original' value.
  public static ColorNmz[] ColorFixed =
      new ColorNmz[] {
        //TORCH 648   1b0007 306 398  570016 452 399  f41d53 701 399  fda1b8 900 399  fedfe7 968 399  fff5f8 990 396
        TORCH,
        //PURE_RED 627  1d0000 304 032  5e0101 455 031  f1261b 700 032  ffa497 899 032  fce1dd 968 031  fff6f4 990 036
        DIESEL, ROSEWOOD, PURE_RED, SALMON_PALE, ROSEWHITE,
        //VERMILLION 746  120400 303 059  3a0e00 453 062  d43500 701 063  ffa978 900 063  fde3d2 969 063  fff6f0 990 063
        VERMILLION,
        //ORANGE 834  0c0600 301 091  281501 454 091  944d03 701 091  feb049 900 091  ffe5ba 968 091  fff7ea 990 089
        BEAN, ORANGE, TEQUILA,
        //GOLDEN 906  0a0700 303 107  211800 455 108  755901 701 110  f8bc04 900 110  feec8f 968 110  fffad7 990 111
        GOLDEN,
        //YELLOW 967  080800 305 121  1a1a01 452 121  616100 701 121  cccd09 900 121  ffff04 968 121  fcffb5 989 121
        OILBLACK, BLACKPINE, OLIVE, YELLOW, IVORY,
        //CHARTREUSE 945  070900 310 130  151c01 453 131  4e6800 700 131  a7db09 900 131  daff69 968 130  f3ffcc 989 130
        CHARTREUSE,
        //LIME 920  050900 303 138  101f01 457 140  397001 700 140  7bed07 901 140  ccff91 968 140  efffdb 990 140
        BLACKFOREST, LEAF, LIME,
        //HIGHLIGHTER 894   020a00 302 151  092100 453 148  1e7b00 700 150  4bff0c 900 149  c4ffa9 968 149  eeffe5 991 149
        HIGHLIGHTER,
        //NEON_GREEN 866  000b00 303 158  002500 455 158  068405 701 158  3dff36 901 158  bdffb7 968 158  ebffe9 990 158
        GORDONS, HUNTERGREEN, GREEN, NEON_GREEN /* FLUORESCENT */, HONEYDEW,
        //RADIOACTIVE 876   010a03 302 168  012309 453 169  058023 700 169  2cff60 900 169  b9fec9 968 169  eafeef 990 170
        RADIOACTIVE,
        //SPEARMINT 886   010a05 304 180  012211 454 182  037d40 701 183  1dff8e 900 183  b0ffd8 967 183  e3fff2 988 185
        PALM, SPEARMINT, AERO,
        //TURQUOISE 896   010a08 308 201  022019 452 201  02795c 700 200  0cfec2 900 200  abffea 968 199  e1fff9 989 203
        TURQUOISE,
        //CYAN 905  000a0b 308 222  022020 456 216  017676 701 216  09f8f8 900 216  a4fffd 968 216  dfffff 989 218
        WOODSMOKE, BURNHAM, TEAL, CYAN, FROSTED,
        //AZURE 834   00090d 302 240  021e27 452 235  027195 700 235  52dafe 900 234  c0f4ff 968 234  eefbfe 990 236
        AZURE,
        //PURE_SKY 748  000912 308 259  001d39 455 258  0069d2 700 259  7ec9fd 901 259  cdeeff 967 253  f1faff 990 256
        NAVY, PURE_SKY, CERULEAN, /* CERULEAN_PALE */
        //BLUE 634  00071e 306 283  00175e 452 282  195bef 700 282  94c0ff 901 281  dbeafc 968 279  f3f9ff 990 274
        BLUE,
        //PURE_BLUE 452   00004f 305 293  0002f1 452 293  2754fc 701 293  9ebcff 901 293  dfe8fc 968 295  f5f8fe 990 293
        STRATOS, PURE_BLUE, DODGER, TITAN, GHOST,
        //VIVID_IRIS 540  0b002d 302 329  260194 456 330  673cef 700 329  c1aeff 900 329  eae4fc 968 330  f9f7ff 991 330
        VIVID_IRIS,
        //PURE_INDIGO 604   110021 307 347  36006d 454 346  9927fd 701 346  d8a5fc 900 347  f3e0ff 968 347  fbf6ff 990 345
        INDIGO, PURE_INDIGO, LAVENDER,
        //PURE_PURPLE 657   14001a 308 357  3f0054 453 357  c318f4 701 357  ee9cfe 900 357  fcdcff 968 359  fef5ff 991 358
        PURE_PURPLE,
        //MAGENTA 701   150015 305 364  460046 456 364  fe00fe 700 364  ff97f8 900 364  fedcfb 968 364  fff4fe 990 364
        JAGUAR, EGGPLANT, MAGENTA, PEARL, PINKWHITE,
        //NEON_PINK 684   160011 303 371  4b0038 455 372  ff07c4 698 371  ff98e5 899 371  feddf5 968 372  fff5fc 991 373
        NEON_PINK,
        //CERISE 667  18000c 303 383  510029 455 382  f31685 701 383  fc9ece 900 382  fedeee 968 383  fff5fa 990 383
        BLACKBERRY, CERISE, PINKLACE,
      };

  public static enum ColorDistinct implements QIfs.QEnumIf {
    /*  0 000 00 00  000000 */ FG__BLACK,
    /*  1 000 00 99  ffffff */ BG__WHITE,
    SHADE__RAISIN_GRAY,
    SHADE__WHITESMOKE,
    /*  2 297 38 74  4166f5 */ ULTRAMARINE,
    /*  3 342 10 94  e4cbff */ LILAC_PALE,
    /*  4 064 31 84  ff8040 */ MANGO,
    /*  5 116 58 93  fedf08 */ DANDELION,
    /*  6 032 65 51  8e0000 */ REDBERRY,
    /*  7 398 11 93  ffbacd */ PASTEL_PINK,
    /*  8 291 63 44  000f89 */ PHTHALO,
    /*  9 163 17 96  b6ffbb */ MINT_LIGHT,
    /* 10 004 50 71  e52b50 */ AMARANTH,
    /* 11 112 07 98  fff9d0 */ PALE,
    /* 12 156 31 78  4e9843 */ APPLE_GREEN,
    /* 13 218 32 92  41fdfe */ CYAN_BRIGHT,
    /* 14 365 60 76  e838e0 */ PURPLISH_PINK,
    /* 15 078 10 95  ffd8b1 */ PEACH_LIGHT,
    /* 16 212 21 79  3c9992 */ SEA,
    /* 17 141 44 92  9cef43 */ KIWI,
    /* 18 069 20 76  a2653e */ EARTH,
    /* 19 036 15 88  f29e8e */ BLUSH,
    /* 20 000 00 86  a6a6a6 */ QUICKSILVER,
    /* 21 000 00 96  e4e4e4 */ PLATINUM,
    /*  + 032 79 62  ff0000 */ PURE_RED,
    /*  + 362 65 67  ac1db8 */ BARNEY,
    /*  + 124 42 74  6e750e */ OLIVE,
    ;

    public final int rgb0;
    //    public final int argb0;
    public final ColorNmz nmz;

    private ColorDistinct() {
      final String nam = name();
      nmz = ColorNmz.valueOf(nam.contains("__") ? nam.substring(nam.indexOf("__") + 2) : nam);
      rgb0 = nmz.rgb0 | ((ordinal() < 4) ? 0x1000000 : 0);
      //      argb0 = (rgb0 | 0xff000000);
    }

    /** Calibrated value adjusted for display mode: 1 = full, 0 = normal, -1 = weak. */
    // WHITE/ BLACK swapped and emphasis as elevation for dark mode.
    public int argbQ(int emphasis) {
      if (ordinal() < 4) {
        int ord = ordinal() ^ (isDarkMode() ? 1 : 0);
        final int col = values()[ord].rgb0 & 0xffffff;
        if (0 != (ordinal() & 1)) {
          ///// Background
          if (0x800000 < col) {
            return 0xff000000 | (col - ((0 < emphasis) ? 0 : ((0 == emphasis) ? 0 : 0x303030)));
          }
          return 0xff000000
              | (col + ((0 < emphasis) ? 0x303030 : ((0 == emphasis) ? 0x121212 : 0)));
        }
        ///// Foreground
        if (0x800000 < col) {
          return 0xff000000
              | (col - ((0 < emphasis) ? 0 : ((0 == emphasis) ? 0x202020 : 0x707070)));
        }
        return 0xff000000 | (col + ((0 < emphasis) ? 0 : ((0 == emphasis) ? 0x303030 : 0x808080)));
        //        if ((0 != (ord & 1)) || (1 <= emphasis) || ((0 <= emphasis) && !isDarkMode())) {
        //          return 0xff000000 | col;
        //        }
        //        return alpha4Emphasis(emphasis) | col;
      }
      if (isDarkMode()) {
        final double opac = (0 < emphasis) ? 1.0 : ((0 == emphasis) ? 0.87 : 0.7);
        return nmz.argb4Dark(opac, 0 != (ordinal() & 1));
      }
      final double opac = (0 < emphasis) ? 1.0 : ((0 == emphasis) ? 0.85 : 0.4);
      return (((int) (opac * 255.9)) << 24) | rgb0;
    }

    @Override
    public long getShash() {
      return nmz.rgb0;
    }
  }

  public static ColorDistinct findDistinct(int rgb0) {
    for (ColorDistinct cx : ColorDistinct.values()) {
      if (rgb0 == cx.rgb0) {
        return cx;
      }
    }
    if (0x1000000 > rgb0) {
      return findDistinct(rgb0 | 0x1000000);
    }
    return isDarkMode() ? ColorDistinct.PLATINUM : ColorDistinct.QUICKSILVER;
  }

  public static final ColorDistinct[] kColorValsFg =
      new ColorDistinct[] {
        ColorDistinct.FG__BLACK, ColorDistinct.ULTRAMARINE, ColorDistinct.QUICKSILVER, // 100 %
        // . Colors.BLACK, Colors.BLUE20, Colors.GRAY20,
        ColorDistinct.MANGO, ColorDistinct.REDBERRY, ColorDistinct.PHTHALO, // >99 %
        // . Colors.ORANGE20, Colors.MAROON20, Colors.NAVY20,
        ColorDistinct.AMARANTH, ColorDistinct.APPLE_GREEN, ColorDistinct.PURPLISH_PINK, ColorDistinct.SEA, ColorDistinct.EARTH, // 99 %
        // . Colors.RED20, Colors.GREEN20, Colors.MAGENTA20, Colors.TEAL20, Colors.BROWN20,
        ColorDistinct.BARNEY, ColorDistinct.OLIVE, // 95 %
        // . Colors.PURPLE20, Colors.OLIVE20,
        ColorDistinct.PURE_RED,
      };

  public static final ColorDistinct[] kColorValsBg =
      new ColorDistinct[] {
        ColorDistinct.BG__WHITE, ColorDistinct.DANDELION, // 100 %
        // . Colors.WHITE, Colors.YELLOW20,
        ColorDistinct.PASTEL_PINK, ColorDistinct.LILAC_PALE, // >99 %
        // . Colors.PINK20, Colors.LAVENDER20,
        ColorDistinct.CYAN_BRIGHT, ColorDistinct.PALE, ColorDistinct.MINT_LIGHT, // 99 %
        // . Colors.CYAN20, Colors.BEIGE20, Colors.MINT20,
        ColorDistinct.KIWI, ColorDistinct.PEACH_LIGHT, // 95 %
        // . Colors.LIME20, Colors.APRICOT20,
        ColorDistinct.BLUSH,
      };

  @Override
  public long getShash() {
    return rgb0;
  }
}
