// Copyright (C) 2020, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.io.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;

// =====

/** Partial implementation of the following If, also needed as stand-alone class. */
public abstract class PlatformFunc implements QIfs.PlatformIf {

  // =====

  public static char[] kLines = {
    ' ', '-', '|', '+',
    // ' ', '\u2500', '\u2502', '\u253c',
    '\\', '\u00ac', '$', '&',
    '/', '\u00a5', 'Y', '#',
    'X', '\u00a5', '$', '#'
  };

  public static char[] kBoxes = {
    ' ', // 0
    '\u2580', // 1 upper
    '\u2584', // 2 lower
    '\u2588', // 3 both
  };

  /**
   * Not as easy as it should be ... :-)
   *
   * @param parameters empty='main'/ 'safe'/ 'external'(=downloads for regular env)/ 'tmp'/ 'home'.
   * @return File, based on '.' as fallback.
   */
  @Override
  public File getFilesDir(String... parameters) {
    try {
      if (0 < parameters.length) {
        final String par = StringFunc.toLowerCase(parameters[0]);
        if (par.contains("safe")) {
          // Bad luck on PCs :-(
          return (Dib2Constants.DEBUG) ? new File("/tmp") : null;
        } else if (par.contains("external") || par.contains("home")) {
          final String home = System.getProperty("user.home");
          final File dir = new File(home + (par.contains("home") ? "" : "/Downloads"));
          if (dir.isDirectory()) {
            return dir;
          }
        } else if (par.contains("tmp")) {
          final String tmp = System.getProperty("java.io.tmpdir");
          final File dir = new File(tmp);
          if (dir.isDirectory()) {
            return dir;
          }
        }
      }
    } catch (Exception e) {
    }
    return new File(".");
  }

  private String[] getLicense_lines;

  @Override
  public String[] getLicense(String[] xAdditionalVersionInfo, String... resources) {
    if (null != getLicense_lines) {
      return getLicense_lines;
    }
    resources =
        ((null == resources) || (0 >= resources.length)) ? Dib2Constants.LICENSE_LIST : resources;
    String license = "(Version " + Dib2Constants.VERSION_STRING + ")\n";
    if (null != xAdditionalVersionInfo) {
      for (String line : xAdditionalVersionInfo) {
        license += line + '\n';
      }
    }
    try {
      for (String rsc : resources) {
        InputStream is;
        if (null == rsc) {
          continue;
        }
        is = new BufferedInputStream(MiscFunc.class.getResourceAsStream(rsc)); // url.openStream();
        license += MiscFunc.readStream(is);
      }
    } catch (Exception e) {
      if (!license.contains("Could not access")) {
        license =
            '\n'
                + Dib2Constants.NO_WARRANTY[0]
                + "\n(Could not access license files.)\n\n"
                + license;
      }
    }
    getLicense_lines = license.split("\n");
    return getLicense_lines;
  }

  // =====
}
