// Copyright (C) 2020, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_ui;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;

// =====

// Not only for UI thread ...
public enum ClickRepeater implements QIfs.QRunnableIf {

  // =====

  INSTANCE;

  public static final int maxTime = 2500;
  public static final int maxCount = 25;

  private static final int repDelay = 250;
  private static final int syncDelay = 30;

  static volatile int qUiPointerX0 = -1;
  static volatile int qUiPointerY0 = -1;
  static volatile int qUiPointerX1 = -1;
  static volatile int qUiPointerY1 = -1;
  static volatile int qUiPointerX2 = -1;
  static volatile int qUiPointerY2 = -1;
  static volatile boolean qUiPointerMode = false;

  char key = 0;
  int factor4CanvasPointer = 1;
  long dEnd;

  long start;
  int maxPerRound;
  int tot;

  static void shiftPointerCanvas4Board(int xX, int xY) {
    qUiPointerX2 = qUiPointerX1;
    qUiPointerY2 = qUiPointerY1;
    qUiPointerX1 = qUiPointerX0;
    qUiPointerY1 = qUiPointerY0;
    qUiPointerX0 = xX;
    qUiPointerY0 = xY;
    qUiPointerMode = false;
    // System.out.println("Pointer " + xX + ' ' + xY);
  }

  static char cmd4Pointer(int eX, int eY) {
    char out = 0;
    if ((0 != eX) && (0 != eY)) {
      if (0 > eX) {
        out = (0 > eY) ? StringFunc.SCROLL_LEFT : StringFunc.SCROLL_DOWN;
      } else {
        out = (0 > eY) ? StringFunc.SCROLL_UP : StringFunc.SCROLL_RIGHT;
      }
    } else if (0 != eY) {
      out = (0 > eY) ? StringFunc.MOVE_UP : StringFunc.MOVE_DOWN;
    } else if (0 != eX) {
      out = (0 > eX) ? StringFunc.MOVE_LEFT : StringFunc.MOVE_RIGHT;
    }
    return out;
  }

  static char movePointer(int eX, int eY) {
    final int halfPointShift = Dib2Constants.UI_PT10_SHIFT - 1;
    ClickRepeater.qUiPointerY0 =
        ((ClickRepeater.qUiPointerY0 >> halfPointShift) + ((0 < eY) ? 1 : ((0 > eY) ? -1 : 0)))
            << halfPointShift;
    ClickRepeater.qUiPointerX0 =
        ((ClickRepeater.qUiPointerX0 >> halfPointShift) + ((0 < eX) ? 1 : ((0 > eX) ? -1 : 0)))
            << halfPointShift;
    return cmd4Pointer(eX, eY);
  }

  static void movePointer4Cmd(char cmd) {
    int eX = 0;
    int eY = 0;
    switch (cmd) {
      case StringFunc.MOVE_LEFT:
        eX = -1;
        break;
      case StringFunc.MOVE_UP:
        eY = -1;
        break;
      case StringFunc.MOVE_DOWN:
        eY = 1;
        break;
      case StringFunc.MOVE_RIGHT:
        eX = 1;
        break;
      case StringFunc.SCROLL_LEFT:
        eX = -1;
        eY = -1;
        break;
      case StringFunc.SCROLL_UP:
        eY = -1;
        eX = 1;
        break;
      case StringFunc.SCROLL_DOWN:
        eY = 1;
        eX -= 1;
        break;
      case StringFunc.SCROLL_RIGHT:
        eX = 1;
        eY = 1;
        break;
      default:
        return;
    }
    movePointer(eX, eY);
  }

  public void stop() {
    dEnd = 0;
  }

  @Override
  public int start(long xTick, QToken xmTask) {
    key = (char) xmTask.parX;
    factor4CanvasPointer = 1 + (int) (xmTask.parN0 & 1L);
    if (0 == key) {
      return 0;
    }
    start = DateFunc.currentTimeMillisLinearized();
    dEnd = start + maxTime - syncDelay;
    start += repDelay;
    maxPerRound = 1;
    tot = 0;
    return maxCount - tot;
  }

  @Override
  public int step(long xTick) {
    return maxCount - tot;
  }

  @Override
  public int stepAsync() {
    if ((Dib2Lang.AppState.ACTIVE.ordinal() < Dib2Root.app.appState.ordinal())
        || (0 >= dEnd)
        || (dEnd <= start)) {
      dEnd = 0;
      return 0;
    }
    if (0 == tot) {
      if (1 >= factor4CanvasPointer) {
        UiPres.INSTANCE.handleKey(key, false);
      }
      tot = 1;
      return maxCount - tot;
    }
    try {
      Thread.sleep(syncDelay);
    } catch (InterruptedException e) {
      dEnd = 0;
      return 0;
    }
    final long dCur = DateFunc.currentTimeMillisLinearized();
    int count = 1 + (int) (dCur - start) / 200 - tot;
    if (dCur <= start) {
      return maxCount - tot;
    } else if (dEnd <= dCur) {
      dEnd = 0;
      maxPerRound <<= 1;
      count = (maxPerRound >= count) ? count : maxPerRound;
      for (; 0 < count; count -= factor4CanvasPointer) {
        UiPres.INSTANCE.handleKey(key, true);
      }
      return 0;
    } else if ((dCur - start) >= 1000) {
      count = maxCount - (int) (dEnd - dCur - 1000) * maxCount / (int) (dEnd - start - 1000) - tot;
    }
    maxPerRound =
        (((dCur - start) <= (2 * (repDelay + syncDelay))) || (9 >= tot))
            ? 1
            : ((maxPerRound < count) ? (1 + maxPerRound) : maxPerRound);
    count = (maxPerRound >= count) ? count : maxPerRound;
    tot += (0 < count) ? count : 0;
    for (; 0 < count; count -= factor4CanvasPointer) {
      UiPres.INSTANCE.handleKey(key, true);
    }
    return maxCount - tot;
  }

  @Override
  public QToken call() {
    while (0 < stepAsync()) {}
    return null;
  }

  public static boolean stopMouseRep() {
    INSTANCE.stop();
    return true;
  }

  @Override
  public boolean guard(QToken xmTask, long... xParameters) {
    return xmTask.op instanceof QOpUi;
  }

  @Override
  public void removeWipData4Interrupts() {}

  // =====
}
