// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_ui;

import static net.sf.dibdib.thread_any.StringFunc.*;

import com.gitlab.dibdib.picked.common.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.ColorNmz.ColorDistinct;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_feed.*;

// =====

/** Variables and helpers for presenting data. */
public enum UiValTag implements QIfs.QEnumIf {

  // =====

  UI_READY(0),
  // UI_LANGUAGE(0),

  // DEPR
  UI_PX_SHIFT_MIN(0),
  UI_PX_SHIFT_MAX(24),

  UI_FONT_SIZE_FRAME_PT10(12 << Dib2Constants.UI_PT10_SHIFT),

  UI_BOARD_HEIGHT(Dib2Constants.UI_PAGE_HEIGHT),
  UI_BOARD_WIDTH(Dib2Constants.UI_PAGE_WIDTH),
  UI_BACKGROUND_BARS(ColorDistinct.SHADE__WHITESMOKE.nmz.rgb0),

  // Display showing part of board with data and sheets ...
  UI_DISPLAY_HEIGHT(Dib2Constants.UI_DSPL_SIZE_MIN_PT10),
  UI_DISPLAY_WIDTH(Dib2Constants.UI_DSPL_SIZE_MIN_PT10),

  // Title, status, tools, textField:
  UI_DISPLAY_BARS_R(Dib2Constants.UI_FRAME_BARS),
  UI_DISPLAY_BAR_HEIGHT(9 * Dib2Constants.UI_DSPL_NMZ_LF / 8), // 9 * UI_LINE_SPACING_PT10 / 8),
  UI_DISPLAY_OFFS_CANVAS_X(0), // on top of margin
  UI_DISPLAY_OFFS_CANVAS_Y(0),
  UI_DISPLAY_SPLIT_CANVAS_X(Dib2Constants.UI_DSPL_INIT_SPLIT_X),
  UI_DISPLAY_SPLIT_CANVAS_Y(Dib2Constants.UI_DSPL_INIT_SPLIT_Y),
  UI_DISPLAY_SPLIT_CANVAS_GAP_X(Dib2Constants.UI_DSPL_NMZ_TAB / 2),
  UI_DISPLAY_SPLIT_CANVAS_GAP_Y(0),

  // 1 = 150%, 2 = 200%, -1 = 75%, ...
  UI_ZOOMLVL_BOARD(0),
  // UI_ZOOMLVL_FRAME(0),

  UI_NUMBER_BASE_SEC(16),

  X_UI_SIZE(0),
  ;

  // =====

  private final int nInit;

  // =====

  public static final int[] kBarTitle = {
    // LEFT
    ColorDistinct.PURE_RED.nmz.rgb0,
    ESCAPE,
    ColorDistinct.ULTRAMARINE.nmz.rgb0,
    ZOOM_OUT,
    ColorDistinct.ULTRAMARINE.nmz.rgb0,
    ZOOM_IN, // "", " ",
    ColorDistinct.APPLE_GREEN.nmz.rgb0,
    'A', // keyboard
    // CENTER
    //  "", " ", "", "Dib2x", "", "", "", "", //"", "",
    // RIGHT
    ColorDistinct.PURE_RED.nmz.rgb0,
    XCUT,
    ColorDistinct.APPLE_GREEN.nmz.rgb0,
    XPASTE,
    ColorDistinct.ULTRAMARINE.nmz.rgb0,
    XCOPY, // "", " ",
    ColorDistinct.FG__BLACK.nmz.rgb0,
    '=', // menu
  };

  public static int kBarTitle_qiSwitchKeyboard = 6;

  public static final String[] kBarTools = {
    // Fixed position:
    "LANG", "VW  ", "BS16", "QDFC", "  GO",
    // Flexible:
    " CLR", "SWAP", "STQ", "RCQ", "RCL", " ADD", "SUB ", "MUL ", "DIV ",
  };

  /////

  public static final char[] kKeys_0 = {
    'ä', 'ê', MOVE_UP, SCROLL_UP, 'ë', 'ü', BACKSP,
    'f', 'g', 'ç', 'r', 'l', '/', ' ',
    MOVE_LEFT, 'à', 'ô', 'é', 'û', 'î', ' ',
    SCROLL_LEFT, 'd', 'h', 't', 'n', 'ß', SCROLL_RIGHT,
    TAB, 'â', 'ö', 'è', 'ù', 'ï', MOVE_RIGHT,
    SHIFT, 'b', 'm', 'w', 'v', 'Z', PSHIFT,
    ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
  };

  public static final char[] kKeys_1 = {
    '_', '{', MOVE_UP, SCROLL_UP, '&', '}', BACKSP,
    '!', '[', '%', '0', '=', ']', '/',
    MOVE_LEFT, '(', '1', '2', '3', ')', '-',
    SCROLL_LEFT, '<', '4', '5', '6', '>', SCROLL_RIGHT,
    TAB, XCUT, '7', '8', '9', '0', MOVE_RIGHT,
    SHIFT, XPASTE, '*', '0', '+', '#', PSHIFT,
    ALT, XCOPY, ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
  };

  public static final char[] kKeys_1_calc = {
    '_', '{', '\u2228', SCROLL_UP, '&', '}', BACKSP,
    /*'\u215f'*/'\u00b9', '[', /*'\u22bb'*/'\u2207', '!', '\u2206', ']', '/',
    '\u00b1', '(', '1', '2', '3', ')', '-',
    SCROLL_LEFT, '\u00ab', '4', '5', '6', '\u00bb', SCROLL_RIGHT,
    'A', 'B', '7', '8', '9', 'E', 'F',
    SHIFT, 'C', '*', '0', '+', '%', PSHIFT,
    ALT, 'D', ' ', SCROLL_DOWN, '#', '.', CR,
  };

  public static final char[] kKeys_a_Dvorak = {
    '_', ':', MOVE_UP, SCROLL_UP, 'p', 'y', BACKSP,
    '\'', 'f', 'g', 'c', 'r', 'l', '/',
    MOVE_LEFT, 'a', 'o', 'e', 'u', 'i', '-',
    SCROLL_LEFT, 'd', 'h', 't', 'n', 's', SCROLL_RIGHT,
    TAB, '?', 'q', 'j', 'k', 'x', MOVE_RIGHT,
    SHIFT, 'b', 'm', 'w', 'v', 'z', PSHIFT,
    ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
  };

  public static final char[] kKeys_a = kKeys_a_Dvorak.clone();

  public static final char[] kKeys_A_Dvorak = {
    '_', '>', MOVE_UP, SCROLL_UP, 'P', 'Y', BACKSP,
    '"', 'F', 'G', 'C', 'R', 'L', '/',
    MOVE_LEFT, 'A', 'O', 'E', 'U', 'I', '-',
    SCROLL_LEFT, 'D', 'H', 'T', 'N', 'S', SCROLL_RIGHT,
    TAB, '!', 'Q', 'J', 'K', 'X', MOVE_RIGHT,
    SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
    ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
  };

  public static final char[] kKeys_A = kKeys_A_Dvorak.clone();
  public static final char[] keys_UnicodeSel = kKeys_A_Dvorak.clone();

  public static final char[] kKeys_Z = {
    'Ä', 'Ê', MOVE_UP, SCROLL_UP, 'Ë', 'Ü', BACKSP,
    'F', 'G', 'Ç', 'R', 'L', '/', ' ',
    MOVE_LEFT, 'À', 'Ô', 'É', 'Û', 'Î', ' ',
    SCROLL_LEFT, 'D', 'H', 'T', 'N', 'ß', SCROLL_RIGHT,
    TAB, 'Â', 'Ö', 'È', 'Ù', 'Ï', MOVE_RIGHT,
    SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
    ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
  };

  public static final char[][] kPadKeys_Dvorak = {
    kKeys_0, kKeys_1, kKeys_a_Dvorak, kKeys_A_Dvorak, kKeys_Z
  };
  public static final char[][] kPadKeys_Calc = {
    kKeys_0, kKeys_1_calc, kKeys_a, kKeys_A, keys_UnicodeSel
  };

  public static char[][] qPadKeys = kPadKeys_Calc;

  public static int qcKeys4Win = 7;
  public static int keys_UniBlock_Offset = 0x2200;
  public static int keys_UniBlock_Current = keys_UniBlock_Offset;
  public static int keys_UniBlock_FromPad = 1;

  private static QObject zUiVal = null;
  private static QComponent zComponent = null;

  // =====

  private UiValTag(int i32) {
    nInit = i32;
  }

  public int getInitial() {
    return ((QSeqInt) zComponent.initial).items[ordinal()];
  }

  public void setInitial(int i32) {
    ((QSeqInt) zComponent.initial).items[ordinal()] = i32;
  }

  @Override
  public long getShash() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  static void resetAll() {
    for (UiValTag tag : UiValTag.values()) {
      if (UiValTag.X_UI_SIZE != tag) {
        tag.setFut(tag.getInitial());
      }
    }
  }

  static long getTick() {
    return zComponent.getShash();
  }

  static void tick(long xNanobis) {
    zComponent.tick(xNanobis);
  }

  public long i64(long tick, Object... dummy) {
    return ((QSeqInt) zComponent.get(tick)).items[ordinal()];
  }

  public int i32(long tick, Object... dummy) {
    return ((QSeqInt) zComponent.get(tick)).items[ordinal()];
  }

  public double d4(long tick, Object... dummy) {
    return Dib2Constants.INT_D4_FACT * ((QSeqInt) zComponent.get(tick)).items[ordinal()];
  }

  public String strFull(long tick, Object... dummy) {
    return "" + ((QSeqInt) zComponent.get(tick)).items[ordinal()];
  }

  int i32Fut() {
    return ((QSeqInt) zComponent.future).items[ordinal()];
  }

  void setFut(int xVal) {
    if (zComponent.future == zComponent.current) {
      zComponent.future = ((QSeqInt) zComponent.current).clone();
    }
    ((QSeqInt) zComponent.future).items[ordinal()] = xVal;
  }

  // =====

  /** UiVals with primary component, singletons. */
  public static QObject create() {
    if (null != zUiVal) {
      return zUiVal;
    }
    zComponent = new QComponent(new QSeqInt(X_UI_SIZE.ordinal()));
    zUiVal = QObject.create(2);
    zUiVal.components[0] = zComponent;
    zUiVal.components[1] = UiValFeedTag.create();
    char cA = 'Z';
    char ca = 'z';
    for (int i0 = kKeys_a.length - 1; (i0 >= 0) && ('A' <= cA); --i0) {
      kKeys_A[i0] = ('A' <= kKeys_A[i0]) ? (cA--) : kKeys_A[i0];
      kKeys_a[i0] = ('A' <= kKeys_a[i0]) ? (ca--) : kKeys_a[i0];
    }
    setUnicodeBlock(keys_UniBlock_Current, 0);
    for (UiValTag el : values()) {
      if (X_UI_SIZE == el) {
        break;
      }
      el.setInitial(el.nInit);
    }
    return zUiVal;
  }

  //  private void reset(QObject ui) {
  //    if (QWord.V_NULL != vInit) {
  //      ui.components[0].itemsFuture.items[ordinal()] = vInit;
  //      //      prepI32(nInit);
  //    } else {
  //      //      prep(vInit);
  //      ui.components[0].itemsFuture.primValsInt[ordinal()] = nInit;
  //    }
  //  }

  private static int setUnicodeBlock_perRound = 0;

  public static int setUnicodeBlock(int block, int rounds) {
    block = (0 > block) ? keys_UniBlock_Current : block;
    if (0 >= setUnicodeBlock_perRound) {
      int tot = 0;
      for (int i0 = 0; i0 < keys_UnicodeSel.length; ++i0) {
        tot += (kKeys_A_Dvorak[i0] > '.') ? 1 : 0;
      }
      setUnicodeBlock_perRound = tot;
    }
    keys_UniBlock_Current = (block + rounds * setUnicodeBlock_perRound) & 0xffff;
    if (0xd800 <= keys_UniBlock_Current) {
      keys_UniBlock_Current = 0x2000;
    }
    char c0 = (char) (keys_UniBlock_Current + setUnicodeBlock_perRound);
    for (int i0 = keys_UnicodeSel.length - 1; i0 >= 0; --i0) {
      keys_UnicodeSel[i0] = (kKeys_A_Dvorak[i0] > '.') ? (--c0) : kKeys_A_Dvorak[i0];
    }
    keys_UniBlock_FromPad = 1;
    return keys_UniBlock_Current;
  }

  public static void setUnicodeBlockOffset(String blockNameOrOffset) {
    int block = (int) BigSxg.long4String(blockNameOrOffset, -1);
    if (0 > block) {
      for (int i0 = 1; i0 < Rfc1345.kUnicodeBlocks.length; i0 += 2) {
        if (Rfc1345.kUnicodeBlocks[i0].contains(blockNameOrOffset)) {
          block = Rfc1345.kUnicodeBlocks[i0 - 1].charAt(0);
        }
      }
    }
    if (0 <= block) {
      keys_UniBlock_Offset = setUnicodeBlock(block, 0);
    }
  }

  // =====
}
