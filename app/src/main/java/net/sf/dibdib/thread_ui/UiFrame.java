// Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_ui;

import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.ColorNmz;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.generic.QToken.QScript;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;

// =====

public final class UiFrame {

  // =====

  /** Canvas width = frame width + 1 if using border lines, defaults to page width. */
  public static volatile int qWidthCanvasPx = Dib2Constants.UI_PAGE_MIN;

  /** Canvas height = frame height + 1 if using border lines, defaults to page width. */
  public static volatile int qHeightCanvasPx = Dib2Constants.UI_PAGE_MIN;

  /** 2*72 dpi as base value. */
  private static int zShift2PxMajor = Dib2Constants.UI_PT10_SHIFT - 1;

  private static boolean zbShift2PxMinor = false;
  // private static int zShift2Px_0 = Dib2Constants.UI_PT10_SHIFT - 1;

  // =====

  private static int shift2Px4Pt10(int val) {
    final int shift = zShift2PxMajor;
    if (zbShift2PxMinor) {
      return (val >> shift) - (val >> (shift + 2));
    }
    return (val >> shift);
  }

  static int shift2Pt10oPx(int val) {
    final int shift = zShift2PxMajor;
    if (zbShift2PxMinor) {
      return (val << (shift + 2)) / 3 + ((0 < val) ? 1 : 0);
    }
    return (val << shift);
  }

  /** For platform's UI thread, exclusively! */
  public int getTextSize4Ui() {
    final int fontSizePt10 = UiValTag.UI_FONT_SIZE_FRAME_PT10.i32Fut();
    return shift2Pt10oPx(fontSizePt10);
  }

  /** Canvas width = frame width + 1 if using border lines. Size as virtual paper size. */
  public static void checkSizeNZoom(int xWidthPx, int xHeightPx, int xSizeMinPt10) {
    xWidthPx =
        (Dib2Constants.UI_DSPL_SIZE_MIN_PX > xWidthPx)
            ? Dib2Constants.UI_DSPL_SIZE_MIN_PX
            : xWidthPx;
    xHeightPx =
        (Dib2Constants.UI_DSPL_SIZE_MIN_PX > xHeightPx)
            ? Dib2Constants.UI_DSPL_SIZE_MIN_PX
            : xHeightPx;
    if ((qWidthCanvasPx == xWidthPx) && (qHeightCanvasPx == xHeightPx)) {
      return;
    }
    qWidthCanvasPx = xWidthPx;
    qHeightCanvasPx = xHeightPx;
    // Check virtual display size.
    if ((xSizeMinPt10 < Dib2Constants.UI_DSPL_SIZE_MIN_PT10)
        || (Dib2Constants.UI_DSPL_SIZE_MAX_PT10 < xSizeMinPt10)) {
      xSizeMinPt10 =
          (Dib2Constants.UI_DSPL_SIZE_MAX_PT10 < xSizeMinPt10)
              ? Dib2Constants.UI_DSPL_SIZE_MAX_PT10
              : Dib2Constants.UI_DSPL_SIZE_MIN_PT10;
    }
    final int min = (xWidthPx < xHeightPx) ? xWidthPx : xHeightPx;
    int dpi = min * Dib2Constants.UI_PT10_P_INCH / xSizeMinPt10;
    if ((min / dpi) < Dib2Constants.UI_DSPL_SIZE_MIN_INCH) {
      // Display too small even for short text lines => pretend ...
      dpi = min / Dib2Constants.UI_DSPL_SIZE_MIN_INCH;
    } else if ((min / dpi) > Dib2Constants.UI_DSPL_SIZE_MAX_INCH) {
      dpi = min / Dib2Constants.UI_DSPL_SIZE_MAX_INCH;
    }
    int level = -1;
    zbShift2PxMinor = (dpi < (2 * Dib2Constants.UI_PT_P_INCH / 3));
    // Calculate with dpp, rounded up:
    for (int dpp = (dpi * 2 + 10) / Dib2Constants.UI_PT_P_INCH; dpp != 0; dpp /= 2) {
      ++level;
      zbShift2PxMinor = zbShift2PxMinor || (2 == dpp);
    }
    int shifts = (Dib2Constants.UI_PT10_SHIFT <= level) ? 0 : (Dib2Constants.UI_PT10_SHIFT - level);
    zShift2PxMajor = shifts;
    if (shift2Pt10oPx(min) < (5 * Dib2Constants.UI_DSPL_SIZE_MIN_PT10 / 4)) {
      if (zbShift2PxMinor) {
        zbShift2PxMinor = false;
        ++shifts;
      } else {
        zbShift2PxMinor = true;
      }
    }
    // zShift2Px_0 = shifts;
    zShift2PxMajor = shifts;
    final int widPt10 = shift2Pt10oPx(xWidthPx);
    final int heightPt10 = shift2Pt10oPx(xHeightPx);
    UiValTag.UI_DISPLAY_WIDTH.setInitial(widPt10);
    UiValTag.UI_DISPLAY_WIDTH.setFut(widPt10);
    UiValTag.UI_DISPLAY_HEIGHT.setInitial(heightPt10);
    UiValTag.UI_DISPLAY_HEIGHT.setFut(heightPt10);
    int zoomLvl = (zbShift2PxMinor) ? -1 : 0;
    if (widPt10 < (3 * Dib2Constants.UI_DSPL_SIZE_MIN_PT10 / 2)) {
      zoomLvl = -1;
    } else {
      for (int rel = ((widPt10 < heightPt10) ? widPt10 : heightPt10) / Dib2Constants.UI_PAGE_MIN;
          rel != 0;
          rel /= 2) {
        ++zoomLvl;
      }
    }
    UiValTag.UI_ZOOMLVL_BOARD.setInitial(zoomLvl);
    UiValTag.UI_ZOOMLVL_BOARD.setFut(zoomLvl);
  }

  private boolean setContextVals(GraphicsIf g, QOpGraph.GraphContext ctx) {
    g.setColor((null == ctx) ? ColorNmz.ColorDistinct.FG__BLACK.argbQ(1) : ctx.color);
    g.setMatching128FontHeight(
        1
            + shift2Px4Pt10(
                (null == ctx) ? UiValTag.UI_FONT_SIZE_FRAME_PT10.getInitial() : ctx.heightPt10));
    //  g.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING,
    // RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
    return true;
  }

  private boolean paintScript(
      GraphicsIf g,
      int xjXPx,
      int xjYPx,
      long bFramePointer,
      QOpGraph.GraphContext previousVals,
      QScript... xScripts) {
    // Atomic!
    QScript[] script = xScripts;
    if (null == script) {
      // Race condition.
      return false;
    }
    QOpGraph.GraphContext ctx =
        QOpGraph.makeScriptContext(previousVals, (0 != (1 & bFramePointer)));
    int jXLine = ctx.xPt10;
    if (null == previousVals) {
      g.setColor(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
      ctx.zHeightPx = g.setMatching128FontHeight(1 + shift2Px4Pt10(ctx.heightPt10));
    }
    int nx = 0;
    for (QScript scrEl : script) {
      if (null == scrEl) {
        break;
      } else if (null != scrEl.script) {
        // TODO without recursion
        if (script != scrEl.script) {
          paintScript(g, xjXPx, xjYPx, bFramePointer, ctx, (QScript[]) scrEl.script);
        }
        continue;
      } else if (null == scrEl.op) {
        continue;
      }
      switch ((QOpGraph) scrEl.op) {
        case ARC:
          break;
        case CURVE:
          break;
        case DASH:
          break;
        case ENTRY:
          g.drawText(
              UiPres.INSTANCE.getEntry(false),
              xjXPx + shift2Px4Pt10(ctx.xPt10),
              xjYPx + 1 + shift2Px4Pt10((ctx.yPt10 + ctx.eBasePt10)));
          break;
        case FACE:
          nx = 1 + shift2Px4Pt10(ctx.heightPt10);
          ctx.zHeightPx = g.setMatching128Font(nx, scrEl.parS0, scrEl.parX % 3);
          break;
        case HEIGHT:
          ctx.heightPt10 = scrEl.parX;
          ctx.zHeightPx = g.setMatching128FontHeight(1 + shift2Px4Pt10(ctx.heightPt10));
          break;
        case IMAGE:
          break;
          //    case LEADING:
          //      break;
        case LINE:
          g.drawLine(
              xjXPx + shift2Px4Pt10(ctx.xPt10),
              xjYPx + shift2Px4Pt10(ctx.yPt10),
              xjXPx + shift2Px4Pt10(scrEl.parX),
              xjYPx + shift2Px4Pt10(scrEl.parY));
          ctx.xPt10 = scrEl.parX;
          ctx.yPt10 = scrEl.parY;
          break;
        case LNCAP:
          break;
        case LNJOIN:
          break;
        case LNWIDTH:
          break;
        case POS:
          ctx.xPt10 = scrEl.parX;
          ctx.yPt10 = scrEl.parY;
          break;
        case POSX:
          ctx.xPt10 = scrEl.parX;
          break;
        case POSY:
          ctx.yPt10 = scrEl.parY;
          break;
        case RBASE:
          ctx.eBasePt10 = scrEl.parX;
          break;
        case RGBCOLOR:
          ctx.color = scrEl.parX;
          g.setColor(ctx.color);
          break;
        case RMOVE:
          break;
        case STYLE:
          break;
        case TEXT:
          String sText = scrEl.parS0;
          nx = 0;
          if ((0 < sText.length()) && (' ' >= sText.charAt(0))) {
            for (; nx < sText.length() && ' ' >= sText.charAt(nx); ++nx) {}
            sText = sText.substring(nx);
            nx = -nx * (shift2Px4Pt10(UiFunc.boundWidthNmz(" ", ctx.heightPt10)));
          }
          nx += g.getBoundsLeft(sText);
          g.drawText(
              sText,
              xjXPx - nx + shift2Px4Pt10(ctx.xPt10),
              xjYPx + 1 + shift2Px4Pt10((ctx.yPt10 + ctx.eBasePt10)));
          // TODO monospaced ...
          ctx.xPt10 += UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10);
          break;
        case TXBOX:
          nx = g.getBoundsRight(scrEl.parS0);
          g.drawText(
              scrEl.parS0,
              // TODO stretch
              xjXPx + shift2Px4Pt10(ctx.xPt10) + ((shift2Px4Pt10(scrEl.parX) - nx) >> 1),
              xjYPx + 1 + shift2Px4Pt10(ctx.yPt10 + ctx.eBasePt10));
          ctx.xPt10 += scrEl.parX;
          break;
        case TXCTR: // ,
          nx = g.getBoundsMiddle(scrEl.parS0);
          g.drawText(
              scrEl.parS0,
              xjXPx + shift2Px4Pt10(ctx.xPt10) - nx,
              xjYPx + 1 + shift2Px4Pt10(ctx.yPt10 + ctx.eBasePt10));
          ctx.xPt10 += UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10) >> 1;
          break;
        case TXSHLEFT: // ,
          nx = g.getBoundsRight(scrEl.parS0); // g.getBoundsMiddle(scrEl.parS0)<<1;
          g.drawText(
              scrEl.parS0,
              xjXPx + shift2Px4Pt10(ctx.xPt10) - nx,
              xjYPx + 1 + shift2Px4Pt10(ctx.yPt10 + ctx.eBasePt10));
          ctx.xPt10 -= UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10);
          break;
        case TXLF:
          ctx.xPt10 = jXLine;
          ctx.yPt10 += ctx.eLine;
          break;
        case WEIGHT:
          break;
        default:
          break;
      }
    }
    setContextVals(g, previousVals);
    ctx.recycleMe();
    return true;
  }

  private void fillDataImage(int index, int heightC, GraphicsIf xGr) {

    final int heightF = qHeightCanvasPx;
    final int widthF = qWidthCanvasPx;
    // Right border line not visible (+1), bottom yes, but status bar pushed (+1):
    xGr.setCanvasImage(
        index,
        widthF + 1,
        heightC + 1,
        ColorNmz.ColorDistinct.BG__WHITE.argbQ(0),
        ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    QScript scr = ((FeederRf) (Dib2Root.app.feederCurrent)).get().getLastFeed();
    if ((Dib2Root.app.appState.ordinal() > Dib2Lang.AppState.DISCLAIMER.ordinal())
        && (ColorNmz.ColorDistinct.PURE_RED.nmz.rgb0
            != UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])) {
      paintScript(xGr, 0, 0, 0L, null, UiPres.qUiKeypad);
    }
    if (null != scr) {
      final int hBoard = shift2Px4Pt10(UiValTag.UI_BOARD_HEIGHT.i32Fut());
      final int wBoard = shift2Px4Pt10(UiValTag.UI_BOARD_WIDTH.i32Fut());
      final int splitX = shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_X.i32Fut());
      final int splitY = shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_Y.i32Fut());
      final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut() - (zbShift2PxMinor ? 1 : 0);
      final int oldShift2PxMajor = zShift2PxMajor;
      final boolean oldShift2PxMinor = zbShift2PxMinor;
      zShift2PxMajor = zShift2PxMajor - ((zoom + 1) >> 1);
      zShift2PxMajor = (0 > zShift2PxMajor) ? 0 : zShift2PxMajor;
      zbShift2PxMinor = (0 != (zoom & 1));
      final int offsX =
          -shift2Px4Pt10(Dib2Constants.UI_DSPL_INIT_MARGIN)
              + shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_X.i32Fut());
      final int offsY =
          -shift2Px4Pt10(Dib2Constants.UI_DSPL_INIT_MARGIN)
              + shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_Y.i32Fut());
      final int eSplitX = shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X.i32Fut());
      final int eSplitY = shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.i32Fut());
      final boolean doX = (10 < splitX) && ((splitX + 10) < widthF) && (10 < eSplitX);
      final boolean doY = (10 < splitY) && ((splitY + 10) < heightC) && (10 < eSplitY);
      if (!doX && !doY) {
        paintScript(xGr, -offsX, -offsY, 0x2L, null, scr);
      } else {
        xGr.setClip(0, 0, doX ? splitX : widthF, doY ? splitY : heightC);
        paintScript(xGr, -offsX, -offsY, 0x2L, null, scr);
        if (doX) {
          xGr.setClip(splitX, 0, widthF, (doY ? splitY : heightC));
          paintScript(xGr, -offsX - eSplitX, -offsY, 0x2L, null, scr);
        }
        if (doY) {
          xGr.setClip(0, splitY, (doX ? splitX : widthF), heightC);
          paintScript(xGr, -offsX, -offsY - eSplitY, 0x2L, null, scr);
        }
        if (doX && doY) {
          xGr.setClip(splitX, splitY, widthF, heightC);
          paintScript(xGr, -offsX - eSplitX, -offsY - eSplitY, 0x2L, null, scr);
        }
        xGr.setClip(0, 0, widthF, heightC);
        xGr.setColorTool(ColorNmz.ColorDistinct.ULTRAMARINE.argbQ(1));
        if (doX) {
          xGr.drawLine(splitX - 1, 0, splitX - 1, heightC);
        }
        if (doY) {
          xGr.drawLine(0, splitY - 1, widthF, splitY - 1);
        }
        xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
      }
      zShift2PxMajor = oldShift2PxMajor;
      zbShift2PxMinor = oldShift2PxMinor;

      int scrollBarX =
          ((widthF * widthF / wBoard)
                  >> ((0 < zoom) ? (zoom >> 1) : 0)
                  << ((0 > zoom) ? ((-zoom) >> 1) : 0))
              - (zoom << (zoom & 1));
      scrollBarX = (10 > scrollBarX) ? 10 : ((scrollBarX > widthF) ? widthF : scrollBarX);
      int scrollBarY =
          ((heightC * heightC / hBoard) // ,
                  >> ((0 < zoom) ? (zoom >> 1) : 0)
                  << ((0 > zoom) ? ((-zoom) >> 1) : 0))
              - (zoom << (zoom & 1));
      scrollBarY = (10 > scrollBarY) ? 10 : ((scrollBarY > heightC) ? heightC : scrollBarY);
      int jX = shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_X.i32Fut()) * widthF / wBoard;
      int jY = shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_Y.i32Fut()) * heightC / hBoard;
      jX = (jX > (widthF - scrollBarX)) ? (widthF - scrollBarX) : jX;
      jY = (jY > (heightF - scrollBarY)) ? (heightF - scrollBarY) : jY;
      xGr.setColorTool(ColorNmz.ColorDistinct.BG__WHITE.argbQ(0));
      xGr.drawLine(0, 0, widthF, 0);
      xGr.drawLine(0, 0, 0, heightC);
      xGr.drawLine(widthF - 1, 0, widthF - 1, heightC);
      xGr.drawLine(0, heightC - 1, widthF, heightC - 1);
      xGr.setColorTool(ColorNmz.ColorDistinct.ULTRAMARINE.argbQ(1));
      xGr.drawLine(jX, 0, jX + scrollBarX, 0);
      xGr.drawLine(jX, 1, jX + scrollBarX, 1);
      xGr.drawLine(0, jY, 0, jY + scrollBarY);
      xGr.drawLine(1, jY, 1, jY + scrollBarY);
      if (doX) {
        jX =
            (shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_X.i32Fut())
                    + shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_X.i32Fut()))
                * widthF
                / wBoard;
        jX = (jX > (widthF - scrollBarX)) ? (widthF - scrollBarX) : jX;
      }
      if (doY) {
        jY =
            (shift2Px4Pt10(UiValTag.UI_DISPLAY_OFFS_CANVAS_Y.i32Fut())
                    + shift2Px4Pt10(UiValTag.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.i32Fut()))
                * heightC
                / hBoard;
        jY = (jY > (heightC - scrollBarY)) ? (heightC - scrollBarY) : jY;
      }
      xGr.drawLine(widthF - 1, jY, widthF - 1, jY + scrollBarY);
      xGr.drawLine(widthF - 2, jY, widthF - 2, jY + scrollBarY);
      xGr.drawLine(jX, heightC - 1, jX + scrollBarX, heightC - 1);
      xGr.drawLine(jX, heightC - 2, jX + scrollBarX, heightC - 2);
      xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    }
  }

  private void paintBar(int index, int barHeightPx, QScript script, int backColor, GraphicsIf xGr) {
    if (0 != (paint4Frame_bRefreshNeeded & (1 << index))) {
      // Right border line not visible (+1), bottom yes (+0):
      xGr.setCanvasImage(
          index,
          qWidthCanvasPx + 1,
          barHeightPx,
          backColor,
          ColorNmz.ColorDistinct.FG__BLACK.argbQ(0));
      paintScript(xGr, 0, 0, 0x1L, null, script);
    }
  }

  /** Currently, full refresh is fast enough. */
  static long paint4Frame_bRefreshNeeded = -1;

  public boolean paint4Frame(GraphicsIf xGr) {
    if (Dib2Root.app.appState.ordinal() >= AppState.EXIT_REQUEST.ordinal()) {
      if (Dib2Root.app.appState.ordinal() == AppState.EXIT_REQUEST.ordinal()) {
        Dib2Root.triggerExitProcess();
      }
      return false;
      // paint4Frame_bRefreshNeeded = 0;
    }
    if (MainThreads.isIdle() || (0 != paint4Frame_bRefreshNeeded)) {
      final long tick = DateFunc.currentTimeNanobisLinearized(true);
      UiValTag.tick(tick);
      UiPres.prepareUiFrameData();
    }

    final int heightF = qHeightCanvasPx;
    final int barHeightPx = shift2Px4Pt10(UiValTag.UI_DISPLAY_BAR_HEIGHT.i32Fut());
    final int heightB = heightF - Dib2Constants.UI_FRAME_BARS * barHeightPx;
    if (0 != (paint4Frame_bRefreshNeeded & (1 << 0))) {
      fillDataImage(0, heightB, xGr);
    }
    final int backColor = ColorNmz.findDistinct(UiValTag.UI_BACKGROUND_BARS.i32Fut()).argbQ(0);
    paintBar(1, barHeightPx, UiPres.qUiBarTitle, backColor, xGr);
    paintBar(2, barHeightPx, UiPres.qUiBarTools, backColor, xGr);
    paintBar(3, barHeightPx, UiPres.qUiBarEntry, ColorNmz.ColorDistinct.BG__WHITE.argbQ(0), xGr);
    paintBar(4, barHeightPx, UiPres.qUiBarStatus, backColor, xGr);
    xGr.drawImage(0, 0, (Dib2Constants.UI_FRAME_BARS - 1) * barHeightPx);
    xGr.drawImage(1, 0, 0);
    xGr.drawImage(2, 0, barHeightPx);
    xGr.drawImage(3, 0, 2 * barHeightPx);
    // Bottom border line not needed => + 1.
    xGr.drawImage(4, 0, (heightF - barHeightPx) + 1);
    if ((0 <= ClickRepeater.qUiPointerX0) && (0 <= ClickRepeater.qUiPointerY0)) {
      final int vx = shift2Px4Pt10(ClickRepeater.qUiPointerX0);
      int vy = shift2Px4Pt10(ClickRepeater.qUiPointerY0);
      final int e0 = 1 + shift2Px4Pt10(2 << Dib2Constants.UI_PT10_SHIFT);
      // System.out.println("PDraw " + qUiPointerX0 + ' ' + qUiPointerY0 + ' ' + vx + ' ' + vy + ' '
      // + e0);
      if ((vx < qWidthCanvasPx) && (vy < heightB)) {
        xGr.setColorTool(ColorNmz.ColorDistinct.ULTRAMARINE.argbQ(1));
        vy += 3 * barHeightPx;
        xGr.drawLine(vx - e0, vy, vx + e0, vy);
        xGr.drawLine(vx, vy - e0, vx, vy + e0);
      }
    }
    xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    xGr.show();
    return (Dib2Root.app.appState.ordinal() < AppState.EXIT_REQUEST.ordinal());
  }

  // =====
}
