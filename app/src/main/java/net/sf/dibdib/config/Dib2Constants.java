// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.config;

import java.nio.charset.Charset;

// =====

public interface Dib2Constants {

  // =====

  public static final String[] NO_WARRANTY =
      new String[] {"ABSOLUTELY NO WARRANTY", "-- see license!"};

  public static final String[] LICENSE_LIST =
      new String[] {
        "/license.txt",
        "/spongycastle_license.txt",
        "/javamail_license.txt",
        "/apache_license2.txt",
      };

  /** Version myysb :=> major '.' (year/branch & ~1) - (stable ? 0 : 1)) '.' ... */
  // major '.' (year/branch & ~1) - (stable ? 0 : 1)) '.' (step * 10 + patch/build/substep)
  public static final int VERSION = 2323; // alpha, 0.23.xx for 0.24.10

  public static final int TIME_LEAPSECS_2022 = 37;

  public static final boolean DEBUG = false; //(1 == (VERSION & 1));

  public static final String VERSION_STRING =
      "0" + ("" + VERSION).replaceAll("..", ".$0").replace(".00", "");
  public static final int RFC4880_EXP2 = 0x80 | 0x40 | 62; // 0xfe
  public static /*final*/ byte FILE_STRUC_VERSION = 62; // 50; // Actual version
  public static final byte FILE_STRUC_VERSION_CMPAT =
      60; // 50; // Matching lower version, for header
  public static /*final*/ byte FILE_STRUC_VERSION_MIN = 60; // 50; Minimum supported
  public static final String FILE_STRUC_VERSION_STR =
      "" + (FILE_STRUC_VERSION / 100) + '.' + (FILE_STRUC_VERSION % 100);

  public static final String MAGIC_BYTES_STR = "dm";

  /** 'dm(' + date + ')' + version for CSV/TSV list (plain), YYXXX (hex) ... */
  // 'dm(' + date + ')' + version for CSV/TSV list (plain),
  // YYXXX (hex) as id for single TSV row,
  // "dmM(" with "...)" + version for mnemonic encoding.
  // RFC4880_EXP2 + len + ... for compressed (message) data.
  // Zipped CSV/TSV data:
  // RFC4880_EXP2 + len + 'dm' + 'z' + stamp (+'x').
  // Encoded TSV data, version 0.62 (with reversed TLVs/ TCVs):
  // RFC4880_EXP2 + len + 'dm' + cipher ('C') + algo ('A') + reqVersion*100 (62) + version*100
  // (>=62)
  // + salt iteration mag (it = (1 << mag) + 3)
  // + TC1 (0xa7) + senderAddress/Hash + TC2 + senderPK/Hash + TC3 + otherPK/Hash + TC4
  // + IV + TC5 + data + TC6 + sign/SIV + TC7 + 0x97.
  // Encoded data, version 0.6: (same as 0.5, but indicates support of 0.7x).
  // Encoded data, version 0.5:
  // RFC4880_EXP2 + len + 'dm' + version + algo
  // + block count of header (salt/IV with key info + address + additional key data)
  // + block count of appended signature.
  public static final byte[] MAGIC_BYTES = MAGIC_BYTES_STR.getBytes(Charset.forName("ISO-8859-1"));

  public static final String PASS_FILENAME = "DibdibP.txt";
  public static final String PASS_FILENAME_X = "DibdibX.dm";
  public static final int SALT_ITERATION_MAG = 10; // 10:(1<<10)+3, -1:1174;
  public static final long SAVE_INTERNAL_MSEC = 10 * 60 * 1000;
  public static final long MAX_DELTA_ACCESS_CHECK_MSEC = 18 * 3600 * 1000; // 18 h

  public static final long INT_D4_FACT_LONG = 5 * 5 * 5 * 3 * 3 * 7;
  /** Binary (2**) multiple of 1000, and of 3, 5, 7, 9 etc. */
  // Reducing precision for very big values, but providing full precision for millis,
  // money calculations (cents), and simple fractions (or up to 4 fraction digits if rounded).
  public static final double INT_D4_FACT = INT_D4_FACT_LONG;

  public static final double INT_D4_F_INV = Math.nextUp(1.0 / INT_D4_FACT);

  public static final String ERROR_Str = "ERROR";
  /** Up to 30 chars or truncated as 31 chars or 21 chars + SHA1. */
  public static final int SHASH_MAX = 32;
  // public static final int MAPPING_KEY_TYPES = 2; // shash (1) for String, shash + ID (1+1) for
  // Mapping
  // public static final int MAPPING_KEY_TYPE_OID = 1;
  /** For QMap: Dedicated lists with ID + mapping data */
  public static final int CSVDB_MAP_INDEX = 1;

  public static final String DATA_DEFAULT_ID__0 = "0";
  // public static final String[] SOURCES_DEFAULT = { "n.n.", "dm", "n.n." };
  public static final String[] SOURCES_DEFAULT = {"0", "0", "0"};

  // Hemera time HT-Y uses the fixed rounded value, not considerung changes (~0.1 sec/ 1000y ?).
  public static final double YEAR_SIDEREAL_2000 = 31558149.76;
  public static final double YEAR_ANNUS = 31556925.445;
  // Cmp. https://en.wikipedia.org/wiki/deltaT (timekeeping),
  // https://academic.oup.com/astrogeo/article/44/2/2.22/278981,
  // http://astro.ukho.gov.uk/nao/lvm @2022-06-06 (~1.7msec/cy ..., > 1.4).
  // 'HT-16' for -27-x msec at 1BC, 0 msec around 1820/1850, and delta-t ~65 for ~2000.
  // Very-long-term value (0.5..0.7 hours per 100 million years?) is not considered.
  public static final int TIME_LOD_DELTA_FIXED_MSEC_P_MILL = 16;
  /** HT-16 calculates with 'optimistic' values for LOY (31558150) and projected LOD. */
  // LOY = 31558149.5+x for both Y0 and J2000, delta-T = ~10500 for Y0, ~200 for ~2100.
  public static final long YEAR_SECONDS_YEAR = (int) (YEAR_SIDEREAL_2000 + 0.5);

  // public static final double YEAR_DAYS_REF = YEAR_SECONDS_YEAR / (24.0 * 3600);
  public static final double TIME_LOD_REF_YEAR = 1850;

  /** J2000 = JD 2451545.0 TT = 11:58:55.816 UTC = 11:58:56.171 UT1 = 11:59:27.816 TAI. */
  public static final double TIME_2000_HT_SECONDS = 2000.0 * YEAR_SECONDS_YEAR;

  public static final double TIME_DELTA_TAI = 32.184;
  public static final double TIME_DELTA_T_2000_UTC = 32 + TIME_DELTA_TAI;
  public static final double TIME_DELTA_T_2000_UT = 63.8285;

  public static final long TIME_2000_JD = 2451545;
  public static final long TIME_G2000_UNIX_MILLIS = 946684800000L;
  public static final long TIME_J2000_UNIX_MILLIS =
      (long) (TIME_G2000_UNIX_MILLIS + 12 * 3600 * 1000 - 1000 * TIME_DELTA_T_2000_UTC);
  public static final long TIME_MIN_2017_01_01_UNIX_MILLIS = 1483228800000L;
  public static final long TIME_MAX_AS_CURR_REASONABLE_UNIX_MILLIS =
      4 * TIME_MIN_2017_01_01_UNIX_MILLIS;
  public static final long TIME_MIN_NANOBIS =
      (((TIME_MIN_2017_01_01_UNIX_MILLIS - TIME_G2000_UNIX_MILLIS) << 18) / 1000) << 12;
  public static final int TIME_GREG_0_01_01_JD = 1721060;
  public static final int TIME_GREG_0_02_29_JD = 1721119;

  public static final double TIME_LOD_2000_PROJ =
      (2000 - TIME_LOD_REF_YEAR) / 1000.0 * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / 1000.0
          + (24.0 * 3600);
  public static final double TIME_LOD_0_PROJ =
      -TIME_LOD_REF_YEAR / 1000.0 * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / 1000.0 + (24.0 * 3600);
  // Fairly safe regarding underlying assumptions for LOD etc. ...
  public static final int TIME_Y0_JD = // 1721033
      (int)
          (0.99
              + TIME_2000_JD
              - TIME_2000_HT_SECONDS / ((TIME_LOD_0_PROJ + TIME_LOD_2000_PROJ) / 2));
  public static final long TIME_2000_ED = TIME_2000_JD - TIME_Y0_JD;
  public static final double TIME_DELTA_T_0_PROJ =
      -(TIME_2000_HT_SECONDS / 86400.0) * ((TIME_LOD_2000_PROJ + TIME_LOD_0_PROJ) / 2 - 86400)
          + TIME_DELTA_T_2000_UT;

  public static final int TIME_DELTA_COUNT_2022 = (int) TIME_DELTA_TAI + TIME_LEAPSECS_2022;
  public static final double TIME_LOD_2022_PROJ =
      (2022 - TIME_LOD_REF_YEAR) / 1000.0 * TIME_LOD_DELTA_FIXED_MSEC_P_MILL / 1000.0
          + (24.0 * 3600);
  public static final double TIME_DELTA_T_2022_PROJ =
      TIME_DELTA_T_2000_UT
          + (((TIME_LOD_2000_PROJ + TIME_LOD_2022_PROJ) / 2) - (24.0 * 3600))
              * ((22 * YEAR_SECONDS_YEAR) / (24.0 * 3600));

  public static final int TIME_DELTADELTA_CURR =
      (int) (0.5 + TIME_DELTA_COUNT_2022 - TIME_DELTA_T_2022_PROJ);

  public static final double TIME_DELTA_TERR12_Y0 = // 63200
      TIME_2000_HT_SECONDS - (TIME_2000_ED * 86400.0);
  /** Era Day 0.0 (ED 0 = JD 1721033) is Sunday (noon), ~73500 secs after tick 0. */
  public static final double TIME_DELTA_Y_Y0 =
      TIME_2000_HT_SECONDS
          + TIME_DELTA_T_2000_UT
          - TIME_2000_ED * (TIME_LOD_0_PROJ + TIME_LOD_2000_PROJ) / 2;

  public static final long TIME_SHIFTED = 3L;
  /** Use bit 1 of msec value to indicate bad or unaligned clock value. */
  public static final long TIME_SHIFTED_UNKNOWN = 2L;
  /** Use bit 0 of msec value to indicate bad or unaligned time zone value. */
  public static final long TIME_SHIFTED_HOUR = 1L;

  public static final int MAXVIEW_MSGS_INIT = 15;
  public static final int INIT_LINES_FILTER = 80;

  /** Minimum virtual paper size for reasonable viewing angle. */
  public static final int UI_DSPL_SIZE_MIN_INCH = 2;
  /** Maximum virtual paper size for reasonable viewing angle. */
  public static final int UI_DSPL_SIZE_MAX_INCH = 50;

  public static final int UI_PT_P_INCH = 72;
  public static final int UI_PT10_SHIFT = 10;
  public static final int UI_PT10_P_INCH = UI_PT_P_INCH << UI_PT10_SHIFT;
  public static final int UI_PAGE_MIN = 8 * UI_PT10_P_INCH;
  public static final int UI_PAGE_HEIGHT = 12 * UI_PT10_P_INCH;
  public static final int UI_PAGE_WIDTH = 12 * UI_PT10_P_INCH;
  public static final int UI_DSPL_SIZE_MIN_PX = UI_DSPL_SIZE_MIN_INCH * UI_PT_P_INCH;
  public static final int UI_DSPL_SIZE_MIN_PT10 =
      (UI_DSPL_SIZE_MIN_INCH * UI_PT_P_INCH) << UI_PT10_SHIFT;
  public static final int UI_DSPL_SIZE_MAX_PT10 =
      (UI_DSPL_SIZE_MAX_INCH * UI_PT_P_INCH) << UI_PT10_SHIFT;
  public static final int UI_DSPL_NMZ_TAB = (8 * 8) << UI_PT10_SHIFT;
  public static final int UI_DSPL_NMZ_LF = 16 << UI_PT10_SHIFT; // 14
  public static final int UI_TOUCH_TOLERANCE = (UI_PT10_P_INCH / 2);
  public static final int UI_DSPL_INIT_MARGIN = 3 << UI_PT10_SHIFT;
  public static final int UI_DSPL_INIT_SPLIT_X = 5 * UI_DSPL_NMZ_TAB / 16;
  public static final int UI_DSPL_INIT_SPLIT_Y = 3 * UI_DSPL_NMZ_LF + 2 * UI_DSPL_INIT_MARGIN;

  public static final int UI_FRAME_BARS = 4;
  public static final int UI_FRAME_BAR_ITEMS_PER_SIDE = 4; // 5
  public static final int UI_FRAME_BAR_TITLE = 0;
  public static final int UI_FRAME_BAR_TOOLS = 1;
  public static final int UI_FRAME_BAR_ENTRY = 2;
  public static final int UI_FRAME_BAR_STATUS = 3;

  ///// 'Normalized' font metrics, fraction of height h/256

  public static final int UI_FONT_NMZ_SHIFT = 8; // 256th
  public static final int UI_FONT_NMZ_HEIGHT = 256; // h = a+d
  public static final int UI_FONT_NMZ_SIZE = 320; // g = h + leading (x/2)
  public static final int UI_FONT_NMZ_CAP_H = 181; // a = sqrt(2) * x
  public static final int UI_FONT_NMZ_X_HEIGHT = 128; // x = h/2
  public static final int UI_FONT_NMZ_DESCENT = 75; // d = 2x - a; a+d = 2x
  public static final int UI_FONT_NMZ_M_ADV = 240; // m
  public static final int UI_FONT_NMZ_N_WIDTH = 128; // x
  public static final int UI_FONT_NMZ_N_ADV = 140; // avg
  public static final int UI_FONT_NMZ_MONO_ADV = 160;
  public static final int UI_FONT_NMZ_MONO_MAX = 180;

  ///// DEPR

  public static final int UI_X_WIN_X_MARGIN = 8;

  ///// Others

  /** Keep sort order and hex onset, skip char 'O' for potential confusion with '0'. */
  // 4 extra char's (z..~) for base64.
  public static final String base60String =
      "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz{|~";

  public static final char[] base60Chars = base60String.toCharArray();

  /** Keep sort order and hex onset, skip char 'O' for potential confusion with '0'. */
  public static final String base64XString =
      "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ^_abcdefghijklmnopqrstuvwxyz~";

  public static final char[] base64XChars = base64XString.toCharArray();

  // =====
}
