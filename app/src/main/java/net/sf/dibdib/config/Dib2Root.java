// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.config;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.generic.QToken.QScript;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.*;

// =====

public class Dib2Root extends Dib2State {

  // =====

  public static PlatformIf platform;

  /** For triggering threads, directly or via platform's service. */
  public static TriggerIf schedulerTrigger = MainThreads.TOPNET;

  public static QObject uiVals = null;
  public static CcmSto ccmSto = null;

  public static final Dib2State.Threaded app = new Dib2State.Threaded();
  public static final Dib2State.Ui ui = new Dib2State.Ui();

  private static String zLoadPath = null;

  /** First one is default for encoding. */
  public static TsvCodecIf[] codecs =
      new TsvCodecIf[] {
        CodecFunc.instance,
      };

  public static QIfs.QEnumIf[][] ops = null;

  // =====

  public static enum CmdLineArgs {

    // =====

    HELP("h?", "Print help."),
    WIDTH("", "Width of window/ frame, e.g. '--width=320'."),
    HEIGHT("", "Height of window/frame, e.g. '--height=480'."),
    DPI("", "Dots per inch, e.g. '--dpi=96'."),
    ZOOM("", "Zoom level, e.g. '--zoom=-1'."),
    // READONLY("r", "Read-only mode."),
    TTY("t", "Switch to simple text mode/ terminal mode - no GUI."),
    CARRIAGERETURN("", "Text mode using CR char's - for some special terminals."),
    ASCII("", "Text mode without Unicode - for some terminals."),
    // FILE("f", "Use specific file for loading data."),
    SECURE("p", "Dummy passphrase not allowed."),
    VERSION("vV", "Print version info."),
    DEBUG("d", "Switch to debug mode - in case of errors."),
    X0(null, "Path of encoded data file."),
    X1(null, null),
    X2(null, null),
    ;

    public final String abbrev;
    public final String descr;
    public String value;

    private CmdLineArgs(String abbrev, String descr) {
      this.abbrev = abbrev;
      this.descr = descr;
      value = null;
    }
  }

  // =====

  public static void setAppName(String xmAppShort) {
    if ((null == app.appName) || ("Dib2".length() >= app.appName.length())) {
      app.appShort = xmAppShort;
      app.appName = "Dib2" + (char) (xmAppShort.charAt(0) - ('a' - 'A')) + xmAppShort.substring(1);
    }
  }

  public static UiFrame create(
      char xPlatformMarker,
      String xmAppShort,
      PlatformIf xPlatform,
      // Runnable xWorker,
      TsvCodecIf[] xmCsvCodecs) {
    app.qTickMin = DateFunc.currentTimeNanobisLinearized(false);
    app.qTick.set(app.qTickMin);
    uiVals = UiValTag.create();
    ccmSto = new CcmSto();
    setAppName(xmAppShort);
    app.dbFileName = app.appName + "." + Dib2Constants.MAGIC_BYTES_STR;
    platform = (null == xPlatform) ? platform : xPlatform;
    codecs = (null == xmCsvCodecs) ? codecs : xmCsvCodecs;
    // app.mainWorker = xWorker;
    DateFunc.timeZoneDone = false;
    ops = new QIfs.QEnumIf[6][];
    ops[0] = QOpMain.create();
    ops[5] = QOpWk.create();
    ops[4] = QOpUi.create();
    ops[3] = QOpIo.create();
    ops[2] = QOpFeed.create();
    ops[1] = QOpGraph.create();
    TcvCodec.instance.create(xPlatformMarker);
    app.appState = Dib2Lang.AppState.INIT;
    return new UiFrame();
  }

  public static void init(boolean allowDummyPass) {
    ui.bTerminalMode =
        (null != CmdLineArgs.TTY.value) || (null != CmdLineArgs.CARRIAGERETURN.value);
    allowDummyPass = (null == CmdLineArgs.SECURE.value) ? allowDummyPass : false;
    MainThreads.TOPNET.init();
    app.bAllowDummyPass = allowDummyPass;
    MainThreads.TOPNET.reset(UiPres.INSTANCE.wxGateIn4Feed);
    UiPres.INSTANCE.init();
    app.mainFeeder = FeederRf.findFeeder(app.appShort);
    zLoadPath = IoRunner.check4Load();
    app.feederCurrent = FeederRf.DISCLAIMER;
    if (allowDummyPass) {
      ///// Try with dummy phrase -- maybe good enough for simple calculator etc.
      TcvCodec.instance.setDummyPhrase(false);
    } else if (null != zLoadPath) {
      ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).setPath(zLoadPath);
      app.feederCurrent = FeederRf.LOGIN;
    }
    ((FeederRf) app.feederCurrent).get().start();
    app.appState = (null == zLoadPath) ? Dib2Lang.AppState.DISCLAIMER : Dib2Lang.AppState.LOGIN;
  }

  public static void resume() {
    ExceptionAdapter.doAssert(Dib2Root.app.bPermitted, Dib2Root.class, "Missing permission.");
    QPlace.qExitRequested = false;
    app.bServiceThreadsHalted = false;
    if (app.appState.ordinal() < Dib2Lang.AppState.ACTIVE.ordinal()) {
      if (null != zLoadPath) {
        ((FeederRf) app.feederCurrent).get().start();
        QToken task = // (app.feederCurrent == FeederRf.LOGIN)
            // ? QScript.createTask(QOpFeed.zzREFRESH)  :
            QScript.createTask(QOpFeed.zzINIT_LOAD, QWord.createQWord(zLoadPath, true));
        zLoadPath = null;
        schedulerTrigger.trigger(task);
        return;
      }
    }
    if (app.appState.ordinal() > Dib2Lang.AppState.DISCLAIMER.ordinal()) {
      app.appState = Dib2Lang.AppState.ACTIVE;
      app.feederCurrent = app.mainFeeder;
    }
    ((FeederRf) app.feederCurrent).get().start();
    QToken task = QScript.createTask(QOpFeed.zzREFRESH);
    schedulerTrigger.trigger(task);
  }

  /** Enable early logging as much as possible */
  public static void log(String... aMsg) {
    //  MiscFunc.keepLog( aMsg[ 0 ], aMsg[ 1 ] );
    if (null != platform) {
      platform.log(aMsg);
    }
  }

  private static boolean onClose(int msecMax, boolean shutdown) {
    if (Dib2Lang.AppState.ACTIVE.ordinal() > app.appState.ordinal()) {
      return false;
    }
    final long wait = msecMax + DateFunc.currentTimeMillisLinearized();
    for (; (msecMax > 0) || (wait > DateFunc.currentTimeMillisLinearized()); --msecMax) {
      if (Dib2Lang.AppState.EXIT_DONE.ordinal() <= app.appState.ordinal()) {
        break;
      }
      try {
        Thread.yield();
        // if (shutdown) {
        try {
          Thread.sleep(100);
          msecMax -= 100;
          if (MainThreads.isIdle()) {
            break;
          }
        } catch (InterruptedException e) {
          msecMax = 0;
        }
      } catch (Exception e) {
        break;
      }
    }
    MainThreads.cancelAll(false);
    if (shutdown) {
      MainThreads.cancelAll(true);
    }
    return true;
  }

  public static synchronized boolean triggerExitProcess() {
    if (Dib2Lang.AppState.EXIT_TRIGGERED.ordinal() <= app.appState.ordinal()) {
      return false;
    }
    if (Dib2Lang.AppState.DISCLAIMER.ordinal() < app.appState.ordinal()) {
      app.appState = Dib2Lang.AppState.EXIT_TRIGGERED;
      schedulerTrigger.trigger(QScript.createTask(QOpFeed.zzSAV0_zzEXIT));
    }
    QPlace.qExitRequested = true;
    return true;
  }

  public static boolean onCloseApp(int msecMax) {
    triggerExitProcess();
    return onClose(msecMax, true);
  }

  //  public static boolean onCloseFrame(int msecMax) {
  //    if (Dib2Lang.AppState.EXIT_TRIGGERED.ordinal() <= app.appState.ordinal()) {
  //      return false;
  //    } else if (Dib2Lang.AppState.EXIT_REQUEST.ordinal() <= app.appState.ordinal()) {
  //      triggerExitProcess();
  //    } else if (Dib2Lang.AppState.ACTIVE.ordinal() <= app.appState.ordinal()) {
  //      app.appState = Dib2Lang.AppState.BACKGROUND;
  //    }
  //    return onClose(msecMax, false);
  //  }

  public static void scanArgs(String[] args) {
    CmdLineArgs[] aRegular = {CmdLineArgs.X0, CmdLineArgs.X1, CmdLineArgs.X2};
    int iRegular = 0;
    String abbrev = "";
    for (int i0 = 0; i0 < args.length; ++i0) {
      final String arg = args[i0].toUpperCase(Locale.ROOT);
      int offs =
          (arg.startsWith("--") ? 2 : ((arg.startsWith("-") || arg.startsWith("/"))) ? 1 : 0);
      if (0 >= offs) {
        if (iRegular < aRegular.length) {
          aRegular[iRegular++].value = arg;
        }
        continue;
      }
      for (CmdLineArgs opt : CmdLineArgs.values()) {
        String option = opt.name();
        int ix;
        if (arg.substring(offs).startsWith(option)) {
          if ((0 < (ix = arg.indexOf(':'))) || (0 < (ix = arg.indexOf('=')))) {
            opt.value = arg.substring(ix + 1);
            offs = -1;
            break;
          }
          opt.value = "";
          offs = -1;
          break;
        }
      }
      if (1 == offs) {
        abbrev += args[i0].substring(1);
      }
    }
    if (0 < abbrev.length()) {
      for (char flag : abbrev.toCharArray()) {
        for (CmdLineArgs opt : CmdLineArgs.values()) {
          if ((null != opt.abbrev) && (0 <= opt.abbrev.indexOf(flag)) && (null == opt.value)) {
            opt.value = "";
          }
        }
      }
    }
  }

  public static String getVersionInfo(String module) {
    String out = module + " version" + VERSION_STRING + '\n';
    out += Dib2Constants.NO_WARRANTY[0] + ' ' + Dib2Constants.NO_WARRANTY[1] + '\n';
    return out;
  }

  public static String getHelp(String module) {
    StringBuilder out = new StringBuilder(200 + 64 * CmdLineArgs.values().length);
    out.append(getVersionInfo(module));
    out.append("\n" + "Usage: Call this program with the following arguments/ options:\n");
    for (CmdLineArgs opt : CmdLineArgs.values()) {
      if (null == opt.abbrev) {
        if (null != opt.descr) {
          out.append("ARG" + "  \t" + opt.descr + '\n');
        }
        continue;
      }
      out.append((0 >= opt.abbrev.length()) ? "" : ("-" + opt.abbrev.charAt(0) + ", "));
      out.append("--" + opt.name().toLowerCase(Locale.ROOT) + "  \t");
      out.append(opt.descr + '\n');
    }
    return out.toString();
  }

  private static QEnumIf findOp(String cmd, boolean includeInternals) {
    QEnumIf op;
    final String cmdU = StringFunc.toUpperCase(cmd);
    try {
      if (null != (op = QOpMain.valueOf(cmdU))) {
        return op;
      }
    } catch (Exception e0) {
      // NOP
    }
    if (!cmd.equals(cmdU)) {
      try {
        if (null != (op = QOpMain.valueOf(cmd))) {
          return op;
        }
      } catch (Exception e0) {
        // NOP
      }
    }
    if (!includeInternals) {
      return null;
    }
    for (QIfs.QEnumIf[] ens : ops) {
      if (null == ens) {
        break;
      }
      for (QIfs.QEnumIf en : ens) {
        final String name = en.name();
        if (name.equals(cmdU) || name.equals(cmd)) {
          return en;
        }
      }
    }
    return null;
  }

  public static QEnumIf valueOfOr(String cmd, boolean includeInternals, QEnumIf xFallback) {
    if ((null == cmd) || (0 >= cmd.length())) {
      return xFallback;
    }
    QEnumIf funct = findOp(cmd, includeInternals);
    if ((null == funct) && (1 == cmd.length())) {
      final char c0 = cmd.charAt(0);
      int iSym = Arrays.binarySearch(QOpMain.functSymbols, c0);
      if (0 <= iSym) {
        funct = QOpMain.functEnums[iSym];
      }
    }
    if ((null == funct) && includeInternals) {
      funct = QOpMain.opsInternal.get(cmd);
    }
    final String cmdU = StringFunc.toUpperCase(cmd);
    if (null == funct) {
      for (QOpMain fx : QOpMain.values()) {
        for (int i0 = fx.optionals.length - 1; i0 >= 0; --i0) {
          if (fx.optionals[i0].equals(cmdU)) {
            funct = fx;
            break;
          }
        }
      }
    }
    if (null == funct) {
      for (QOpMain fx : QOpMain.values()) {
        final String nam = fx.name();
        if (((3 <= nam.length()) && cmdU.startsWith(nam))
            || ((4 <= cmdU.length()) && nam.startsWith(cmdU))) {
          funct = fx;
          break;
        }
      }
    }
    return (null != funct) ? funct : xFallback;
  }

  // =====
}
