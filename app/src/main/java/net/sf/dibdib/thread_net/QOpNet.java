// Copyright (C) 2022, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_net;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.QSeqIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.*;

public enum QOpNet implements QIfs.QEnumIf {
  INVIT,
  RCV,
  zzACK,
  zzCLR,
  zzLOOP,
  zzSEND,
  ;

  /////

  public static interface MessengerIf {

    public boolean init(String userAddress);

    public boolean inviteOrConfirm(long xhContact, boolean force);

    public String[] receiveMessages(int msecs, boolean looping);

    public String[] sendOrAckMessages(long xhContact, boolean ackOnly, long... xahMsgs);
  }

  /////

  public static QOpNet.MessengerIf messenger = null;
  static boolean messengerStarted = false;

  private static boolean initMessenger(boolean forced) {
    if ((null == messenger)
        || Dib2Root.app.bAllowDummyPass
        || (0 >= Dib2Root.ccmSto.mUserAddr.indexOf("@"))) {
      return false;
    }
    if (messengerStarted && !forced) {
      return true;
    }
    if (forced) {
      Dib2Root.ccmSto.hidden_remove("KEY.0.SIG.ECDSA256.S");
    }
    final String hostAddress =
        StringFunc.string4Utf8(Dib2Root.ccmSto.hidden_get("smtp_server")).substring(5);
    final String user = StringFunc.string4Utf8(Dib2Root.ccmSto.hidden_get("smtp_user"));
    //    final String optImapPort =
    // StringFunc.string4Utf8(Dib2Root.ccmSto.hidden_get("imap_port"));
    //    final String optSmtpPort =
    // StringFunc.string4Utf8(Dib2Root.ccmSto.hidden_get("smtp_port"));
    if ((null != user) && (null != hostAddress)) {
      messenger.init(Dib2Root.ccmSto.mUserAddr); // , hostAddress, user, optImapPort, optSmtpPort);
      messengerStarted = true;
    }
    return messengerStarted;
  }

  private static long[] handles4Seq(QSeq arg) {
    final QIfs.QWordIf[] ah0 = arg.atoms();
    final long[] ahMsgs = new long[ah0.length];
    int c0 = 0;
    for (QIfs.QWordIf hx : ah0) {
      if (hx instanceof QWord) {
        final long h0 = ((QWord) hx).i64();
        if (0 != h0) {
          ahMsgs[c0++] = h0;
        }
      }
    }
    return ahMsgs;
  }

  static QToken exec(QToken task) {
    QSeqIf arg = task.argX;
    switch ((QOpNet) task.op) {
      case INVIT:
        if ((arg instanceof QSeq) && initMessenger(false)) {
          long pid = CcmSto.peek((QSeq) arg, Cats.CONTACT.flag, true);
          if (0 != pid) {
            messenger.inviteOrConfirm(pid, false);
          }
        }
        return null;
      case RCV:
        if (initMessenger(false)) {
          long endTime = DateFunc.currentTimeNanobisLinearized(false);
          endTime += ((QWord) arg).d4() * (1 << 30) * Dib2Constants.INT_D4_F_INV;
          String[] dat = messenger.receiveMessages(5000, false);
          if ((null != dat) && (0 < dat.length)) {
            QToken t0 = QToken.createTask(QOpWk.zzIMPORT);
            t0.wip = dat;
            Dib2Root.schedulerTrigger.trigger(t0);
          }
          if (endTime < DateFunc.currentTimeNanobisLinearized(false)) {
            task.argX = QWord.createQWordInt(endTime);
            task.op = zzLOOP;
            return task;
          }
        }
        return null;
      case zzACK:
        if ((arg instanceof QSeq) && (task.argY instanceof QWord) && initMessenger(false)) {
          String[] dat =
              messenger.sendOrAckMessages(((QWord) task.argY).i64(), true, handles4Seq((QSeq) arg));
          if ((null != dat) && (0 < dat.length)) {
            QToken t0 = QToken.createTask(QOpWk.zzIMPORT);
            t0.wip = dat;
            Dib2Root.schedulerTrigger.trigger(t0);
          }
        }
        return null;
      case zzCLR:
        initMessenger(true);
        return null;
      case zzLOOP:
        if (arg instanceof QWord) {
          long endTime = ((QWord) arg).i64();
          String[] dat = messenger.receiveMessages(5000, true);
          if ((null != dat) && (0 < dat.length)) {
            QToken t0 = QToken.createTask(QOpWk.zzIMPORT);
            t0.wip = dat;
            Dib2Root.schedulerTrigger.trigger(t0);
          }
          endTime -= DateFunc.currentTimeNanobisLinearized(false);
          if ((0 < endTime) && (endTime < (20 * 60 * (1L << 30)))) {
            return task;
          }
        }
        return null;
      case zzSEND:
        if ((arg instanceof QSeq) && (task.argY instanceof QWord) && initMessenger(false)) {
          String[] dat =
              messenger.sendOrAckMessages(
                  ((QWord) task.argY).i64(), false, handles4Seq((QSeq) arg));
          if ((null != dat) && (0 < dat.length)) {
            QToken t0 = QToken.createTask(QOpWk.zzIMPORT);
            t0.wip = dat;
            Dib2Root.schedulerTrigger.trigger(t0);
          }
        }
        return null;
      default:
        break;
    }
    return null;
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }
}
