// Copyright (C) 2016, 2017  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2017  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by NBP.

package com.gitlab.dibdib.dib2qm;

import java.util.*;

public class AccountTypes {
  // =====

  public static AccountType generic =
      new AccountType(
          "GMail, GMX, mail.de etc.",
          "foo.bar@gmail.com",
          null,
          "imap.gmail.com",
          "993",
          "smtp.gmail.com",
          "587");

  public static AccountType mail_de =
      new AccountType(
          "MAIL.DE", "foobar@mail.de", null, "imap.mail.de", "993", "smtp.mail.de", "587");

  public static AccountType web_de =
      new AccountType("WEB.DE", "foobar@web.de", null, "imap.web.de", "993", "smtp.web.de", "587");

  public static AccountType gmx =
      new AccountType("GMX", "foobar@gmx.de", null, "imap.gmx.com", "993", "mail.gmx.com", "587");

  public static AccountType mykolab =
      new AccountType(
          "MyKolab",
          "foobar@mykolab.com",
          null,
          "imap.mykolab.com",
          "993",
          "smtp.mykolab.com",
          "587");

  public static AccountType riseup =
      new AccountType(
          "Riseup",
          "foobar@riseup.net",
          "@riseup.net",
          "mail.riseup.net",
          "143",
          "mail.riseup.net",
          "465");

  public static List<AccountType> account_types =
      new ArrayList<AccountType>(Arrays.asList(generic));

  // =====
}
