// Copyright (C) 2016, 2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import com.gitlab.dibdib.dib2qm.*;
import net.sf.dibdib.thread_any.StringFunc;

public class Pgp {
  // =====

  public static String my_user_id = null;
  protected int miProv = 0;
  /** First one is default for sending. */
  public static PrivProvIf[] privProvs = null; // EcDhQm(), Pgp_1()

  public Pgp(ContextIf_OLD context) {
    // To be set whenever DB is used.
    //  if (null == Dib2Root.qContextQm) {
    //    Dib2Root.qContextQm = app_context;
    //  }
    my_user_id = StringFunc.string4Utf8(context.get("email_address", null));
    for (PrivProvIf pp : Pgp.privProvs) {
      pp.init(context);
    }
  }

  public void setProv(int xiProv) {
    miProv = xiProv;
  }

  public void load_keys() {
    for (PrivProvIf pp : Pgp.privProvs) {
      pp.load_keys();
    }
  }

  public MailAttachment pgpmime_id() {
    return Pgp.privProvs[miProv].pgpmime_id();
  }

  public MailAttachment encrypt_sign(MailAttachment unenc, String to) {
    return Pgp.privProvs[miProv].encrypt_sign(unenc, to);
  }

  public MailAttachment key_attachment(String memberAddr) {
    return Pgp.privProvs[miProv].key_attachment(memberAddr);
  }

  /**
   * @param xInputStream /** @param xyKeyFound /** @return Fallback key value, null if error or if
   *     value has not changed
   */
  public byte[] public_keyring_add_key(byte[] xInputStream, String[] xyKeyFound) {

    /* WRAPPER:
    public boolean public_keyring_add_key(byte[] is, String[] added) {
      Object oldkr = public_keyring_collection;
      added[0] = public_keyring_add_key(new ByteArrayInputStream( is));
      return oldkr != public_keyring_collection;
    }
    */

    byte[] old = Pgp.privProvs[miProv].public_keyring_add_key(xInputStream, xyKeyFound);
    if ((null != old) || ((null != xyKeyFound[0]) && (xyKeyFound[0].contains("@")))) {
      return old;
    }
    for (PrivProvIf pp : Pgp.privProvs) {
      old = pp.public_keyring_add_key(xInputStream, xyKeyFound);
      if (null != old) {
        return old;
      }
    }
    return null;
  }

  public MailAttachment decrypt_verify(MailAttachment attachment) {
    MailAttachment att = Pgp.privProvs[miProv].decrypt_verify(attachment);
    if (null != att) {
      return att;
    }
    for (PrivProvIf pp : Pgp.privProvs) {
      att = pp.decrypt_verify(attachment);
      if (null != att) {
        return att;
      }
    }
    return null;
  }

  public String fingerprint(String addr) {
    return Pgp.privProvs[miProv].fingerprint(addr);
  }

  public void public_keyring_remove_by_address(String addr) {
    for (PrivProvIf pp : Pgp.privProvs) {
      pp.public_keyring_remove_by_address(addr);
    }
  }

  // =====
}
