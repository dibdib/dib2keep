// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.InputType;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.gitlab.dibdib.picked.net.*;
import java.io.*;
import java.util.*;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.thread_any.*;
import net.sourceforge.dibdib.android.dib2qm.R;

public class ListActivity extends com.gitlab.dibdib.dib2qm.ListActivity_1 {
  // =====

  static int hAccessDialog = 0;

  @Override
  public void update_ui() {

    // TODO: move pgp tasks to background thread <== exclude UI's context data
    if (null == pgp) {
      byte[] email = prefs.get("email_address", new byte[0]);
      if ((null != email) && (null != db.contact_get_by_id(1))) {
        if ((null != toast) && (0 == toast.length())) {
          toast = null;
          Dib2Root.log("la receiver", "create pgp");
          create_pgp();
          Dib2Root.log("la receiver", "pgp done");
          db.save();
          LocalMessage.lastMsg = null;
          checkBackground();
        } else {
          Dib2Root.log("la receiver", "toast prepare");
          toast = getString(R.string.toast_keygen);
        }
      }
    }
    update_ui_data(true);
    //  }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //  mMessageReceiver = new BroadcastReceiver() {
    //  ...

    if (null == pgp) {
      int rc = db.load();
      Dib2Root.log("onCreate", "Data loaded: " + rc);
      if (0 > rc) {
        // access_dialog() called by resume().
        return;
      }
      byte[] fn = db.get("flagsNotify", null);
      fn = ((null == fn) || (0 == fn.length)) ? "3".getBytes(StringFunc.CHAR8) : fn;
      //      fn = fn.replaceAll( "[^0-9]", "" );
      fn = ((null == fn) || (0 == fn.length)) ? "3".getBytes(StringFunc.CHAR8) : fn;
      QmBg_0.flagsNotify = fn[0] & 0xf;
      create_pgp();
    }

    //  update_ui_data( true );
    //  checkBackground();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if ((1000 >= initialDialogPassed) && (0 == hAccessDialog)) {
      DateFunc.timeZoneDone = false;
      access_dialog();
    }
  }

  @Override
  protected void onDestroy() {
    isActive = false;
    //  SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( this.getApplication()
    // );
    //  Editor editor = SP.edit();
    //  editor.putString( "accessCodeDate", "0" );
    //  editor.commit();
    super.onDestroy();
  }

  private String[] getData(Cursor cur) {
    String[] out = new String[] {"", "", "", "", "", "", ""};
    for (int i0 = 0; i0 < 7; ++i0) {
      try {
        out[i0] = cur.getString(i0);
      } catch (Exception e) {
      }
      out[i0] = (null == out[i0]) ? "" : out[i0];
    }
    return out;
  }

  @SuppressLint("InflateParams")
  protected void access_dialog() {
    LayoutInflater factory = LayoutInflater.from(this);
    hAccessDialog =
        ((null == db.fileReady()) // false )) //,
                && (1000 <= initialDialogPassed)) // ,
            ? R.layout.passphrase // ,
            : ((1000 <= initialDialogPassed) ? R.layout.passphrase1 : R.layout.passphrase2);
    Dib2Root.log("ac", "dialog " + hAccessDialog);
    final View textEntryView = factory.inflate(hAccessDialog, null);
    final ScrollView scroll_view = new ScrollView(this);
    final EditText input_code = (EditText) textEntryView.findViewById(R.id.pass_phrase);
    final EditText input_pass =
        (R.layout.passphrase2 != hAccessDialog)
            ? null // ,
            : (EditText) textEntryView.findViewById(R.id.gmail_pass);
    input_code.setText("", TextView.BufferType.EDITABLE);
    if (null != input_pass) {
      input_pass.setText("", TextView.BufferType.EDITABLE);
    }
    final AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.action_warn_settings);
    scroll_view.addView(textEntryView);
    alert.setView(scroll_view);
    alert.setInverseBackgroundForced(true);
    //  final Intent intent = new Intent( this,
    // net.sourceforge.dibdib.android.dib2qm.SettingsActivity.class );
    alert.setPositiveButton(
        "Ok",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            hAccessDialog = 0;
            String code = input_code.getText().toString().trim();
            // Enforce dummy value if needed:
            code = (0 < code.length()) ? code : "0";
            initialDialogPassed = DateFunc.currentTimeMillisLinearized();
            boolean ok = false;
            if (null == pgp) {
              TcvCodec.instance.setAccessCode(StringFunc.bytesUtf8(code));
              if (null != input_pass) {
                String pass = input_pass.getText().toString().trim();
                // Enforce dummy value if needed:
                pass = (0 < pass.length()) ? pass : "0";
                TcvCodec.instance.settleHexPhrase(
                    StringFunc.hexUtf8(pass, false)); // db.setCombinedPhrase( pass );
              }
              Dib2Root.log("dx", "loading");
              if (0 < db.load(true)) {
                create_pgp();
                ok = true;
              } else if (null == db.fileReady()) { // false )) {
                warn_settings_dialog();
                return;
              }
            } else {
              ok = TcvCodec.instance.checkAccessCode(StringFunc.bytesUtf8(code));
            }
            if (ok) {
              update_ui_data(true);
              LocalMessage.send_statusMsg(getApplicationContext(), "?");
              checkBackground();
              db.export_db(false);
              return;
            }
            initialDialogPassed = 1;
            access_dialog();
          }
        });

    alert.setNegativeButton(
        (null != input_pass) ? "Clear" : "Stop",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            hAccessDialog = 0;
            String code = input_code.getText().toString().trim();
            if ((null == input_pass) || (0 >= code.length())) {
              finish();
            } else {
              initialDialogPassed = DateFunc.currentTimeMillisLinearized();
              TcvCodec.instance.setAccessCode(StringFunc.bytesUtf8(code));
              String pass = input_pass.getText().toString().trim();
              if (0 < pass.length()) {
                TcvCodec.instance.settleHexPhrase(
                    StringFunc.hexUtf8(pass, false)); // db.setCombinedPhrase( pass );
                db.load(true);
              }
              warn_settings_dialog();
            }
          }
        });

    alert.setCancelable(false);
    initialDialogPassed = 2;
    alert.show();
  }

  public boolean importContacts() {
    String[] projection =
        new String[] {
          ContactsContract.Data.CONTACT_ID,
          ContactsContract.Data.DISPLAY_NAME,
          ContactsContract.Data.MIMETYPE,
          ContactsContract.Data.DATA1,
        };
    Cursor cur =
        getContentResolver()
            .query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                ContactsContract.Data.MIMETYPE
                    + "=? OR " // -
                    + ContactsContract.Data.MIMETYPE
                    + "=? OR " // -
                    + ContactsContract.Data.MIMETYPE
                    + "=? OR " // -
                    + ContactsContract.Data.MIMETYPE
                    + "=? OR " // -
                    + ContactsContract.Data.MIMETYPE
                    + "=?",
                new String[] {
                  ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                  ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                  ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE,
                  ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE,
                  ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE,
                },
                ContactsContract.Data.CONTACT_ID);
    boolean found = false;
    HashMap<Long, QmDbContact> map = new HashMap<Long, QmDbContact>();
    long time_old = DateFunc.currentTimeMillisLinearized() - 365 * 24 * 3600 * 1000;
    time_old = (0 >= time_old) ? 42000 : time_old;
    while (cur.moveToNext()) {
      long id = cur.getLong(0);
      String[] dat = getData(cur);
      QmDbContact con = map.get(id);
      if (null == con) {
        time_old += 10;
        con = new QmDbContact();
        con.type_set(QmDbContact.TYPE_PERSON);
        con.time_lastact_set(time_old);
        map.put(id, con);
      }
      if (ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE.equals(dat[2])) {
        con.name_set(dat[1]);
        con.phone_set((con.phone_get() + " " + dat[3].replaceAll("[^0-9\\-\\+]", "")).trim());
      } else if (ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE.equals(dat[2])) {
        String email = con.address_get();
        con.address_set((((email != null) ? (email + " ") : "") + dat[3].replace(" ", "")).trim());
        if (con.name_get().isEmpty()) {
          con.name_set(dat[1]);
        }
      } else if (ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE.equals(dat[2])) {
        con.notes_set((con.notes_get() + " /Q/ " + dat[3]).trim());
        if (con.name_get().isEmpty()) {
          con.name_set(dat[1]);
        }
        if (con.name_get().isEmpty()) {
          con.name_set(dat[3]);
        }
      } else if (ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE.equals(dat[2])) {
        con.notes_set((con.notes_get() + " /N/ " + dat[3]).trim());
      } else if (ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE.equals(dat[2])) {
        con.notes_set((con.notes_get() + " /W/ " + dat[3]).trim());
      }
    }
    projection =
        new String[] {
          ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID,
          ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME,
          ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY,
          ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE,
          ContactsContract.CommonDataKinds.StructuredPostal.CITY,
          ContactsContract.CommonDataKinds.StructuredPostal.STREET,
          ContactsContract.CommonDataKinds.StructuredPostal.POBOX,
        };
    cur =
        getContentResolver()
            .query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                ContactsContract.Data.MIMETYPE + "=?",
                new String[] {
                  ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE,
                },
                ContactsContract.Data.CONTACT_ID);
    while (cur.moveToNext()) {
      long id = cur.getLong(0);
      String[] dat = getData(cur);
      QmDbContact con = map.get(id);
      if (null == con) {
        time_old += 10;
        con = new QmDbContact();
        con.type_set(QmDbContact.TYPE_PERSON);
        con.time_lastact_set(time_old);
        map.put(id, con);
      }
      con.notes_set(
          (con.notes_get() // -
                  + ((0 >= dat[5].length()) ? "" : " /S/ ")
                  + dat[5] // -
                  + ((0 >= dat[4].length()) ? "" : " /C/ ")
                  + dat[4] // -
                  + ((0 >= dat[3].length()) ? "" : " /Z/ ")
                  + dat[3] // -
                  + ((0 >= dat[2].length()) ? "" : " /Y/ ")
                  + dat[2] // -
                  + ((0 >= dat[6].length()) ? "" : " /B/ ")
                  + dat[6] // -
              )
              .trim());
      if (con.name_get().isEmpty()) {
        con.name_set(dat[1]);
      }
    }
    for (QmDbContact ct : map.values()) {
      String name = ct.name_get();
      String email = ct.address_get();
      email = (null == email) ? "" : email;
      if (0 >= name.length()) {
        if (0 >= email.length()) {
          continue;
        }
        name = email;
      }
      found = true;
      QmDbContact con = db.contact_get_by_name(name);
      if (null == con) {
        int split = email.indexOf(' ');
        if (0 < split) {
          String note = "/E/ " + email.substring(split + 1);
          email = email.substring(0, split);
          ct.notes_set((note + " " + ct.notes_get()).trim());
        }
        ct.address_set((0 >= email.length()) ? (name.replace(" ", "") + "@x.x") : email);
        db.contact_add(ct);
      } else if (QmDbContact.TYPE_PERSON == con.type_get()) {
        String phone = con.phone_get();
        phone = (phone + " " + ct.phone_get().replace(phone, "")).trim();
        if (!phone.isEmpty()) {
          con.phone_set(phone);
        }
        email = email.isEmpty() ? "" : (email + " / ");
        email = ((null == con.address_get()) || email.contains(con.address_get())) ? "" : email;
        String notes = email + ct.notes_get();
        if ((3 < notes.length()) && con.notes_get().isEmpty()) {
          notes = notes.replace("\t", "  ").replace("\n", " / ");
          con.notes_set(notes);
        }
        db.contact_update(con);
      }
    }
    return found;
  }

  void import_dialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.action_import);
    alert.setMessage(R.string.dialog_import);
    final EditText input = new EditText(this);
    input.setInputType(InputType.TYPE_CLASS_TEXT);
    alert.setView(input);

    alert.setPositiveButton(
        "Ok",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            String value = input.getText().toString();
            db.import_db(value);
            pgp.load_keys();
            update_ui_data(true);
          }
        });

    alert.setNegativeButton(
        "Stop",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {}
        });

    alert.show();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.action_import_contacts:
        {
          boolean ok = importContacts();
          if (ok) {
            update_ui_data(true);
          }
          Toast.makeText(this, ok ? "OKAY" : "ERROR", Toast.LENGTH_SHORT).show();
          return true;
        }

      case R.id.action_import:
        {
          import_dialog();
          return true;
        }

      case R.id.action_archive:
        {
          if (pgp == null) return true;
          AlertDialog.Builder alert = new AlertDialog.Builder(this);
          alert.setTitle(R.string.action_archive);
          alert.setMessage(R.string.dialog_archive);

          alert.setPositiveButton(
              "Ok",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                  // prefs as former master: prefs.pref2db();
                  pgp.load_keys();
                  db.archive_old_messages();
                }
              });

          alert.setNegativeButton(
              "Stop",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {}
              });

          alert.show();
          return true;
        }

      case R.id.action_export:
        {
          if (pgp == null) return true;
          pgp.load_keys();
          db.export_db(true);
          return true;
        }

      case R.id.action_sync:
        {
          net.sourceforge.dibdib.android_qm.background.setOffline(-1);
          LocalMessage.lastMsg = "Enabled";
          LocalMessage.lastMsgColor = 'Y';
          prefs.set("offline", "FALSE");
          LocalMessage.send_connection(getApplicationContext(), false);
          LocalMessage.send_background(getApplicationContext(), true);
          return true;
        }

      case R.id.action_stop:
        {
          net.sourceforge.dibdib.android_qm.background.setOffline(2);
          LocalMessage.lastMsg = null;
          LocalMessage.lastMsgColor = 'R';
          prefs.set("offline", "TRUE");
          LocalMessage.send_background(getApplicationContext(), false);
          return true;
        }

      case R.id.action_temp:
        {
          net.sourceforge.dibdib.android_qm.background.setOffline(0);
          LocalMessage.lastMsg = null;
          LocalMessage.lastMsgColor = 'Y';
          prefs.set("offline", "0");
          LocalMessage.send_background(getApplicationContext(), true);
          return true;
        }

      case R.id.action_save_ac:
        {
          db.preference_set("save_ac", new byte[] {(byte) 1});
          TcvCodec.instance.setAccessCode(null);
          return true;
        }

      case R.id.action_sort_name:
        {
          db.setByNames(true);
          update_ui_data(true);
          return true;
        }

      case R.id.action_reset_settings:
        {
          db.preference_set("save_ac", new byte[0]);
          TcvCodec.instance.setAccessCode(null);
          db.setByNames(false);
          update_ui_data(true);
          checkBackground();
          return true;
        }

      case R.id.action_show_help:
        {
          String help = "";
          AssetManager am = getAssets();
          InputStream is;
          try {
            is = am.open("help.txt");
            byte[] buffer = new byte[7000];
            int len = 0;
            while (true) {
              len += is.read(buffer, len, buffer.length - len);
              if (len < (buffer.length)) {
                break;
              }
              buffer = Arrays.copyOf(buffer, buffer.length * 2);
            }
            is.close();
            help += new String(Arrays.copyOf(buffer, len), "UTF-8");
          } catch (IOException e) {
            Log.e("show help", e.getMessage());
          }
          AlertDialog.Builder alert = new AlertDialog.Builder(this);
          String log = MiscFunc.logBuffer.toString();
          log = log.replaceAll("\\n[^\\n]+updat[^\\n]+\\n", "\n");
          log = (3200 < log.length()) ? ("..." + log.substring(log.length() - 3100)) : log;
          alert.setMessage(help + "\n\n=====\n LOG\n=====\n\n" + log);
          alert.setTitle(R.string.action_show_help);
          alert.setPositiveButton("OK", null);
          alert.setCancelable(true);
          alert.create().show();
          return true;
        }

      default:
        ;
    }
    return super.onOptionsItemSelected(item);
  }

  // =====
}
