// Copyright (C) 2017, 2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.content.*;
import android.os.Handler;
import net.sf.dibdib.config.*;
import net.sourceforge.dibdib.android_qm.background;

public class LocalMessage {
  // =====

  public static int usingOriginalThrowNull = 0;

  public static String lastMsg = null;
  public static char lastMsgColor = 'Y'; // Green/Yellow/Red

  public static QuickmsgActivity_add zUiActivity = null;
  public static Handler zUiHandler = null;

  /** @param ctx Context /** @param msg 'T' connected, 'F' disconnected, 'E' severe error */
  public static void send_statusMsg(Context ctx, String message) {
    if ((null != message) && (2 < message.length())) {
      Dib2Root.log("locMsg", message);
    }
    boolean ok = (null != Mail_1.mail_folder); // isOpen() vs idle()
    message = ((null == message) || (0 >= message.length())) ? (ok ? "T" : "F") : message;
    ok = ('?' == message.charAt(0)) ? ok : ('T' == message.charAt(0));
    if (message.startsWith("E")) {
      background.setOffline(1);
      message = (2 >= message.length()) ? ("e" + Dib2Constants.ERROR_Str) : message;
    }
    int offline = background.isOffline();
    message = message.substring(1);
    if (1 <= offline) {
      lastMsgColor = 'R';
      if (2 <= offline) {
        lastMsg = null;
        message = "";
      }
    } else if (ok && (0 > offline)) {
      if ('G' != lastMsgColor) {
        // Let warnings stay visible, e.g. for key changes:
        lastMsg = ('R' == lastMsgColor) ? null : lastMsg;
        lastMsgColor = 'G';
      }
    } else {
      if ('G' == lastMsgColor) {
        message = (2 >= message.length()) ? "Loop" : message;
      }
      lastMsgColor = 'Y';
    }
    if (null != lastMsg) {
      lastMsg = lastMsg.replace("  ", " ").replace("/ ", "... ").replace(". .", "..");
    }
    if (null == lastMsg) {
      lastMsg = (2 >= message.length()) ? null : message;
    } else if (ok && lastMsg.contains("O") && (lastMsg.contains("ON") || lastMsg.contains("On."))) {
      // 'ON.' is for idle(), 'On.' for others.
      lastMsg = lastMsg.replace("ON", ".. ").replace("On.", ".. ");
      if (2 <= message.length()) {
        lastMsg = lastMsg.substring(lastMsg.indexOf("..") + 2) + "/ " + message;
      } else {
        lastMsg += ".On.";
      }
    } else if (1 < message.length()) {
      if (lastMsg.contains(message)) {
        lastMsg = lastMsg.replace(message, "... ");
        lastMsg = lastMsg.substring(lastMsg.indexOf("..") + 2) + "/ " + message;
      } else {
        lastMsg += "/ " + message;
      }
    } else {
      lastMsg += ok ? ".On." : ".";
    }
    if (null != lastMsg) {
      lastMsg = lastMsg.replace(".....", "...").replace("....", "...");
      lastMsg =
          (95 < lastMsg.length()) ? (".." + lastMsg.substring(lastMsg.length() - 80)) : lastMsg;
    }
    if ((zUiHandler != null) && (zUiActivity != null) && (zUiActivity.doUi != null)) {
      zUiHandler.post(zUiActivity.doUi);
    }
  }

  public static void send_background(Context ctx, boolean cmd) {
    Intent bg;
    bg = new Intent(ctx, net.sourceforge.dibdib.android_qm.background.class);
    // 'T' network okay, 'F' no network, 't' try to connect.
    bg.putExtra("state", cmd ? "t" : "f");
    ctx.startService(bg);
  }

  public static void send_connection(Context ctx, boolean stat) {
    send_statusMsg(ctx, stat ? "T" : "F");
  }

  // =====
}
