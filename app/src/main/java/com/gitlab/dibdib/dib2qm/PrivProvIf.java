// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG, based on the
// corresponding API from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import com.gitlab.dibdib.dib2qm.*;

public interface PrivProvIf {
  // =====

  public void init(ContextIf_OLD app_context);

  public MailAttachment pgpmime_id();

  public MailAttachment encrypt_sign(MailAttachment unenc, String to);

  public MailAttachment key_attachment(String memberAddr);

  public boolean load_keys();

  /**
   * @param xInputStream /** @param xyKeyFound /** @return Fallback key value, null if error or if
   *     value has not changed
   */
  public byte[] public_keyring_add_key(byte[] xInputStream, String[] xyKeyFound);

  public MailAttachment decrypt_verify(MailAttachment attachment);

  public String fingerprint(String addr);

  public void public_keyring_remove_by_address(String addr);

  // =====
}
