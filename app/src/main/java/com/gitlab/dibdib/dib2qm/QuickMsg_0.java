/*
    QuickMSG
    Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// ... with small adaptations for Dib2QM
// Formatted by GJF.

package com.gitlab.dibdib.dib2qm;

import android.content.*;
import android.net.Uri;
import android.util.Log;
import com.gitlab.dibdib.dib2qm.*;
import java.io.*;
import java.util.*;
import javax.activation.*;
import javax.mail.util.ByteArrayDataSource;
import net.sourceforge.dibdib.android_qm.background;

@SuppressWarnings("static-access")
public class QuickMsg_0 {

  public Boolean is_post = false;
  public Boolean is_grouppost = false;
  public Boolean is_group = false;
  QmDbMessage _msg = new QmDbMessage();
  QmDbContact _contact = new QmDbContact();
  public int group_id = 0;
  public String group_owner = "";

  public QuickMsg_0() {
    _contact.type_set(_contact.TYPE_PERSON);
  }

  public MailAttachment send_message(Context context, QmDbContact from, QmDbContact to, QmDbMessage message) {
    // Log.d("message", "Send message");
    String msg = "";

    msg += "Action: post\n";
    msg += "Name: " + from.name_get() + "\n";
    if (to.type_get() == to.TYPE_GROUP) {
      msg += "Group: " + to.group_get() + "\n";
      msg += "Owner: " + to.address_get() + "\n";
      // Log.d("send message", "Send to group " + to.group_get());
    }
    msg += "Time: " + message.time_get() /*rho*/ / 1000 + "\n";
    msg += "\n";
    msg += message.text_get();

    DataSource ds;
    try {
      ds = new ByteArrayDataSource(msg, "application/quickmsg");
    } catch (IOException e) {
      Log.e("message", "new ds: " + e.getMessage());
      return null;
    }
    MailAttachment a = new MailAttachment();
    a.datahandler = new DataHandler(ds);
    a.name = "QuickMSG";
    List<MailAttachment> as = new LinkedList<MailAttachment>();

    as.add(a);

    Uri uri = (Uri) message.uri_get();
    if (uri != null) {
      // Log.d("quickmsg", "send attachment: " + uri.toString());

      ContentResolver cR = context.getContentResolver();
      InputStream is;
      try {
        is = cR.openInputStream(uri);
      } catch (FileNotFoundException e) {
        // Log.d("send_message", e.getMessage());
        return null;
      }
      String type = cR.getType(uri);

      a = new MailAttachment();
      try {
        a.datahandler = new DataHandler(new ByteArrayDataSource(is, type));
      } catch (IOException e) {
        // Log.d("send_message", e.getMessage());
        return null;
      }
      List<String> names = uri.getPathSegments();
      a.name = names.get(names.size() - 1);

      as.add(a);
    }

    // mail m = new mail();

    // return m.multipart_create(as);
    return background.mail.multipart_create(as);
  }

  public MailAttachment send_group(QmDbContact group, Pgp pgp, QmDb_0 db) {
    String msg = "";
    if (group.type_get() != group.TYPE_GROUP) return null;

    msg += "Action: group\n";
    msg += "Name: " + group.name_get() + "\n";
    msg += "Group: " + group.group_get() + "\n";
    msg += "Owner: " + group.address_get() + "\n";
    msg += "Members: " + group.members_get_string() + "\n";
    msg += "\n";

    DataSource ds;
    try {
      ds = new ByteArrayDataSource(msg, "application/quickmsg");
    } catch (IOException e) {
      Log.e("message", "new ds: " + e.getMessage());
      return null;
    }
    MailAttachment a = new MailAttachment();
    a.datahandler = new DataHandler(ds);
    a.name = "QuickMSG";
    List<MailAttachment> as = new LinkedList<MailAttachment>();

    as.add(a);

    List<String> members = group.members_get();

    for (int i = 0; i < members.size(); i++) {
      String member = members.get(i);
      // rho
      if (member.equals(Pgp.my_user_id)) {
        continue;
      }
      MailAttachment ak = pgp.key_attachment(member);
      if (ak != null) as.add(ak);
    }

    // mail m = new mail();

    // return m.multipart_create(as);
    return background.mail.multipart_create(as);
  }

  public void parse_attachment(MailAttachment a) {
    InputStream is;
    try {
      is = a.datahandler.getInputStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      String line;
      while ((line = br.readLine()) != null) {
        if (line.length() == 0) {
          break;
        }
        // Log.d("parse_attachment", line);

        String[] splitted = line.split(": ");
        String field = splitted[0];
        String value = splitted[1];

        if (field.equals("Action")) {
          if (value.equals("post")) {
            is_post = true;
          }
          if (value.equals("group")) {
            is_group = true;
            // Date now = new Date();
            // long time_now = now.getTime() / 1000;

            _contact.time_lastact_set(); // time_now);
          }
        }
        if (field.equals("Members")) {
          _contact.members_set(value);
        }
        if (field.equals("Group")) {
          group_id = Integer.parseInt(value);

          is_grouppost = is_post;

          _contact.type_set(_contact.TYPE_GROUP);
          _contact.group_set(group_id);
        }
        if (field.equals("Owner")) {
          group_owner = value;
        }
        if (field.equals("Name")) {
          _contact.name_set(value);
        }
        if (field.equals("Time")) {
          _msg.time_set(Long.parseLong(value) /*rho*/ * 1000);
        }
      }
      if (is_post) {
        while ((line = br.readLine()) != null) {
          _msg.text_set(_msg.text_get() + "\n" + line);
        }

        return;
      }
    } catch (Exception e) {
      Log.e("parse attachment", e.getMessage());
      return;
    }

    return;
  }

  public QmDbContact get_contact() {
    return _contact;
  }

  public QmDbMessage get_message() {
    if (is_post) return _msg;
    else return null;
  }

  public int get_group() {
    return group_id;
  }
}
