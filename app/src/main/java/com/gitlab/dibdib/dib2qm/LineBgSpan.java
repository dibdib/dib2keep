// package net.vreeken.quickmsg;

// ... with adaptations for Dib2QM.
// Formatted by GJF.

package com.gitlab.dibdib.dib2qm;

import android.graphics.*;
import android.graphics.Paint.FontMetricsInt;
import android.text.style.LineBackgroundSpan;

public class LineBgSpan implements LineBackgroundSpan {
  private final int color;
  private final int bordercolor;
  private final Boolean op;
  private int min = Integer.MAX_VALUE;

  public LineBgSpan(int color, int border_color, Boolean oposite) {
    this.color = color;
    this.bordercolor = border_color;
    this.op = oposite;
  }

  private static final int PADDING = 20;
  int zWidth = 10;

  public int getSize(Paint paint, CharSequence text, int start, int end, FontMetricsInt fm) {
    zWidth = (int) paint.measureText(text, start, end);
    return zWidth + PADDING;
  }

  public void draw(
      Canvas canvas,
      CharSequence text,
      int start,
      int end,
      float x,
      int top,
      int y,
      int bottom,
      Paint paint) {
    final int paintColor = paint.getColor();
    x += PADDING / 2;
    int right = (int) (x + zWidth + PADDING / 2);
    paint.setColor(color);
    canvas.drawRect(x, top, right, bottom, paint);
    paint.setColor(bordercolor);
    canvas.drawLine(right - 1, top, right - 1, bottom, paint);
    canvas.drawLine(x, bottom + 1, right, bottom + 1, paint);
    paint.setColor(paintColor);
    // int y2 = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
    canvas.drawText(text, start, end, x, y, paint);
  }

  @Override
  public void drawBackground(
      Canvas c,
      Paint p,
      int left,
      int right,
      int top,
      int baseline,
      int bottom,
      CharSequence text,
      int start,
      int end,
      int lnum) {
    final int paintColor = p.getColor();

    p.setColor(color);
    c.drawRect(new Rect(left, top, right, bottom), p);
    p.setColor(bordercolor);
    if (op) {
      c.drawLine(right - 1, top, right - 1, bottom, p);
      c.drawLine(left, bottom + 1, right, bottom + 1, p);
    } else {
      c.drawLine(left, top, left, bottom, p);
      if (start <= min) {
        c.drawLine(left, top, right, top, p);
        min = start;
      }
    }
    p.setColor(paintColor);
  }
}
