// Copyright (C) 2016  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import net.sf.dibdib.thread_any.StringFunc;

public class AccountType {
  // =====

  String _name;
  String _username_help;
  String _username_remove; /* what should be removed from address to get username*/
  String _imap_server;
  String _smtp_server;
  String _imap_port;
  String _smtp_port;

  public AccountType(
      String name,
      String username_help,
      String username_remove,
      String imap_server,
      String imap_port,
      String smtp_server,
      String smtp_port) {
    _name = name;
    _username_help = username_help;
    _username_remove = username_remove;
    _imap_server = imap_server;
    _imap_port = imap_port;
    _smtp_server = smtp_server;
    _smtp_port = smtp_port;
  }

  public String name_get() {
    return _name;
  }

  private static void set(ContextIf_OLD preferences, String key, String value) {
    preferences.set(key, StringFunc.bytesUtf8(value));
  }

  public void preferences_set(ContextIf_OLD preferences, String name, String address, String pass) {
    String username;

    if (_username_remove != null) {
      username = address.replace(_username_remove, "");
    } else username = address;

    set(preferences, "display_name", name);
    set(preferences, "email_address", address);
    set(preferences, "imap_user", username);
    set(preferences, "smtp_user", username);
    set(preferences, "imap_pass", pass);
    set(preferences, "smtp_pass", pass);

    set(preferences, "imap_server", _imap_server);
    set(preferences, "imap_port", _imap_port);
    set(preferences, "smtp_server", _smtp_server);
    set(preferences, "smtp_port", _smtp_port);
    //  preferences.db2pref();
  }

  // =====
}
