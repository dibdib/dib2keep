// Copyright (C) 2016  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2018  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.*;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.*;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.gitlab.dibdib.dib2qm.*;
import com.gitlab.dibdib.picked.net.*;
import java.io.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android_qm.*;

// Due to old naming of classes.
@SuppressWarnings("static-access")
public class ListActivity_1 extends QuickmsgActivity_1 {
  // =====

  public static final int SETTINGS_DONE = 1;

  // protected BroadcastReceiver mMessageReceiver;

  protected void settings_set_account(
      AccountType account, String name, String address, String pass) {
    account.preferences_set( // ,
        db, // new preferences(getBaseContext()),
        name, address, pass);
    db.db2pref();
    //  settings_done();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Dib2Root.log("listactivity onActivityResult", "" + requestCode + " " + resultCode);
    if (requestCode == SETTINGS_DONE) {
      //    Dib2Root.log( "listactivity", "requestCode == SETTINGS_DONE" );
      settings_done();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    //  this.unregisterReceiver( this.connection_receiver );
    //  LocalBroadcastManager.getInstance( this ).unregisterReceiver( mMessageReceiver );
    //  unregisterReceiver( mMessageReceiver );
  }

  @Override
  protected void onResume() {
    super.onResume();
    //  LocalBroadcastManager.getInstance( this ).registerReceiver( mMessageReceiver,
    //  registerReceiver( mMessageReceiver, //,
    //    new IntentFilter( "local_message" ) );
    update_ui_data(true);
    checkBackground();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);
    Dib2Root.log("listactivity onCreate", "Version: " + version_get());
  }

  protected void create_pgp() {
    Dib2Root.log(
        "listactivity create_pgp",
        "Create pgp: " + new String(prefs.get("email_address", "x".getBytes(StringFunc.CHAR8))));
    pgp = new Pgp(db); // getApplicationContext() );
    TcvCodec.instance.settleHexPhrase(null);
    //  db.preference_set( "imap_pass", value )
    pgp.load_keys();
  }

  protected void create_pgp(String email_address) {}

  public void update_ui_data(boolean contacts) {
    if ((null != toast) && (0 == toast.length())) {
      Dib2Root.log("listactivity update_ui", "?toast?");
      return;
    }
    int offline = Background_1.isOffline();
    if ('G' != LocalMessage.lastMsgColor) {
      //    MiscFunc.keepLog_OLD( "listactivity update_ui", "status: " + background_1.flagsNotify
      //      + isActive + " /" + offline + "/ " + local_message.lastMsg);
    }
    TextView textview = (TextView) findViewById(R.id.connection_status);
    String txt = getString(R.string.status_connection) + ": ";
    if (('G' == LocalMessage.lastMsgColor) && (1 <= offline)) {
      LocalMessage.lastMsgColor = 'Y';
    }
    char color = LocalMessage.lastMsgColor;
    if (toast != null) {
      txt = toast;
      color = 'Y';
    } else if ((null == LocalMessage.lastMsg) || (1 >= LocalMessage.lastMsg.length())) {
      txt +=
          getString(
              ('G' == LocalMessage.lastMsgColor)
                  ? R.string.status_connected // ,
                  : (('R' == LocalMessage.lastMsgColor)
                      ? R.string.status_stopped
                      : R.string.status_disconnected));
    } else {
      txt += LocalMessage.lastMsg;
    }
    if (contacts) {
      view_contacts();
    }
    textview.setText(txt);
    textview.setTextColor(
        (color == 'G')
            ? Color.parseColor("#004400") // ,
            : ((color == 'R') ? Color.parseColor("#990000") : Color.parseColor("#999900")));
    if (null != toast) {
      Dib2Root.log("listactivity update_ui", "toast");
      Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
      checkBackground();
    }
  }

  @Override
  public void update_ui() {
    //  Dib2Root.log( "listactivity update_ui", "main" );
    update_ui_data(true);
  }

  public void view_contacts() {
    ListView listview = (ListView) findViewById(R.id.contact_list);
    ArrayAdapter<Spanned> contacts_adapter;
    ArrayList<Spanned> contacts_list;
    contacts_list = new ArrayList<Spanned>();

    final List<QmDbContact> contacts = db.contact_get_all();

    contacts_list.clear();

    for (int i = 0; i < contacts.size(); i++) {
      long time_lastact;
      Spanned view_span;
      QmDbContact contact = contacts.get(i);
      String view_html;
      long unread = 0;

      time_lastact = contact.time_lastact_get();

      String name_start = "";
      String name_end = "";

      if ((contact.time_lastact_get() > contact.unread_get()) && (1 < contact.id_get())) {
        name_start = "<b>";
        name_end = "</b>";
        unread = db.message_get_unread_by_chat(contact.id_get(), contact.unread_get());
      }

      view_html = "<font ";
      if (contact.type_get() == QmDbContact.TYPE_PERSON) {
        // Dib2Root.log( "list", "person" );
        switch (contact.keystat_get()) {
          case QmDbContact.KEYSTAT_SENT:
            // Dib2Root.log( "list", "keystat none" );
            view_html += "color='#990000'";
            break;
          case QmDbContact.KEYSTAT_PENDING:
            view_html += "color='#005555'";
            break;
          case QmDbContact.KEYSTAT_RECEIVED:
            // Dib2Root.log( "list", "keystat received" );
            view_html += "color='#cc6600'";
            break;
          case QmDbContact.KEYSTAT_CONFIRMED:
          case QmDbContact.KEYSTAT_VERIFIED:
            // Dib2Root.log( "list", "keystat verified" );
            view_html += "color='#005500'";
            break;
          default:
            break;
        }
      } else {
        view_html += "color='#000077'";
      }
      view_html += ">";
      String name = contact.name_get();
      name = (1 >= contact.id_get()) ? ("= (" + name + ')') : name;
      view_html += name_start + name + name_end + " <small>- "; // "<br><small>";
      if (contact.type_get() == QmDbContact.TYPE_GROUP) {
        String owner = contact.address_get();
        List<String> members = contact.members_get();
        if (null != members) {
          for (int j = 0; j < members.size(); j++) {
            String member = members.get(j);
            List<QmDbContact> names = db.contact_get_by_address(member);
            String nam =
                ((null != names) && (0 < names.size())) // -
                    ? names.get(0).name_get()
                    : member;
            if (member.equals(owner)) {
              view_html += "<i>" + nam + "</i> ";
            } else {
              view_html += nam.replaceFirst("@.*", "..") + " ";
            }
          }
        }
      } else {
        String phone = contact.phone_get();
        phone = (0 < phone.indexOf(' ')) ? phone.substring(0, phone.indexOf(' ')) : phone;
        view_html += "<i>" + contact.address_get() + " - " + phone + "</i> ";
      }
      //      view_html += "<br>";
      view_html +=
          "<br>"
              + ((contact.notes_get().length() >= 30)
                  ? (contact.notes_get().substring(0, 25) + "...")
                  : contact.notes_get());
      view_html += (contact.notes_get().length() > 0) ? "<br>" : "";
      String timeInfo = DateFunc.dateShort4Millis(time_lastact, -3);
      int diff = findTimeDiff(timeInfo);
      view_html +=
          timeInfo.substring(0, diff)
              + "<b>"
              + timeInfo.substring(diff, diff + 2)
              + "</b>"
              + timeInfo.substring(diff + 2); // dateformat.format(lastact);
      view_html += "</small></font>";

      /* add number of unread messages */
      if (unread > 0) {
        view_html += "<small><font color='#00cc00'> (" + unread + ")</font></small>";
      }

      view_span = Html.fromHtml(view_html);

      contacts_list.add(view_span);
    }

    contacts_adapter =
        new ArrayAdapter<Spanned>(this, android.R.layout.simple_list_item_1, contacts_list);

    listview.setAdapter(contacts_adapter);
    contacts_adapter.notifyDataSetChanged();
    listview.setAdapter(contacts_adapter);
    listview.setOnItemClickListener(list_OnItemClickListener);
    listview.setOnItemLongClickListener(list_OnItemLongClickListener);
  }

  protected void settings_done() {
    SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(this);
    //  String notify = preferences.get( "notifications_new_message" );
    Boolean notifyL = SP.getBoolean("notifications_new_message_led", true);
    Boolean notifyV = SP.getBoolean("notifications_new_message_vibrate", true);
    Boolean notifyS = SP.getBoolean("notifications_new_message_ringtone", false);
    QmBg_0.flagsNotify =
        (notifyL ? 1 : 0)
            + (notifyV ? 2 : 0)
            + (notifyS ? 4 : 0); // ((null == notify) || !notify.toUpperCase().contains( "F" ));
    prefs.set("flagsNotify", "" + QmBg_0.flagsNotify);
    db.pref2db();

    String email_address = prefs.getLiteral("email_address", "");
    Dib2Root.log(
        "listactivity settings_done",
        "pgp "
            + email_address
            + " "
            + (null != pgp)
            + " "
            + QmBg_0.flagsNotify
            + " "
            + email_address);
    //  if (null == pgp) {
    if (0 >= email_address.length()) {
      warn_settings_dialog();
      return;
    }

    QmDbContact me_org = db.contact_get_by_id(1);
    QmDbContact me;

    if (me_org != null) {
      Dib2Root.log("listactivity settings done", "contact with id 1 exists: " + me_org.id_get());
      me = me_org;
    } else {
      Dib2Root.log("listactivity settings done", "no contact with id 1 yet");
      me = new QmDbContact();
    }
    me.id_set(1);
    me.name_set(prefs.getLiteral("display_name", email_address));
    me.address_set(email_address);
    //  long time_now = DateFunc.currentTimeMillisLinearized();
    me.type_set(QmDbContact.TYPE_PERSON);
    me.time_lastact_set(); // time_now );
    me.keystat_set(QmDbContact.KEYSTAT_VERIFIED);
    if (me_org == null) {
      db.contact_add(me);
    } else {
      db.contact_update(me);
      TcvCodec.instance.settleHexPhrase(null);
      db.save();
      checkBackground();
    }
  }

  public void add_contact(String contact_address) {
    boolean local = contact_address.indexOf('@') < 0;

    /* Don't add ourself */
    if (contact_address.equals(pgp.my_user_id)) return;
    if (!local) {
      if (!android.util.Patterns.EMAIL_ADDRESS.matcher(contact_address).matches()) {
        Dib2Root.log("listactivity add", contact_address + " no match");
        return;
      }
    }

    QmDbContact con = new QmDbContact();
    con.address_set(contact_address);
    con.type_set(QmDbContact.TYPE_PERSON);
    con.time_lastact_set(); // time_now );

    if (local) {
      con.keystat_set(QmDbContact.KEYSTAT_VERIFIED);
    }
    db.contact_add(con);
    update_ui_data(true);
  }

  public void add_group(String name) {
    String me = pgp.my_user_id;
    name = name.split("\n")[0];

    List<String> members = new LinkedList<String>();
    members.add(me);

    QmDbContact contact = new QmDbContact();
    contact.address_set(me);
    contact.name_set(name);
    contact.type_set(QmDbContact.TYPE_GROUP);
    contact.time_lastact_set(); // time_now );
    contact.members_set(members);

    int group;
    QmDbContact ce;
    Random r = new Random();
    do {
      group = r.nextInt(32768);
      ce = db.contact_get_group_by_address_and_group(me, group);
    } while (ce != null);
    Dib2Root.log("listactivity add_group", "Group: " + me + " " + group);
    contact.group_set(group);

    db.contact_add(contact);

    update_ui_data(true);
  }

  @SuppressLint("InflateParams")
  protected void warn_settings_dialog() {
    Dib2Root.log("listactivity warn_dialog", "1");
    LayoutInflater factory = LayoutInflater.from(this);

    final View textEntryView = factory.inflate(R.layout.gmail_account, null);
    final ScrollView scroll_view = new ScrollView(this);

    final EditText input_name = (EditText) textEntryView.findViewById(R.id.gmail_name);
    final EditText input_address = (EditText) textEntryView.findViewById(R.id.gmail_address);
    final EditText input_pass = (EditText) textEntryView.findViewById(R.id.gmail_pass);
    final RadioGroup group = (RadioGroup) textEntryView.findViewById(R.id.account_type_radio);

    input_name.setText("", TextView.BufferType.EDITABLE);
    input_address.setText("", TextView.BufferType.EDITABLE);
    input_pass.setText("", TextView.BufferType.EDITABLE);

    for (int i = 0; i < AccountTypes.account_types.size(); i++) {
      RadioButton button = new RadioButton(this);
      button.setText(AccountTypes.account_types.get(i).name_get());
      group.addView(button);
      if (i == 0) button.setChecked(true);
    }

    final AlertDialog.Builder alert = new AlertDialog.Builder(this);

    alert.setTitle(R.string.action_warn_settings);
    scroll_view.addView(textEntryView);
    alert.setView(scroll_view);
    alert.setInverseBackgroundForced(true);

    final Intent intent =
        new Intent(this, net.sourceforge.dibdib.android_qm.SettingsActivity.class);

    alert.setPositiveButton(
        "Ok",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            String name = input_name.getText().toString();
            String address = input_address.getText().toString();
            String pass = input_pass.getText().toString();
            AccountType account;
            // Enforce dummy value if needed:
            name = (0 < name.length()) ? name : "a";
            address = (0 < address.indexOf('@')) ? address : "a@a";
            pass = (0 < pass.length()) ? pass : "0";
            if (name.length() <= 0 || address.length() <= 0 || pass.length() <= 0) {
              return;
            }

            account = AccountTypes.account_types.get(0);
            account = address.contains("@mail.de") ? AccountTypes.mail_de : account;
            account = address.contains("@web.de") ? AccountTypes.web_de : account;
            account = address.contains("@gmx") ? AccountTypes.gmx : account;
            account = address.contains("@mykolab.com") ? AccountTypes.mykolab : account;
            account = address.contains("@riseup.net") ? AccountTypes.riseup : account;
            name = name.replace('@', '^');

            Dib2Root.log("listactivity selected", account.name_get() + " " + address);
            settings_set_account(account, name, address, pass);
            startActivityForResult(intent, SETTINGS_DONE);
          }
        });
    alert.setNegativeButton(
        "Stop",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            AccountType account = AccountTypes.account_types.get(0);
            settings_set_account(account, "a", "a@a", "0");
            startActivityForResult(intent, SETTINGS_DONE);
          }
        });

    alert.setCancelable(true);
    alert.show();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.list, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.action_settings:
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, SETTINGS_DONE);

        return true;

      case R.id.action_my_fingerprint:
        {
          String fingerprint;
          if (pgp == null) {
            fingerprint = "No keys available, fill in email address first in settings";
          } else {
            fingerprint = pgp.fingerprint(pgp.my_user_id);
            if (fingerprint == null) {
              fingerprint = "No key found";
            }
          }

          AlertDialog.Builder alert = new AlertDialog.Builder(this);

          alert.setMessage(fingerprint);
          alert.setTitle(R.string.action_my_fingerprint);
          alert.setPositiveButton("OK", null);
          alert.setCancelable(true);
          alert.create().show();

          return true;
        }

      case R.id.action_add_contact:
        {
          if (pgp == null) return true;

          AlertDialog.Builder alert = new AlertDialog.Builder(this);

          alert.setTitle(R.string.action_add_contact);
          alert.setMessage(R.string.dialog_add_contact);

          // Set an EditText view to get user input
          final EditText input = new EditText(this);
          input.setInputType(
              InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
          alert.setView(input);

          alert.setPositiveButton(
              "Ok",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                  String value = input.getText().toString();

                  Dib2Root.log("listactivity add_contact", value);
                  add_contact(value);
                }
              });

          alert.setNegativeButton(
              "Stop",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {}
              });

          alert.show();
          return true;
        }
      case R.id.action_add_group:
        {
          if (pgp == null) return true;

          AlertDialog.Builder alert = new AlertDialog.Builder(this);

          alert.setTitle(R.string.action_add_group);
          alert.setMessage(R.string.dialog_add_group);

          // Set an EditText view to get user input
          final EditText input = new EditText(this);
          input.setInputType(InputType.TYPE_CLASS_TEXT);
          alert.setView(input);

          alert.setPositiveButton(
              "Ok",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                  String value = input.getText().toString();

                  Dib2Root.log("listactivity add_group", value);
                  add_group(value);
                }
              });

          alert.setNegativeButton(
              "Stop",
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {}
              });

          alert.show();
          return true;
        }

      case R.id.action_show_license:
        {
          String license =
              "(Version " + version_get() + "/ " + Dib2Constants.VERSION_STRING + ")\n\n";

          AssetManager am = getAssets();
          InputStream is;
          try {
            is = am.open("license.txt");
            license += Filestring.is2str(is);
            is.close();
            is = am.open("spongycastle_license.txt");
            license += Filestring.is2str(is);
            is.close();
            is = am.open("javamail_license.txt");
            license += Filestring.is2str(is);
            is.close();
            is = am.open("apache_license2.txt");
            license += Filestring.is2str(is);
            is.close();
          } catch (IOException e) {
            Log.e("show license", e.getMessage());
          }
          license = StringFunc.flowText(license);

          AlertDialog.Builder alert = new AlertDialog.Builder(this);

          alert.setMessage(license);
          alert.setTitle(R.string.action_show_license);
          alert.setPositiveButton("OK", null);
          alert.setCancelable(true);
          alert.create().show();

          return true;
        }

      case R.id.action_export:
        {
          return true;
        }

      case R.id.action_import:
        {
          return true;
        }

      case R.id.action_sync:
        {
          return true;
        }

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  // =====
}
