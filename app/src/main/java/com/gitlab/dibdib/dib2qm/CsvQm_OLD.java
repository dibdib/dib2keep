// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import com.gitlab.dibdib.picked.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;

/**
 * Temporary simplification for categorical database: see QMapping.
 * ==> CSV with tabs (commas if tabs missing) as single line (trailing '\n' as '\t'):
 */
public class CsvQm_OLD {
  // =====

  public static final String qDbFileName = "DibdibQM.dm";
  public final ContextIf_OLD context;

  public static final int kiOid = 0;
  public static final int kiLabel = 1;
  public static final int kiCats = 2;
  public static final int kiTime = 3;
  public static final int kiContrib = 4;
  public static final int kiSrcs = 5;
  public static final int kiRecv = 6;
  public static final int kiData = 7;

  protected static boolean zByNames = false;
  // private static String zHexPhrase = null; // access_code + imap_pass
  private static long zLastSave = 0;
  private static boolean zLoadSuccess = false;
  private static boolean zStray = false;
  private static ArrayList<QmDbContact> zContacts = new ArrayList<QmDbContact>();
  private static ConcurrentHashMap<String, QmDbMessage> zMessages =
      new ConcurrentHashMap<String, QmDbMessage>();
  protected static ConcurrentHashMap<String, byte[]> zPrefs =
      new ConcurrentHashMap<String, byte[]>();

  private static long millis4Date(String date) {
    long hash = DateFunc.hash62oDate(DateFunc.dateNormalize(date));
    return DateFunc.millisUnixEst4J2000(
        DateFunc.eraTicks4Hash62(hash) - Dib2Constants.TIME_2000_HT_SECONDS);
  }

  public CsvQm_OLD(ContextIf_OLD context) {
    // Might be called from subclass:
    this.context = (null == context) ? ((ContextIf_OLD) this) : context;
    if (0 >= zPrefs.size()) {
      zPrefs.put("pub", new byte[0]);
      zPrefs.put("sec", new byte[0]);
      //    zPrefs.put( "imap_pass", new byte[ 0 ] );
      zContacts.add(
          new QmDbContact(
              QmDbContact.TYPE_PERSON,
              QmDbContact.KEYSTAT_SENT,
              "NN",
              "N.N.",
              DateFunc.millisUnixEst4J2000(-99000))); // millis4Date("990815.08:15")));
    }
  }

  /** Setter. */
  // @param wPhrase Hex string of pass phrase, not cloned!, null for main value.
  // public void setHexPhrase( String xmPhraseHex ) {
  //  zHexPhrase = ((null == xmPhraseHex) || (2 >= xmPhraseHex.length())) ? zHexPhrase :
  // xmPhraseHex;
  //  zLastSave = DateFunc.currentTimeMillisLinearized();
  // }

  public void setByNames(boolean byNames) {
    zByNames = byNames;
  }

  public synchronized void close() {
    save();
  }

  public synchronized void save() {
    write(new File(context.getFilesDir(), qDbFileName).getAbsolutePath(), true, true);
  }

  public String fileReady() {
    File file = new File(context.getFilesDir(), qDbFileName);
    return file.isFile() ? file.getAbsolutePath() : null;
  }

  public void write() {
    write(new File(context.getFilesDir(), qDbFileName).getAbsolutePath(), false, true);
  }

  public synchronized int write(String path, boolean immediately, boolean backupOld) {
    if (null == TcvCodec.instance.getPassFull()) { // zHexPhrase) {
      return -1;
    }
    if (!immediately && ((zLastSave + 60 * 1000) >= DateFunc.currentTimeMillisLinearized())) {
      return -1;
    }
    Dib2Root.log("save", " .. " + zPrefs.size());
    File pathFTemp = new File(path + ".tmp");
    if (pathFTemp.isFile()) {
      pathFTemp.delete();
    }
    zLastSave = DateFunc.currentTimeMillisLinearized();
    byte[] dat = toCsv(null, 0); // exportLines();
    Dib2Root.log("exportLines", "ok " + dat.length);

    int len =
        TcvCodec.instance.writePacked(
            dat,
            0,
            dat.length,
            pathFTemp.getAbsolutePath()); // , StringFunc.bytes4Hex( zHexPhrase ) );
    Dib2Root.log("save", "ok? " + len);
    if (0 <= len) {
      File pathFNew = new File(path);
      if (pathFNew.isFile()) {
        if (backupOld) {
          path = pathFNew.getAbsolutePath();
          File old = new File(path + (immediately ? ".old" : ".bak"));
          if (old.exists()) {
            if (!immediately || !zLoadSuccess) {
              old = new File(path + ".bak");
            }
            if (old.exists()) {
              old.delete();
            }
          }
          pathFNew.renameTo(old);
        } else {
          pathFNew.delete();
        }
      }
      pathFTemp.renameTo(pathFNew);
    }
    zLastSave = DateFunc.currentTimeMillisLinearized();
    return len;
  }

  public int load(boolean useBackup) {
    if (null == TcvCodec.instance.getPassFull()) { // zHexPhrase) {
      return -1;
    }
    int rc = load(qDbFileName);
    Dib2Root.log("load", "" + rc + " .. " + qDbFileName);
    if ((0 <= rc) || !useBackup) {
      zLoadSuccess = (0 < rc);
      return rc;
    }
    //  Dib2Root.log( "load", Dib2Root.csvCodecs[ 0 ].getLog().toString() );
    Dib2Root.log("load", " ..bak ");
    rc = load(qDbFileName + ".bak");
    if (0 <= rc) {
      return rc;
    }
    Dib2Root.log("load", " ..old ");
    return load(qDbFileName + ".old");
  }

  protected int load(String fileName) {
    if (null == TcvCodec.instance.getPassFull()) { // zHexPhrase) {
      return -1;
    }
    Dib2Root.log("load", " .. " + zContacts.size());
    String path = new File(context.getFilesDir(), fileName).toString();
    int out = importFile(path, null, null, true); // StringFunc.bytes4Hex( zHexPhrase ), true );
    zLastSave = DateFunc.currentTimeMillisLinearized();
    return out;
  }

  public synchronized int importFile(
      String filePath, String xPhrase, String accessCode, boolean replace) {
    //  byte[] phrase = (null != xPhrase) ? xPhrase.getBytes( StringFunc.STR16X ) :
    // Codec.instance.getPassFull(); //StringFunc.bytes4Hex( zHexPhrase ) : phrase;
    byte[] header = new byte[8];
    byte[] dat = TcvCodec.instance.readPacked(filePath, header, xPhrase, accessCode);
    int version = header[2] & 0xff;
    version = (6 >= version) ? (10 * version) : header[4];
    long flags_oldVersion_replace_oldCaau = (replace ? 0 : 2) | ((30 >= version) ? 1 : 0);
    flags_oldVersion_replace_oldCaau |= (Dib2Constants.FILE_STRUC_VERSION_MIN > version) ? 4 : 0;
    if (null == dat) {
      Dib2Root.log("import", "read failed.");
      return -1;
    }
    int count = 0;
    try {
      count = importLines(dat, flags_oldVersion_replace_oldCaau);
    } catch (Exception e) {
      Dib2Root.log("import", e.getMessage());
      return -1;
    }
    Dib2Root.log("import", "" + count);
    return count;
  }

  public synchronized void contact_add(QmDbContact it) {
    Dib2Root.log("contact_add", "contact: " + it.address_get());
    it.name_set(it.name_get().replace('\t', ' ').replace('\n', ' '));
    it.address_set(it.address_get().replace(" ", ""));
    //  Contact other = contact_get_by_name( it.name_get() );
    //  other = (null == other) ? contact_get_person_by_address( it.address_get() ) //-
    //    : ((other.address_get().equals( it.address_get() )) ? other : null);
    QmDbContact other = contact_get_person_by_address(it.address_get());
    if ((null != other) && (it.type_get() == QmDbContact.TYPE_PERSON)) {
      //    if (other.address_get().equals( it.address_get() )) {
      it.id_set(other.id_get());
      String v0;
      v0 = it.name_get();
      if ((null == v0) || (0 >= v0.length())) it.name_set(other.name_get());
      v0 = it.phone_get();
      if ((null == v0) || (0 >= v0.length())) it.phone_set(other.phone_get());
      v0 = it.notes_get();
      if ((null == v0) || (0 >= v0.length())) it.notes_set(other.notes_get());
      contact_update(it);
      return;
    }
    other = null;
    if (it.type_get() == QmDbContact.TYPE_PERSON) {
      QmDbContact check_contact = contact_get_person_by_address(it.address_get());
      if (check_contact != null) {
        Dib2Root.log("contact_add", "contact exists with address " + it.address_get());
        return;
      }
    } else {
      QmDbContact check_contact =
          contact_get_group_by_address_and_group(it.address_get(), it.group_get());
      if (check_contact != null) {
        Dib2Root.log("contact_add", "contact exists with address " + it.address_get());
        return;
      }
      check_contact = contact_get_person_by_address(it.address_get());
      if (null == check_contact) {
        other =
            new QmDbContact(
                QmDbContact.TYPE_PERSON,
                QmDbContact.KEYSTAT_NONE,
                it.address_get(),
                it.address_get(),
                it.time_lastact_get());
        other.id_set(zContacts.size());
        zContacts.add(other);
      }
    }
    it.id_set(zContacts.size());
    zContacts.add(it);
    write();
    Dib2Root.log("contact_add", "done: " + zContacts.size() + it.address_get());
  }

  public void contact_update(QmDbContact it) {
    it.name_set(it.name_get().replace('\t', ' ').replace('\n', ' '));
    it.address_set(it.address_get().replace(" ", ""));
    Dib2Root.log("contact_update", "update contact with id " + it.id_get());
    QmDbContact other = contact_get_person_by_address(it.address_get());
    if (it.type_get() == QmDbContact.TYPE_PERSON) {
      if ((other != null) && (it.id_get() != other.id_get())) {
        // possible race condition
        Dib2Root.log("contact_update", "contact exists with address " + it.address_get());
        zContacts.set(it.id_get(), null);
        it.id_set(other.id_get());
      }
    } else if (null == other) {
      other =
          new QmDbContact(
              QmDbContact.TYPE_PERSON,
              QmDbContact.KEYSTAT_NONE,
              it.address_get(),
              it.address_get(),
              it.time_lastact_get());
      other.id_set(zContacts.size());
      zContacts.add(other);
    }
    zContacts.set(it.id_get(), it);
    write();
  }

  public void contact_remove(QmDbContact contact) {
    if (1 >= contact.id_get()) {
      Dib2Root.log("contact remove", "Can't remove myself or initial value.");
      return;
    }
    zContacts.set(contact.id_get(), null);
  }

  public QmDbContact contact_get_by_name(String name) {
    for (QmDbContact ct : zContacts) {
      if ((ct != null) && name.equals(ct.name_get())) {
        return ct;
      }
    }
    return null;
  }

  public QmDbContact contact_get_person_by_address(String address) {
    address = address.replace(" ", "").toLowerCase();
    for (QmDbContact ct : zContacts) {
      if ((ct != null)
          && (ct.type_get() == QmDbContact.TYPE_PERSON)
          && address.equals(ct.address_get().toLowerCase())) {
        return ct;
      }
    }
    return null;
  }

  public List<QmDbContact> contact_get_by_address(String address) {
    address = address.replace(" ", "").toLowerCase();
    List<QmDbContact> out = new ArrayList<QmDbContact>();
    for (QmDbContact ct : zContacts) {
      if ((ct != null) && address.equals(ct.address_get().toLowerCase())) {
        out.add(ct);
      }
    }
    return out;
  }

  public List<QmDbContact> contact_get_by_type(int type) {
    List<QmDbContact> out = new ArrayList<QmDbContact>();
    for (QmDbContact ct : zContacts) {
      if ((ct != null) && (type == ct.type_get())) {
        out.add(ct);
      }
    }
    return out;
  }

  public QmDbContact contact_get_group_by_address_and_group(String address, int group) {
    address = address.replace(" ", "").toLowerCase();
    for (QmDbContact ct : zContacts) {
      if ((ct != null)
          && (QmDbContact.TYPE_GROUP == ct.type_get())
          && (group == ct.group_get())
          && address.equals(ct.address_get().toLowerCase())) {
        return ct;
      }
    }
    return null;
  }

  public QmDbContact contact_get_by_id(int id) {
    return ((id < 0) || (id >= zContacts.size())) ? null : zContacts.get(id);
  }

  public int message_get_unread_by_chat(int id, long since) {
    int count = 0;
    for (QmDbMessage msg : zMessages.values()) {
      if ((id == msg.id_get()) && (1 < msg.from_get()) && (since < msg.time_get())) {
        ++count;
      }
    }
    return count;
  }

  public QmDbMessage message_get_by_oid(String oid) {
    return zMessages.get(oid);
  }

  public List<QmDbMessage> message_get_by_id(int id, int max) {
    TreeMap<Long, QmDbMessage> map0 = new TreeMap<Long, QmDbMessage>();
    TreeMap<Long, QmDbMessage> map1 = new TreeMap<Long, QmDbMessage>();
    for (QmDbMessage msg : zMessages.values()) {
      if (id == msg.id_get()) {
        long ord = msg.time_get();
        while (map0.containsKey(ord)) {
          ++ord;
        }
        Long min = map0.ceilingKey(0L);
        if (map0.size() < max) {
          map0.put(ord, msg);
        } else if (min < msg.time_get()) {
          map0.remove(min);
          map0.put(ord, msg);
        }
        if (0 != msg.ack) { // || (null != msg.queue_get())) {
          min = map1.ceilingKey(0L);
          if (map1.size() <= 3) {
            map1.put(ord, msg);
          } else if (min < msg.time_get()) {
            map1.remove(min);
            map1.put(ord, msg);
          }
        }
      }
    }
    ArrayList<QmDbMessage> out = new ArrayList<QmDbMessage>();
    for (Long tim : map0.descendingKeySet()) {
      if (out.size() > max) {
        break;
      }
      out.add(0, map0.get(tim));
    }
    for (Long tim : map1.descendingKeySet()) {
      if (!map0.containsKey(tim)) {
        out.add(0, map1.get(tim));
      }
    }
    return out;
  }

  public List<QmDbMessage> message_dangling_by_id(int id, int max) {
    TreeMap<Long, QmDbMessage> map1 = new TreeMap<Long, QmDbMessage>();
    final long now = DateFunc.currentTimeMillisLinearized();
    for (QmDbMessage msg : zMessages.values()) {
      if (id == msg.id_get()) {
        long ord = msg.time_get();
        if ((0 != msg.ack) && (now > msg.ack)) { // && (null == msg.queue_get())) {
          while (map1.containsKey(ord)) {
            ++ord;
          }
          Long min = map1.ceilingKey(0L);
          if (map1.size() < max) {
            map1.put(ord, msg);
          } else if (min < msg.time_get()) {
            map1.remove(min);
            map1.put(ord, msg);
          }
        }
      }
    }
    ArrayList<QmDbMessage> out = new ArrayList<QmDbMessage>();
    for (Long tim : map1.descendingKeySet()) {
      out.add(0, map1.get(tim));
    }
    return out;
  }

  public synchronized void message_remove_all_by_contact(int id) {
    for (boolean go = true; go; ) {
      go = false;
      for (String oid : zMessages.keySet()) {
        if (id == zMessages.get(oid).id_get()) {
          zMessages.remove(oid);
          go = true;
          break;
        }
      }
    }
    zStray = (0 == id) ? false : zStray;
  }

  public List<QmDbContact> contact_get_all() {
    ArrayList<QmDbContact> out = new ArrayList<QmDbContact>();
    int first = zStray ? 0 : 1;
    if (zByNames) {
      TreeMap<Long, QmDbContact> map = new TreeMap<Long, QmDbContact>();
      for (QmDbContact ct : zContacts) {
        if ((ct != null) && (first <= ct.id_get())) {
          long shash = ShashFunc.shashBits4String(ct.name_get(), true); // [0];
          if (null != map.get(shash)) {
            for (int i0 = 1; true; ++i0) {
              if (null == map.get(shash + i0)) {
                shash = shash + i0;
                break;
              }
            }
          }
          map.put(shash, ct);
        }
      }
      for (Long shash : map.keySet()) {
        out.add(map.get(shash));
      }
    } else {
      TreeMap<Long, QmDbContact> map = new TreeMap<Long, QmDbContact>();
      for (QmDbContact ct : zContacts) {
        if ((ct != null) && (first <= ct.id_get())) {
          long ord = ct.time_lastact_get();
          while (map.containsKey(ord)) {
            ++ord;
          }
          map.put(ord, ct);
        }
      }
      for (Long la : map.descendingKeySet()) {
        out.add(map.get(la));
      }
    }
    if (0 >= out.size()) {
      out.add(zContacts.get(0));
    }
    return out;
  }

  public List<QmDbContact> contact_get_sendable() {
    TreeMap<String, QmDbContact> map = new TreeMap<String, QmDbContact>();
    for (QmDbContact ct : zContacts) {
      if ((ct != null) && ((ct.type_get() == QmDbContact.TYPE_GROUP))
          || ((QmDbContact.KEYSTAT_BIT_ACTIVE & ct.keystat_get()) != 0)) {
        if ((ct.address_get().indexOf('@') > 0) && (0 < ct.id_get())) {
          map.put(ct.name_get(), ct);
        }
      }
    }
    ArrayList<QmDbContact> out = new ArrayList<QmDbContact>();
    for (String nam : map.keySet()) {
      out.add(map.get(nam));
    }
    return out;
  }

  public synchronized void message_add(QmDbMessage message) {
    String txt = message.text_get();
    zStray = zStray || (0 == message.id_get());
    if (0 < txt.length()) {
      if (0 <= txt.indexOf('\t')
          || ('\n' == txt.charAt(0))
          || ('\n' == txt.charAt(txt.length() - 1))) {
        txt = txt.replace('\t', '\n').replace("\r\n", "\n").replace('\r', '\n');
        while (txt.startsWith("\n")) {
          txt = txt.substring(1);
        }
        while (txt.endsWith("\n")) {
          txt = txt.substring(0, txt.length() - 1);
        }
        message.text_set(txt);
      }
    }
    String oid = message.oid;
    if ((null == oid) || (6 >= oid.length())) {
      oid = "" + DateFunc.createId();
      //  DateFunc.createOldId();
      //  (null != txt) && (0 < txt.length())
      //      ? txt
      //      : ((null == message.uri_get()) ? "" : message.uri_get().toString()),
      //  message.time_get());
      message.oid = oid;
    }
    QmDbMessage mx;
    for (int ox = 0; (null != (mx = zMessages.get(oid))); ++ox) {
      if ((message.id_get() != mx.id_get()) || (message.from_get() != mx.from_get())) {
        continue;
      }
      if (StringFunc.equalsRoughly(mx.text_get(), txt)) {
        if (mx.time_get() > message.time_get()) {
          return;
        }
        break;
      } else if (mx.text_get().contains(txt)) {
        return;
      } else if ((mx.time_get() >>> 8) == (message.time_get() >>> 8)) {
        message.text_set(txt + " // " + mx.text_get());
        break;
      }
      oid = message.oid + ox;
    }
    message.oid = oid;
    zMessages.put(oid, message);
  }

  public synchronized void message_update(QmDbMessage message) {
    if (0 <= message.text_get().indexOf('\t')) {
      message.text_set(message.text_get().replace('\t', '\n'));
    }
    String oid = message.oid;
    zStray = zStray || (0 == message.id_get());
    QmDbMessage mx;
    for (int ox = 0; (null != (mx = zMessages.get(oid))); ++ox) {
      if ((message.id_get() != mx.id_get()) || (message.from_get() != mx.from_get())) {
        continue;
      }
      if (message.time_get() == mx.time_get()) {
        message.oid = oid;
        zMessages.put(oid, message);
        return;
      }
      oid = message.oid + ox;
    }
    Dib2Root.log("message_update", "failed "); // + message.toString() );
  }

  public synchronized void preference_remove(String key) {
    Dib2Root.log("preference_remove", key);
    zPrefs.remove(key);
  }

  public synchronized void preference_set(String key, byte[] value) {
    key = key.replace('\t', ' ').replace('\n', ' ');
    if (null == value) {
      zPrefs.remove(key);
    } else if ("lastId".equals(key)) {
      if (8 < value.length) {
        // DateFunc.initLastId(new String(value));
      }
    } else {
      value = value.clone();
      zPrefs.put(key, value);
    }
    write();
  }

  public byte[] preference_get(String key) {
    key = key.replace('\t', ' ').replace('\n', ' ');
    byte[] out = zPrefs.get(key);
    return (out == null) ? null : out.clone();
  }

  public byte[] toCsv(Mapping__OLD[] map, int cMap) {
    if (null == map) {
      map = new Mapping__OLD[zContacts.size() + zMessages.size() + zPrefs.size() + 1000];
    }
    String contrib = zContacts.get(0).name_get();
    map[cMap++] = new Mapping__OLD(contrib, -1, null, "SRC", contrib, 1);
    HashMap<String, String> oids = new HashMap<String, String>();
    for (String key : zPrefs.keySet()) {
      map = (cMap >= map.length) ? Arrays.copyOf(map, 2 * map.length) : map;
      byte[] val = zPrefs.get(key);
      if (null == val) {
        continue;
      }
      map[cMap++] =
          new Mapping__OLD(key, -1, null, "PREF", contrib, 1, StringFunc.hex4Bytes(val, true));
    }
    for (QmDbContact ct : zContacts) {
      if ((ct == null) || (ct.id_get() <= 0)) {
        continue;
      }
      String mems = ct.members_get_string().replace(", ", ",");
      map = (cMap >= map.length) ? Arrays.copyOf(map, 2 * map.length) : map;
      map[cMap++] =
          new Mapping__OLD(
              ct.name_get(), // -
              -1, // MiscFunc.toBytes( ct.name_get(), //-
              null, // -
              ((QmDbContact.TYPE_GROUP == ct.type_get()) ? "GROUP" : "CONTACT"), // -
              contrib, // -
              1, // -
              ":EMAIL: " + ct.address_get(), // -
              ":ADMIN: "
                  + ct.group_get()
                  + ','
                  + ct.keystat_get()
                  + ','
                  + ct.time_lastact_get() / 1000
                  + ','
                  + ct.unread_get() / 1000, // -
              ""
                  + ((ct.members_get() == null)
                      ? (":PHONE: " + ct.phone_get())
                      : (":OTHER: " + mems)), // -
              ":NOTES: " + ct.notes_get() // -
              );
      oids.put(map[cMap - 1].label, map[cMap - 1].oid);
    }
    for (QmDbMessage msg : zMessages.values()) {
      QmDbContact ct = zContacts.get(msg.id_get());
      if (null == ct) {
        ct = zContacts.get(0);
      }
      String dat = msg.text_get(); // .replace( '\n', '\t' );
      if (null != msg.uri_get() && !msg.text_get().startsWith("::")) {
        dat =
            "::"
                + msg.uri_get_string().replace(":", "::") // ,
                + '\n'
                + dat;
      }
      //    if (msg.queue_get() != null) {
      //      dat = "::QUEUE::" + msg.queue_get() + '\n' + dat;
      //    }
      QmDbContact cctrb = zContacts.get(msg.from_get());
      cctrb = (null == cctrb) ? zContacts.get(0) : cctrb;
      String ctrb = cctrb.name_get();
      ctrb = oids.containsKey(ctrb) ? oids.get(ctrb) : ctrb;
      map = (cMap >= map.length) ? Arrays.copyOf(map, 2 * map.length) : map;
      map[cMap++] =
          new Mapping__OLD(ct.name_get(), msg.time_get(), msg.oid, "MSG", ctrb, msg.ack, dat);
    }
    return CsvQm_OLD.toCsvMap(map, cMap);
  }

  public synchronized int importLines(byte[] dat, long flags_oldVersion_replace_oldCaau) {
    int flags = (int) flags_oldVersion_replace_oldCaau;
    boolean replace = (0 == (flags & 2));
    Mapping__OLD[] map = CsvQm_OLD.fromCsvMap(dat, flags);
    int count = 0;
    HashMap<String, String> oids = new HashMap<String, String>();
    HashSet<String> allowDouble = new HashSet<String>();
    for (int inx = 0; inx < map.length; ++inx) {
      try {
        final String name = map[inx].label;
        final String oid = (5 >= map[inx].oid.length()) ? null : map[inx].oid;
        oids.put(map[inx].oid, name);
        String cats = "";
        for (String cat : map[inx].categories) {
          cats += " " + cat;
        }
        cats = cats.trim();
        String contrib = map[inx].contributorOid;
        contrib = (oids.containsKey(contrib)) ? oids.get(contrib) : contrib;
        final long timestamp = map[inx].timeStamp;
        final long ack = map[inx].ack;
        //      if (3 >= version) {
        //        timestamp = timestamp ^ (timestamp & Dib2Constants.TIME_SHIFTED);
        //      }
        String[] data = map[inx].listElements;
        if (cats.contains("PREF")) {
          // Do not override current entries when importing older data:
          if (replace
              || !zPrefs.containsKey(name)
              || (null == zPrefs.get(name))
              || (0 >= zPrefs.get(name).length)) {
            if (0 != (flags & 4)
                && name.startsWith("KEY")
                && (data[0].length() > 3 * "CAAU".length())
                && data[0].matches("3.3.3.3.*")) {
              zPrefs.put(name, StringFunc.bytes4Hex(StringFunc.string4HexUtf8(data[0])));
            } else {
              // zPrefs.put( name, MiscFunc.bytes4Hex( data[ 0 ] ) );
              preference_set(name, StringFunc.bytes4Hex(data[0]));
            }
            ++count;
          }
        } else if (cats.contains("GROUP") || cats.contains("CONTACT")) {
          boolean group = cats.contains("GROUP");
          String[] a0 = map[inx].listElements;
          String email = a0[0].substring(a0[0].indexOf(": ") + 2).trim();
          String adm = a0[1].substring(a0[1].indexOf(": ") + 2).trim();
          String mems =
              ((2 >= a0.length) || !group)
                  ? null // -
                  : a0[2].substring(a0[2].indexOf(": ") + 2).trim();
          String phone =
              ((2 >= a0.length) || group)
                  ? null // -
                  : a0[2].substring(a0[2].indexOf(": ") + 2).trim();
          String notes =
              (3 >= a0.length)
                  ? null // -
                  : a0[3].substring(a0[3].indexOf(": ") + 2).trim();
          a0 = adm.split(",");
          QmDbContact ct =
              new QmDbContact( // -
                  (group ? QmDbContact.TYPE_GROUP : QmDbContact.TYPE_PERSON), // -
                  Integer.parseInt(a0[1]),
                  email,
                  name,
                  Long.parseLong(a0[2]) * 1000);
          ct.group_set(Integer.parseInt(a0[0]));
          ct.unread_set(Long.parseLong(a0[3]) * 1000);
          if (null != mems) {
            ct.members_set(Arrays.asList(mems.split(",")));
          }
          if (null != phone) {
            ct.phone_set(phone);
          }
          if (null != notes) {
            ct.notes_set(notes);
          }
          List<QmDbContact> act = contact_get_by_address(email);
          // Do not override current entries when importing older data:
          if (group || (0 >= act.size())) {
            ct.id_set(zContacts.size());
            zContacts.add(ct);
            ++count;
          } else if (replace && (1 <= act.size())) {
            QmDbContact ct2 = act.get(0);
            if (0 >= ct2.name_get().length()) {
              ct2.name_set(ct.name_get());
            }
            if (0 >= ct2.phone_get().length()) {
              ct2.phone_set(ct.phone_get());
            }
            if (0 >= ct2.notes_get().length()) {
              ct2.notes_set(ct.notes_get());
            }
            ++count;
          }
        } else {
          // Messages: assuming contacts are listed ahead of the messages.
          QmDbContact ct = contact_get_by_name(name);
          if (null == ct) {
            ct = zContacts.get(0);
          }
          int id = ct.id_get();
          ct = contact_get_by_name(contrib);
          if (null == ct) {
            ct = zContacts.get(0);
          }
          int from = ct.id_get();
          String txt = data[0];
          for (int ix = 1; ix < data.length; ++ix) {
            txt += "\n" + data[ix];
          }
          QmDbMessage msg = new QmDbMessage(id, from, timestamp, txt);
          msg.oid = oid;
          msg.ack = ack;
          //        int part2 = txt.indexOf( '\n' );
          //        part2 = (0 < part2) ? part2 : txt.length();
          if (txt.startsWith("::QUEUE::")) {
            msg.ack = -1;
            //          msg.queue_set( data[ 0 ].substring( txt.lastIndexOf( "::" ) + 2 ) ); //
            // txt.substring( txt.lastIndexOf( "::" ) + 2, part2 ) );
          } else if (txt.startsWith("::")) {
            //          String uri = data.substring( 0, part2 );
            msg.uri_set(data[0]); // uri );
          }
          if (!zMessages.containsKey(oid) || allowDouble.contains(oid)) {
            allowDouble.add(oid);
            message_add(msg);
            ++count;
          } else if (replace || (null == zMessages.get(oid))) {
            message_update(msg);
            ++count;
          }
        }
      } catch (Exception e) {
        // Ignore.
      }
    }
    Dib2Root.log("load", "" + count);
    return count;
  }

  public synchronized int archive(File file, int minLeft) {
    long threshold = DateFunc.currentTimeMillisLinearized() - (30 + 9) * 24L * 3600 * 1000L;
    long veryOld = threshold - 2 * 365L * 24 * 3600 * 1000L;
    int[] cMsg4Contact = new int[zContacts.size()];
    int cTotal = 0;
    // Calculate as step 0, then perform as step 1:
    for (int step = 0; step <= 1; ++step) {
      cTotal = 0;
      for (int id = cMsg4Contact.length - 1; id > 0; --id) {
        cMsg4Contact[id] = -minLeft;
      }
      cMsg4Contact[0] = 0;
      TreeMap<Long, String> map = new TreeMap<Long, String>();
      for (String oid : zMessages.keySet()) {
        QmDbMessage msg = zMessages.get(oid);
        long ord = msg.time_get();
        while (map.containsKey(ord)) {
          ++ord;
        }
        map.put(ord, oid);
        ++cMsg4Contact[msg.id_get()];
      }
      for (Long tim : map.keySet()) {
        QmDbMessage msg = zMessages.get(map.get(tim));
        if ((threshold < tim) && (0 != msg.id_get())) {
          if (2 * minLeft >= cMsg4Contact[msg.id_get()]) {
            continue;
          }
          --cMsg4Contact[msg.id_get()];
        } else if ((veryOld < tim) && (0 >= cMsg4Contact[msg.id_get()])) {
          continue;
        }
        if (1 == step) {
          zMessages.remove(map.get(tim));
        }
        ++cTotal;
        --cMsg4Contact[msg.id_get()];
      }
      if (0 == step) {
        if ((100 > cTotal) && (minLeft / 2 > cMsg4Contact[0])) {
          return 0;
        }
        if (file.exists() || (0 >= write(file.getAbsolutePath(), true, false))) {
          return 0;
        }
      }
    }
    return cTotal;
  }

  public static QmDbMessage message4CsvFields(int from, String[] els, boolean ack) {
    if ((kiData >= els.length)
        || (5 >= els[kiOid].length())
        || (5 >= els[kiTime].length())
        || !"MSG".equals(els[kiCats])) {
      return null;
    }
    char ch = els[kiOid].charAt(0);
    if (('0' > ch) || ('f' < ch)) {
      return null;
    } else if (('9' < ch) && (ch < 'A')) {
      return null;
    } else if (('F' < ch) && (ch < 'a')) {
      return null;
    }
    long time = millis4Date(els[kiTime]);
    if (Dib2Constants.TIME_MIN_2017_01_01_UNIX_MILLIS > time) {
      return null;
    } else if (time > DateFunc.currentTimeMillisLinearized()) {
      time = // (DateFunc.currentTimeMillisLinearized() & ~Dib2Constants.TIME_SHIFTED) |
          // Dib2Constants.TIME_SHIFTED_UNKNOWN;
          DateFunc.alignTime(time, -1);
    }
    QmDbMessage msg0 = new QmDbMessage();
    msg0.id_set(from);
    msg0.from_set(from);
    msg0.time_set(time);
    msg0.oid = els[kiOid];
    msg0.ack = ack ? time : 0;
    StringBuilder txt = new StringBuilder(30 * els.length);
    for (int i0 = kiData; i0 < els.length; ++i0) {
      txt.append(els[i0]);
      txt.append('\n');
    }
    msg0.text_set(txt.toString());
    return msg0;
  }

  public static Mapping__OLD[] fromCsvMap(byte[] map, int flagsMarkAdjusttimeKeyhex) {

    if (map.length <= 2) {
      return new Mapping__OLD[0];
    }
    int i1 = 1;
    int count = 0;
    int i0 = 0;
    int iOid = 0;
    Mapping__OLD[] out = new Mapping__OLD[24];
    if ((map[0] == Dib2Constants.MAGIC_BYTES[0]) && (map[1] == Dib2Constants.MAGIC_BYTES[1])) {
      // Skip header:
      i0 = 1 + MiscFunc.indexOf(map, new byte[] {'\n'});
      iOid = (map[2] == '\t') ? -1 : 0;
    }
    for (; i0 < map.length; i0 = i1 + 1) {
      i1 = MiscFunc.indexOf(map, new byte[] {'\n'}, i0);
      if (i1 < 0) {
        i1 = map.length;
      }
      String line;
      try {
        line = new String(map, i0, i1 - i0, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        line = new String(map, i0, i1 - i0);
      }
      if ((line.indexOf("\t") < 0) && (line.indexOf(",") > 0)) {
        line = line.replaceAll("\"? *, *\"?", "\t");
      }
      String[] a0 = line.split("\t"); // , 6 + iOid );
      if (5 > a0.length) {
        continue;
      }
      try {
        if (count >= out.length) {
          out = Arrays.copyOf(out, 2 * count);
        }
        out[count] = new Mapping__OLD(a0, iOid, flagsMarkAdjusttimeKeyhex);
        ++count;
      } catch (Exception e) {
        // Ignore.
      }
    }
    return Arrays.copyOf(out, count);
  }

  /** Create encoded list of mappings with header "dm(TTT)N.N" (time TTT, version N.N).
   * -- "dm(..)N.N" starts plain-text CSV with header row (version N.N).
   * -- "dm^Cxxx" precedes encoded container as salt value.
   */
  public static byte[] toCsvMap(Mapping__OLD[] entries, int count) {
    String header = new String(Dib2Constants.MAGIC_BYTES);
    // old: header += "\t" + MiscFunc.toDate4Millis() + ... + '\n';
    header +=
        "("
            + DateFunc.dateShort4Millis()
            + ')'
            + Dib2Constants.FILE_STRUC_VERSION_STR
            + Mapping__OLD.fieldNames
            + '\n';
    StringBuilder out = new StringBuilder(header);
    for (Mapping__OLD entry : entries) {
      if (0 >= count) {
        break;
      }
      --count;
      Mapping__OLD.toCsvLine(entry, out);
    }
    try {
      return out.toString().getBytes("UTF-8");
    } catch (UnsupportedEncodingException e) {
    }
    return out.toString().getBytes(StringFunc.CHAR16UTF8);
  }

  // =====
}
