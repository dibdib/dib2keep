// Copyright (C) 2016  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2017  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import java.util.*;
import net.sf.dibdib.thread_any.*;

public class QmDbContact {
  // =====

  public static final int TYPE_PERSON = 0;
  public static final int TYPE_GROUP = 1;

  public static final int KEYSTAT_BIT_ACTIVE = 1;
  public static final int KEYSTAT_BIT_CHANGED = 2;
  public static final int KEYSTAT_BIT_TO_SEND = 4;
  // public static final int KEYSTAT_BIT_CONFLICT = 8;

  public static final int KEYSTAT_NONE = 0;
  public static final int KEYSTAT_VERIFIED = KEYSTAT_BIT_ACTIVE + KEYSTAT_BIT_CHANGED;
  public static final int KEYSTAT_CONFIRMED = KEYSTAT_BIT_ACTIVE;
  public static final int KEYSTAT_SENT = KEYSTAT_BIT_CHANGED;
  public static final int KEYSTAT_PENDING = KEYSTAT_BIT_ACTIVE + KEYSTAT_BIT_TO_SEND;
  public static final int KEYSTAT_RECEIVED = KEYSTAT_BIT_CHANGED + KEYSTAT_BIT_TO_SEND;
  // public static final int KEYSTAT_CONFLICT = KEYSTAT_BIT_ACTIVE + KEYSTAT_BIT_CONFLICT;

  int _id;
  int _type;
  int _keystat;
  String _name;
  long _time_lastact;
  String _address; // for group this is the owner
  List<String> _members; // only for group
  long _unread;
  int _group;

  String _phone;
  String _notes;

  public QmDbContact() {}

  public QmDbContact(int type, int keystat, String address, String name, long time_lastact) {
    type_set(type);
    keystat_set(keystat);
    address_set(address);
    if (name != null) name_set(name);
    time_lastact_set(time_lastact);
  }

  /*
  public Contact( int type, int keystat, String owner, String name, List< String > members, long time_lastact, int group )
  {
    type_set( type );
    keystat_set( keystat );
    address_set( owner );
    members_set( members );
    group_set( group );
    if (name != null)
      name_set( name );
    time_lastact_set( time_lastact );
  }
  */

  /*
  public Contact( int type, int keystat, String owner, String name, String members, long time_lastact )
  {
    type_set( type );
    keystat_set( keystat );
    address_set( owner );
    members_set( members );
    if (name != null)
      name_set( name );
    time_lastact_set( time_lastact );
  }
  */

  public void type_set(int type) {
    _type = type;
  }

  public int type_get() {
    return _type;
  }

  public void keystat_set(int keystat) {
    _keystat = keystat;
  }

  public int keystat_get() {
    return _keystat;
  }

  public void address_set(String address) {
    _address = address;
    if (_name == null) {
      int at = address.indexOf('@');
      _name = (0 < at) ? address.substring(0, at) : address;
    }
  }

  public String address_get() {
    return _address;
  }

  public void name_set(String name) {
    _name = name;
  }

  public String name_get() {
    if (_name == null) {
      // Log.e("contact", "name == null");
      return "";
    }
    return _name;
  }

  public void phone_set(String phone) {
    _phone = phone;
  }

  public String phone_get() {
    return (_phone == null) ? "" : _phone;
  }

  public void notes_set(String notes) {
    _notes = notes;
  }

  public String notes_get() {
    return (_notes == null) ? "" : _notes;
  }

  public void time_lastact_set(long... time_lastact) {
    long current = DateFunc.currentTimeMillisLinearized();
    long time =
        ((null == time_lastact)
                || (0 >= time_lastact.length)
                || (time_lastact[0] > current)
                || (0 >= time_lastact[0])) // ,
            ? current
            : time_lastact[0];
    _time_lastact = (_time_lastact > current) ? current : _time_lastact;
    if (time > _time_lastact) {
      _time_lastact = DateFunc.alignTime(time, 1);
    }
  }

  public long time_lastact_get() {
    return _time_lastact;
  }

  public void members_set(List<String> members) {
    _members = members;
  }

  public void members_set(String members) {
    if ((members == null) || (2 >= members.trim().length())) {
      // Log.e("contact", "members_set(null)");
      _members = null;
      return;
    }
    members = members.trim();
    _members = new ArrayList<String>(Arrays.asList(members.split(", ")));
  }

  public List<String> members_get() {
    return _members;
  }

  public String members_get_string() {
    if (_members == null) return "";
    // return TextUtils.join(", ", _members);
    String out = _members.toString();
    // Assuming '[...]' notation:
    return out.substring(1, out.length() - 1);
  }

  public int id_get() {
    return _id;
  }

  public void id_set(int id) {
    _id = id;
  }

  public long unread_get() {
    return _unread;
  }

  public void unread_set(long unread) {
    _unread = DateFunc.alignTime(unread, 1);
  }

  public int group_get() {
    return _group;
  }

  public void group_set(int group) {
    _group = group;
  }

  @Override
  public String toString() {
    if (_address == null) return "address==null";

    return "id ="
        + _id
        + ", "
        + "address="
        + _address
        + ", "
        + "type="
        + _type
        + ", "
        + "keystat="
        + _keystat
        + ", "
        + "name="
        + ((_name == null) ? "null" : _name)
        + ", "
        + "members="
        + ((_members == null) ? "null" : members_get_string())
        + ", "
        + "goup="
        + _group
        + ", "
        + "unread="
        + _unread
        + ", "
        + "time_lastact="
        + _time_lastact;
  }

  // =====
}
