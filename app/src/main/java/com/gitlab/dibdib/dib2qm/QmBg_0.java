// Copyright (C) 2016  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2018  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.app.*;
import android.content.*;
import android.graphics.BitmapFactory;
import android.media.*;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.gitlab.dibdib.dib2qm.*;
import com.gitlab.dibdib.picked.net.*;
import java.io.*;
import java.util.*;
import javax.mail.internet.ContentType;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android_qm.*;

public class QmBg_0 extends Service { // net.vreeken.quickmsg.background_0 {
  // =====

  public static final int MIN_IDLE_TIME = 11 * 1000;
  public static final int STD_IDLE_TIME = 4 * 60 * 1000;
  public static final int MAX_IDLE_TIME = 14 * 60 * 1000;

  protected static QmBg_0 svcContext = null;
  protected preferences prefs = null;

  public static volatile int unread;
  protected static volatile boolean alarmDone = false;
  public static volatile long flagsNotify = 3; // 1: LED, 2: vibrate, 4: sound
  protected static volatile int idleTimeNet = MIN_IDLE_TIME;

  protected Notification.Builder notification = null;
  protected int notification_id = 0x1337;
  protected static QmDb_0 db = null;

  public static QmBg_0 getSvcContext() {
    return svcContext;
  }

  public static Mail_1 mail =
      new Mail_1() {
        @Override
        public Boolean recv_quickmsg_cb(
            String from, List<MailAttachment> attachments, String subtype, Date timeMax) {
          Dib2Root.log("bg recv cb", "Received quickmsg from " + from + " of subtype: " + subtype);
          unread = (0 > unread) ? 0 : unread;

          Pgp pgp = new Pgp(db); // svcContext );

          for (int i = 0; i < attachments.size(); i++) {
            MailAttachment attachment = attachments.get(i);
            //      Dib2Root.log( "bg recv cb", "name: " + attachment.name +
            //        ", content type :" + attachment.datahandler.getContentType() );
            try {
              String cts = attachment.datahandler.getContentType();
              if (null == cts) {
                continue;
              }
              ContentType ct = new ContentType(cts);
              if ((null != subtype)
                  && subtype.toLowerCase().equals("encrypted")
                  && ct.getSubType().toLowerCase().equals("octet-stream")) {
                //          Dib2Root.log( "bg recv cb", "Got encrypted message" );
                MailAttachment ac;
                try {
                  ac = pgp.decrypt_verify(attachment);
                } catch (OutOfMemoryError e) {
                  Log.e("recv_cb", "out of memory: attachment too big?");
                  return false;
                }
                if (ac != null) {
                  Dib2Root.log("bg recv cb", "Got decrypted message - " + attachment.name);
                  List<MailAttachment> as = multipart_get_attachments(ac);
                  MailAttachment a_msg = null;
                  QmDbMessage msg = null;
                  if (null == as) {
                    // Try to keep chat alive ...
                    QmDbContact contact = db.contact_get_person_by_address(from);
                    if (null == contact) {
                      return false;
                    }
                    msg =
                        new QmDbMessage(
                            contact.id_get(),
                            contact.id_get(),
                            DateFunc.currentTimeMillisLinearized(),
                            "...");
                    as = new ArrayList<MailAttachment>();
                  }
                  String a_ext = ".dat";

                  for (int j = 0; j < as.size(); j++) {
                    MailAttachment a = as.get(j);
                    if (null == a) {
                      continue;
                    }
                    ContentType ct2 = new ContentType(a.datahandler.getContentType());
                    String subtype2 = ct2.getSubType();
                    String basetype = ct2.getBaseType();
                    //              Dib2Root.log( "bg recv db", "attachment " + j + " " + basetype
                    // );

                    if (subtype2.toLowerCase().equals("pgp-keys")) {
                      svcContext.received_key(pgp, db, from, a, true);
                    } else if (subtype2.toLowerCase().equals("quickmsg")) {
                      msg = svcContext.received_quickmsg(pgp, db, from, a);
                      if ((null != timeMax) && (timeMax.getTime() < msg.time_get())) {
                        msg.time_set(timeMax.getTime());
                      }
                      if (msg.time_get() > DateFunc.currentTimeMillisLinearized()) {
                        final long mTime = msg.time_get();
                        final long cTime = DateFunc.currentTimeMillisLinearized();
                        msg.time_set(DateFunc.alignTime(mTime, 1));
                        if (mTime <= (cTime + 2 * 60 * 1000)) {
                          DateFunc.alignTime(cTime, mTime);
                        }
                      }
                    } else {
                      /* maybe part of a message */
                      String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(basetype);

                      Dib2Root.log("bg recv db", "probably an attachment ext: " + ext);
                      a_msg = a;
                      a_ext = ext;
                    }
                  }

                  if (msg != null && a_msg != null) {
                    svcContext.received_attachment(db, msg, a_msg, a_ext);
                  }
                  if (msg != null) {
                    String txt = msg.text_get();
                    int i0 = 0;
                    for (; i0 < txt.length(); ++i0) {
                      if (' ' < txt.charAt(i0)) {
                        break;
                      }
                    }
                    if ((0 < i0) && (i0 < txt.length())) {
                      txt = txt.substring(i0);
                    }
                    final String magic = new String(Dib2Constants.MAGIC_BYTES, StringFunc.CHAR8);
                    if (txt.startsWith(magic + "M(")) {
                      txt = StringFunc.string4Mnemonics(txt.replace("\n", ""));
                      String[] csvs = txt.split("\n");
                      boolean first = true;
                      for (String csv : csvs) {
                        if (first) {
                          first = false;
                          if (csv.startsWith(magic)) {
                            continue;
                          }
                        }
                        QmDbMessage msg0 = new QmDbMessage();
                        msg0.id_set(msg.id_get());
                        msg0.from_set(msg.from_get());
                        String[] els = csv.split("\t");
                        if (CsvQm_OLD.kiData >= els.length) {
                          msg0.time_set(msg.time_get());
                          msg0.text_set(csv);
                        } else if (els[CsvQm_OLD.kiOid].equals("TMP")
                            && els[CsvQm_OLD.kiCats].equals("ACK")) {
                          for (int ix = CsvQm_OLD.kiData; ix < els.length; ++ix) {
                            QmDbMessage mx = db.message_get_by_oid(els[ix]);
                            if (null != mx) {
                              mx.ack = 0;
                            }
                          }
                          msg0 = null;
                        } else {
                          QmDbMessage mx = CsvQm_OLD.message4CsvFields(msg.from_get(), els, true);
                          if (null != mx) {
                            msg0 = mx;
                          }
                        }
                        if (null != msg0) {
                          db.message_add(msg0);
                        }
                      }
                    } else {
                      db.message_add(msg);
                    }
                  }

                } else {
                  //            Dib2Root.log( "bg recv cb", "Message could not be
                  // decrypted/verified" );
                  QmDbContact contact = db.contact_get_person_by_address(from);
                  if (contact != null) {
                    contact.keystat_set(QmDbContact.KEYSTAT_SENT);
                    contact.time_lastact_set();
                  }
                  LocalMessage.send_statusMsg(svcContext, "?Key error - " + from);
                }
              }
              if (ct.getSubType().toLowerCase().equals("pgp-keys")) {
                svcContext.received_key(pgp, db, from, attachment, false);
              }
            } catch (Exception e) {
              Log.e("recv_cb", "" + e + e.getMessage());
            }
          }
          return true;
        }
      };

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onCreate() {
    //  svcContext = this;
    //  db = new quickmsg_db( svcContext );
    ++notification_id;
    Dib2Root.log("background_1", "onCreate");
  }

  public QmDbMessage received_quickmsg(Pgp pgp, QmDb_0 db, String from, MailAttachment a) {

    QuickMsg_0 qm = new QuickMsg_0();
    qm.parse_attachment(a);
    //  Dib2Root.log( "bg recv db", "got quickmsg" );
    //  idleTimeNet = MIN_IDLE_TIME;

    QmDbContact cdb, cdbf;
    QmDbContact mc = qm.get_contact();
    QmDbMessage msg = qm.get_message();
    int mgroup = qm.get_group();

    cdbf = db.contact_get_person_by_address(from);
    //  Dib2Root.log( "bg recv db", "got quickmsg " + mf.time_get() + '/' + cdbf.unread_get() );
    if (null != msg) {
      msg.time_set(DateFunc.alignTime(msg.time_get(), cdbf.unread_get()));
      if (msg.time_get() <= (cdbf.unread_get() + Dib2Constants.TIME_SHIFTED)) {
        cdbf.unread_set(msg.time_get() - Dib2Constants.TIME_SHIFTED - 1);
      }
    }
    if ((null == cdbf) || (QmDbContact.KEYSTAT_BIT_ACTIVE & cdbf.keystat_get()) == 0) {
      LocalMessage.send_statusMsg(svcContext, "?Chat-error - re-send key to " + from);
      return null;
    }

    ///// Group post might arrive before group info!
    if (qm.is_group || qm.is_grouppost) {
      cdb = db.contact_get_group_by_address_and_group(qm.group_owner, mgroup);
      if (cdb == null) {
        if (qm.group_owner.length() == 0) qm.group_owner = from;
        cdb = new QmDbContact();
        cdb.type_set(QmDbContact.TYPE_GROUP);
        cdb.address_set(qm.group_owner);
        cdb.group_set(qm.group_id);
        cdb.time_lastact_set(
            (null != msg)
                ? msg.time_get() // ,
                : ((null != mc) ? mc.time_lastact_get() : -1));
        cdb.name_set("group" + mgroup + "...");
        cdb.members_set(qm.group_owner);
        db.contact_add(cdb);
        cdb = db.contact_get_group_by_address_and_group(qm.group_owner, mgroup);
      }
    } else {
      cdb = cdbf;
    }
    if (qm.is_post && msg != null) {
      //    Dib2Root.log( "bg recv cb", "Has a post, add to db" );
      msg.id_set(cdb.id_get());
      msg.from_set(cdbf.id_get());
      //    Dib2Root.log( "bg recv cb", "id: " + cdb.id_get() );
      if (cdb.id_get() == 0) {
        Dib2Root.log("bg recv cb", "Not a valid id");
        return null;
      }
      List<String> cdbmembers = cdb.members_get();
      if ((qm.is_grouppost) && (cdbmembers != null)) {
        boolean foundmember = false;

        for (int i = 0; i < cdbmembers.size(); i++) {
          if (from.equals(cdbmembers.get(i))) {
            foundmember = true;
            break;
          }
        }
        if (!foundmember) {
          //        Dib2Root.log( "bg msg received", "Sender is not a group member" );
          //        return null;
          String cdbms = cdb.members_get_string() + ", " + from;
          cdb.members_set(cdbms);
        }
      }
      cdb.time_lastact_set(msg.time_get());
      //    doAlarm = true;
      //    pollthread.interrupt();
    }
    if (!qm.is_group) {
      if ((null == cdbf.name_get())
          || (0 >= cdbf.name_get().length())
          || (0 <= cdbf.name_get().indexOf('@'))) {
        String nam = mc.name_get();
        if ((null != nam) && ((1 < nam.trim().length()))) {
          cdbf.name_set(nam.trim());
        }
      }
    } else {
      cdb.time_lastact_set(mc.time_lastact_get());
      //      if (!from.equals(qm.group_owner)) {
      //        Log.e("recv cb", "Only owner can modify group");
      //        return null;
      //      }
      List<String> msgmembers = mc.members_get();
      List<String> cdbmembers = cdb.members_get();
      String cdbms = cdb.members_get_string();
      boolean foundSender = false;
      for (int i = 0; i < msgmembers.size(); i++) {
        final String mm = msgmembers.get(i);
        if (!cdbmembers.contains(mm)) {
          cdbms += ", " + mm;
        }
        if (mm.equals(from)) {
          foundSender = true;
        }
      }

      //    cdb.members_set( cf.members_get() );
      if (!foundSender) {
        // You may remove yourself.
        cdbms.replace(from + ", ", "");
        cdbms.replace(from, "");
      }
      cdb.members_set(cdbms);
      cdbmembers = cdb.members_get();

      if (cdbmembers.size() == 0) {
        Dib2Root.log("bg recv cb", "group has no members"); // , delete it" );
        //      db.contact_remove( cdb );
        return null;
      }
      String nam = mc.name_get();
      if ((null != nam) && (1 < nam.trim().length())) {
        cdb.name_set(nam.trim());
      }
      for (String mem : cdbmembers) {
        if (null == db.contact_get_person_by_address(mem)) {
          QmDbContact add = new QmDbContact();
          add.name_set(mem);
          add.address_set(mem);
          add.type_set(QmDbContact.TYPE_PERSON);
          db.contact_add(add);
        }
      }
    }

    db.contact_update(cdb);
    db.contact_update(cdbf);

    return msg;
  }

  public void received_key(Pgp pgp, QmDb_0 db, String from, MailAttachment a, Boolean signed) {
    String add;
    Boolean signedOk = false;
    Dib2Root.log("bg recv cb", "Received pgp key attachement");

    byte[] is;
    try {
      ByteArrayOutputStream buf = new ByteArrayOutputStream();
      //    is = a.datahandler.getInputStream();
      a.datahandler.writeTo(buf);
      buf.flush();
      is = buf.toByteArray();
    } catch (IOException e) {
      Dib2Root.log("bg recv cb", e.getMessage());
      // e.printStackTrace();
      return;
    }
    QmDbContact contact = db.contact_get_person_by_address(from);
    String oldfp = null;
    if (contact != null) {
      if ((QmDbContact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) != 0) {
        oldfp = pgp.fingerprint(from);
        signedOk = signed;
      }
    }
    // Dib2Root.log("recv cb", is.toString());
    // For now, do not override active key: the parsed key may belong to other address.
    String[] getter = new String[] {signedOk ? "X" : from}; // 1 ];
    byte[] oldK = pgp.public_keyring_add_key(is, getter);
    boolean added = (null != oldK);
    add = getter[0];

    if ((add != null) && add.contains("@")) {

      contact = db.contact_get_person_by_address(add);
      if ((null != contact) && (1 >= contact.id_get())) {
        return;
      }
      Dib2Root.log("bg recv cb", "key added to ring");
      if ((contact == null) || (0 == (QmDbContact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()))) {
        Dib2Root.log("bg recv_cb", "new key, add contact");
        if (null == contact) {
          contact = new QmDbContact();
        }
        contact.address_set(add);
        contact.type_set(QmDbContact.TYPE_PERSON);
        contact.time_lastact_set(); // time_now );
        // if (!signedOk)
        contact.keystat_set(QmDbContact.KEYSTAT_RECEIVED);
        // else
        // contact.keystat_set(contact.KEYSTAT_VERIFIED);

        db.contact_add(contact);
        LocalMessage.send_statusMsg(svcContext, "");
        return;
      }
      contact.time_lastact_set();
      if (added) { // && add.equals( from )) {
        contact.keystat_set(QmDbContact.KEYSTAT_RECEIVED);
        return;
      }
      if (add.equals(from)) {
        oldfp = (null == oldfp) ? pgp.fingerprint(add) : oldfp;
        try {
          int old = contact.keystat_get(); // : Contact.KEYSTAT_CONFLICT;
          contact.keystat_set(0);
          pgp.public_keyring_remove_by_address(add);
          // is = buf.toByteArray();
          pgp.public_keyring_add_key(is, getter);
          if (null != getter[0]) {
            //          if ((Contact.KEYSTAT_CONFLICT == old) && pgp.fingerprint( add ).equals(
            // oldfp ))
            // {
            //            old = Contact.KEYSTAT_VERIFIED;
            //          }
            contact.keystat_set(old);
          }
        } catch (Exception e) {
          Dib2Root.log("bg recv_db", "key change failed " + e);
          oldfp = null;
        }
        Dib2Root.log("bg recv_db", "Existing contact, update if needed " + contact.keystat_get());
        //      long time_now = DateFunc.currentTimeMillisLinearized();
        //      contact.time_lastact_set(); // time_now );
        if (!pgp.fingerprint(add).equals(oldfp)) {
          contact.keystat_set(QmDbContact.KEYSTAT_RECEIVED);
        } else {
          switch (contact.keystat_get()) {
            case QmDbContact.KEYSTAT_RECEIVED:
              break;
            case QmDbContact.KEYSTAT_VERIFIED:
              contact.keystat_set(QmDbContact.KEYSTAT_CONFIRMED);
              break;
            default:
              if (!signedOk) contact.keystat_set(QmDbContact.KEYSTAT_PENDING);
          }
        }
        Dib2Root.log("bg recv_db", "update db " + contact.keystat_get());
        db.contact_update(contact);
      }
    } else if (null == add) {
      Dib2Root.log("bg recv_db", "something is wrong with the key");
    }
    LocalMessage.send_statusMsg(svcContext, "");
  }

  public void received_attachment(QmDb_0 db, QmDbMessage m, MailAttachment a, String ext) {
    Dib2Root.log("bg received attachement", "going to save attachment");
    //    File sd = Environment.getExternalStorageDirectory();
    //    String dir = sd.getAbsolutePath() + "/QuickMSG";
    File df = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    df = new File(df, "dibdib");
    df.mkdirs();
    String dir = df.getAbsolutePath();
    //    File df = new File(dir);
    String filenamebase;
    if (a.name == null) {
      filenamebase = UUID.randomUUID().toString() + "." + ext;
    } else {
      filenamebase = a.name;
    }
    File mf;
    int nr = 0;
    String filename;
    do {
      filename = filenamebase + (nr == 0 ? "" : "_" + nr);
      filename =
          ((0 != nr) && (1 < filename.indexOf('.')))
              ? filenamebase.replace(".", "_" + nr + '.')
              : filename;
      filename = ((ext == null) || filename.endsWith(ext)) ? filename : (filename + '.' + ext);
      mf = new File(dir, filename);
      if (!mf.exists()) {
        break;
      }
      Dib2Root.log("bg received attachment", "exists: " + nr);
      nr++;
    } while (true);
    String name = mf.getPath();
    df.mkdirs();
    OutputStream os;
    String type = a.datahandler.getDataSource().getContentType();
    type = type.split(";")[0];
    String basetype = type.split("/")[0].toLowerCase();

    Dib2Root.log("bg received attachment", "name: " + name);

    try {
      os = new FileOutputStream(mf);
      a.datahandler.writeTo(os);
      os.flush();
      os.close();
    } catch (Exception e) {
      String err = e.getMessage();
      Log.e("received attachment", "err: " + (err != null ? err : "null"));
      return;
    }

    Uri uri;
    if (basetype.equals("image")) {
      Dib2Root.log("bg received attachment", "add image to media library");
      ContentValues values = new ContentValues(5);

      values.put(Images.Media.TITLE, filename);
      values.put(Images.Media.DISPLAY_NAME, name);
      values.put(Images.Media.DATE_TAKEN, m.time_get());
      values.put(Images.Media.MIME_TYPE, type);
      values.put(Images.Media.DATA, name);

      ContentResolver cr = svcContext.getContentResolver();
      uri = cr.insert(Images.Media.EXTERNAL_CONTENT_URI, values);
    } else if (basetype.equals("video")) {
      Dib2Root.log("bg received attachment", "add video to media library");
      ContentValues values = new ContentValues(5);

      values.put(Video.Media.TITLE, filename);
      values.put(Video.Media.DISPLAY_NAME, name);
      values.put(Video.Media.DATE_TAKEN, m.time_get());
      values.put(Video.Media.MIME_TYPE, type);
      values.put(Video.Media.DATA, name);

      ContentResolver cr = svcContext.getContentResolver();
      uri = cr.insert(Video.Media.EXTERNAL_CONTENT_URI, values);
    } else if (basetype.equals("audio")) {
      Dib2Root.log("bg received attachment", "add audio to media library");
      ContentValues values = new ContentValues(5);

      values.put(Audio.Media.TITLE, filename);
      values.put(Audio.Media.DISPLAY_NAME, name);
      values.put(Audio.Media.DATE_ADDED, m.time_get());
      values.put(Audio.Media.MIME_TYPE, type);
      values.put(Audio.Media.DATA, name);

      ContentResolver cr = svcContext.getContentResolver();
      uri = cr.insert(Audio.Media.EXTERNAL_CONTENT_URI, values);
    } else {
      uri = Uri.parse(name);
    }
    m.text_set("::" + basetype + "::" + name + '\n' + m.text_get());
    m.uri_set(uri);
  }

  public int findUnread() {
    //  int unread = 0;
    int unread_contacts = 0;
    int unread_contact = 0;

    /* get unread messages */
    List<QmDbContact> contacts = db.contact_get_all();
    //  Dib2Root.log( "background", "notification " + contacts.size() );
    boolean arriving = (unread >= 0);
    unread = 0;
    for (int i = 0; i < contacts.size(); i++) {
      QmDbContact contact = contacts.get(i);
      if (contact.id_get() <= 1) {
        continue;
      }
      long lastact = contact.time_lastact_get();
      long unreadt = contact.unread_get();

      if (lastact > unreadt) {
        int cu = db.message_get_unread_by_chat(contact.id_get(), unreadt);
        unread += cu;
        if (cu > 0) {
          unread_contacts++;
          unread_contact = contact.id_get();
        }
      }
    }
    unread = ((0 < unread) || (arriving && !alarmDone)) ? unread : -1;
    return (unread_contacts == 1) ? unread_contact : ((unread > 0) ? 0 : (unread - 1));
  }

  public void update_ui(boolean ack) {
    int unread_contact = findUnread();
    Dib2Root.log(
        "bg update_ui", "unread: " + unread + alarmDone + idleTimeNet / 1000 + ' ' + flagsNotify);

    if (notification != null) {

      // Creates an explicit intent for an Activity in your app
      Intent resultIntent;
      if (unread_contact <= 1) {
        resultIntent = new Intent(this, net.sourceforge.dibdib.android_qm.ListActivity.class);
      } else {
        resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("id", unread_contact);
        //      Dib2Root.log( "bg update_ui", "extra id: " + unread_contact + ", " +
        // background_1.flagsNotify + ack );
      }

      resultIntent.setFlags(
          Intent.FLAG_ACTIVITY_NEW_TASK
              | Intent.FLAG_ACTIVITY_CLEAR_TOP); // Intent.FLAG_ACTIVITY_CLEAR_TASK );
      PendingIntent pending_intent =
          PendingIntent.getActivity(
              svcContext,
              0,
              resultIntent,
              PendingIntent.FLAG_CANCEL_CURRENT); // .FLAG_UPDATE_CURRENT);

      NotificationManager mNotificationManager =
          (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      notification.setNumber((0 < unread) ? unread : 0);
      notification.setContentTitle(getString(R.string.app_name));
      if (0 <= unread) { // || (0 < unread_contacts)) { // && !ack) {
        notification.setSmallIcon(R.drawable.ic_launcher_t);
        notification.setLargeIcon(
            BitmapFactory.decodeResource(svcContext.getResources(), R.drawable.ic_launcher_t));
        int flags = ((0 != (QmBg_0.flagsNotify & 1)) ? Notification.DEFAULT_LIGHTS : 0);
        if (!alarmDone) {
          if (0 < unread) {
            alarmDone = true;
            flags |=
                ((0 != (QmBg_0.flagsNotify & 2)) ? Notification.DEFAULT_VIBRATE : 0) // ,
                    | ((0 != (QmBg_0.flagsNotify & 4)) ? Notification.DEFAULT_SOUND : 0);
          }
        }
        notification.setDefaults(flags);
      } else {
        alarmDone = false;
        notification.setSmallIcon(R.drawable.ic_stat_qm);
        notification.setLargeIcon(
            BitmapFactory.decodeResource(svcContext.getResources(), R.drawable.ic_stat_qm));
        notification.setDefaults(0);
      }
      notification.setContentText(getString(R.string.status_unread) + ((0 < unread) ? unread : 0));
      notification.setContentIntent(pending_intent);
      mNotificationManager.notify(notification_id, notification.getNotification()); // .build() );
    }
  }

  public void alarm() {
    // Done by Notification flags ...
    // NOP
    ;
    Uri uri = Uri.parse("default ringtone");
    if ((null != uri) && (0 != (flagsNotify & 4))) {
      Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), uri);
      if (null != r) {
        r.play();
      }
    }
  }

  public void alarm_OLD() {
    //  SharedPreferences getAlarms = PreferenceManager.
    //    getDefaultSharedPreferences( this ); // getBaseContext() );
    //  Boolean notify = getAlarms.getBoolean( "notifications_new_message", false );
    boolean notify = false;
    Dib2Root.log("alarm", "notify? " + (notify ? "yes" : "no"));
    if (!notify) return;
    //
    //  String alarms = getAlarms.getString( "notifications_new_message_ringtone", "default
    // ringtone"
    // );
    Uri uri = Uri.parse("default ringtone"); // alarms );
    if (null != uri) {
      Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), uri);
      if (null != r) {
        r.play();
      }
    }
    Boolean vibrate = true; // getAlarms.getBoolean( "notifications_new_message_vibrate", false );
    Dib2Root.log("alarm", "vibrate? " + (vibrate ? "yes" : "no"));
    if (!vibrate) return;

    Vibrator v = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
    if (null != v) {
      // Vibrate for 500 milliseconds
      v.vibrate(500);
    }
  }
  // =====
}
