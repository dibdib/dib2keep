// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.content.*;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.gitlab.dibdib.dib2qm.*;
import java.io.*;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;

public class QmDb_0 extends // ,
    CsvQm_OLD // ,
    implements com.gitlab.dibdib.dib2qm.ContextIf_OLD {
  // =====

  public final Context qCtx;
  private static String zPassImapHex = "";
  private static String zPassSmtpHex = "";

  public QmDb_0(Object context) {
    super(null);
    qCtx = (Context) context;
  }

  private static String getShortDay() {
    return DateFunc.dateShort4Millis(DateFunc.currentTimeMillisLinearized()).substring(0, 6);
  }

  @Override
  public String fileReady() {
    String out = super.fileReady();
    if (null != out) {
      return out;
    }
    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    if (!dir.exists()) {
      return null;
    }
    File file = new File(dir, qDbFileName);
    if (file.exists()) {
      return file.getAbsolutePath();
    }
    dir = new File(dir, "dibdib");
    if (!dir.exists()) {
      return null;
    }
    String[] names = dir.list();
    if (null == names) {
      return null;
    }
    String name = null;
    long time = -1;
    for (String x0 : names) {
      if (x0.matches(qDbFileName.replace(".", ".*"))) {
        long x1 = new File(dir, x0).lastModified();
        if (time < x1) {
          time = x1;
          name = x0;
        }
      }
    }
    if (0 < time) {
      return new File(dir, name).getAbsolutePath();
    }
    return null;
  }

  public void export_db(boolean full) {
    //  Dib2Root.log( "qdb export", "start" );
    save();
    close();
    //  if (!Environment.getExternalStorageState().equals(
    String log = MiscFunc.logBuffer.toString();
    set("log", "");
    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    Dib2Root.log("qdb export", path.getAbsolutePath());
    path = new File(path, "dibdib");
    path.mkdirs();
    String name = full ? getShortDay().substring(4, 6) : "bak";
    name = qDbFileName.replace(".dm", "." + name + ".dm");
    File csvPath = new File(path, name);
    if (!full
        && csvPath.exists()
        && ((csvPath.lastModified() + 12 * Dib2Constants.MAX_DELTA_ACCESS_CHECK_MSEC)
            >= DateFunc.currentTimeMillisLinearized())) {
      return;
    }
    int ok = write(csvPath.getAbsolutePath(), true, false);
    if (!full) {
      return;
    }
    if (0 < ok) {
      Toast.makeText(qCtx, "ENCODED CSV FILE: " + csvPath.toString(), Toast.LENGTH_LONG).show();
    }
    AssetManager am = qCtx.getAssets();
    String zipPath = new File(path, "dibdib_src.7z").getAbsolutePath();
    try {
      byte[] buffer = log.getBytes(StringFunc.CHAR16UTF8);
      MiscFunc.writeFile(
          new File(path, "dibdib_log.tmp").getAbsolutePath(), buffer, 0, buffer.length, null);
      InputStream is = am.open("assets.7z");
      buffer = new byte[1800000];
      int len = 0;
      while (true) {
        len += is.read(buffer, len, buffer.length - len);
        if (len < (buffer.length)) {
          break;
        }
        buffer = Arrays.copyOf(buffer, buffer.length * 2);
      }
      MiscFunc.writeFile(zipPath, buffer, 0, len, null);
    } catch (Exception e) {
      // Ignore.
    }
  }

  public void archive_old_messages() {
    save();
    close();
    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    Dib2Root.log("qdb archive", path.getAbsolutePath());
    path = new File(path, "dibdib");
    path.mkdirs();
    String name = getShortDay().substring(0, 6);
    name = qDbFileName.replace("QM.", ".arc.").replace(".dm", "." + name + ".QM.dm");
    File csvPath = new File(path, name);
    if (csvPath.exists()) {
      return;
    }
    int ok = archive(csvPath, Dib2Constants.MAXVIEW_MSGS_INIT);
    if (0 < ok) {
      Toast.makeText(qCtx, "ENCODED CSV FILE: " + csvPath.toString(), Toast.LENGTH_LONG).show();
    }
  }

  public Boolean import_db(String... optionalAcNPhrase) {
    save();
    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    String combined =
        ((null == optionalAcNPhrase)
                || (0 >= optionalAcNPhrase.length) // -
                || (null == optionalAcNPhrase[0])
                || (1 >= optionalAcNPhrase[0].length())) // -
            ? null
            : optionalAcNPhrase[0].trim();
    combined = (3 >= combined.length()) ? null : combined;
    int split = (null == combined) ? 0 : combined.indexOf(' ');
    split = (0 >= split) ? 2 : split;
    final String ac = (null == combined) ? null : combined.substring(0, split);
    final String phrase =
        (null == combined)
            ? null
            : combined.substring(((' ' == combined.charAt(split)) ? 1 : 0) + split);
    int count = importFile(new File(path, qDbFileName).getAbsolutePath(), phrase, ac, false);
    Toast.makeText(qCtx, (0 <= count) ? "OKAY" : "ERROR", Toast.LENGTH_SHORT).show();
    return true;
  }

  @Override
  public File getFilesDir(String... parameters) {
    return qCtx.getFilesDir();
  }

  @Override
  public void log(String... aMsg) {
    Log.d(aMsg[0], aMsg[1]);
  }

  // @Override
  @Override
  public void toast(String msg) {
    Toast.makeText(qCtx, msg, Toast.LENGTH_LONG).show();
  }

  public int load4Sp(boolean useBackup) {
    //  SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
    int rc = super.load(useBackup);
    if (0 >= rc) {
      String name = fileReady(); // false );
      Dib2Root.log("qdb load4Sp", name + " " + useBackup);
      if ((null == name) || !useBackup) {
        return rc;
      }
      rc = importFile(name, null, null, true);
      if (0 >= rc) {
        return rc;
      }
    }
    //  String last = getLiteral( "lastId", "" );
    //  if (8 < last.length()) {
    //    MiscFunc.initLastId( last );
    //  }
    db2pref();
    return rc;
  }

  @Override
  public int load(boolean useBackup) {
    Dib2Root.log("qdb load", "bak " + useBackup);
    if (!TcvCodec.instance.settleHexPhrase(null)) {
      return -1;
    }
    return load4Sp(useBackup);
  }

  public int load() {
    return load(true); // 1 <= zCountAttempts );
  }

  @Override
  public byte[] preference_get(String pref) {
    byte[] out = null;
    if ((null == zPassImapHex) || (2 >= zPassImapHex.length())) {
      zPassImapHex = TcvCodec.instance.getPassPhraseHex();
      zPassSmtpHex = TcvCodec.instance.getAdditionalCodes();
      zPassSmtpHex =
          ((null == zPassSmtpHex) || (2 >= zPassSmtpHex.length())) ? zPassImapHex : zPassSmtpHex;
    }
    out = pref.equals("imap_pass") ? StringFunc.bytes4Hex(zPassImapHex) : out;
    out = pref.equals("smtp_pass") ? StringFunc.bytes4Hex(zPassSmtpHex) : out;
    out = pref.equals("dbFileName") ? StringFunc.bytesUtf8(CsvQm_OLD.qDbFileName) : out;
    out = (null == out) ? super.preference_get(pref) : out;
    return out;
  }

  @Override
  public void preference_set(String key, byte[] value) {
    if (!key.endsWith("_pass")) {
      super.preference_set(key, value);
      return;
    } else if (key.startsWith("imap")) {
      String pass = StringFunc.hex4Bytes(value, false);
      boolean equal = (null != zPassImapHex) && zPassImapHex.equals(zPassSmtpHex);
      zPassImapHex = (2 < pass.length()) ? pass : zPassImapHex;
      TcvCodec.instance.settleHexPhrase(zPassImapHex); // zAccessCodeHex + zPassImapHex );
      zPassSmtpHex =
          ((null == zPassSmtpHex) || (2 >= zPassSmtpHex.length()) || equal) ? pass : zPassSmtpHex;
    } else if (key.startsWith("smtp")) {
      String pass = StringFunc.hex4Bytes(value, false);
      zPassSmtpHex = (2 < pass.length()) ? pass : zPassSmtpHex;
    }
    if ((null != zPassSmtpHex) && !zPassSmtpHex.equals(zPassImapHex)) {
      TcvCodec.instance.setAdditionalCodes(zPassSmtpHex);
    }
  }

  public void preference_add(String key, byte[] value) {
    preference_set(key, value);
  }

  public void set(String pref, String value) {
    preference_set(pref, (null == value) ? null : StringFunc.bytesUtf8(value));
  }

  @Override
  public void remove(String pref) {
    preference_set(pref, null);
  }

  @Override
  public String getLiteral(String pref, String defaultValue) {
    byte[] out = preference_get(pref);
    return (null == out) ? defaultValue : StringFunc.string4Utf8(out);
  }

  @Override
  public byte[] get(String prefKey, byte[] defaultValue) {
    byte[] out = preference_get(prefKey);
    //  String out = getLiteral( prefKey, (null == defaultValue) ? null : MiscFunc.toString(
    // defaultValue ) );
    return (null == out) ? defaultValue : out; // MiscFunc.toBytes( out );
  }

  @Override
  public String getHex(String prefKey, String defaultValueHex) {
    boolean marked = (null != defaultValueHex) && defaultValueHex.startsWith("X");
    byte[] out = get(prefKey, StringFunc.bytes4Hex(defaultValueHex));
    return (null == out) ? null : StringFunc.hex4Bytes(out, marked);
  }

  @Override
  public void set(String prefKey, byte[] value) {
    preference_set(prefKey, value);
  }

  @Override
  public void setHex(String prefKey, String hexLiteral) {
    set(prefKey, (null == hexLiteral) ? null : StringFunc.bytes4Hex(hexLiteral));
  }

  @Override
  public void setLiteral(String prefKey, String value) {
    set(prefKey, (null == value) ? null : value); // StringFunc.bytes4Hex( value ) );
  }

  @Override
  public synchronized void save() {
    TcvCodec.instance.writePhrase();
    super.save();
  }

  public void pref2db() {
    final String[] keys = {
      "display_name",
      "email_address",
      "imap_user",
      "smtp_user",
      "imap_server",
      "imap_port",
      "smtp_server",
      "smtp_port",
    };
    SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(qCtx);
    String check;
    check = SP.getString("imap_pass", "").trim();
    if (0 < check.length()) {
      preference_set("imap_pass", check.getBytes(StringFunc.CHAR16UTF8));
    }
    check = SP.getString("smtp_pass", "").trim();
    if (0 < check.length()) {
      preference_set("smtp_pass", check.getBytes(StringFunc.CHAR16UTF8));
    }
    Editor editor = SP.edit();
    editor.putString("imap_pass", "");
    editor.putString("smtp_pass", "");
    editor.commit();
    for (String key : keys) {
      check = SP.getString(key, "").trim();
      if (0 < check.length()) {
        preference_set(key, check.getBytes(StringFunc.CHAR16UTF8));
      }
    }
  }

  public void db2pref() {
    final String[] keys = {
      "display_name",
      "email_address",
      "imap_user",
      "smtp_user",
      "imap_server",
      "imap_port",
      "smtp_server",
      "smtp_port",
    };
    if (qCtx != null) {
      SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(qCtx);
      Editor editor = SP.edit();
      for (String key : keys) {
        String value = getLiteral(key, "");
        if ((null == value) || (0 >= value.length())) {
          editor.remove(key);
        } else {
          editor.putString(key, value);
        }
      }
      editor.putString("imap_pass", "");
      editor.putString("smtp_pass", "");
      editor.commit();
    }
  }

  @Override
  public void invalidate() {
    assert false : "Not to be used here.";
  }

  // @Override
  // public int getTextRow( int xYReal, int xRange ) {
  //  assert false : "Not to be used here.";
  //  return 0;
  // }
  //
  // @Override
  // public int getTextColumn( int xXReal, String xText ) {
  //  assert false : "Not to be used here.";
  //  return 0;
  // }
  //
  // @Override
  // public void setTouched4TextPos() {
  //  assert false : "Not to be used here.";
  // }

  @Override
  public String[] getLicense(String[] xAdditionalVersionInfo, String... resources) {
    assert false : "Not to be used here.";
    return null;
  }

  @Override
  public boolean pushClipboard(String label, String text) {
    assert false : "Not to be used here.";
    return false;
  }

  @Override
  public String getClipboardText() {
    assert false : "Not to be used here.";
    return null;
  }

  @Override
  public InputStream openInputStreamGetType(Object xUri, String[] yType) {
    assert false : "TODO.";
    return null;
  }

  @Override
  public Object parseUri(String path) {
    assert false : "Not to be used here.";
    return null;
  }

  // =====
}
