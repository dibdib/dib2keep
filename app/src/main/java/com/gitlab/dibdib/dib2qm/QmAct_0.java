// Copyright (C) 2016  Roland Horsch and others:
// -- Changes:  Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.app.AlertDialog;
import android.content.*;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.*;
import android.text.Layout.Alignment;
import android.text.method.LinkMovementMethod;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.gitlab.dibdib.dib2qm.*;
import com.gitlab.dibdib.picked.net.*;
import java.io.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android_qm.background;

// Due to old naming of classes.
@SuppressWarnings("static-access")
public class QmAct_0 extends QuickmsgActivity_1 {
  // =====

  // DIFF (_0):
  // protected quickmsg_db db = new quickmsg_db(this);
  // protected Contact contact;
  // protected long unreadTime = 0;
  // m.time_set(-1);
  // (onCreate:) unreadTime = contact.unread_get(); checkBackground();
  // Pgp pgp_enc = new Pgp(db); //context);

  // TODO: HTML tags
  // WebView webView = (WebView) findViewById(R.id.webView);
  // webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "UTF-8", null);
  /*
  b  Bold
  i  Italics
  u  Underline
  sub  Subtext
  sup  Supertext
  big  Big
  small  Small
  tt  Monospace
  h1 … h6  Headlines
  <font color="red"...
  <blockquote>Example <a href=\"www...\">www</a></blockquote>
  */

  // public static final int MAXVIEW_MSGS_ADD = 5;
  int maxview = Dib2Constants.MAXVIEW_MSGS_INIT;
  protected static String zMessageTextbox = null;
  protected static int zMessageTextboxRef = 0;

  protected QmDbContact contact;
  protected TextView msg_viewer;
  // final MainActivity act = this;
  protected long unreadTime = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);
    Intent intent = getIntent();
    int id;
    String action = intent.getAction();
    EditText new_msg = (EditText) findViewById(R.id.new_msg);

    new_msg.setText("");
    // maxview = MAXVIEW_INIT;
    msg_viewer = (TextView) findViewById(R.id.msg_viewer);

    if (Intent.ACTION_SEND.equals(action)) {
      Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
      if (uri != null) {
        select_contact(this, uri);
      }
    } else {
      id = intent.getIntExtra("id", -1);
      Dib2Root.log("main actvitity", "id: " + id);

      contact = db.contact_get_by_id(id);
    }
    if (contact == null) return;

    setTitle(getString(R.string.title_activity_main) + " - " + contact.name_get());

    // rho
    unreadTime = contact.unread_get();
    display_contact();
    checkBackground();
  }

  public void select_contact(final Context context, final Uri uri) {
    final List<QmDbContact> contactlist = db.contact_get_sendable();
    List<CharSequence> names = new ArrayList<CharSequence>();

    for (int i = 0; i < contactlist.size(); i++) {
      QmDbContact p = contactlist.get(i);
      String add = p.address_get();
      String name = p.name_get() + " (" + add + ")";

      names.add(name);
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    // Set the dialog title
    builder.setTitle(R.string.action_select_contact);
    // Specify the list array, the items to be selected by default (null for none),
    // and the listener through which to receive callbacks when items are selected
    builder.setSingleChoiceItems(
        names.toArray(new CharSequence[names.size()]),
        -1,
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            contact = contactlist.get(which);
            Dib2Root.log("select contact", "which: " + which);
          }
        });
    // Set the action buttons
    builder.setPositiveButton(
        "Ok",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int id) {
            Dib2Root.log("select contact", "OK " + (contact == null ? "null" : "contact"));
            if (contact != null) {
              Dib2Root.log("onresume", "going to send message");
              send_msg_attachment(context, uri);
            }
            finish();
            return;
          }
        });

    builder.setNegativeButton(
        "Stop",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int id) {
            Dib2Root.log("select contact", "Stop");
            contact = null;
            finish();
            return;
          }
        });
    builder.show();
  }

  @Override
  public void onUserLeaveHint() {
    super.onUserLeaveHint();
    long viewed = DateFunc.currentTimeMillisLinearized() - 2000;
    if (viewed > contact.unread_get()) {
      contact.unread_set(viewed);
    }
  }

  @Override
  public void onUserInteraction() {
    super.onUserInteraction();
    unreadTime = contact.unread_get();
  }

  @Override
  protected void onPause() {
    super.onPause();
    EditText new_msg = (EditText) findViewById(R.id.new_msg);
    String tb = new_msg.getText().toString();
    if ((null != tb) && (0 < tb.length())) {
      zMessageTextbox = tb;
    }
    zMessageTextboxRef = 0;
  }

  @Override
  protected void onResume() {
    super.onResume();
    if ((null != zMessageTextbox) && (0 < zMessageTextbox.length())) {
      EditText new_msg = (EditText) findViewById(R.id.new_msg);
      new_msg.setText(zMessageTextbox);
      zMessageTextbox = null;
    }
    zMessageTextboxRef = 0;
    contact = (null == contact) ? db.contact_get_by_id(0) : contact;
    unreadTime = contact.unread_get();
    if (unreadTime < contact.time_lastact_get()) {
      contact.unread_set(contact.time_lastact_get());
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.contact, menu);
    return true;
  }

  public boolean send_msg_attachment(final Context context, Uri uri) {
    EditText new_msg = (EditText) findViewById(R.id.new_msg);
    String sMsg = new_msg.getText().toString();

    if (0 < background.unread) {
      background.alarmDone = true;
    }

    if (sMsg.length() < 1 && uri == null) return false;

    zMessageTextbox = sMsg;
    maxview = Dib2Constants.MAXVIEW_MSGS_INIT;
    //  long time_now = DateFunc.currentTimeMillisLinearized();
    final QmDbMessage msg0 = new QmDbMessage();
    msg0.id_set(contact.id_get());
    msg0.from_set(1);
    msg0.time_set(-1); // time_now );
    msg0.text_set(zMessageTextbox);
    if (uri != null) {
      msg0.uri_set(uri);
    }
    msg0.oid = "" + DateFunc.createId();
    // DateFunc.createOldId(
    //    (null != zMessageTextbox) && (0 < zMessageTextbox.length())
    //        ? zMessageTextbox
    //        : ((null == uri) ? "" : uri.toString()),
    //    msg0.time_get());

    boolean local = (contact.id_get() <= 1) || !contact.address_get().contains("@");
    if (local) {
      db.message_add(msg0);
      //    Intent intent = new Intent();
      LocalMessage.send_statusMsg(context, "?");
      new_msg.setText("");
      return true;
    }

    final long now = DateFunc.currentTimeMillisLinearized();
    QmDbMessage msgFull = msg0;
    db.message_add(msg0);
    if ((contact.type_get() == QmDbContact.TYPE_PERSON)
        && ((QmDbContact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) == 0)) {
      zMessageTextbox = "Not sent (MISSING KEY):\n" + zMessageTextbox;
      msg0.text_set(zMessageTextbox);
    } else
    // TODO ACK/ re-send for groups
    if ((uri == null) && (contact.type_get() != QmDbContact.TYPE_GROUP)) {
      //    String addr = contact.address_get();
      //    final int version = (prefs.get( "KEY." + addr + ".CIPHER", new byte[1])[0] == 'C')
      //      ? Dib2Constants.FILE_STRUC_VERSION : Dib2Constants.FILE_STRUC_VERSION_MIN;
      //    if (Dib2Constants.FILE_STRUC_VERSION > version) {
      //      if (!mail.toFlush) {
      //        List<message> messages = db.message_dangling_by_id( contact.id_get(), 30);
      //        for (message mx : messages) {
      //          mx.ack = 0;
      //          db.message_update( mx);
      //        }
      //      }
      //    } else {
      msg0.ack = now + 10 * 60 * 1000;
      db.message_update(msg0);
      msgFull = new QmDbMessage();
      msgFull.id_set(msg0.id_get());
      msgFull.from_set(1);
      msgFull.time_set(-1); // time_now );
      List<QmDbMessage> messages = db.message_dangling_by_id(contact.id_get(), 30);
      StringBuilder mText =
          new StringBuilder(50 + 3 * msg0.text_get().length() + 30 * messages.size());
      mText.append(
          new String(Dib2Constants.MAGIC_BYTES)
              + "M("
              + DateFunc.date4Millis()); // date4Millis_OLD(false));
      mText.append(")" + Dib2Constants.VERSION_STRING + '\n');
      mText.append(Mapping__OLD.toCsvLine(msg0.oid, null, msg0.time_get(), "MSG", msg0.text_get()));
      mText.append('\n');
      String ack = "";
      int cResend = 0;
      for (QmDbMessage mx : messages) {
        if (0 == mx.ack) {
          continue;
        } else if ((null != mx.uri_get()) || (0 >= mx.text_get().length())) {
          mx.ack = 0;
        } else if ((1 >= mx.from_get()) && (10 > cResend)) {
          // Queued? Do not re-send too quickly:
          if (0 > mx.ack) {
            mx.ack = mx.time_get() + 30 * 60 * 1000;
          }
          if (mx.ack < now) {
            ++cResend;
            mText.append(Mapping__OLD.toCsvLine(mx.oid, null, mx.time_get(), "MSG", mx.text_get()));
            mText.append('\n');
            mx.ack = now + (30 * 60 * 1000 + mx.ack - mx.time_get());
            db.message_update(mx);
          }
        }
        if (1 < mx.from_get()) { // && (null == mx.queue_get())) {
          mx.ack = 0;
          db.message_update(mx);
          ack += "\t" + mx.oid;
        }
      }
      if (0 < ack.length()) {
        mText.append(Mapping__OLD.toCsvLine("TMP", null, -1, "ACK", ack));
        mText.append('\n');
      }
      if (5 > cResend) {
        for (QmDbMessage mx : messages) {
          if ((mx.ack >= (now + 40 * 60 * 1000)) && (1 >= mx.from_get()) && (5 >= cResend)) {
            ++cResend;
            mText.append(Mapping__OLD.toCsvLine(mx.oid, null, mx.time_get(), "MSG", mx.text_get()));
            mText.append('\n');
          }
        }
      }
      msgFull.text_set(StringFunc.mnemonics4String(mText.toString(), false, false));
    }
    final QmDbMessage msg1 = msgFull;

    final MailAttachment unenc;
    //  zMessageTextbox = null;
    //  zMessageTextboxRef = 0;

    Dib2Root.log(
        "mainactivity send_msg", "send to " + contact.name_get() + "id: " + contact.id_get());

    QuickMsg_0 qmsg = new QuickMsg_0();
    unenc = qmsg.send_message(this, db.contact_get_by_id(1), contact, msg1);
    //  Dib2Root.log( "mainactivity send_msg", "got message unencrypted" );
    final List<String> to_adds;
    if (contact.type_get() == QmDbContact.TYPE_GROUP) {
      to_adds = contact.members_get();
      if ((null == to_adds) || (0 == to_adds.size())) return true;
    } else {
      to_adds = new LinkedList<String>();
      to_adds.add(contact.address_get());
    }

    new Thread(
            new Runnable() {
              @Override
              public void run() {
                Pgp pgp_enc = new Pgp(db);

                MailAttachment id = pgp_enc.pgpmime_id();

                for (int i = 0; i < to_adds.size(); i++) {
                  String to = to_adds.get(i);
                  if (to.equals(pgp.my_user_id)) continue;
                  if ((contact.type_get() == QmDbContact.TYPE_PERSON)
                      && ((QmDbContact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) == 0)) continue;

                  MailAttachment enc;
                  try {
                    enc = pgp_enc.encrypt_sign(unenc, to);
                  } catch (OutOfMemoryError e) {
                    Log.e("send_msg", "Out of memory during encryption, attachment to big?");
                    enc = null;
                  } catch (Exception e) {
                    Log.e("send_msg", "pgp exception (missing key?...): " + e);
                    enc = null;
                  }
                  if (enc == null) {
                    if (!msg1.text_get().contains("[Error")) {
                      msg1.text_set(msg1.text_get() + "\n[Error!]");
                      db.message_update(msg1);
                    }
                    continue;
                  }
                  enc.disposition = "inline";

                  Dib2Root.log("mainactivity send_msg", "got message encrypted");
                  List<MailAttachment> attachments = new LinkedList<MailAttachment>();
                  attachments.add(id);
                  attachments.add(enc);

                  String queue = background.mail.send(context, to, attachments, "encrypted");
                  if (queue != null) {
                    msg1.ack = -1; // .queue_set( queue );
                    db.message_update(msg1);
                    LocalMessage.send_statusMsg(context, "?");
                  }
                }
                Dib2Root.log("mainactivity send_msg", "mail.send done");
              }
            })
        .start();

    new_msg.setText("");
    return true;
  }

  /** Called when the user clicks the Send button */
  public void send_msg(View view) {
    checkBackground();
    if (send_msg_attachment(view.getContext(), null)) {
      zMessageTextbox = null;
      zMessageTextboxRef = 0;
    } else if (null == zMessageTextbox) {
      zMessageTextbox = "";
      zMessageTextboxRef = 0;
    } else if (0 == zMessageTextboxRef) {
      ///// Copy to text field.
      zMessageTextboxRef = -1;
      List<QmDbMessage> messages = db.message_get_by_id(contact.id_get(), 2);
      if ((null != messages) && (0 < messages.size())) {
        final int cnt = messages.size();
        String sLast = messages.get(cnt - 1).text_get();
        if (1 < cnt) {
          sLast = messages.get(cnt - 2).text_get() + "\n" + sLast;
        }
        EditText new_msg = (EditText) findViewById(R.id.new_msg);
        new_msg.setText(sLast);
      }
    } else if (0 > zMessageTextboxRef) {
      ///// Copy to clipboard.
      --zMessageTextboxRef;
      List<QmDbMessage> messages = db.message_get_by_id(contact.id_get(), maxview + 1);
      if ((null != messages) && (0 < messages.size())) {
        int from = messages.size() + zMessageTextboxRef;
        if (0 > from) {
          zMessageTextboxRef = -1;
          from = messages.size() - 1;
        }
        String sLast = messages.get(from).text_get();
        if ((from + 1) < messages.size()) {
          sLast += "\n" + messages.get(from + 1).text_get();
        }
        pushClipboard("edit", sLast);
        zMessageTextbox = sLast;
      }
    }
    contact.unread_set(-1);
    contact.time_lastact_set(contact.unread_get());
    display_contact();

    // todo: service for sending
    if (contact.equals(null)) {
      try {
        // case 1:
        String smsNumber = contact.phone_get().substring(1); // without '+'
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setAction(Intent.ACTION_SEND);
        // sendIntent.setType("text/plain");
        // sendIntent.putExtra(Intent.EXTRA_TEXT, "text");
        Uri uri = Uri.fromFile(null); // file in download area
        sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
        // SetType("application/pdf")
        sendIntent.setType("application/vnd.ms-word");
        sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
      } catch (Exception e) {
        // Toast.makeText(this, "Error " + e, Toast.LENGTH_SHORT).show();
      }
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_edit_contact:
        {
          if (QmDbContact.TYPE_GROUP == contact.type_get()) {
            set_member_dialog(contact);
          } else {
            edit_contact_dialog(contact);
          }
          return true;
        }
      case R.id.action_set_name:
        {
          if (QmDbContact.TYPE_GROUP == contact.type_get()) {
            group_name_dialog(contact);
          } else {
            edit_contact_dialog(contact);
          }
          return true;
        }
      case R.id.action_contact_dial:
        {
          contact_dial(contact);
          return true;
        }
      case R.id.action_fingerprint:
        {
          view_fingerprint_dialog(contact);
          return true;
        }
      case R.id.action_send_key:
        {
          send_key_dialog(contact);
          return true;
        }
      case R.id.action_verify:
        {
          verify_dialog(contact);
          return true;
        }
      case R.id.action_sync:
        {
          background.setOffline(-1);
          prefs.set("offline", "FALSE");
          LocalMessage.send_background(getApplicationContext(), true);
          return true;
        }

      case R.id.action_stop:
        {
          background.setOffline(2);
          prefs.set("offline", "TRUE");
          LocalMessage.send_background(getApplicationContext(), false);
          return true;
        }

      case R.id.action_temp:
        {
          background.setOffline(0);
          prefs.set("offline", "0");
          LocalMessage.send_background(getApplicationContext(), true);
          return true;
        }

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void display_contact() {
    List<QmDbMessage> messages = db.message_get_by_id(contact.id_get(), maxview + 1);
    boolean unread = false;
    boolean more;
    Spanned span = new SpannableString("");

    if (messages.size() > maxview) {
      messages.remove(0);
      more = true;
    } else {
      more = false;
    }

    final Context context = this;
    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
    final int maxw = metrics.widthPixels / 2 + 1;
    final int maxh = metrics.heightPixels / 2 + 1;
    if (unreadTime > contact.unread_get()) {
      // Delayed message/ ...
      unreadTime = contact.unread_get();
    }

    for (int iMsg = 0; iMsg < messages.size(); iMsg++) {
      String uri_html = null;
      final QmDbMessage msg = messages.get(iMsg);
      final boolean from_me = (msg.from_get() == 1);
      QmDbContact from = db.contact_get_by_id(msg.from_get());
      if (from == null) {
        Dib2Root.log("display contact", "from == null" + msg.from_get());
        from = new QmDbContact();
      }

      final boolean queued = 0 > msg.ack;
      final boolean ack = (0 != msg.ack);
      final long time_ms = msg.time_get();
      if ((time_ms > unreadTime) && !unread) {
        unread = true;
        if (time_ms > contact.unread_get()) {
          background.unread = (0 == background.unread) ? -1 : background.unread;
        }
      }
      int hdr_color;
      if (msg.from_get() == 1) {
        hdr_color = ColorNmz.GRANITE_GRAY.argb(); // 0xff808080;
      } else {
        hdr_color = ColorNmz.BLUEBLUE.argb(); // 0xff0050a0;
      }
      int bgcolor = ColorNmz.MINT_LIGHT.argb(); // 0xff80ff80; // 0xffd0e8ff;
      int bgcolorborder = ColorNmz.MINT.argb(); // 0xff008040; // 0xff66b2ff;
      if (from_me) {
        bgcolor = // !unread ? 0xff80ffff : // 0xffe0e0e0;
            (queued
                ? ColorNmz.LILAC_PALE.argb()
                : ColorNmz.YELLOW.argb()); // 0xffff8080 : 0xffffff80);
        bgcolorborder = ColorNmz.TEAL.argb(); // 0xff008080; // 0xffa0a0a0;
      }

      String hdr_name = from.name_get();
      String hdr_date = DateFunc.dateShort4Millis(time_ms, -3) + "\n";
      int diff = findTimeDiff(hdr_date) + hdr_name.length() + 1;
      SpannableString span_hdr = new SpannableString(hdr_name + " " + hdr_date);
      span_hdr.setSpan(
          new StyleSpan(Typeface.ITALIC),
          span_hdr.length() - hdr_date.length(),
          span_hdr.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      span_hdr.setSpan(
          new StyleSpan(Typeface.BOLD), 0, hdr_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      span_hdr.setSpan(
          new StyleSpan(Typeface.BOLD), diff, diff + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      if (from_me) {
        span_hdr.setSpan(
            new AlignmentSpan.Standard(Alignment.ALIGN_OPPOSITE),
            0,
            span_hdr.length(),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      } else if (!unread) {
        span_hdr.setSpan(
            new ForegroundColorSpan(hdr_color),
            0,
            span_hdr.length(),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      }
      Uri uri = getUri(msg.uri_get());
      if (uri == null) {
        if (msg.text_get().startsWith("::")) {
          uri = getUri(msg.text_get());
        }
      } else {
        //      Dib2Root.log("display contact", "uri: " + uri.toString());
        ContentResolver cR = getContentResolver();
        String fulltype;
        boolean uri_fault = false;
        try {
          fulltype = cR.getType(uri);
        } catch (IllegalStateException e) {
          Log.e("display contact", "could not get full type");
          fulltype = "unknown type";
          uri_fault = true;
        }
        boolean handled = false;
        String handled_string = "";

        if (fulltype != null) {
          String type = fulltype.split("/")[0].toLowerCase();
          Dib2Root.log("display contact", "fulltype: " + fulltype + ", type: " + type);

          if (type.equals("image")) {
            handled_string = "<img src='image#" + uri.toString() + "'>";
            handled = true;
          }
          if (type.equals("video")) {
            handled_string = "<img src='video#" + uri.toString() + "'>";
            handled = true;
          }
          if (type.equals("audio")) {
            handled_string = "<img src='audio#" + uri.toString() + "'>";
            handled = true;
          }
        }
        Dib2Root.log("display contact", "handled_string: " + handled_string);
        if (uri_fault) {
          handled = false;
        }
        if (handled) {
          uri_html = "<p>";
          Intent intent = new Intent(Intent.ACTION_VIEW);
          intent.setData(uri);
          List<ResolveInfo> ia = this.getPackageManager().queryIntentActivities(intent, 0);
          if (ia.size() > 0) {
            uri_html +=
                "<a href='"
                    + uri.toString()
                    + "'>"
                    + (handled ? handled_string : uri.toString())
                    + "</a>";
          } else {
            uri_html += handled ? handled_string : uri.toString();
          }
          uri_html += "</p>";
        }
        Dib2Root.log("display contact", "uri_html: " + uri_html);
      }

      Spannable span_msg = new SpannableString(msg.text_get());
      span_msg.setSpan(
          new BulletSpan(from_me ? 3 : 0, 0),
          0,
          span_msg.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      Spanned span_uri = null;
      if (uri_html != null) {
        span_uri =
            Html.fromHtml(
                uri_html,
                new Html.ImageGetter() {
                  @Override
                  public Drawable getDrawable(final String img_source) {
                    String source_type = img_source.split("#")[0];
                    String source = img_source.split("#")[1];
                    Dib2Root.log(
                        "getdrawable",
                        "img_source: "
                            + img_source
                            + "source_type: "
                            + source_type
                            + " source: "
                            + source);
                    Bitmap bm;

                    if (source_type.equals("image")) {
                      BitmapFactory.Options options = new BitmapFactory.Options();

                      InputStream is;
                      try {
                        is = getContentResolver().openInputStream(Uri.parse(source));
                      } catch (FileNotFoundException e) {
                        Dib2Root.log("getDrawable", e.getMessage());
                        return null;
                      }

                      options.inJustDecodeBounds = true;
                      BitmapFactory.decodeStream(is, null, options);
                      int h = options.outHeight;
                      int w = options.outWidth;

                      int scaleh = (maxh + h) / maxh;
                      int scalew = (maxw + w) / maxw;

                      options.inSampleSize = Math.max(scaleh, scalew);
                      options.inJustDecodeBounds = false;

                      try {
                        is = getContentResolver().openInputStream(Uri.parse(source));
                      } catch (FileNotFoundException e) {
                        Dib2Root.log("getDrawable", e.getMessage());
                        return null;
                      }

                      bm = BitmapFactory.decodeStream(is, null, options);
                    } else if (source_type.equals("video")) {
                      String[] projection = {MediaStore.Video.Media._ID};
                      Cursor cursor =
                          getContentResolver()
                              .query(Uri.parse(source), projection, null, null, null);

                      int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);

                      cursor.moveToFirst();
                      long video_id = cursor.getLong(column_index);
                      cursor.close();

                      ContentResolver crThumb = getContentResolver();

                      bm =
                          MediaStore.Video.Thumbnails.getThumbnail(
                              crThumb, video_id, MediaStore.Video.Thumbnails.MICRO_KIND, null);
                    } else if (source_type.equals("audio")) {
                      Dib2Root.log("get drawable", "audio uri");
                      Drawable dp = getResources().getDrawable(R.drawable.play);
                      if (dp == null) {
                        return null;
                      }
                      dp.setBounds(0, 0, dp.getIntrinsicWidth(), dp.getIntrinsicHeight());
                      return dp;
                    } else {
                      Dib2Root.log("get drawable", "unknown source type: " + source_type);
                      return null;
                    }

                    Drawable d = new BitmapDrawable(getResources(), bm);
                    d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());

                    return d;
                  }
                },
                null);
      }

      span_msg.setSpan(
          new LineBgSpan(
              ((unread || queued || ack) ? bgcolor : ColorNmz.WHITE.argb()),
              (unread ? bgcolorborder : 0xff113333),
              !from_me),
          0,
          span_msg.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      span = (Spanned) TextUtils.concat(span, span_hdr);
      span = (Spanned) TextUtils.concat(span, span_msg);
      if (uri != null) {
        final String content = "::" + uri.toString();
        Spannable spOpen = new SpannableString("  OPEN");
        spOpen.setSpan(
            new ClickableSpan() {
              @Override
              public void onClick(View widget) {
                Dib2Root.log("display span", "click");
                openFile(content);
              }
            },
            0,
            spOpen.length(),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span = (Spanned) TextUtils.concat(span, spOpen);
      }

      String between = "<br>";
      if (uri_html != null) {
        span = (Spanned) TextUtils.concat(span, span_uri);
        between = "<p><br></p>";
      }

      if (queued) { // queue != null) {
        //      if (background.mail.queue_check( this, queue )) {
        if (Mail_1.toFlush) {
          between = "<p><font color='#990000'> *** Still in mail queue</font></p>";
        } else {
          msg.ack =
              (60 <= Dib2Constants.FILE_STRUC_VERSION) ? msg.time_get() : 0L; // .queue_set( null );
          db.message_update(msg);
        }
      }
      Spanned between_span = Html.fromHtml(between);
      span = (Spanned) TextUtils.concat(span, between_span);
    }
    //  if (contact.time_lastact_get() > contact.unread_get()) {
    //    contact.unread_set( contact.time_lastact_get() - 55000 );
    //  }
    db.contact_update(contact);

    if (more) {
      Spannable span_more = new SpannableString("view more messages\n\n");
      span_more.setSpan(
          new ClickableSpan() {
            @Override
            public void onClick(View view) {
              //        maxview += MAXVIEW_MSGS_ADD;
              maxview = maxview * 2 + Dib2Constants.MAXVIEW_MSGS_INIT;
              //              Dib2Root.log("view contacts", "new maxview: " + maxview);
              display_contact();
            }
          },
          0,
          span_more.length(),
          Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
      span = (Spanned) TextUtils.concat(span_more, span);
    }
    msg_viewer.setText(span);
    msg_viewer.setBackgroundColor(0xff77eeee); // rho
    msg_viewer.getRootView().setBackgroundColor(0xff77eeee);
    msg_viewer.setMovementMethod(LinkMovementMethod.getInstance());

    final ScrollView scrollview = ((ScrollView) findViewById(R.id.scrollView1));
    scrollview.post(
        new Runnable() {
          @Override
          public void run() {
            if (maxview > Dib2Constants.MAXVIEW_MSGS_INIT)
              scrollview.fullScroll(ScrollView.FOCUS_UP);
            else scrollview.fullScroll(ScrollView.FOCUS_DOWN);
          }
        });
  }

  public void on_update_ui() {
    update_ui();
  }

  @Override
  protected void update_ui() {
    if (contact == null) return;
    display_contact();
  }

  // =====
}
