// Copyright (C) 2016, 2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2qm;

import android.app.*;
import android.content.*;
import android.net.Uri;
import android.text.InputType;
import android.widget.*;
import net.sf.dibdib.thread_any.StringFunc;
import net.sourceforge.dibdib.android.dib2qm.R;

public abstract class QuickmsgActivity_add
    extends Activity { // net.sf.dibdib.android_join.quickmsg_activity_1 {
  // =====

  protected abstract void update_ui();

  public Runnable doUi =
      new Runnable() {
        @Override
        public void run() {
          update_ui();
        }
      };

  public void contact_dial(QmDbContact c) {
    String phone = c.phone_get();
    phone = (0 < phone.indexOf(' ')) ? phone.substring(0, phone.indexOf(' ')) : phone;
    phone = StringFunc.csvField4Text(phone);
    phone = phone.replace(" ", "").replace("-", "");
    if (phone.length() > 1) {
      Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
      startActivity(intent);
    }
  }

  public void edit_contact_dialog(final QmDbContact c) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.action_edit_contact);
    alert.setMessage(R.string.dialog_edit_contact);
    final EditText input0 = new EditText(this);
    final EditText input1 = new EditText(this);
    final EditText input2 = new EditText(this);
    final EditText input3 = new EditText(this);
    input0.setInputType(InputType.TYPE_CLASS_TEXT);
    input1.setInputType(InputType.TYPE_CLASS_TEXT);
    input2.setInputType(InputType.TYPE_CLASS_TEXT);
    input3.setInputType(InputType.TYPE_CLASS_TEXT);
    input0.setText(c.name_get());
    input1.setText(c.phone_get());
    input2.setText(c.address_get());
    input3.setText(c.notes_get());
    LinearLayout layout = new LinearLayout(this);
    layout.setOrientation(LinearLayout.VERTICAL);
    layout.addView(input0);
    layout.addView(input1);
    layout.addView(input2);
    layout.addView(input3);
    alert.setView(layout);

    alert.setPositiveButton(
        "Ok",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {
            String value;
            value = input0.getText().toString();
            if (0 < value.length()) {
              c.name_set(value);
            }
            value = input1.getText().toString();
            if (0 < value.length()) {
              c.phone_set(value);
            }
            value = input2.getText().toString();
            if ((0 < value.length()) && (1 < c.id_get())) {
              if (!value.equals(c.address_get())) {
                c.keystat_set(QmDbContact.KEYSTAT_NONE);
              }
              c.address_set(value);
            }
            value = input3.getText().toString();
            if (0 < value.length()) {
              c.notes_set(value);
            }
            c.time_lastact_set(); // time_now );
            update_ui();
          }
        });

    alert.setNegativeButton(
        "Stop",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int whichButton) {}
        });

    alert.show();
  }

  // =====
}
