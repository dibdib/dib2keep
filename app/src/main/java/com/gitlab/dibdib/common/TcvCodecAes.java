// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.common;

import com.gitlab.dibdib.picked.common.CodecAlgoFunc;
import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;
import net.sf.dibdib.thread_any.*;

// =====

public class TcvCodecAes implements TsvCodecIf {

  // =====

  public static final TcvCodecAes instance = new TcvCodecAes();

  private static short zPbkdVariantTempAsWorkaround = 0;

  @Override
  public byte[] create(char platform, Object... parameters) {
    if ((1 < parameters.length)
        && (parameters[0] instanceof String)
        && (parameters[1] instanceof Short)
        && "VAR".equals(parameters[0])) {
      // For workarounds ...
      zPbkdVariantTempAsWorkaround = (Short) parameters[1];
      return null;
    }
    return null;
  }

  @Override
  public byte[] compress(byte[] xyData, int xOffset4Reuse, int to) {
    return MiscFunc.compress('z', xyData, xOffset4Reuse, to);
  }

  @Override
  public byte[] decompress(byte[] xData, int len) {
    return MiscFunc.decompress(xData, 0, len);
  }

  @Override
  public String getKeyInfo(HashSet<String> entries) {
    StringBuilder out = new StringBuilder(entries.size() * 30);
    byte[] pk = Dib2Root.ccmSto.hidden_get("KEY." + "0" + ".SIG.ECDSA256.P");
    if (null != pk) {
      out.append("Fingerprint " + CodecAlgoFunc.fingerprint(pk, false));
      out.append('\n');
    }
    for (String entry : entries) {
      int i0 = entry.indexOf(":EMAIL:");
      if (0 < i0) {
        String email = entry.substring(i0 + 7);
        i0 = email.indexOf('\t');
        email = ((0 >= i0) ? email : email.substring(0, i0)).trim();
        pk = Dib2Root.ccmSto.hidden_get("KEY." + email + ".SIG.ECDSA256.P");
        if (null != pk) {
          out.append(email + '\t' + CodecAlgoFunc.fingerprint(pk, false));
          out.append('\n');
        }
      }
    }
    return out.toString();
  }

  @Override
  public byte[] getKey(byte[] pass, byte[] accessCode, byte[] xSalt16, int iterations) {
    byte[] key = null;
    if (3 > iterations) {
      return null;
    }
    try {
      if (null == accessCode) {
        // Old format.
        final char[] pass32 = new String(CodecAlgoFunc.toPass32(pass, null, true)).toCharArray();
        PBEKeySpec kspec = new PBEKeySpec(pass32, xSalt16, iterations, 256);
        key =
            new SecretKeySpec( // .
                    SecretKeyFactory.getInstance(CodecAlgoFunc.INIT_cryptoPbkd)
                        .generateSecret(kspec)
                        .getEncoded(),
                    "AES")
                .getEncoded();
        kspec.clearPassword();
      } else {
        byte[] secret;
        if (0 >= zPbkdVariantTempAsWorkaround) {
          zPbkdVariantTempAsWorkaround = 0;
          // Poor Java, having fallen into the UTF-16 trap, affecting also this side of the world
          // ... :-)
          // We have to compensate in a somewhat silly way for the sake of portability.
          final char[] hex = StringFunc.hex4Bytes(pass, false).toCharArray();
          PBEKeySpec kspec = new PBEKeySpec(hex, xSalt16, iterations, 128);
          final byte[] secret0 =
              SecretKeyFactory.getInstance(CodecAlgoFunc.INIT_cryptoPbkd)
                  .generateSecret(kspec)
                  .getEncoded();
          kspec.clearPassword();
          MessageDigest md = MessageDigest.getInstance("SHA-256");
          md.update(secret0);
          md.update(pass);
          secret = md.digest(accessCode);
        } else {
          byte[] pass32 = CodecAlgoFunc.toPass32(pass, accessCode, true);
          // For devices that used the former workaround ...
          char[] pass32Compensated = StringFunc.string4Ansi(pass32).toCharArray();
          if (2 == zPbkdVariantTempAsWorkaround) {
            // For interop with other such devices ...
            pass32 = StringFunc.bytesUtf8(new String(pass32Compensated));
            pass32Compensated = StringFunc.string4Ansi(pass32).toCharArray();
          }
          // Do not keep using the former workaround.
          zPbkdVariantTempAsWorkaround = 0;
          PBEKeySpec kspec =
              new PBEKeySpec(
                  Arrays.copyOfRange(pass32Compensated, 0, 16), xSalt16, iterations, 128);
          final byte[] secret0 =
              SecretKeyFactory.getInstance(CodecAlgoFunc.INIT_cryptoPbkd)
                  .generateSecret(kspec)
                  .getEncoded();
          kspec.clearPassword();
          kspec =
              new PBEKeySpec(
                  Arrays.copyOfRange(pass32Compensated, 16, 32), xSalt16, iterations, 128);
          final byte[] secret1 =
              SecretKeyFactory.getInstance(CodecAlgoFunc.INIT_cryptoPbkd)
                  .generateSecret(kspec)
                  .getEncoded();
          kspec.clearPassword();
          secret = Arrays.copyOf(secret0, 32);
          System.arraycopy(secret1, 0, secret, 16, 16);
        }
        key = new SecretKeySpec(secret, "AES").getEncoded();
      }
    } catch (Exception e) {
      key = null;
    }
    return key;
  }

  @Override
  public byte[] encodePhrase(byte[] phrase, byte[] accessCode) {
    return CodecAlgoFunc.toPass32(accessCode, phrase, true);
  }

  @Override
  public byte[] decodePhrase(byte[] encoded, byte[] accessCode) {
    return CodecAlgoFunc.fromPass32(accessCode, encoded);
  }

  @Override
  public byte[] encode(
      byte[] compressedData,
      int from,
      int to,
      byte[] key,
      byte[] iv16,
      int oldKeyInfo,
      byte[] header,
      byte[] signatureKey)
      throws Exception {
    if (null == header) {
      // Old file format.
      return null;
    }
    if (0 < oldKeyInfo) {
      // Old chat format.
      return null; // encodeOLD( compressedData, from, to, key, oldKeyInfo, header, signatureKey);
    }
    Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
    if (null == iv16) {
      iv16 = getInitialValue(16);
    }
    SecretKeySpec skspec = new SecretKeySpec(key, "AES");
    byte[] iv = iv16;
    if (null != signatureKey) {
      PKCS8EncodedKeySpec ecSpec = new PKCS8EncodedKeySpec(signatureKey);
      Signature dsa = Signature.getInstance("SHA256withECDSA");
      dsa.initSign(KeyFactory.getInstance("EC").generatePrivate(ecSpec));
      // Last tag is skipped => join arrays:
      dsa.update(header, 0, header.length - 1);
      dsa.update(iv16);
      dsa.update(compressedData, from, to - from);
      // As SIV:
      iv = dsa.sign();
    }
    cipher.init(Cipher.ENCRYPT_MODE, skspec, new IvParameterSpec(iv, iv.length - 16, 16));
    byte[] enc = cipher.doFinal(compressedData, from, to - from);
    if (null == signatureKey) {
      iv16 = iv;
      iv = new byte[0];
    }
    int offs = 8;
    // Join arrays => last tag is skipped
    byte[] out =
        new byte[offs + 1 + header.length - 1 + iv16.length + 1 + enc.length + 5 + iv.length + 2];
    System.arraycopy(header, 0, out, offs, header.length - 1);
    System.arraycopy(iv16, 0, out, offs = offs + header.length - 1, iv16.length);
    out[offs = offs + iv16.length] = (byte) (SerFunc.TAG_BYTES_L0 + iv16.length);
    int len = enc.length;
    System.arraycopy(enc, 0, out, ++offs, len);
    out[offs = offs + len] = (byte) len;
    out[++offs] = (byte) (len >>> 8);
    if (len < (1 << 16)) {
      out[++offs] = SerFunc.TAG_BYTES_L2;
    } else {
      out[++offs] = (byte) (len >>> 16);
      out[++offs] = (byte) (len >>> 24);
      out[++offs] = SerFunc.TAG_BYTES_L4;
    }
    System.arraycopy(iv, 0, out, ++offs, iv.length);
    out[offs = offs + iv.length] = (byte) iv.length;
    out[++offs] = SerFunc.TAG_BYTES_L1;
    out[++offs] = (byte) (SerFunc.TAG_ARR_C0 + 3 + (0xf & header[header.length - 1]));
    out = MiscFunc.packet4880X(Dib2Constants.RFC4880_EXP2, null, out, 8, ++offs);
    return Arrays.copyOfRange(out, out[0], offs);
  }

  @Override
  public byte[] decode(byte[] data, int from, int to, byte[] key, byte[] signatureKey)
      throws Exception {
    int offs = from;
    if (data[offs] == (byte) Dib2Constants.RFC4880_EXP2) {
      final int hdlen = MiscFunc.getPacketHeaderLen(data, offs);
      to = offs + hdlen + MiscFunc.getPacketBodyLen(data, 1 + offs);
      offs += hdlen;
      if ((to > data.length) || ((to - 16) <= from)) {
        return null;
      }
    }
    if (data[offs] != Dib2Constants.MAGIC_BYTES[0]) {
      return null;
    }
    if (0 == data[offs + 6]) {
      // Old file format, no longer supported.
      return null;
    }
    // Check version and key info (0 = local).
    int version = data[offs + 2];
    version = (6 >= version) ? 50 : data[offs + 4];
    if (60 > version) {
      // No longer supported.
      return null;
    } else if (Dib2Constants.FILE_STRUC_VERSION < version) {
      // Future version, not backwards compatible ...
      return null;
    }
    // SIG/SIV
    long offsLen = SerFunc.getTcvOffsetLength(data, offs, data.length - 2, 1);
    final int iSig = (int) offsLen;
    final int cSig = (int) (offsLen >>> 32);
    Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
    SecretKeySpec skspec = new SecretKeySpec(key, "AES");
    // ENC
    offsLen = SerFunc.getTcvOffsetLength(data, offs, -1 + (int) offsLen, 1);
    if (3 > (int) (offsLen >>> 32)) {
      return null;
    }
    final long jEnc = offsLen;
    // IV
    offsLen = SerFunc.getTcvOffsetLength(data, offs, -1 + (int) offsLen, 1);
    cipher.init(
        Cipher.DECRYPT_MODE,
        skspec,
        (0 < cSig)
            ? new IvParameterSpec(data, iSig + cSig - 16, 16)
            : new IvParameterSpec(data, (int) offsLen, 16));
    byte[] compressed = cipher.doFinal(data, (int) jEnc, (int) (jEnc >>> 32));
    if (16 != (int) (offsLen >>> 32)) {
      return null;
    }
    if (null != signatureKey) {
      if (16 >= cSig) {
        return null;
      }
      X509EncodedKeySpec ecSpec = new X509EncodedKeySpec(signatureKey);
      Signature dsa = Signature.getInstance("SHA256withECDSA");
      dsa.initVerify(KeyFactory.getInstance("EC").generatePublic(ecSpec));
      dsa.update(data, offs, (int) offsLen - offs);
      dsa.update(data, (int) offsLen, 16);
      dsa.update(compressed);
      if (!dsa.verify(data, iSig, cSig)) {
        return null;
      }
    }
    return compressed;
  }

  @Override
  public byte getMethodTag() {
    // AES
    return 'A';
  }

  @Override
  public byte[] getInitialValue(int len) {
    final byte[] rand = new byte[len];
    new SecureRandom().nextBytes(rand);
    if (10 < len) {
      final long tx = DateFunc.currentTimeNanobisLinearized(true);
      final char[] sx =
          new char[] {(char) (tx >>> 48), (char) (tx >>> 32), (char) (tx >>> 16), (char) tx};
      final int rx32 = MiscFunc.hash32_fnv1a(32, new String(sx));
      rand[0] = (byte) (rx32 >>> 24);
      rand[1] = (byte) (rx32 >>> 16);
      rand[2] = (byte) (rx32 >>> 8);
      rand[3] = (byte) rx32;
    }
    return rand;
  }

  // =====
}
