// Copyright (C) 2014, 2023  Roland Horsch and others:
// -- For the changes:  Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original: Copyright (C) 2014/2015  Jeroen Vreeken <jeroen@vreeken.net>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from quickmsg.vreeken.net.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import com.gitlab.dibdib.picked.net.Mail.*;
import com.gitlab.dibdib.picked.net.QuickMsg.*;
import java.io.*;
import java.util.*;
import javax.mail.internet.ContentType;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.CcmSto;

public class QmBg {
  // =====

  //  public static final int MIN_IDLE_TIME = 11 * 1000;
  //  public static final int STD_IDLE_TIME = 4 * 60 * 1000;
  //  public static final int MAX_IDLE_TIME = 14 * 60 * 1000;

  protected static QmBg svcContext = null;

  public static volatile int unread;
  protected static volatile boolean alarmDone = false;
  public static volatile long flagsNotify = 3; // 1: LED, 2: vibrate, 4: sound
  //  protected static volatile int idleTimeNet = MIN_IDLE_TIME;

  public static Mail mail =
      new Mail() {
        @Override
        public Boolean recv_quickmsg_cb(
            String from, List<Attachment> attachments, String subtype, Date timeMax) {
          Dib2Root.log("bg recv cb", "Received quickmsg from " + from + " of subtype: " + subtype);
          unread = (0 > unread) ? 0 : unread;

          EcDhQm pgp = QmDb.pgp; // new EcDhQm();// db); // svcContext );

          for (int i = 0; i < attachments.size(); i++) {
            Attachment attachment = attachments.get(i);
            // Dib2Root.log( "bg recv cb", "name: " + attachment.name +
            // ", content type :" + attachment.datahandler.getContentType() );
            try {
              String cts = attachment.datahandler.getContentType();
              if (null == cts) {
                continue;
              }
              ContentType ct = new ContentType(cts);
              if ((null != subtype)
                  && subtype.toLowerCase().equals("encrypted")
                  && ct.getSubType().toLowerCase().equals("octet-stream")) {
                // Dib2Root.log( "bg recv cb", "Got encrypted message" );
                Attachment ac;
                try {
                  ac = pgp.decrypt_verify(attachment);
                } catch (OutOfMemoryError e) {
                  Mail.log_e("recv_cb", "out of memory: attachment too big?", null);
                  return false;
                }
                if (ac != null) {
                  Dib2Root.log("bg recv cb", "Got decrypted message - " + attachment.name);
                  List<Attachment> as = multipart_get_attachments(ac);
                  Attachment a_msg = null;
                  QmDb.MessageData msg = null;
                  if (null == as) {
                    // Try to keep chat alive ...
                    QmDb.Contact contact = QmDb.db.contact_get_person_by_address(from);
                    if (null == contact) {
                      return false;
                    }
                    msg =
                        new QmDb.MessageData(
                            contact._inx,
                            contact._inx,
                            DateFunc.currentTimeMillisLinearized(),
                            "...");
                    as = new ArrayList<Attachment>();
                  }
                  String a_ext = ".dat";

                  for (int j = 0; j < as.size(); j++) {
                    Attachment a = as.get(j);
                    if (null == a) {
                      continue;
                    }
                    ContentType ct2 = new ContentType(a.datahandler.getContentType());
                    String subtype2 = ct2.getSubType();
                    //String basetype = ct2.getBaseType();
                    // Dib2Root.log( "bg recv db", "attachment " + j + " " + basetype
                    // );

                    if (subtype2.toLowerCase().equals("pgp-keys")) {
                      svcContext.received_key(pgp, QmDb.db, from, a, true);
                    } else if (subtype2.toLowerCase().equals("quickmsg")) {
                      msg = svcContext.received_quickmsg(pgp, QmDb.db, from, a);
                      if ((null != timeMax) && (timeMax.getTime() < msg._time)) {
                        msg._time = timeMax.getTime();
                      }
                      if (msg._time > DateFunc.currentTimeMillisLinearized()) {
                        final long mTime = msg._time;
                        final long cTime = DateFunc.currentTimeMillisLinearized();
                        msg._time = DateFunc.alignTime(mTime, 1);
                        if (mTime <= (cTime + 2 * 60 * 1000)) {
                          DateFunc.alignTime(cTime, mTime);
                        }
                      }
                    } else {
                      /* maybe part of a message */
                      // String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(basetype);
                      // a_ext = ext;
                      Dib2Root.log("bg recv db", "probably an attachment ext: " + a_ext);
                      a_msg = a;
                    }
                  }

                  if (msg != null && a_msg != null) {
                    svcContext.received_attachment(QmDb.db, msg, a_msg, a_ext);
                  }
                  if (msg != null) {
                    String txt = msg._text;
                    int i0 = 0;
                    for (; i0 < txt.length(); ++i0) {
                      if (' ' < txt.charAt(i0)) {
                        break;
                      }
                    }
                    if ((0 < i0) && (i0 < txt.length())) {
                      txt = txt.substring(i0);
                    }
                    final String magic = new String(Dib2Constants.MAGIC_BYTES, StringFunc.CHAR8);
                    if (txt.startsWith(magic + "M(")) {
                      txt = StringFunc.string4Mnemonics(txt.replace("\n", ""));
                      String[] csvs = txt.split("\n");
                      boolean first = true;
                      for (String csv : csvs) {
                        if (first) {
                          first = false;
                          if (csv.startsWith(magic)) {
                            continue;
                          }
                        }
                        QmDb.MessageData msg0 = new QmDb.MessageData();
                        msg0._c = msg._c;
                        msg0._from = msg._from;
                        String[] els = csv.split("\t");
                        if (7 >= els.length) {
                          msg0._time = msg._time;
                          msg0._text = csv;
                        } else if (els[0].equals("TMP")
                            && els[1 + CcmSto.CcmTag.CATS.ordinal()].equals("ACK")) {
                          for (int ix = 1 + 7; ix < els.length; ++ix) {
                            if (5 >= els[ix].length()) {
                              continue;
                            }
                            QmDb.MessageData mx = QmDb.db.message_get_by_oid(els[ix]);
                            if (null != mx) {
                              mx.ack = 0;
                            } else {
                              QmDb.db.message_set_ack(els[ix]);
                            }
                          }
                          msg0 = null;
                        } else {
                          // msg0._id = msg0._from;
                          QmDb.MessageData mx = msg0.message4TsvFields(msg._from, els, true);
                          if (null != mx) {
                            msg0 = mx;
                          }
                        }
                        if (null != msg0) {
                          QmDb.db.message_add(msg0);
                        }
                      }
                    } else {
                      QmDb.db.message_add(msg);
                    }
                  }

                } else {
                  // Dib2Root.log( "bg recv cb", "Message could not be
                  // decrypted/verified" );
                  QmDb.Contact contact = QmDb.db.contact_get_person_by_address(from);
                  if (contact != null) {
                    // contact._keystat = QuickMsg.KEYSTAT_SENT;
                    contact._time_lastact = DateFunc.currentTimeMillisLinearized();
                  }
                  // local_message.send_statusMsg(svcContext, "?Key error - " + from);
                }
              }
              if (ct.getSubType().toLowerCase().equals("pgp-keys")) {
                svcContext.received_key(pgp, QmDb.db, from, attachment, false);
              }
            } catch (Exception e) {
              Mail.log_e("recv_cb", "" + e + e.getMessage(), e);
            }
          }
          return true;
        }
      };

  private String members_get_string(QmDb.Contact cdb) {
    if (cdb._members == null) return "";
    String out = cdb._members.toString();
    // Assuming '[...]' notation:
    return out.substring(1, out.length() - 1);
  }

  public QmDb.MessageData received_quickmsg(EcDhQm pgp, QmDb db, String from, Attachment a) {

    QuickMsg qm = new QuickMsg();
    qm.parse_attachment(a);
    //  Dib2Root.log( "bg recv db", "got quickmsg" );
    //  idleTimeNet = MIN_IDLE_TIME;

    QmDb.Contact cdb, cdbf;
    QmDb.Contact mc = qm.get_contact();
    QmDb.MessageData msg = qm.get_message();
    int mgroup = qm.get_group();

    cdbf = db.contact_get_person_by_address(from);
    //  Dib2Root.log( "bg recv db", "got quickmsg " + mf.time_get() + '/' + cdbf.unread_get() );
    if (null != msg) {
      msg._time = DateFunc.alignTime(msg._time, cdbf._unread);
      if (msg._time <= (cdbf._unread + Dib2Constants.TIME_SHIFTED)) {
        cdbf._unread = msg._time - Dib2Constants.TIME_SHIFTED - 1;
      }
    }
    if (null == cdbf) { // || (QuickMsg.KEYSTAT_BIT_ACTIVE & cdbf._keystat) == 0) {
      // local_message.send_statusMsg(svcContext, "?Chat-error - re-send key to " +
      // from);
      return null;
    }

    ///// Group post might arrive before group info!
    if (qm.is_group || qm.is_grouppost) {
      cdb = db.contact_get_group_by_address_and_group(qm.group_owner, mgroup);
      if (cdb == null) {
        if (qm.group_owner.length() == 0) qm.group_owner = from;
        cdb = new QmDb.Contact();
        cdb._type = QuickMsg.TYPE_GROUP;
        cdb._address = qm.group_owner;
        cdb._group = qm.group_id;
        cdb._time_lastact = (null != msg) ? msg._time : ((null != mc) ? mc._time_lastact : -1);
        cdb._name = "group" + mgroup + "...";
        cdb._members = new ArrayList<String>(Arrays.asList(new String[] {qm.group_owner}));
        db.contact_add(cdb);
        cdb = db.contact_get_group_by_address_and_group(qm.group_owner, mgroup);
      }
    } else {
      cdb = cdbf;
    }
    if (qm.is_post && msg != null) {
      //    Dib2Root.log( "bg recv cb", "Has a post, add to db" );
      msg._c = cdb._inx;
      msg._from = cdbf._inx;
      //    Dib2Root.log( "bg recv cb", "id: " + cdb.id_get() );
      if (cdb._inx == 0) {
        Dib2Root.log("bg recv cb", "Not a valid id");
        return null;
      }
      List<String> cdbmembers = cdb._members;
      if ((qm.is_grouppost) && (cdbmembers != null)) {
        boolean foundmember = false;

        for (int i = 0; i < cdbmembers.size(); i++) {
          if (from.equals(cdbmembers.get(i))) {
            foundmember = true;
            break;
          }
        }
        if (!foundmember) {
          String cdbms = members_get_string(cdb) + ", " + from;
          cdb._members = new ArrayList<String>(Arrays.asList(cdbms.trim().split(", ")));
        }
      }
      cdb._time_lastact = msg._time;
    }
    if (!qm.is_group) {
      if ((null == cdbf._name) || (0 >= cdbf._name.length()) || (0 <= cdbf._name.indexOf('@'))) {
        String nam = mc._name;
        if ((null != nam) && ((1 < nam.trim().length()))) {
          cdbf._name = nam.trim();
        }
      }
    } else {
      cdb._time_lastact = mc._time_lastact;
      List<String> msgmembers = mc._members;
      List<String> cdbmembers = cdb._members;
      String cdbms = members_get_string(cdb);
      boolean foundSender = false;
      for (int i = 0; i < msgmembers.size(); i++) {
        final String mm = msgmembers.get(i);
        if (!cdbmembers.contains(mm)) {
          cdbms += ", " + mm;
        }
        if (mm.equals(from)) {
          foundSender = true;
        }
      }

      //    cdb.members_set( cf.members_get() );
      if (!foundSender) {
        // You may remove yourself.
        cdbms.replace(from + ", ", "");
        cdbms.replace(from, "");
      }
      cdb._members = new ArrayList<String>(Arrays.asList(cdbms.trim().split(", ")));
      cdbmembers = cdb._members;

      if (cdbmembers.size() == 0) {
        Dib2Root.log("bg recv cb", "group has no members"); // , delete it" );
        //      db.contact_remove( cdb );
        return null;
      }
      String nam = mc._name;
      if ((null != nam) && (1 < nam.trim().length())) {
        cdb._name = nam.trim();
      }
      for (String mem : cdbmembers) {
        if (null == db.contact_get_person_by_address(mem)) {
          QmDb.Contact add = new QmDb.Contact();
          add._name = mem;
          add._address = mem;
          add._type = QuickMsg.TYPE_PERSON;
          db.contact_add(add);
        }
      }
    }

    db.contact_update(cdb);
    db.contact_update(cdbf);

    return msg;
  }

  public void received_key(EcDhQm pgp, QmDb db, String from, Attachment a, Boolean signed) {
    String add;
    Boolean signedOk = false;
    Dib2Root.log("bg recv cb", "Received pgp key attachement");

    byte[] is;
    try {
      ByteArrayOutputStream buf = new ByteArrayOutputStream();
      //    is = a.datahandler.getInputStream();
      a.datahandler.writeTo(buf);
      buf.flush();
      is = buf.toByteArray();
    } catch (IOException e) {
      Dib2Root.log("bg recv cb", e.getMessage());
      return;
    }
    QmDb.Contact contact = db.contact_get_person_by_address(from);
    String oldfp = null;
    if (contact != null) {
      if (pgp.public_keyring_check_by_address(
          from)) { // (QuickMsg.KEYSTAT_BIT_ACTIVE & contact._keystat) != 0) {
        oldfp = pgp.fingerprint(from);
        signedOk = signed;
      }
    }
    // Dib2Root.log("recv cb", is.toString());
    // For now, do not override active key: the parsed key may belong to other address.
    String[] getter = new String[] {signedOk ? "X" : from}; // 1 ];
    byte[] oldK = pgp.public_keyring_add_key(is, getter);
    boolean added = (null != oldK);
    add = getter[0];

    if ((add != null) && add.contains("@")) {

      contact = db.contact_get_person_by_address(add);
      if ((null != contact) && (1 >= contact._inx)) {
        return;
      }
      Dib2Root.log("bg recv cb", "key added to ring");
      if ((contact == null)
          || !pgp.public_keyring_check_by_address(
              from)) { // (0 == (QuickMsg.KEYSTAT_BIT_ACTIVE & contact._keystat))) {
        Dib2Root.log("bg recv_cb", "new key, add contact");
        if (null == contact) {
          contact = new QmDb.Contact();
        }
        contact._address = add;
        contact._type = QuickMsg.TYPE_PERSON;
        contact._time_lastact = DateFunc.currentTimeMillisLinearized();
        db.contact_add(contact);
        // local_message.send_statusMsg(svcContext, "");
        return;
      }
      contact._time_lastact = DateFunc.currentTimeMillisLinearized();
      if (added) { // && add.equals( from )) {
        return;
      }
      if (add.equals(from)) {
        oldfp = (null == oldfp) ? pgp.fingerprint(add) : oldfp;
        try {
          pgp.public_keyring_remove_by_address(add);
          // is = buf.toByteArray();
          pgp.public_keyring_add_key(is, getter);
        } catch (Exception e) {
          Dib2Root.log("bg recv_db", "key change failed " + e);
          oldfp = null;
        }
        db.contact_update(contact);
      }
    } else if (null == add) {
      Dib2Root.log("bg recv_db", "something is wrong with the key");
    }
    // local_message.send_statusMsg(svcContext, "");
  }

  public void received_attachment(QmDb db, QmDb.MessageData m, Attachment a, String ext) {
    Dib2Root.log("bg received attachement", "going to save attachment");
    File df = Dib2Root.platform.getFilesDir("main");
    df = new File(df, "dibdib");
    df.mkdirs();
    String dir = df.getAbsolutePath();
    //    File df = new File(dir);
    String filenamebase;
    if (a.name == null) {
      filenamebase = UUID.randomUUID().toString() + "." + ext;
    } else {
      filenamebase = a.name;
    }
    File mf;
    int nr = 0;
    String filename;
    do {
      filename = filenamebase + (nr == 0 ? "" : "_" + nr);
      filename =
          ((0 != nr) && (1 < filename.indexOf('.')))
              ? filenamebase.replace(".", "_" + nr + '.')
              : filename;
      filename = ((ext == null) || filename.endsWith(ext)) ? filename : (filename + '.' + ext);
      mf = new File(dir, filename);
      if (!mf.exists()) {
        break;
      }
      Dib2Root.log("bg received attachment", "exists: " + nr);
      nr++;
    } while (true);
    String name = mf.getPath();
    df.mkdirs();
    OutputStream os;
    String type = a.datahandler.getDataSource().getContentType();
    type = type.split(";")[0];
    String basetype = type.split("/")[0].toLowerCase();

    Dib2Root.log("bg received attachment", "name: " + name);

    try {
      os = new FileOutputStream(mf);
      a.datahandler.writeTo(os);
      os.flush();
      os.close();
    } catch (Exception e) {
      String err = e.getMessage();
      Mail.log_e("received attachment", "err: " + (err != null ? err : "null"), e);
      return;
    }

    Object uri = Dib2Root.platform.parseUri(name); // uriFromMediaLibrary()
    m._text = "::" + basetype + "::" + name + '\n' + m._text;
    m._uri = uri;
  }

  // =====
}
