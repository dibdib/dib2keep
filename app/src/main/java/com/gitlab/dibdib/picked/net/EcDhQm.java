// Copyright (C) 2017, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on 'pgp' from quickmsg.vreeken.net, and many valuable
// hints about frugal crypto, e.g. crypto.stackexchange.com: BC usage had
// to be replaced (RSA/ DH/ ECDH (RFC4880_EXP2)). Sorry for not keeping a list ...
// Due to the former dependency on an outdated BC, backwards compatibility had to be
// dropped.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import com.gitlab.dibdib.common.TcvCodecAes;
import com.gitlab.dibdib.picked.common.CodecAlgoFunc;
import com.gitlab.dibdib.picked.net.Mail.Attachment;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.activation.*;
import javax.crypto.KeyAgreement;
import javax.mail.util.ByteArrayDataSource;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;

/** Static keys for authentication, session keys for encryption. */
// Old key info for alternating keys (1/2/0): CAAU = Count, Added/Include, Acknowledge, Use.
// Now used as C0A0 (Count + Acknowledge).
public class EcDhQm {

  protected QmDb prefs;

  private final byte DER_TAG_SEQUENCE = 0x30;
  private final byte PGP_TAG_PK = (byte) (0x80 | 0x40 | 6);
  private final byte PGP_TAG_UID = (byte) (0x80 | 0x40 | 0x13);
  private final byte PGP_PK_VERSION = 4;
  private final byte PGP_PK_ALGO_X_ECDH = 101;
  private final byte[] PGP_PK_PRE = new byte[] {PGP_PK_VERSION, 0, 0, 0, 1, PGP_PK_ALGO_X_ECDH};

  private static boolean errorMsg = false;

  public void init(QmDb context) {
    // Dib2Root.log( "ecdh init", "a" );
    prefs = context;
    if (null == prefs.get("KEY.0.SIG.ECDSA256.S", null)) {
      KeyPairGenerator kpg;
      try {
        kpg = KeyPairGenerator.getInstance("EC");
      } catch (NoSuchAlgorithmException e) {
        Dib2Root.log("ecdh init", "err1 " + e);
        return;
      }
      // Waiting for Java/BC/Android to support other curves ...
      kpg.initialize(256);
      KeyPair kp = kpg.generateKeyPair();
      byte[] sigPk = kp.getPublic().getEncoded();
      byte[] sigSk = kp.getPrivate().getEncoded();
      prefs.set("KEY.0.SIG.ECDSA256.P", sigPk);
      prefs.set("KEY.0.SIG.ECDSA256.S", sigSk);
      Dib2Root.log("ecdh init", "created " + fingerprint("0"));
    } else {
      Dib2Root.log("ecdh init", "fi " + fingerprint("0"));
    }
  }

  /** FNV-1a, also for short keys. From UtilMisc ... */
  // Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
  private static int hash32_fnv1a(byte[] data) {
    int hash = 0x811c9dc5;
    for (byte b0 : data) {
      hash ^= 0xff & b0;
      // hash *= prime;
      hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
    }
    return hash;
  }

  public boolean load_keys() {
    return null != prefs.get("KEY.0.SIG.ECDSA256.S", null);
  }

  public byte[] getPkSignatureEncoded(String addr) {
    final String which =
        addr.equalsIgnoreCase(StringFunc.string4Utf8(prefs.get("email_address", new byte[0])))
            ? "0"
            : addr;
    final byte[] pk = prefs.get("KEY." + which + ".SIG.ECDSA256.P", null);
    if (null == pk) {
      return null;
    }
    return new X509EncodedKeySpec(pk).getEncoded();
  }

  public String fingerprint(String addr) {
    byte[] pk = getPkSignatureEncoded(addr);
    return CodecAlgoFunc.fingerprint(pk, true);
  }

  public byte[] public_keyring_add_key(byte[] xInputStream, String[] xyAddr) {
    byte[] old = null;
    if ((null == xInputStream) || (10 >= xInputStream.length)) {
      return null;
    }
    byte[] pk = new byte[0];
    if (xyAddr[0].contains("@") && (DER_TAG_SEQUENCE == xInputStream[0])) {
      ///// Deprecated format
      pk = xInputStream.clone();
    } else if (PGP_TAG_PK == xInputStream[0]) {
      int offs = MiscFunc.getPacketHeaderLen(xInputStream, 0);
      int len = MiscFunc.getPacketBodyLen(xInputStream, 1);
      if ((1 >= offs)
          || (6 < offs)
          || (PGP_PK_PRE.length >= len)
          || ((offs + len) > xInputStream.length) // ,
          || (xInputStream[offs] != PGP_PK_PRE[0])
          || (xInputStream[offs + PGP_PK_PRE.length - 1] != PGP_PK_PRE[PGP_PK_PRE.length - 1])) {
        return null;
      }
      pk = Arrays.copyOfRange(xInputStream, offs + PGP_PK_PRE.length, offs + len);
      offs += len;
      if (offs < xInputStream.length) {
        len = MiscFunc.getPacketBodyLen(xInputStream, offs + 1);
        offs += MiscFunc.getPacketHeaderLen(xInputStream, offs);
        if ((offs + len) > xInputStream.length) {
          return null;
        }
        String uid = StringFunc.string4Utf8(Arrays.copyOfRange(xInputStream, offs, offs + len));
        int iL = uid.indexOf('<');
        int iR = uid.lastIndexOf('>');
        if ((0 <= iL) && (iL < iR)) {
          uid = uid.substring(iL + 1, iR).replace(" ", "");
        } else {
          return null;
        }
        if (xyAddr[0].contains("@") && !uid.equalsIgnoreCase(xyAddr[0]) && (3 < uid.length())) {
          old = prefs.get("KEY." + uid + ".SIG.ECDSA256.P", null);
          if (null != old) {
            return null;
          }
        }
        xyAddr[0] = uid;
      }
    }
    if ((0 < pk.length) && xyAddr[0].contains("@") && !xyAddr[0].equalsIgnoreCase(QmDb.my_addr)) {
      old = prefs.get("KEY." + xyAddr[0] + ".SIG.ECDSA256.P", null);
      prefs.set("KEY." + xyAddr[0] + ".SIG.ECDSA256.P", pk);
      ///// DEPRECATED: Use it as extra session key:
      prefs.set("KEY." + xyAddr[0] + ".ENC.ECDH.P1", pk); // P1/S1
      prefs.set("KEY.0.ECDH." + xyAddr[0] + ".P1", prefs.get("KEY.0.SIG.ECDSA256.P", null));
      prefs.set("KEY.0.ECDH." + xyAddr[0] + ".S1", prefs.get("KEY.0.SIG.ECDSA256.S", null));
      // Use it as initial session key:
      prefs.set("KEY.0.ECDH." + xyAddr[0] + ".CAAU", "0000".getBytes(StringFunc.CHAR8)); // 0111" );
      if (Arrays.equals(pk, old)) {
        return null;
      } else if (null == old) {
        old = pk;
      }
      // prefs.remove( "KEY." + xyAddr[0] + ".CIPHER");
    }
    return old;
  }

  public Attachment key_attachment(String keyuserAddr) {
    Dib2Root.log("key_attachment", "generate public key");
    Attachment attachment = new Attachment();
    byte[] pk = getPkSignatureEncoded(keyuserAddr);
    if (null == pk) {
      return null;
    }
    if ((null != keyuserAddr) && (1 < keyuserAddr.length()) && !keyuserAddr.equals(QmDb.my_addr)) {
      int secs = (int) (DateFunc.currentTimeMillisLinearized() / 1000);
      byte[] pkPack = Arrays.copyOf(PGP_PK_PRE, pk.length + PGP_PK_PRE.length);
      pkPack[1] = (byte) (secs >>> 24);
      pkPack[2] = (byte) (secs >>> 16);
      pkPack[3] = (byte) (secs >>> 8);
      // pkPack[4] = variant;
      System.arraycopy(pk, 0, pkPack, PGP_PK_PRE.length, pk.length);
      pkPack = MiscFunc.packet4880X(PGP_TAG_PK, null, pkPack, 0, pkPack.length);
      byte[] uid = StringFunc.bytesUtf8("<" + keyuserAddr + ">");
      byte[] uidPack = MiscFunc.packet4880X(PGP_TAG_UID, null, uid, 0, uid.length);
      pk = Arrays.copyOf(pkPack, pkPack.length + uidPack.length);
      System.arraycopy(uidPack, 0, pk, pkPack.length, uidPack.length);
    } else {
      // Use deprecated format for the time being.
    }
    attachment.datahandler = new DataHandler(new ByteArrayDataSource(pk, "application/pgp-keys"));
    attachment.name = fingerprint(keyuserAddr) + ".asc";
    prefs.set("KEY.0.ECDH." + keyuserAddr + ".CAAU", "0000".getBytes(StringFunc.CHAR8)); // 0101" );
    // prefs.remove( "KEY." + keyuserAddr + ".CIPHER");
    // prefs.remove( "KEY." + keyuserAddr + ".ENC.ECDH.P1" );
    // prefs.remove( "KEY." + keyuserAddr + ".ENC.ECDH.P2" );
    return attachment;
  }

  public Attachment pgpmime_id() {
    Attachment a = new Attachment();
    a.disposition = "inline";
    DataSource ds;
    try {
      ds = new ByteArrayDataSource("Version: 1\n", "application/pgp-encrypted");
    } catch (IOException e) {
      return null;
    }
    a.datahandler = new DataHandler(ds);
    a.name = "pgp_mime_id";
    return a;
  }

  private byte[] calcAesKey(byte[] mySk, byte[] myPk, byte[] otherPk) {
    byte[] out = null;
    try {
      X509EncodedKeySpec pkSpec = new X509EncodedKeySpec(otherPk);
      PKCS8EncodedKeySpec skSpec = new PKCS8EncodedKeySpec(mySk);
      KeyFactory kfp = KeyFactory.getInstance("EC");
      KeyFactory kfs = KeyFactory.getInstance("EC");
      KeyAgreement ka = KeyAgreement.getInstance("ECDH");
      ka.init(kfs.generatePrivate(skSpec));
      ka.doPhase(kfp.generatePublic(pkSpec), true);
      byte[] sharedSecret = ka.generateSecret();
      ///// Derive a key from shared secret.
      boolean meFirst = false;
      for (int i0 = 0; (i0 < myPk.length) && (i0 < otherPk.length); ++i0) {
        if (myPk[i0] != otherPk[i0]) {
          meFirst = (myPk[i0] > otherPk[i0]);
          break;
        }
      }
      MessageDigest hash = MessageDigest.getInstance("SHA-256");
      hash.update(sharedSecret);
      hash.update(meFirst ? myPk : otherPk);
      hash.update(meFirst ? otherPk : myPk);
      out = hash.digest();
    } catch (Exception e) {
      return null;
    }
    return out;
  }

  /** Use hash of public key for checking in case of lost packages. */
  private byte[] getKey4Hash(String who, boolean mine, int hash, boolean senderIsMe) {
    byte[] check;
    final String which = mine ? ("KEY.0.ECDH." + who + ".P") : "KEY." + who + ".ENC.ECDH.P";
    for (int n0 = 1; n0 <= 2; ++n0) {
      check = prefs.get(which + n0, null);
      if ((null != check) && (hash32_fnv1a(check) == hash)) {
        if (mine && !senderIsMe && (n0 == 1)) {
          String caau =
              new String(
                  prefs.get("KEY.0.ECDH." + who + ".CAAU", "0000".getBytes(StringFunc.CHAR8)),
                  StringFunc.CHAR8);
          if ('9' < caau.charAt(0)) {
            // Is acknowledged. Start count-down.
            prefs.set(
                "KEY.0.ECDH." + who + ".CAAU",
                ("9" + caau.substring(1)).getBytes(StringFunc.CHAR8));
          }
        }
        return check;
      }
    }
    check = prefs.get(mine ? "KEY.0.SIG.ECDSA256.P" : ("KEY." + who + ".SIG.ECDSA256.P"), null);
    if ((null != check) && (hash32_fnv1a(check) == hash)) {
      return check;
    }
    return null;
  }

  private byte[] getMatchingSk(byte[] pk, String who) {
    final String which = "KEY.0.ECDH." + who + ".P";
    for (int n0 = 1; n0 <= 2; ++n0) {
      if (Arrays.equals(pk, prefs.get(which + n0, null))) {
        return prefs.get("KEY.0.ECDH." + who + ".S" + n0, null);
      }
    }
    if (Arrays.equals(pk, prefs.get("KEY.0.SIG.ECDSA256.P", null))) {
      return prefs.get("KEY.0.SIG.ECDSA256.S", null);
    }
    return null;
  }

  private boolean setNewKey(String who, byte[] key, byte[] matchingSk) {
    int hash = hash32_fnv1a(key);
    String which =
        (null != matchingSk) ? ("KEY.0.ECDH." + who + ".P") : "KEY." + who + ".ENC.ECDH.P";
    byte[] key2 = getKey4Hash(who, (null != matchingSk), hash, (null != matchingSk));
    if (null != key2) {
      if (null != matchingSk) {
        return false;
      } else if (Arrays.equals(key, key2)) {
        return true;
      }
      prefs.remove("KEY." + who + ".ENC.AES");
      prefs.set(which + 2, key2);
      prefs.set("KEY.0.ECDH." + who + ".CAAU", "0000".getBytes(StringFunc.CHAR8));
      return true;
    }
    prefs.remove("KEY." + who + ".ENC.AES");
    prefs.set(which + 2, prefs.get(which + 1, null));
    prefs.set(which + 1, key);
    if (null == matchingSk) {
      String caau =
          new String(
              prefs.get("KEY.0.ECDH." + who + ".CAAU", "X000".getBytes(StringFunc.CHAR8)),
              StringFunc.CHAR8);
      prefs.set(
          "KEY.0.ECDH." + who + ".CAAU", ("" + caau.charAt(0) + "010").getBytes(StringFunc.CHAR8));
    } else {
      prefs.set("KEY.0.ECDH." + who + ".S2", prefs.get("KEY.0.ECDH." + who + ".S1", null));
      prefs.set("KEY.0.ECDH." + who + ".S1", matchingSk);
      prefs.set("KEY.0.ECDH." + who + ".CAAU", "X000".getBytes(StringFunc.CHAR8));
    }
    return true;
  }

  private boolean createDhKey(String who, char id) throws NoSuchAlgorithmException {
    KeyPairGenerator kpg;
    kpg = KeyPairGenerator.getInstance("EC");
    kpg.initialize(256);
    KeyPair kp = kpg.generateKeyPair();
    byte[] pk = kp.getPublic().getEncoded();
    byte[] sk = kp.getPrivate().getEncoded();
    if (0 == id) {
      prefs.remove("KEY." + who + ".ENC.AES.K1");
      prefs.remove("KEY." + who + ".ENC.AES.K2");
      return setNewKey(who, pk, sk);
    }
    prefs.remove("KEY." + who + ".ENC.AES.K" + id);
    prefs.set("KEY.0.ECDH." + who + ".P" + id, pk);
    prefs.set("KEY.0.ECDH." + who + ".S" + id, sk);
    prefs.remove("KEY." + who + ".ENC.AES");
    return true;
  }

  private byte[] getPkInfo4Sending(String who) {
    byte[] myPk = prefs.get("KEY.0.ECDH." + who + ".P1", null);
    if (null == myPk) {
      try {
        if (!createDhKey(who, (char) 0)) {
          createDhKey(who, '1');
        }
      } catch (NoSuchAlgorithmException e) {
        return null;
      }
      myPk = prefs.get("KEY.0.ECDH." + who + ".P1", null);
    }
    String caau =
        new String(
            prefs.get("KEY.0.ECDH." + who + ".CAAU", "X000".getBytes(StringFunc.CHAR8)),
            StringFunc.CHAR8);
    byte[] otherPk = prefs.get("KEY." + who + ".SIG.ECDSA256.P", null);
    boolean full = true;
    if ('1' == caau.charAt(2)) {
      otherPk = prefs.get("KEY." + who + ".ENC.ECDH.P1", otherPk);
      full = '9' <= caau.charAt(0);
    }
    if (null == otherPk) {
      return null;
    }
    int len = myPk.length;
    byte[] out = new byte[10];
    if (full) {
      out = Arrays.copyOf(myPk, 5 + 2 + myPk.length);
      out[len] = (byte) len;
      out[++len] = SerFunc.TAG_BYTES_L1;
      ++len;
    } else {
      MiscFunc.bytes4Long(out, 0, hash32_fnv1a(myPk), 4);
      out[4] = (byte) (SerFunc.TAG_BYTES_L0 + 4);
      len = 5;
    }
    MiscFunc.bytes4Long(out, len, hash32_fnv1a(otherPk), 4);
    out[len + 4] = (byte) (SerFunc.TAG_BYTES_L0 + 4);
    return out;
  }

  private byte[] getAesKey(String who, boolean senderIsMe, byte[] pkInfo, int offsTag2) {
    long offsLen2 = SerFunc.getTcvOffsetLength(pkInfo, 2, offsTag2, 1);
    long offsLen1 = SerFunc.getTcvOffsetLength(pkInfo, 2, -1 + (int) offsLen2, 1);
    byte[] myKeyOrHash =
        senderIsMe
            ? Arrays.copyOfRange(pkInfo, (int) offsLen1, (int) (offsLen1 >>> 32) + (int) offsLen1)
            : Arrays.copyOfRange(pkInfo, (int) offsLen2, (int) (offsLen2 >>> 32) + (int) offsLen2);
    byte[] otherKeyOrHash =
        !senderIsMe
            ? Arrays.copyOfRange(pkInfo, (int) offsLen1, (int) (offsLen1 >>> 32) + (int) offsLen1)
            : Arrays.copyOfRange(pkInfo, (int) offsLen2, (int) (offsLen2 >>> 32) + (int) offsLen2);
    if (4 == myKeyOrHash.length) {
      myKeyOrHash =
          getKey4Hash(who, true, (int) MiscFunc.long4Bytes(myKeyOrHash, 0, 4), senderIsMe);
    }
    if (4 == otherKeyOrHash.length) {
      otherKeyOrHash =
          getKey4Hash(who, false, (int) MiscFunc.long4Bytes(otherKeyOrHash, 0, 4), senderIsMe);
    }
    if ((null == myKeyOrHash) || (null == otherKeyOrHash)) {
      return null;
    }
    boolean isLatest = false;
    byte[] out;
    byte[] check = prefs.get("KEY." + who + ".ENC.ECDH.P1", null);
    String caau =
        new String(
            prefs.get("KEY.0.ECDH." + who + ".CAAU", "0000".getBytes(StringFunc.CHAR8)),
            StringFunc.CHAR8);
    if ((null != check) && Arrays.equals(otherKeyOrHash, check)) {
      check = prefs.get("KEY.0.ECDH." + who + ".P1", null);
      if ((null != check) && Arrays.equals(myKeyOrHash, check)) {
        isLatest = true;
        out = prefs.get("KEY." + who + ".ENC.AES", null);
        if (null != out) {
          if (('0' < caau.charAt(0)) && ('9' >= caau.charAt(0))) {
            // Count down.
            prefs.set(
                "KEY.0.ECDH." + who + ".CAAU",
                ("" + (char) (-1 + '0' + (0xf & caau.charAt(0))) + caau.substring(1))
                    .getBytes(StringFunc.CHAR8));
          }
          return out;
        }
      }
    }
    if (!isLatest) {
      prefs.remove("KEY." + who + ".ENC.AES");
    }
    out = calcAesKey(getMatchingSk(myKeyOrHash, who), myKeyOrHash, otherKeyOrHash);
    if ((isLatest) && (null != out)) {
      prefs.set("KEY." + who + ".ENC.AES", out);
    }
    return out;
  }

  public Attachment encrypt_sign(Attachment unenc, String to) {
    String filename = unenc.name;
    byte[] enc;
    String caau =
        new String(prefs.get("KEY.0.ECDH." + to + ".CAAU", new byte[0]), StringFunc.CHAR8);
    try {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      unenc.datahandler.writeTo(os);
      unenc = null;
      byte[] dat = os.toByteArray();
      if ('0' >= caau.charAt(0)) { // && (null != prefs.get( "KEY." + to + ".ENC.AES", null ))) {
        ///// Countdown done. Need a new key.
        if (createDhKey(to, (char) 0)) {
          prefs.set(
              "KEY.0.ECDH." + to + ".CAAU", ("X" + caau.substring(1)).getBytes(StringFunc.CHAR8));
          prefs.remove("KEY." + to + ".ENC.AES");
          // Dib2Root.log( "encrypt sign", "new key " + hash.. );
        }
      }
      byte[] pkInfo = getPkInfo4Sending(to);
      if (null == pkInfo) {
        return null;
      }
      byte[] aesKey = getAesKey(to, true, pkInfo, pkInfo.length - 1);
      TcvCodec.HeaderInfo hi =
          new TcvCodec.HeaderInfo(
              Dib2Constants.FILE_STRUC_VERSION_CMPAT,
              'C',
              'A',
              prefs.get("email_address", null),
              caau,
              pkInfo); // prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 1 ),
      // null ) );
      enc =
          TcvCodec.instance.pack(
              dat, 0, dat.length, aesKey, null, hi, prefs.get("KEY.0.SIG.ECDSA256.S", new byte[0]));
      if (null == enc) {
        return null;
      }
    } catch (Exception e) {
      Dib2Root.log("encrypt sign", "error " + caau + " " + e + e.getMessage());
      return null;
    }
    Attachment a_out = new Attachment();
    a_out.name = filename + ".asc";
    DataSource ds = new ByteArrayDataSource(enc, "application/octet-stream");
    a_out.datahandler = new DataHandler(ds);
    return a_out;
  }

  public Attachment decrypt_verify(Attachment att) {
    JResult addrPk = JResult.get8Pool();
    String filename = att.name;
    byte[] dat;
    try {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      att.datahandler.writeTo(os);
      att = null;
      dat = os.toByteArray();
      long offsPk0 = TcvCodec.instance.unpackAddrPk(addrPk, dat, TcvCodecAes.instance);
      if (0 >= offsPk0) {
        if (0 > offsPk0) {
          return null;
        }
        // Old format, try to keep the chat alive ...
        // A reply with the new format will do ...
        // dat = decrypt_verify_OLD( dat);
        dat = "...".getBytes();
        Attachment a_out = new Attachment();
        a_out.name = filename + ".asc";
        DataSource ds = new ByteArrayDataSource(dat, "application/octet-stream");
        a_out.datahandler = new DataHandler(ds);
        return a_out;
      }
    } catch (Exception e) {
      Dib2Root.log("decrypt_verify", "error " + e + e.getMessage());
      return null;
    }
    String addr = (String) addrPk.object0;
    byte[] newKey = (byte[]) addrPk.object1;
    int offsPkTag = (int) addrPk.long0;
    try {
      byte[] aesKey = getAesKey(addr, false, dat, offsPkTag);
      if (null == aesKey) {
        dat = null;
      } else {
        byte[] sigKey = prefs.get("KEY." + addr + ".SIG.ECDSA256.P", null);
        dat = TcvCodecAes.instance.decode(dat, 0, dat.length, aesKey, sigKey);
      }
      if (null == dat) {
        // if (16 < key.length) ...;
        Dib2Root.log("decrypt_verify", "error regarding signature?");
      } else {
        dat = TcvCodecAes.instance.decompress(dat, dat.length);
        ///// After having checked the signature ...!
        // String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU",
        // "0000".getBytes( StringFunc.CHAR8 ) ) );
        if (16 < newKey.length) {
          setNewKey(addr, newKey, null);
        }
      }
    } catch (Exception e) {
      Dib2Root.log("decrypt_verify", "(key sync?) error " + e + e.getMessage());
      byte[] pk = prefs.get("KEY." + addr + ".SIG.ECDSA256.P", null);
      ///// Verification might have succeeded => let the info pass a single time.
      if ((null == pk) || (5 >= pk.length)) {
        return null;
      }
      dat = null;
    }
    if (null == dat) {
      dat =
          "(ERROR - KEY SYNC?)\nRe-sync by sending message or invitation."
              .getBytes(StringFunc.CHAR8);
      // Fallback/ Force new key on our side.
      prefs.set("KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes(StringFunc.CHAR8));
      // prefs.set( "KEY." + addr + ".CIPHER", new byte[] { 'C' });
      if (errorMsg) {
        return null;
      }
      errorMsg = true;
    }
    Attachment a_out = new Attachment();
    a_out.name = filename + ".asc";
    DataSource ds = new ByteArrayDataSource(dat, "application/octet-stream");
    a_out.datahandler = new DataHandler(ds);
    return a_out;
  }

  public void public_keyring_remove_by_address(String addr) {
    byte[] em = prefs.get("email_address", null);
    if (null == em) {
      return;
    }
    if (addr.equalsIgnoreCase(new String(em, StringFunc.CHAR16UTF8))
        || (1 >= addr.length())) {
      // Must not remove myself.
      return;
    }
    // prefs.preference_set( "KEY." + addr + ".SIG.ECDSA256.P", null );
    prefs.remove("KEY." + addr + ".SIG.ECDSA256.P");
    // prefs.remove( "KEY." + addr + ".CIPHER");
    prefs.remove("KEY." + addr + ".ENC.ECDH.P1");
    prefs.remove("KEY." + addr + ".ENC.ECDH.P2");
  }

  public boolean public_keyring_check_by_address(String addr) {
    return null != prefs.get("KEY." + addr + ".SIG.ECDSA256.P", null);
  }

  public String testPks(String... ids) {
    StringBuilder out = new StringBuilder(8 * ids.length);
    byte[] key;
    key = prefs.get("KEY.0.SIG.ECDSA256.P", null);
    out.append((null == key) ? '.' : (char) ((key[key.length - 1] & 0x7f) | 0x40));
    for (String id : ids) {
      out.append(' ');
      key = prefs.get("KEY.0.ECDH." + id + ".P1", null);
      out.append((null == key) ? '.' : (char) ((key[key.length - 1] & 0x7f) | 0x40));
      key = prefs.get("KEY." + id + ".ENC.ECDH.P1", null);
      out.append((null == key) ? '.' : (char) ((key[key.length - 1] & 0x7f) | 0x40));
      key = prefs.get("KEY.0.ECDH." + id + ".P2", null);
      out.append((null == key) ? '.' : (char) ((key[key.length - 1] & 0x7f) | 0x40));
      key = prefs.get("KEY." + id + ".ENC.ECDH.P2", null);
      out.append((null == key) ? '.' : (char) ((key[key.length - 1] & 0x7f) | 0x40));
    }
    return out.toString();
  }
}
