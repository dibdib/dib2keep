// Copyright (C) 2014, 2023  Roland Horsch and others:
// -- For the changes:  Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original: Copyright (C) 2014/2015  Jeroen Vreeken <jeroen@vreeken.net>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from quickmsg.vreeken.net.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import com.gitlab.dibdib.picked.net.Mail.Attachment;
import java.util.*;
import javax.activation.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.CcmSto;

public class QmAct extends QmBg {
  // =====

  int maxview = Dib2Constants.MAXVIEW_MSGS_INIT;
  protected static String zMessageTextbox = null;
  protected static int zMessageTextboxRef = 0;

  protected long unreadTime = 0;

  public boolean init(String email) {
    svcContext = this;
    MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
    mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
    mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
    mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
    mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
    mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");

    QmDb.db.init(email);
    return true;
  }

  public void send_key(QmDb.Contact contact, boolean override) {
    final QmDb.Contact sendcontact = contact;
    if (override) {
      QmDb.pgp.public_keyring_remove_by_address(contact._address);
    }
    Attachment attachment = QmDb.pgp.key_attachment(QmDb.my_addr);

    List<Attachment> attachments = new LinkedList<Attachment>();
    attachments.add(attachment);

    mail.send(sendcontact._address, attachments);
  }

  public boolean send_msg_attachment(final QmDb.Contact contact, Object uri, String sMsg) {
    if (sMsg.length() < 1 && uri == null) return false;

    zMessageTextbox = sMsg;
    maxview = Dib2Constants.MAXVIEW_MSGS_INIT;
    final QmDb.MessageData msg0 = new QmDb.MessageData();
    msg0._c = contact._inx;
    msg0._from = 1;
    msg0._time = -1; // time_now );
    msg0._text = zMessageTextbox;
    msg0._uri = uri;

    boolean local = (contact._inx <= 1) || !contact._address.contains("@");
    if (local) {
      QmDb.db.message_add(msg0);
      return true;
    }

    final long now = DateFunc.currentTimeMillisLinearized();
    QmDb.MessageData msgFull = msg0;
    QmDb.db.message_add(msg0);
    if ((contact._type == QuickMsg.TYPE_PERSON) //
        && (0 > contact._inx)) { // ((QuickMsg.KEYSTAT_BIT_ACTIVE & contact._keystat) == 0)) {
      zMessageTextbox = "Not sent (MISSING KEY):\n" + zMessageTextbox;
      msg0._text = zMessageTextbox;
    } else
    // TODO ACK/ re-send for groups
    if ((uri == null) && (contact._type != QuickMsg.TYPE_GROUP)) {
      msg0.ack = now + 10 * 60 * 1000;
      QmDb.db.message_update(msg0);
      msgFull = new QmDb.MessageData();
      msgFull._c = msg0._c;
      msgFull._from = 1;
      msgFull._time = -1; // time_now );
      List<QmDb.MessageData> messages = QmDb.db.message_dangling_by_id(contact._inx, 30);
      StringBuilder mText = new StringBuilder(50 + 3 * msg0._text.length() + 30 * messages.size());
      mText.append(
          new String(Dib2Constants.MAGIC_BYTES)
              + "M("
              + DateFunc.date4Millis()); // date4Millis_OLD(false));
      mText.append(")" + Dib2Constants.VERSION_STRING + '\n');
      mText.append(msg0.toTsvLine("MSG"));
      mText.append('\n');
      String ack = "";
      int cResend = 0;
      for (QmDb.MessageData mx : messages) {
        if (0 == mx.ack) {
          continue;
        } else if ((null != mx._uri)
            || ((3 >= mx._text.length()) && (0 >= mx._text.trim().length()))) {
          mx.ack = 0;
        } else if ((1 >= mx._from) && (10 > cResend)) {
          // Queued? Do not re-send too quickly:
          if (0 > mx.ack) {
            mx.ack = mx._time + 30 * 60 * 1000;
          }
          if (mx.ack < now) {
            ++cResend;
            mText.append(mx.toTsvLine("MSG"));
            mText.append('\n');
            mx.ack = now + (30 * 60 * 1000 + mx.ack - mx._time);
            QmDb.db.message_update(mx);
          }
        }
        if (1 < mx._from) { // && (null == mx.queue_get())) {
          mx.ack = 0;
          QmDb.db.message_update(mx);
          ack += "\t" + mx.oid;
          if (mx._text.contains(":ID:")) {
            ack += "\t" + CcmSto.getTaggedValueOr(":ID:", mx._text, "\n", "");
          }
        }
      }
      if (0 < ack.length()) {
        mText.append(QmDb.toTsvLine("TMP", null, -1, "ACK", ack, null, -1));
        mText.append('\n');
      }
      if (5 > cResend) {
        for (QmDb.MessageData mx : messages) {
          if ((mx.ack >= (now + 40 * 60 * 1000)) && (1 >= mx._from) && (5 >= cResend)) {
            ++cResend;
            mText.append(mx.toTsvLine("MSG"));
            mText.append('\n');
          }
        }
      }
      msgFull._text = StringFunc.mnemonics4String(mText.toString(), false, false);
    }
    final QmDb.MessageData msg1 = msgFull;

    final Attachment unenc;

    Dib2Root.log("mainactivity send_msg", "send to " + contact._name + "id: " + contact._inx);

    QuickMsg qmsg = new QuickMsg();
    unenc = qmsg.send_message(QmDb.db.contact_get_by_id(1), contact, msg1);
    //  Dib2Root.log( "mainactivity send_msg", "got message unencrypted" );
    final List<String> to_adds;
    if (contact._type == QuickMsg.TYPE_GROUP) {
      to_adds = contact._members;
      if ((null == to_adds) || (0 == to_adds.size())) return true;
    } else {
      to_adds = new LinkedList<String>();
      to_adds.add(contact._address);
    }

    EcDhQm pgp_enc = QmDb.pgp;

    Attachment id = pgp_enc.pgpmime_id();

    for (int i = 0; i < to_adds.size(); i++) {
      String to = to_adds.get(i);
      if (to.equals(QmDb.my_addr)) {
        continue;
      }

      Attachment enc;
      try {
        enc = pgp_enc.encrypt_sign(unenc, to);
      } catch (OutOfMemoryError e) {
        // Log.e("send_msg", "Out of memory during encryption, attachment to big?");
        enc = null;
      } catch (Exception e) {
        // Log.e("send_msg", "pgp exception (missing key?...): " + e);
        enc = null;
      }
      if (enc == null) {
        if (!msg1._text.contains("[Error")) {
          msg1._text = msg1._text + "\n[Error!]";
          QmDb.db.message_update(msg1);
        }
        continue;
      }
      enc.disposition = "inline";

      Dib2Root.log("mainactivity send_msg", "got message encrypted");
      List<Attachment> attachments = new LinkedList<Attachment>();
      attachments.add(id);
      attachments.add(enc);

      String queue = mail.send(to, attachments, "encrypted");
      if (queue != null) {
        msg1.ack = -1; // .queue_set( queue );
        QmDb.db.message_update(msg1);
        // local_message.send_statusMsg(context, "?");
      }
    }
    Dib2Root.log("mainactivity send_msg", "mail.send done");
    return true;
  }

  // =====
}
