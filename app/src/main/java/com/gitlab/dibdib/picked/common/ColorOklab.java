// Copyright (C) 2020, 2022  Roland Horsch and others:
// -- For the presented form: Copyright (C) 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original work: Copyright (c) 2020 Björn Ottosson (see below).
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.
// Cmp. https://bottosson.github.io/misc/License.txt

package com.gitlab.dibdib.picked.common;

/** Lightness, chroma, hue calculations. */
// Chroma = sqrt(a^2 + b^2), a = green/red, b = blue/yellow,
// a = Chroma * cos(hue), b = Chroma * sin(hue).
// Cmp. https://bottosson.github.io/posts/oklab (@2022-11-11), with the referenced
// license as inserted at the end of this file.
// <1,0,0>: X=0.950, Y=1.0, Z=1.089.
public class ColorOklab {

  public static class Lab {
    public final double L;
    public final double a;
    public final double b;

    public Lab(double xL, double xA, double xB) {
      L = xL;
      a = xA;
      b = xB;
    }
  }
  ;

  public static class RGB {
    public final double r;
    public final double g;
    public final double b;

    public RGB(double xR, double xG, double xB) {
      r = xR;
      g = xG;
      b = xB;
    }
  }
  ;

  public static Lab linear_srgb_to_oklab(RGB c) {
    double l = 0.4122214708f * c.r + 0.5363325363f * c.g + 0.0514459929f * c.b;
    double m = 0.2119034982f * c.r + 0.6806995451f * c.g + 0.1073969566f * c.b;
    double s = 0.0883024619f * c.r + 0.2817188376f * c.g + 0.6299787005f * c.b;

    double l_ = Math.cbrt(l);
    double m_ = Math.cbrt(m);
    double s_ = Math.cbrt(s);

    return new Lab(
        0.2104542553f * l_ + 0.7936177850f * m_ - 0.0040720468f * s_,
        1.9779984951f * l_ - 2.4285922050f * m_ + 0.4505937099f * s_,
        0.0259040371f * l_ + 0.7827717662f * m_ - 0.8086757660f * s_);
  }

  public static RGB oklab_to_linear_srgb(Lab c) {
    double l_ = c.L + 0.3963377774f * c.a + 0.2158037573f * c.b;
    double m_ = c.L - 0.1055613458f * c.a - 0.0638541728f * c.b;
    double s_ = c.L - 0.0894841775f * c.a - 1.2914855480f * c.b;

    double l = l_ * l_ * l_;
    double m = m_ * m_ * m_;
    double s = s_ * s_ * s_;

    return new RGB(
        +4.0767416621f * l - 3.3077115913f * m + 0.2309699292f * s,
        -1.2684380046f * l + 2.6097574011f * m - 0.3413193965f * s,
        -0.0041960863f * l - 0.7034186147f * m + 1.7076147010f * s);
  }

  public static RGB rgb4Code(int rgbCode) {
    final int red = (rgbCode >> 16) & 0xff;
    final int green = (rgbCode >> 8) & 0xff;
    final int blue = rgbCode & 0xff;
    return new RGB(red / 255.0, green / 255.0, blue / 255.0);
  }

  public static int rgbCode(RGB rgbOk) {
    int red = (int) (0.9 + 255.1 * rgbOk.r);
    int green = (int) (0.9 + 255.1 * rgbOk.g);
    int blue = (int) (0.9 + 255.1 * rgbOk.b);
    red = (0xff <= red) ? 0xff : ((0 <= red) ? red : 0);
    green = (0xff <= green) ? 0xff : ((0 <= green) ? green : 0);
    blue = (0xff <= blue) ? 0xff : ((0 <= blue) ? blue : 0);
    return (red << 16) | (green << 8) | blue;
  }

  public static double chroma4lab(Lab lab) {
    return Math.sqrt(lab.a * lab.a + lab.b * lab.b);
  }

  public static double hue4lab(Lab lab) {
    final double chroma = chroma4lab(lab);
    if (chroma < (1.0 / (1 << 16))) {
      return 0;
    }
    final double hue = Math.acos(lab.a / chroma);
    return (0 <= lab.b) ? hue : (Math.PI * 2 - hue);
  }

  /*
  License for original work:

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
  */
}
