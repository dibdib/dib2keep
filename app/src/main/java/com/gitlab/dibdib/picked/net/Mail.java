// Copyright (C) 2014, 2023  Roland Horsch and others:
// -- For the changes:  Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original: Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.*;
import javax.activation.*;
import javax.mail.*;
import javax.mail.event.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.thread_any.*;

public class Mail {

  public static volatile IMAPFolder mail_folder = null;
  protected static volatile Store mail_store = null;
  // public static volatile boolean receive = false;
  public static volatile int unread = 0;

  public static class Attachment {
    public DataHandler datahandler;
    public String name;
    public String disposition = "attachment";
  }

  static void log_e(String task, String msg, Exception e) {
    if (e != null) {
      String msg2 = e.getMessage();
      msg = (msg2 == null) ? msg : msg2;
      msg = "" + e + ((msg != null) ? (" (" + msg + ")") : "");
    }
    Dib2Root.log(task, msg);
  }

  //  private static void local_message(String status) {
  //    Dib2Root.ui.progress = status.substring(1);
  //  }

  public String get_mqueue_dir() {
    final String path = Dib2Root.platform.getFilesDir("main").getAbsolutePath() + "/mailqueue/";
    new File(path).mkdir();
    return path;
  }

  public Session get_smtp_session(boolean use_tls) {
    // CachedPreferences preferences = new CachedPreferences();

    String host = QmDb.db.getLiteral("smtp_server", "example.com");
    String user = QmDb.db.getLiteral("smtp_user", "");
    String pass = StringFunc.string4HexUtf8(TcvCodec.instance.getPassPhraseHex());
    String port = QmDb.db.getLiteral("smtp_port", "587");

    Properties props = System.getProperties();
    if (use_tls) {
      props.put("mail.smtp.starttls.enable", "true");
    } else {
      props.put("mail.smtp.starttle.enable", "false");
    }
    props.put("mail.smtp.starttls.required", "false");

    props.put("mail.smtp.host", host);
    props.put("mail.smtp.user", user);
    props.put("mail.smtp.password", pass);
    props.put("mail.smtp.port", port);
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.ssl.checkserveridentity", "false");
    props.put("mail.smtp.ssl.trust", "*");
    props.put("mail.smtps.ssl.checkserveridentity", "false");
    props.put("mail.smtps.ssl.trust", "*");

    Session session = Session.getDefaultInstance(props, null);
    // session.setDebug(true);
    return session;
  }

  public String send(String to) {
    return send(to, null);
  }

  public String send(String to, List<Attachment> attachments) {
    return send(to, attachments, "mixed");
  }

  public String send(String to, List<Attachment> attachments, String subtype) {
    return send(true, to, attachments, subtype);
  }

  public String send(boolean immediately, String to, List<Attachment> attachments, String subtype) {
    String queue = null;
    String from = QmDb.db.getLiteral("email_address", "nobody@example.com");

    Multipart multiPart;
    String finalString = "";

    Session session = get_smtp_session(true);

    DataHandler handler =
        new DataHandler(
            new ByteArrayDataSource(finalString.getBytes(Charset.forName("UTF-8")), "text/plain"));
    MimeMessage message = new MimeMessage(session);

    try {
      message.setFrom(new InternetAddress(from));
      message.setDataHandler(handler);

      message.addHeader("X-QuickMSG", "v1.0");

      multiPart = new MimeMultipart(subtype);

      InternetAddress toAddress;
      toAddress = new InternetAddress(to);
      message.addRecipient(Message.RecipientType.TO, toAddress);

      message.setSubject("[quickmsg]");
      Date sent_date = new Date();
      message.setSentDate(sent_date);
      message.setText("This is a QuickMSG.");

      // Log.d("smtp_send", "Message headers ready");

      if (attachments != null) {
        // Log.d("send", "attachments: " + attachments.size());
        for (int i = 0; i < attachments.size(); i++) {
          BodyPart messageBodyPart = new MimeBodyPart();

          messageBodyPart.setDataHandler(attachments.get(i).datahandler);
          messageBodyPart.setFileName(attachments.get(i).name);
          messageBodyPart.setDisposition(attachments.get(i).disposition);
          multiPart.addBodyPart(messageBodyPart);
        }
      }
      message.setContent(multiPart);
    } catch (Exception e) {
      log_e("send_msg", "could not create message: ", e);
      return queue;
    }

    if (!immediately || !smtp_send(message)) {
      queue = save(message);
      //if (!immediately) local_message("TMessage saved.");
    }

    return queue;
  }

  public String save(Message m) {
    String dir = get_mqueue_dir();
    if (dir == null) return null;

    File df = new File(dir);
    String mfile = UUID.randomUUID().toString();
    File mf = new File(dir, mfile);
    df.mkdirs();
    // Log.d("mail save", "save message to dir: " + dir + " file: " + mfile);

    OutputStream of;
    try {
      of = new FileOutputStream(mf);
    } catch (FileNotFoundException e) {
      String msg = e.getMessage();
      // Log.d("save", "Could not create file: " + (msg != null ? msg : "null"));
      return null;
    }

    try {
      m.writeTo(of);
    } catch (IOException e) {
      String msg = e.getMessage();
      // Log.d("save", "IOException: " + (msg != null ? msg : "null"));
    } catch (MessagingException e) {
      String msg = e.getMessage();
      // Log.d("save", "MessagingException: " + (msg != null ? msg : "null"));
    }

    try {
      of.close();
    } catch (Exception e) {
      log_e("mail save", "exception", e);
    }

    return mfile;
  }

  public boolean queue_check(String filename) {
    File f = new File(get_mqueue_dir(), filename);

    return f.exists();
  }

  public void flush() {
    String dir = get_mqueue_dir();
    if (dir == null) return;

    File[] files = new File(dir).listFiles();
    if (files == null) return;
    for (File f : files) {
      if (f.isFile()) {
        // Log.d("mail flush", "Message in queue, size: " + f.length());
        if (f.length() == 0) {
          f.delete();
          // Log.d("mail flush", "Delete empty message, propably from a previous buggy
          // version");
          continue;
        }
        InputStream is;
        try {
          is = new FileInputStream(f);
        } catch (FileNotFoundException e1) {
          continue;
        }

        Session session = get_smtp_session(true);
        try {
          Message message = new MimeMessage(session, is);

          if (smtp_send(message)) {
            f.delete();
          }

          // Log.d("mail flush", "Message send");
        } catch (MessagingException e) {
          log_e("flush", "Sending message failed", e);
          continue;
        }
      }
    }
  }

  public boolean smtp_send(Message message) {
    String host = QmDb.db.getLiteral("smtp_server", "example.com");
    String user = QmDb.db.getLiteral("smtp_user", "");
    String pass = StringFunc.string4HexUtf8(TcvCodec.instance.getPassPhraseHex());
    String port = QmDb.db.getLiteral("smtp_port", "587");
    String transport1;
    String transport2;

    if (port.equals("465")) {
      transport1 = "smtps";
      transport2 = "smtp";
    } else {
      transport1 = "smtp";
      transport2 = "smtps";
    }

    try {

      Session session = get_smtp_session(true);
      Transport transport = session.getTransport(transport1);

      transport.connect(host, user, pass);

      transport.sendMessage(message, message.getAllRecipients());
      transport.close();
    } catch (Exception e) {
      log_e("send_msg", "could not send message with " + transport1, e);
      try {

        Session session = get_smtp_session(true);
        Transport transport = session.getTransport(transport2);

        transport.connect(host, user, pass);

        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
      } catch (Exception e2) {
        log_e(
            "send_msg",
            "could not send message with " + transport2,
            e2); // + ": " + e.getMessage());
        // e.printStackTrace();
        return false;
      }
    }
    // Log.d("send_msg", "message has been sent.");

    return true;
  }

  // To be overridden.
  public Boolean recv_quickmsg_cb(
      String from, List<Attachment> attachments, String subtype, Date timeMax) {
    return true;
  }

  public Store open_imap() {
    if (mail_store != null) {
      if (mail_store.isConnected()) return mail_store;
    }
    imap_close();

    String host = QmDb.db.getLiteral("imap_server", "example.com");
    String port = QmDb.db.getLiteral("imap_port", "993");
    String user = QmDb.db.getLiteral("imap_user", "nobody");
    String pass = StringFunc.string4HexUtf8(TcvCodec.instance.getPassPhraseHex());

    Properties props = System.getProperties();

    SocketFactory sfo = SSLSocketFactory.getDefault();
    props.setProperty("mail.store.protocol", "imaps");
    props.setProperty("mail.imaps.host", host);
    props.setProperty("mail.imaps.port", port);
    props.put("mail.imaps.ssl.socketFactory", sfo);
    props.setProperty("mail.imaps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.setProperty("mail.imaps.socketFactory.fallback", "false");
    props.setProperty("mail.imaps.ssl.socketFactory.port", port);
    props.setProperty("mail.imaps.connectiontimeout", "6000");
    props.setProperty("mail.imaps.ssl.trust", "*");
    props.setProperty("mail.imaps.starttls.enable", "true");
    props.setProperty("mail.imaps.ssl.protocols", "TLSv1.2"); // "TLSv1 TLSv1.1 TLSv1.2");
    props.setProperty("mail.smtp.auth", "true");

    Session session = Session.getInstance(props); // Session.getDefaultInstance(props);
    URLName urlName = new URLName("imaps", host, Integer.parseInt(port), "", user, pass);
    Store store;

    // Log.d("recv_msg", urlName.toString());
    try {
      store = session.getStore(urlName);
      if (!store.isConnected()) {
        store.connect();
      }
    } catch (Exception e) {
      log_e("recv_msg", "connect imap", e); // + e.getMessage());

      urlName = new URLName("imap", host, Integer.parseInt(port), "", user, pass);
      // Log.d("recv_msg", urlName.toString());

      try {
        store = session.getStore(urlName);
        if (!store.isConnected()) {
          store.connect();
        }
      } catch (Exception e2) {
        log_e("recv_msg", "connect imap", e2);
        // local_message.send(context, "Could not connect to the IMAP server");
        return null;
      }
    }

    mail_store = store;

    // Log.d("recv_msg", "store.isConnected():" + store.isConnected());
    // imap_connected = true;
    //local_message("" + imap_connected);

    return store;
  }

  public IMAPFolder open_folder(Store store) {
    // Open the Folder
    String mbox = "INBOX";
    IMAPFolder folder;
    if (store != null) {
      if (store != mail_store) {
        if (mail_store != null) { // || ! mail_store.isConnected()) {
          imap_close();
        }
        mail_store = store;
      }
    }
    if ((mail_store != null) && !mail_store.isConnected()) {
      imap_close();
    }
    if (mail_store == null) {
      mail_folder = null;
      return null;
    }
    if ((mail_folder != null) && mail_folder.isOpen()) {
      return mail_folder;
    }
    if (mail_folder != null) {
      imap_close();
      return null;
    }
    store = mail_store;

    try {
      folder = (IMAPFolder) store.getDefaultFolder();
      if (folder == null) {
        log_e("recv_msg", "No default folder", null);
        return null;
      }

      folder = (IMAPFolder) folder.getFolder(mbox);
      if (!folder.exists()) {
        log_e("recv_msg", mbox + "  does not exist", null);
        return null;
      }

      folder.open(Folder.READ_WRITE);

    } catch (MessagingException e) {
      log_e("open_folder", "exception", e);
      return null;
    } catch (IllegalStateException ei) {
      // String msg = ei.getMessage();
      // Log.e("open_folder", "illegal state: " +(msg != null ? msg : "null"));
      imap_close();
      return null;
    }

    try {
      folder.setSubscribed(true);
    } catch (MessagingException e) {
      log_e("open folder", "setSubscribed", e); // + e.getMessage());
    }

    mail_folder = folder;
    return folder;
  }

  public void noop(IMAPFolder folder) {
    if (folder == null) {
      if (mail_folder != null) folder = mail_folder;
      else return;
    }
    try {
      folder.doCommand(
          new IMAPFolder.ProtocolCommand() {
            public Object doCommand(IMAPProtocol p) throws ProtocolException {
              p.simpleCommand("NOOP", null);
              return null;
            }
          });
    } catch (MessagingException e) {
      log_e("noop", "noop failed", e);
    }
  }

  public void idle(final long timeout) {
    Store store = open_imap();
    if (store == null) {
      try {
        Thread.sleep(500); // 10000
      } catch (InterruptedException e) {
      }
      return;
    }
    final IMAPFolder folder = open_folder(store);

    if (folder == null) {
      return;
    }

    Thread t =
        new Thread(
            new Runnable() {
              public void run() {
                boolean intr = false;
                try {
                  Thread.sleep(timeout);
                } catch (InterruptedException e) {
                  intr = true;
                }
                // Log.d("mail idle", "timeout");
                if (!intr) noop(folder);
              }
            });

    t.start();

    folder.addMessageCountListener(
        new MessageCountListener() {
          @Override
          public void messagesAdded(MessageCountEvent arg0) {
            // Log.d("mail idle", "message added");
            ++unread;
            noop(folder);
          }

          @Override
          public void messagesRemoved(MessageCountEvent arg0) {}
        });

    try {
      folder.idle();
      t.interrupt();
    } catch (MessagingException e) {
      log_e("idle", "exception", e);
      if (!store.isConnected()) {
        // Log.d("idle", "connection closed");
        // imap_connected = false;
        // local_message("" + imap_connected);

        mail_folder = null;
        mail_store = null;
      }
    }
    // Log.d("mail idle", "end of idle");
  }

  public int recv() {
    Store store = open_imap();
    if (store == null) return -1;

    Folder folder = open_folder(store);
    if (folder == null) return -1;

    Boolean exp = false;

    try {

      if (!(folder instanceof UIDFolder)) {
        log_e("recv_msg", "This Provider or this folder does not support UIDs", null);
        return -1;
      }

      UIDFolder ufolder = (UIDFolder) folder;

      int totalMessages = folder.getMessageCount();

      if (totalMessages == 0) {
        // Log.d("recv_msg", "Empty folder");
        return 0;
      }

      // Attributes & Flags for ALL messages ..
      Message[] msgs = ufolder.getMessagesByUID(1, UIDFolder.LASTUID);
      // Use a suitable FetchProfile
      FetchProfile fp = new FetchProfile();
      fp.add(FetchProfile.Item.ENVELOPE);
      fp.add(FetchProfile.Item.FLAGS);
      fp.add("X-QuickMSG");
      fp.add("From");
      folder.fetch(msgs, fp);

      for (int i = 0; i < msgs.length; i++) {
        // ufolder.getUID(msgs[i]) + ":");
        InternetAddress add = (InternetAddress) msgs[i].getFrom()[0];
        String add_str = add.getAddress();
        Pattern pattern = Pattern.compile("<(.*?)>");
        Matcher matcher = pattern.matcher(add_str);
        if (matcher.find()) {
          add_str = matcher.group(1);
        }

        String h[] = msgs[i].getHeader("X-QuickMSG");
        if (h != null) {
          if (msgs[i].isSet(Flags.Flag.DELETED)) continue;

          Object o = msgs[i].getContent();
          if (!(o instanceof MimeMultipart)) continue;

          // rho:
          // if size > ... && 0 == background.isOffline() ) continue;
          Date timeMax = msgs[i].getSentDate();
          Date timeX = msgs[i].getReceivedDate();
          if ((null == timeMax) || ((null != timeX) && timeMax.after(timeX))) timeMax = timeX;

          List<Attachment> attachments = new LinkedList<Attachment>();

          Multipart mp = (Multipart) o;
          o = null;
          ContentType ct = new ContentType(mp.getContentType());
          String subtype = ct.getSubType();

          for (int j = 0; j < mp.getCount(); j++) {
            BodyPart bp = mp.getBodyPart(j);
            Attachment attachment = new Attachment();
            attachment.name = bp.getFileName();
            attachment.datahandler = bp.getDataHandler();
            attachments.add(attachment);
          }
          mp = null;
          ct = null;

          if (recv_quickmsg_cb(add_str, attachments, subtype, timeMax)) {
            msgs[i].setFlag(Flags.Flag.SEEN, true);
            msgs[i].setFlag(Flags.Flag.DELETED, true);
            exp = true;
          }
        }
      }

      if (exp) {
        folder.expunge();
        folder.close(false);
        store.close();
        mail_folder = null;
        mail_store = null;

        // imap_connected = false;
        // local_message.send_connection(context, imap_connected);
      }
    } catch (Exception e) {
      // Log.e("recv_msg", "recv: " + e.getMessage());
    }
    // receive = false; // rho
    return exp ? 1 : 0;
  }

  public void imap_close() {
    IMAPFolder f0 = mail_folder; // rho
    Store s0 = mail_store;
    mail_folder = null;
    mail_store = null;
    if (f0 != null) {
      try {
        if (f0.isOpen()) f0.expunge();
      } catch (Exception e) {
      }
      try {
        f0.close(false);
      } catch (Exception e) {
      }
    }
    if (s0 != null) {
      try {
        s0.close();
      } catch (Exception e) {
      }
    }
  }

  public Attachment multipart_create(List<Attachment> attachments) {
    MimeMultipart multiPart;
    multiPart = new MimeMultipart("mixed");

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    try {
      Properties props = System.getProperties();
      props.put("mail.host", "smtp.dummydomain.com");
      props.put("mail.transport.protocol", "smtp");

      Session mailSession = Session.getDefaultInstance(props, null);
      MimeMessage message = new MimeMessage(mailSession);

      for (int i = 0; i < attachments.size(); i++) {
        Attachment attachment = attachments.get(i);
        BodyPart messageBodyPart = new MimeBodyPart();

        messageBodyPart.setDataHandler(attachment.datahandler);
        messageBodyPart.setFileName(attachment.name);
        messageBodyPart.setDisposition(attachment.disposition);
        multiPart.addBodyPart(messageBodyPart);
      }

      // Log.d("mp", multiPart.getPreamble());
      // multiPart.setPreamble("T");

      message.setContent(multiPart);

      DataHandler dh = message.getDataHandler();
      // Log.d("message", "contenttype: " + dh.getContentType());
      // Log.d("message", message.toString());
      message.writeTo(bos);
    } catch (Exception e) {
      String em = e.getMessage();
      if (em == null) {
        em = "Could not create message with attachments";
      }
      log_e("create mime", em, e);
    }
    DataSource ads = new ByteArrayDataSource(bos.toByteArray(), "multipart/mixed");
    Attachment aout = new Attachment();
    aout.datahandler = new DataHandler(ads);
    aout.name = "encrypted";

    return aout;
  }

  public List<Attachment> multipart_get_attachments(Attachment mpa) {
    Properties props = System.getProperties();
    props.put("mail.host", "smtp.dummydomain.com");
    props.put("mail.transport.protocol", "smtp");
    Session mailSession = Session.getDefaultInstance(props, null);
    MimeMessage msg = null;

    try {
      msg = new MimeMessage(mailSession, mpa.datahandler.getInputStream());
    } catch (Exception e) {
      log_e("multipart_get_attachments", "Could not create mime message", e); // + e.getMessage());

      return null;
    }

    Object o;
    try {
      o = msg.getContent();
    } catch (Exception e) {
      log_e("multipart_get_attachments", "Could not get content", e); // + e.getMessage());
      return null;
    }
    if (!(o instanceof MimeMultipart)) return null;

    List<Attachment> attachments = new LinkedList<Attachment>();

    Multipart mp = (Multipart) o;
    try {
      for (int j = 0; j < mp.getCount(); j++) {
        BodyPart bp = mp.getBodyPart(j);
        Attachment attachment = new Attachment();
        attachment.name = bp.getFileName();
        attachment.datahandler = bp.getDataHandler();
        attachments.add(attachment);
      }
    } catch (Exception e) {
      log_e("multipart_get_attachments", "Could not get attachments", e); // + e.getMessage());
      return null;
    }

    return attachments;
  }
}
