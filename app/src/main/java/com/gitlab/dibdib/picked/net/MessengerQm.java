// Copyright (C) 2022, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import java.util.Arrays;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.DateFunc;
import net.sf.dibdib.thread_net.*;
import net.sf.dibdib.thread_wk.CcmSto;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

public class MessengerQm extends QmAct implements QOpNet.MessengerIf {

  private QmDb.Contact makeContact(long xhContact) {
    QIfs.QItemIf c0 = CcmSto.peek(xhContact, false);
    if (!(c0 instanceof QSTuple)) {
      return null;
    }
    QSTuple t0 = (QSTuple) c0;
    String s0 = ((QSeq) t0.getValue(CcmSto.CcmTag.DAT)).toStringFull();
    final String addr = CcmSto.getFirstEmail(s0);
    final String label = CcmTag.LABEL.getWord(t0).toStringFull();
    if ((null == addr) || (0 >= addr.indexOf('@'))) {
      return null;
    }
    QmDb.Contact out = QmDb.db.contact_get_person_by_address(addr);
    out.oid = t0.stamp;
    out._name = CcmSto.getTaggedValueOr(":TOPIC:", s0, "\n", addr.substring(0, addr.indexOf('@')));
    if (s0.contains(":GROUP:") && label.matches("G[0-9]+")) {
      out._group = (int) Long.parseLong(label.substring(1));
      out._members =
          Arrays.asList(CcmSto.getTaggedValueOr(":GROUP:", s0, "\n", addr).split("[, ]+"));
    }
    return out;
  }

  @Override
  public boolean inviteOrConfirm(long xhContact, boolean force) {
    // if (s0.contains(":FP:")...
    QmDb.Contact cx = makeContact(xhContact);
    if (null != cx) {
      send_key(cx, force);
    }
    return true;
  }

  static final String[] receiveMessages_empty = new String[0];

  @Override
  public String[] receiveMessages(int msecs, boolean looping) {
    if (looping) {
      mail.idle(msecs);
      if (Mail.unread == 0) {
        return receiveMessages_empty;
      }
    }
    Mail.unread = 0;
    int rc = mail.recv();
    if (0 > rc) {
      // Delay next attempt
      try {
        Thread.sleep(msecs / 2);
      } catch (InterruptedException e) {
      }
    } else if (0 == rc) {
      mail.idle(msecs);
    }
    return QmDb.db.dump();
  }

  @Override
  public String[] sendOrAckMessages(long xhContact, boolean ackOnly, long... xahMsgs) {
    QmDb.MessageData next = null;
    QmDb.Contact cx = makeContact(xhContact);
    if (null == cx) {
      return receiveMessages_empty;
    }
    boolean go = false;
    for (long hMsg : xahMsgs) {
      QIfs.QItemIf msg = CcmSto.peek(hMsg, false);
      if (!(msg instanceof QSTuple)) {
        continue;
      }
      QIfs.QItemIf tim62 = CcmTag.TIME.getValue((QSTuple) msg);
      QIfs.QItemIf ack = CcmTag.RECV.getValue((QSTuple) msg);
      QIfs.QItemIf from0 = CcmTag.CNTRB.getValue((QSTuple) msg);
      QIfs.QItemIf trashed = CcmTag.TRASHED.getValue((QSTuple) msg);
      String txt = ((QSeq) (CcmTag.DAT.getValue((QSTuple) msg))).toStringFull();
      long from = (from0 instanceof QWord) ? ((QWord) from0).i64() : 0;
      from = (0 == from) ? 1 : cx._inx;
      QmDb.MessageData mx =
          new QmDb.MessageData(
              cx._inx,
              (int) from,
              ((tim62 instanceof QWord)
                  ? ((long) (1000.0 * (Double) DateFunc.convert4Hash62(((QWord) tim62).shash, "X")))
                  : DateFunc.currentTimeMillisLinearized()),
              txt);
      mx.oid = BigSxg.sxg4Long(msg.getShash()).substring(2);
      final String ax = (ack instanceof QSeq) ? ((QSeq) ack).toStringFull() : "*:0";
      int i0 = ax.indexOf("*:") + 2;
      if (1 < i0) {
        for (; (i0 < ax.length()) && ('0' > ax.charAt(i0) && (ax.charAt(i0) > '9')); ++i0) {}
        int i1 = i0;
        for (; (i1 < ax.length()) && ('0' <= ax.charAt(i1) && (ax.charAt(i1) <= '9')); ++i1) {}
        if (i1 > i0) {
          mx.ack = Long.parseLong(ax.substring(i0, i1));
        }
      }
      if (0 < mx.ack) {
        go = true;
        if (null != trashed) {
          // Optional ID:
          mx._text = ((QSeq) trashed).toStringFull() + '\n' + mx._text;
        }
        QmDb.db.message_add(mx);
      } else {
        if (null != next) {
          send_msg_attachment(cx, null, next._text);
        }
        next = mx;
      }
    }
    if (null != next) {
      send_msg_attachment(cx, null, next._text);
      go = true;
    } else if (go) {
      send_msg_attachment(cx, null, " ");
    }
    return go ? QmDb.db.dump() : receiveMessages_empty;
  }
}
