// Copyright (C) 2014, 2023  Roland Horsch and others:
// -- For the changes:  Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original: Copyright (C) 2014/2015  Jeroen Vreeken <jeroen@vreeken.net>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from quickmsg.vreeken.net.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.*;

public class QmDb {

  public static QmDb db = new QmDb();
  public static String my_addr = null;
  public static volatile EcDhQm pgp = null;

  //////

  public static class Contact { // = group chat or person

    int _inx;
    int _type;
    // int _keystat;
    String _name;
    long _time_lastact;
    String _address; // for group this is the owner
    List<String> _members; // only for group
    long _unread;
    int _group; // common number

    //    String _phone;
    //    String _notes;

    long oid = 0;

    public String toTsvLine() {
      String fp = pgp.fingerprint(_address);
      return QmDb.toTsvLine(
          BigSxg.sxg4Long((0 == oid) ? DateFunc.createId() : oid).substring(2),
          ((_type != QuickMsg.TYPE_GROUP) || (0 >= _group)) ? _address : ("G" + _group),
          _time_lastact,
          "CHAT",
          (":TOPIC: "
              + _name
              + '\n'
              + ((_type == QuickMsg.TYPE_GROUP)
                  ? (":GROUP: " + _members.toString())
                  : (":AT: " + _address + ((null == fp) ? "" : "\n:FP: " + fp)))),
          (_type == QuickMsg.TYPE_GROUP) ? "" : _address,
          -1);
    }
  }

  //////

  public static class MessageData {
    int _c; // chat id this messag belongs to
    int _from; // contact that sent it
    long _time;
    String _text = "";
    // Uri _uri = null;
    // private String _queue = null;
    // rho
    /* Uri */ Object _uri = null;
    public long ack = 0; // time to re-send/ acknowledge, -1 = queued/ not sent
    public String oid; // object ID

    public MessageData() {
      oid = BigSxg.sxg4Long(DateFunc.createId()).substring(2);
    }

    public MessageData(int inxContact, int from, long millis, String string) {
      _c = inxContact;
      _from = from;
      _time = millis;
      _text = string;
      oid = BigSxg.sxg4Long(DateFunc.createId()).substring(2);
    }

    public String toTsvLine(String cat4Short) {
      if (null != cat4Short) {
        return QmDb.toTsvLine(oid, null, _time, cat4Short, _text, null, 0);
      }
      String nam =
          (db.contact_get_by_id(_c)._type != QuickMsg.TYPE_GROUP)
              ? db.contact_get_by_id(_c)._address
              : ("G" + db.contact_get_by_id(_c)._group);
      return QmDb.toTsvLine(
          oid,
          nam + ':' + BigSxg.sxg4Long(DateFunc.convert2Hash62(_time / 1000.0, "X")),
          _time,
          "MSG",
          _text,
          db.contact_get_by_id(_from)._address,
          ack);
    }

    public MessageData message4TsvFields(int from, String[] els, boolean ack) {
      if (0 >= els.length) {
        return null;
      }
      String x0 = els[0] + "..." + els[els.length - 1];
      char[] out = new char[x0.length()];
      int count = 0;
      for (int i0 = 0; (i0 < x0.length()) && (count < 72); ++i0) {
        final char ch = x0.charAt(i0);
        if ((' ' <= ch) && (ch < 0x7f)) {
          out[count++] = ch;
        }
      }
      _text = new String(out);
      if ((7 >= els.length)
          || (5 >= els[0].length())
          || (5 >= els[CcmSto.CcmTag.TIME.ordinal()].length())
          || !"MSG".equals(els[1 + CcmSto.CcmTag.CATS.ordinal()])) {
        return this;
      }
      char ch = els[0].charAt(0);
      if (('0' > ch) || ('f' < ch)) {
        return null;
      } else if (('9' < ch) && (ch < 'A')) {
        return null;
      } else if (('F' < ch) && (ch < 'a')) {
        return null;
      }
      long time = DateFunc.hash62oDate(els[CcmSto.CcmTag.TIME.ordinal()]);
      time = (long) (1000 * (Double) DateFunc.convert4Hash62(time, "X"));
      if (Dib2Constants.TIME_MIN_2017_01_01_UNIX_MILLIS > time) {
        return null;
      } else if (time > DateFunc.currentTimeMillisLinearized()) {
        time = (DateFunc.currentTimeMillisLinearized());
        // & ~Dib2Constants.TIME_SHIFTED) |
        // Dib2Constants.TIME_SHIFTED_UNKNOWN;
        // DateFunc.alignTime(time, -1);
      }
      _time = time;
      oid = els[0].trim();
      this.ack = ack ? time : 0;
      StringBuilder txt = new StringBuilder(30 * els.length);
      int i0 = 1 + CcmSto.CcmTag.SRCS.ordinal();
      for (; i0 < els.length; ++i0) {
        if (0 < els.length) {
          break;
        }
      }
      for (; i0 < els.length; ++i0) {
        txt.append(els[i0]);
        txt.append('\n');
      }
      _text = txt.toString();
      return this;
    }
  }

  //////

  private ConcurrentHashMap<String, Object> cache = null;

  public void init(String email) {
    QmDb.my_addr = email;
    cache = new ConcurrentHashMap<String, Object>();
    long cx = CcmSto.peek(QWord.createQWord(email, true), Cats.CHAT.flag, false);
    if (0 != cx) {
      QIfs.QItemIf item = CcmSto.peek(cx, true);
      if (item instanceof QSTuple) {
        cx = ((QSTuple) item).stamp;
      }
    }
    // First contact with _inx == 1:
    Contact c1 = QmDb.db.contact_get_person_by_address(email);
    c1.oid = (cx == 0) ? c1.oid : cx;
    QmDb.db.set("email_address", email);
    QmDb.pgp = new EcDhQm();
    QmDb.pgp.init(QmDb.db);
  }

  // DEPR (now using ccmSto)
  public void setHost(String host, String user, String password, String imapPort, String smtpPort) {
    final String phex = StringFunc.hexUtf8(password, false);
    set("smtp_server", "smtp." + host);
    set("smtp_user", user);
    TcvCodec.instance.settleHexPhrase(phex); // set("smtp_pass", password);
    set("smtp_port", (null != smtpPort) ? smtpPort : "587");
    set("imap_server", "imap." + host);
    set("imap_user", user);
    // set("imap_pass", password);
    set("imap_port", (null != smtpPort) ? smtpPort : "993");
  }

  public String[] dump() {
    String[] out = new String[cache.size() + 1];
    int c0 = 0;
    for (Object entry : cache.values()) {
      if (entry instanceof Contact) {
        out[c0++] = ((Contact) entry).toTsvLine();
      }
    }
    StringBuilder acks = new StringBuilder();
    for (Map.Entry<String, Object> entry : cache.entrySet()) {
      if (entry.getValue() instanceof MessageData) {
        MessageData md = (MessageData) entry.getValue();
        if ((1 != md._from) || (0 < md._text.trim().length())) {
          out[c0++] = md.toTsvLine(null);
        }
      } else if ((entry.getValue() instanceof String) && "ACK".equals(entry.getValue())) {
        acks.append('\t');
        acks.append(entry.getKey());
      }
    }
    if (0 < acks.length()) {
      out[c0++] =
          new MessageData(1, 1, DateFunc.currentTimeMillisLinearized(), "ACK" + acks.toString())
              .toTsvLine("DONE");
    }
    Contact me = QmDb.db.contact_get_by_id(1);
    cache = new ConcurrentHashMap<String, Object>();
    // First contact with _inx == 1:
    // QmDb.db.contact_get_person_by_address(QmDb.my_addr);
    QmDb.db.contact_add(me);
    return out;
  }

  public QmDb.Contact contact_get_person_by_address(String addr) {
    Object ox = cache.get(addr);
    if (ox instanceof QmDb.Contact) {
      return (QmDb.Contact) ox;
    }
    if (null != ox) {
      return null;
    }
    QmDb.Contact out = new QmDb.Contact();
    out._inx = 1 + cache.size();
    cache.put(addr, out);
    out._address = addr;
    out._name = addr.substring(0, addr.indexOf('@'));
    // out._keystat = (1 >= out._inx) ? QuickMsg.KEYSTAT_VERIFIED : QuickMsg.KEYSTAT_NONE;
    out._type = QuickMsg.TYPE_PERSON;
    return out;
  }

  public byte[] get(String key, Object fallback) {
    Object ox = cache.get(key);
    if (ox instanceof byte[]) {
      return (byte[]) ox;
    }
    return Dib2Root.ccmSto.hidden_get(key);
  }

  public void set(String key, byte[] val) {
    Dib2Root.ccmSto.hidden_set(key, val);
  }

  public void set(String key, String val) {
    Dib2Root.ccmSto.hidden_set(key, StringFunc.bytesUtf8(val));
  }

  public void remove(String key) {
    Dib2Root.ccmSto.hidden_remove(key);
  }

  public String getLiteral(String key, String fallback) {
    byte[] out = get(key, null);
    return (null != out) ? StringFunc.string4Utf8(out) : null;
  }

  public void message_add(QmDb.MessageData msg0) {
    cache.put(msg0.oid, msg0);
  }

  public void message_set_ack(String oid) {
    cache.put(oid, "ACK");
  }

  public QmDb.MessageData message_get_by_oid(String oid) {
    Object out = cache.get(oid);
    return (out instanceof QmDb.MessageData) ? (QmDb.MessageData) out : null;
  }

  public QmDb.Contact contact_update(QmDb.Contact contact) {
    Object ox =
        cache.get(
            (contact._type != QuickMsg.TYPE_GROUP) ? contact._address : ("G" + contact._group));
    if (ox instanceof QmDb.Contact) {
      if (ox == contact) {
        return contact;
      }
    }
    return contact_add(contact);
  }

  public QmDb.Contact contact_add(QmDb.Contact contact) {
    if (contact._type != QuickMsg.TYPE_GROUP) {
      cache.put(contact._address, contact);
    } else {
      cache.put("G" + contact._group, contact);
    }
    return contact;
  }

  public QmDb.Contact contact_get_group_by_address_and_group(String group_owner, int mgroup) {
    Object ox = cache.get("G" + mgroup);
    return (ox instanceof QmDb.Contact) ? (QmDb.Contact) ox : null;
  }

  public void message_update(QmDb.MessageData msg0) {
    //    Object ox = cache.get("" + msg0.oid);
    cache.put(msg0.oid, msg0);
  }

  public List<QmDb.MessageData> message_dangling_by_id(int _id, int max) {
    List<QmDb.MessageData> out = new ArrayList<QmDb.MessageData>();
    for (Object mx : cache.values()) {
      if ((mx instanceof MessageData)
          && (_id == ((MessageData) mx)._c)
          && (0 < ((MessageData) mx).ack)) {
        out.add((MessageData) mx);
        if (0 >= --max) {
          break;
        }
      }
    }
    return out;
  }

  public QmDb.Contact contact_get_by_id(int i) {
    for (Object out : cache.values()) {
      if ((out instanceof QmDb.Contact) && (i == ((QmDb.Contact) out)._inx)) {
        return (QmDb.Contact) out;
      }
    }
    return null;
  }

  //  private static class ContentResolver {
  //
  //    public static ContentResolver getContentResolver() {
  //      return null;
  //    }
  //
  //    public InputStream openInputStream(Object uri) {
  //      return null;
  //    }
  //
  //    public String getType(Object uri) {
  //      return null;
  //    }
  //
  //  }

  public static String toTsvLine(
      String id, String label, long stamp, String cat, String dat, String src, long ack) {
    StringBuilder out = new StringBuilder(100 + dat.length());
    out.append(id);
    String time = DateFunc.date4Millis(stamp); // date4Millis_OLD(false, stamp);
    out.append('\t').append((null == label) ? time : label);
    out.append('\t').append(cat);
    out.append('\t').append(time);
    out.append('\t').append(""); // contributorOid
    out.append('\t').append((null == src) ? "" : src); // sources
    out.append('\t').append((0 >= ack) ? "" : ("*:" + ack)); // shareReceivers
    out.append('\t').append(dat.replace('\n', '\t'));
    return out.toString();
  }
}
