// Copyright (C) 2017, 2022  Roland Horsch and others:
// Original data by Bruce Eckel:
// http://www.mindview.net/Etc/Discussions/CheckedExceptions,
// with adaptation idea from Lukas Eder:
// https://dzone.com/articles/throw-checked-exceptions,
// as available on 2017-09-02.
// For the presented form: Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.common;

/* Usage:

catch(ExceptionAdapter ea) {
  try {
    ea.rethrow();
  } catch(IllegalArgumentException e) {
    // ...
  } catch(FileNotFoundException e) {
    // ...
  }
  // etc.
}

if(futzedUp)
    throw new ExceptionAdapter(new CloneNotSupportedException());


public class ExceptionAdapterTest {
  public static void main(String[] args) {
    try {
      try {
        throw new java.io.FileNotFoundException("Bla");
      } catch(Exception ex) {
        ex.printStackTrace();
        throw new ExceptionAdapter(ex);
      }
    } catch(RuntimeException e) {
      e.printStackTrace();
    }
    System.out.println("That's all!");
  }
}
*/

import java.io.*;
import net.sf.dibdib.config.*;

// =====

public class ExceptionAdapter extends RuntimeException {

  // =====

  ///// Added:

  private static final long serialVersionUID = -8947215243109261997L;
  public static final ExceptionAdapter EXIT =
      new ExceptionAdapter(new InterruptedException(), ExceptionAdapter.class, "Exit request.");
  public static final ExceptionAdapter WARNING_INTERRUPTED =
      new ExceptionAdapter(new InterruptedException(), ExceptionAdapter.class, "Interrupted.");
  public static final ExceptionAdapter MEMORY =
      new ExceptionAdapter(new OutOfMemoryError(), ExceptionAdapter.class, "Out of memory.");
  public static final ExceptionAdapter LOAD =
      new ExceptionAdapter(
          new IllegalStateException(), ExceptionAdapter.class, "Load file/ password error.");
  public static final ExceptionAdapter STATE =
      new ExceptionAdapter(
          new IllegalStateException(), ExceptionAdapter.class, "Illegal state or value.");

  /////

  private final String stackTrace;
  public /*Exception*/ Throwable originalException;

  private ExceptionAdapter(
      Throwable e, // Exception
      ///// Added:
      Class<?> clazz,
      String msg) {
    super( // e.toString());
        e.getClass().getSimpleName()
            + ": "
            + msg
            + ", class = "
            + clazz.getName()
            + ", msg = "
            + e.getMessage());

    originalException = e;
    StringWriter sw = new StringWriter();
    e.printStackTrace(new PrintWriter(sw));
    stackTrace = sw.toString();

    ///// Added:
    Throwable e0 = e;
    Throwable cause = e0;
    while ((null != (e0 = cause.getCause())) && (e0 != cause)) {
      cause = e0;
    }
    originalException = cause;
  }

  @Override
  public void printStackTrace() {
    printStackTrace(System.err);
  }

  @Override
  public void printStackTrace(java.io.PrintStream s) {
    synchronized (s) {
      s.print(getClass().getName() + ": ");
      s.print(stackTrace);
    }
  }

  @Override
  public void printStackTrace(java.io.PrintWriter s) {
    synchronized (s) {
      s.print(getClass().getName() + ": ");
      s.print(stackTrace);
    }
  }

  public void reThrow() {
    // Changed:
    // throw originalException;
    if (null != Dib2Root.app) {
      if (!(originalException instanceof InterruptedException)) {
        Dib2Root.app.error = (null != Dib2Root.app.error) ? Dib2Root.app.error : this;
      }
      if (EXIT == this) {
        if (Dib2Lang.AppState.ACTIVE.ordinal() == Dib2Root.app.appState.ordinal()) {
          Dib2Root.app.appState = Dib2Lang.AppState.EXIT_REQUEST;
        }
      }
    }
    doThrow(
        (originalException instanceof Exception)
            ? (Exception) originalException
            : new RuntimeException(originalException));
  }

  ///// Lukas Eder:

  static void doThrow(Exception e) {
    ExceptionAdapter.<RuntimeException>doThrow0(e);
  }

  @SuppressWarnings("unchecked")
  static <E extends Exception> void doThrow0(Exception e) throws E {
    throw (E) e;
  }

  /////

  public static void throwAdapted(Throwable t, Class<?> c, String msg) {
    new ExceptionAdapter(t, c, msg).reThrow();
  }

  public static void doAssert(boolean cond, Class<?> c, String msg) {
    if (!cond) {
      new ExceptionAdapter(new IllegalStateException(), c, msg).reThrow();
    }
  }

  @Override
  public String toString() {
    return "" + getMessage() + "/ " + originalException.toString();
  }

  // =====
}
