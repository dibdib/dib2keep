// Copyright (C) 2014, 2023  Roland Horsch and others:
// -- For the changes:  Copyright (C) 2018, 2022  Roland Horsch <gx work s{at}mai l.de>.
// -- For the original: Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from quickmsg.vreeken.net.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import com.gitlab.dibdib.picked.net.Mail.Attachment;
import java.io.*;
import java.util.*;
import javax.activation.*;
import javax.mail.util.ByteArrayDataSource;
import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_any.*;

public class QuickMsg {

  //  private static class Uri {
  //
  //    public List<String> getPathSegments() {
  //      // TODO Auto-generated method stub
  //      return null;
  //    }
  //  }

  public static final int TYPE_PERSON = 0;
  public static final int TYPE_GROUP = 1;

  public static final int KEYSTAT_BIT_ACTIVE = 1;
  public static final int KEYSTAT_BIT_CHANGED = 2;
  public static final int KEYSTAT_BIT_TO_SEND = 4;
  // public static final int KEYSTAT_BIT_CONFLICT = 8;

  public static final int KEYSTAT_NONE = 0;
  public static final int KEYSTAT_VERIFIED = KEYSTAT_BIT_ACTIVE + KEYSTAT_BIT_CHANGED;
  public static final int KEYSTAT_CONFIRMED = KEYSTAT_BIT_ACTIVE;
  public static final int KEYSTAT_SENT = KEYSTAT_BIT_CHANGED;
  public static final int KEYSTAT_PENDING = KEYSTAT_BIT_ACTIVE + KEYSTAT_BIT_TO_SEND;
  public static final int KEYSTAT_RECEIVED = KEYSTAT_BIT_CHANGED + KEYSTAT_BIT_TO_SEND;

  public Boolean is_post = false;
  public Boolean is_grouppost = false;
  public Boolean is_group = false;
  QmDb.MessageData _msg = new QmDb.MessageData();
  QmDb.Contact _contact = new QmDb.Contact();
  public int group_id = 0;
  public String group_owner = "";

  public QuickMsg() {
    _contact._type = TYPE_PERSON;
  }

  public Attachment send_message(QmDb.Contact from, QmDb.Contact to, QmDb.MessageData message) {
    // Log.d("message", "Send message");
    String msg = "";

    msg += "Action: post\n";
    msg += "Name: " + from._name + "\n";
    if (to._type == TYPE_GROUP) {
      msg += "Group: " + to._group + "\n";
      msg += "Owner: " + to._address + "\n";
      // Log.d("send message", "Send to group " + to.group_get());
    }
    msg += "Time: " + message._time /* rho */ / 1000 + "\n";
    msg += "\n";
    msg += message._text;

    DataSource ds;
    try {
      ds = new ByteArrayDataSource(msg, "application/quickmsg");
    } catch (IOException e) {
      // Log.e("message", "new ds: " + e.getMessage());
      return null;
    }
    Attachment a = new Attachment();
    a.datahandler = new DataHandler(ds);
    a.name = "QuickMSG";
    List<Attachment> as = new LinkedList<Attachment>();

    as.add(a);

    Object uri = message._uri;
    if (uri != null) {
      // Log.d("quickmsg", "send attachment: " + uri.toString());

      a = new Attachment();
      String[] aType = new String[1];
      try {
        InputStream is = Dib2Root.platform.openInputStreamGetType(uri, aType);
        String type = aType[0];
        a.datahandler = new DataHandler(new ByteArrayDataSource(is, type));
      } catch (Exception e) {
        // Log.d("send_message", e.getMessage());
        return null;
      }
      // List<String> names = uri.getPathSegments();
      a.name = uri.toString(); // names.get(names.size() - 1);
      int ia = a.name.lastIndexOf('/', a.name.length() - 2);
      a.name = a.name.substring(ia + 1);

      as.add(a);
    }

    Mail m = new Mail();

    return m.multipart_create(as);
    // return background.mail.multipart_create(as);
  }

  public Attachment send_group(QmDb.Contact group, EcDhQm pgp) {
    String msg = "";
    if (group._type != TYPE_GROUP) return null;

    msg += "Action: group\n";
    msg += "Name: " + group._name + "\n";
    msg += "Group: " + group._group + "\n";
    msg += "Owner: " + group._address + "\n";
    msg += "Members: " + group._members + "\n";
    msg += "\n";

    DataSource ds;
    try {
      ds = new ByteArrayDataSource(msg, "application/quickmsg");
    } catch (IOException e) {
      // Log.e("message", "new ds: " + e.getMessage());
      return null;
    }
    Attachment a = new Attachment();
    a.datahandler = new DataHandler(ds);
    a.name = "QuickMSG";
    List<Attachment> as = new LinkedList<Attachment>();

    as.add(a);

    List<String> members = group._members;

    for (int i = 0; i < members.size(); i++) {
      String member = members.get(i);
      // rho
      if (member.equals(QmDb.my_addr)) {
        continue;
      }
      Attachment ak = pgp.key_attachment(member);
      if (ak != null) as.add(ak);
    }

    Mail m = new Mail();

    return m.multipart_create(as);
    // return background.mail.multipart_create(as);
  }

  public void parse_attachment(Attachment a) {
    InputStream is;
    try {
      is = a.datahandler.getInputStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      String line;
      while ((line = br.readLine()) != null) {
        if (line.length() == 0) {
          break;
        }
        // Log.d("parse_attachment", line);

        String[] splitted = line.split(": ");
        String field = splitted[0];
        String value = splitted[1];

        if (field.equals("Action")) {
          if (value.equals("post")) {
            is_post = true;
          }
          if (value.equals("group")) {
            is_group = true;
            // Date now = new Date();
            // long time_now = now.getTime() / 1000;

            _contact._time_lastact = DateFunc.currentTimeMillisLinearized(); // time_now);
          }
        }
        if (field.equals("Members")) {
          _contact._members = new ArrayList<String>(Arrays.asList(value.split(", ")));
        }
        if (field.equals("Group")) {
          group_id = Integer.parseInt(value);

          is_grouppost = is_post;

          _contact._type = TYPE_GROUP;
          _contact._group = group_id;
        }
        if (field.equals("Owner")) {
          group_owner = value;
        }
        if (field.equals("Name")) {
          _contact._name = value;
        }
        if (field.equals("Time")) {
          _msg._time = Long.parseLong(value) /* rho */ * 1000;
        }
      }
      if (is_post) {
        while ((line = br.readLine()) != null) {
          _msg._text = _msg._text + "\n" + line;
        }

        return;
      }
    } catch (Exception e) {
      // Log.e("parse attachment", e.getMessage());
      return;
    }

    return;
  }

  public QmDb.Contact get_contact() {
    return _contact;
  }

  public QmDb.MessageData get_message() {
    if (is_post) return _msg;
    else return null;
  }

  public int get_group() {
    return group_id;
  }
}
