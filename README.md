Dib2Qm (aka Dibdib Messenger)
======

    Messenger for secured chats and groups (end-to-end encrypted).

Dib2Qm is a messenger that works via e-mail servers, end-to-end encrypted.

Text messages are not only end-to-end encrypted, they also get stored on your
device in an encrypted way. Images, attachments etc. can be sent -- encrypted,
of course, but since they depend on third-party programs, they are stored
unencrypted. Dib2Qm makes use of your device's 'Download' folder (also for
backups and archives).

You need an e-mail account (on a server with standard IMAP and SMTP services),
for which a small app like this one is allowed to access the services.

Just try it out! -- on a non-critical device (And if your e-mail provider blocks
it, use a different provider. Dib2Qm does not depend on a centralized server.)

- You can only chat with people who also use Dib2Qm.
- After the setup, please first start the messaging service (via menu).
- If you get a connection, the status should turn green (... 'ON').
- Add a contact, via menu: you have to enter the other one's e-mail address.
- If you then touch the new contact, you will be asked to send the 'invitation'
(i.e. the key info for exchanging secured messages).
- Once the key exchange is completely done (black/red, orange, green), the chat
works similar to other apps.

If one of your chats gets 'stuck' (due to a key exchange problem), just send
another 'invitation' from within the chat.

It is possible to use the address of your device's 'Google account', but they
insist on you explicitly allowing messenger apps: you would log in with your
browser (!), go to 'Settings', 'Forwarding...IMAP', 'Enable IMAP', 'Save', then
to 'Accounts', 'Other ... settings', '...Security', '2-Step...', 'Off', and
'Connected apps', 'Allow less secure', 'On'.

Strange? It is easier to use an extra e-mail account for Dib2Qm, but depending
on your provider you might still have to allow IMAP services. Both, using the
device's account address or using a dedicated e-mail address for Dib2Qm, have
their advantages and disadvantages ...

If you want to do a complete re-start, go to the device's settings, clear the
app data, then open a file manager, clear the 'Download/dibdib' data, and then
you can start the app a-fresh.

(Thanks to Jeroen Vreeken for his original QuickMSG app.)

An installable APK file can be downloaded from:

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/net.sourceforge.dibdib.android.dib2qm/)

https://f-droid.org/packages/net.sourceforge.dibdib.android.dib2qm
(for stable versions)

https://www.magentacloud.de/share/x5x-ox77lf
(for experimental (!) versions)

(Cmp. https://dib2x.github.io)

Copyright (C) 2016-2023  Roland Horsch < gx work s{at}ma il.de >.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'net.sf.dibdib'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License and the LICENSE file for more details.

See LICENSE file (= GPLv3-or-later: -- https : // www . gnu . org/ licenses/gpl.html --)
and further details under 'LICENSE_all' or 'assets' or 'resources'
(e.g. https://gitlab.com/dibdib/dib2j/blob/master/LICENSE_all)

(Impressum: IMPRESSUM.md @ github.com/dib2x/dib2x.github.io =
https://github.com/dib2x/dib2x.github.io/blob/main/IMPRESSUM.md)
